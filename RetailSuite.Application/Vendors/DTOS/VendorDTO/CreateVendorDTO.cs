﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Vendors.DTOS
{
    public class CreateVendorDTO
    {
        public string Name { get; set; }
        public string Initial { get; set; }
        public string Owner { get; set; }
        public string BankAccountDetails { get; set; }
        public string ManagerName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
