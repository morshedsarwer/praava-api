﻿using RetailSuite.Application.Vendors.DTOS.VendorPaymentDTO;
using RetailSuite.Domain.Vendor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Vendors.Services.VendorPaymentService
{
    public interface IVendorPaymentRepository
    {
        Task<IEnumerable<VendorPayment>> GetVendorPaymentAsync();
        Task<VendorPayment> GetVendorPaymentByIdAsync(int Id);
        Task<VendorPayment> AddVendorPayment(VendorPayment vendorPayment,string username);
        void UpdateVendorPaymentAsync();
        Task DeleteVendorPaymentAsync(int Id);
        Task<IEnumerable<GetVendorPaymentDTO>> GetVendorPayment();
    }
}
