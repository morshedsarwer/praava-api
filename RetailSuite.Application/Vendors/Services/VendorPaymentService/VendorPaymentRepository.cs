﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Vendors.DTOS.VendorPaymentDTO;
using RetailSuite.Domain.Vendor.Models;
using RetailSuite.Infrastructure.Contexts;


namespace RetailSuite.Application.Vendors.Services.VendorPaymentService
{
    public class VendorPaymentRepository : IVendorPaymentRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public VendorPaymentRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public async Task<VendorPayment> AddVendorPayment(VendorPayment vendorPayment,string username)
        {
            try
            {
                var user = _saveContext.GetUserFromUserName(username);
                vendorPayment.CreatedBy = user.Id;
                var output = await _context.VendorPayments.AddAsync(vendorPayment);
                await _context.SaveChangesAsync();
                return output.Entity;
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }

        }

        public async Task DeleteVendorPaymentAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetVendorPaymentByIdAsync(Id);
                itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }

        }

        public async Task<IEnumerable<VendorPayment>> GetVendorPaymentAsync()
        {
            try
            {
                var payments = await _context.VendorPayments.Where(s=>s.IsDelete==false).ToListAsync();
                return payments;
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }

        }

        public async Task<VendorPayment> GetVendorPaymentByIdAsync(int Id)
        {
            try
            {
                var payment = await _context.VendorPayments.Where(p=>p.IsDelete==false).FirstOrDefaultAsync(s => s.VendorPaymentId == Id);
                return payment;
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }

        }

        public void UpdateVendorPaymentAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<GetVendorPaymentDTO>> GetVendorPayment()
        {
            var query = from c in _context.VendorPayments
                        join v in _context.Vendors
                        on c.VendorId equals v.VendorId
                        where (c.IsDelete == false)
                        select new GetVendorPaymentDTO

                        {   VendorPaymentId = c.VendorPaymentId,
                            VendorName = v.Name,
                            Amount = c.Amount,
                            PaymentDetails = c.PaymentDetails,
                            Description = c.Description,
                            PaymentDate = c.PaymentDate,
                            POCode = c.POCode
                      

                        };

            return query.ToList();


        }
    }
}

