﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RetailSuite.Domain.Vendor.Models;

namespace RetailSuite.Application.Vendors.Services.VendorService
{
    public interface IVendorRepository
    {
        Task<IEnumerable<Vendor>> GetVendorsAsync();
        Task<Vendor> GetVendorByIdAsync(int Id);
        Task<Vendor> AddVendor(Vendor vendor,string username);
        void UpdateVendorAsync();
        Task DeleteVendorAsync(int Id);
    }
}
