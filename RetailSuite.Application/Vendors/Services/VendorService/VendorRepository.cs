﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.Vendor.Models;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Vendors.Services.VendorService
{
    public class VendorRepository : IVendorRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public VendorRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public async Task<Vendor> AddVendor(Vendor vendor,string username)
        {
            try
            {
                var user = _saveContext.GetUserFromUserName(username);
                vendor.CreatedBy = user.Id;
                var output =await _context.Vendors.AddAsync(vendor);
                await _context.SaveChangesAsync();
                return output.Entity;
            }
            catch (Exception e) {

                throw new ArgumentNullException();
            }
            
        }

        public async Task DeleteVendorAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetVendorByIdAsync(Id);
                itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            catch (Exception e) {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<Vendor> GetVendorByIdAsync(int Id)
        {
            try
            {
                var vendor = await _context.Vendors.Where(p=>p.IsDelete == false).FirstOrDefaultAsync(s => s.VendorId == Id);
                return vendor;
            }
            catch (Exception e) {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<IEnumerable<Vendor>> GetVendorsAsync()
        {
            try
            {
                var vendors = await _context.Vendors.Where(p=>p.IsDelete == false).ToListAsync();
                return vendors;
            }
            catch (Exception e) {
                throw new ArgumentNullException();
            }
            
        }

        public void UpdateVendorAsync()
        {
            // NOT IMPLEMENTED
        }
    }
}
