﻿using AutoMapper;
using RetailSuite.Application.Vendors.DTOS;
using RetailSuite.Domain.Vendor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Vendors.Profiles
{
    public class VendorProfile : Profile
    {
        public VendorProfile()
        {
            CreateMap<Vendor, ReadVendorDTO>();
            CreateMap<CreateVendorDTO, Vendor>();
            CreateMap<UpdateVendorDTO, Vendor>();

        }
    }
}
