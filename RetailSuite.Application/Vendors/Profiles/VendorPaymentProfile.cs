﻿using AutoMapper;
using RetailSuite.Application.Vendors.DTOS.VendorPaymentDTO;
using RetailSuite.Domain.Vendor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Vendors.Profiles
{
    public class VendorPaymentPsrofile : Profile
    {
        public VendorPaymentPsrofile()
        {
            CreateMap<VendorPayment, ReadVendorPaymentDTO>();
            CreateMap<CreateVendorPaymentDTO, VendorPayment>();
            CreateMap<UpdateVendorPaymentDTO, VendorPayment>();
        }
    }
}
