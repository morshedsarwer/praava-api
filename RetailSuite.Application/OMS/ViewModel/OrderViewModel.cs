﻿using RetailSuite.Application.OMS.DTOS.OrderDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.ViewModel
{
    public class OrderViewModel
    {
        public CreateOrderDTO CreateOrderDTO { get; set; }
        public IEnumerable<OrderProductsDTO> OrderProductsDTOs { get; set; }
        public IEnumerable<OrderCampaignDTO> CampaignDTOs { get; set; }
        public IEnumerable<OrderComboDTO> OrderComboDTO { get; set; }
        public IEnumerable<CreateRecurringDTO> CreateRecurringDTOs { get; set; }
    }
    
}
