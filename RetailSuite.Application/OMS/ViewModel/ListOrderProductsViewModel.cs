﻿using RetailSuite.Application.OMS.DTOS.OrderDTO;
using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.ViewModel
{
    public class ListOrderProductsViewModel
    {
        public IEnumerable<UpdateOrderProductsDTO> OrderProductsList { get; set; }
    }
}
