﻿using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.ViewModel
{
    public class OrderContextViewModel
    {
        public Order Order { get; set; }
        public IEnumerable<OrderProducts> OrderProducts { get; set; }
        public IEnumerable<OrderCampaign> OrderCampaigns { get; set; }
        public IEnumerable<OrderCombo> OrderCombos { get; set; }
        public IEnumerable<RecurringOrder> RecurringOrders { get; set; }
    }
}
