﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.POCO
{
    public class DivisionPoco
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<DistrictPoco> Districts { get; set; }
    }
}
