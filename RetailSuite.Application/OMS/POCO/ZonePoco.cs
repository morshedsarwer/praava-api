﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.POCO
{
    public class ZonePoco
    {
        public int Id { get; set; }
        public string? ZoneName { get; set; }
        public int DistricId { get; set; }        
        public string? DistricName { get; set; }
        public int DivisionId { get; set; }
        public string? DivisionName { get; set; }
        public decimal RegulaPrice { get; set; }
        public int RegularDeliveryDay { get; set; }
        public decimal ExpressPrice { get; set; }
        public int ExpressDeliDay { get; set; }
    }
}
