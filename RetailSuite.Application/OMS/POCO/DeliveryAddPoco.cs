﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.POCO
{
    public class DeliveryAddPoco
    {
        public string? SaveAddressAs { get; set; }
        public string? Name { get; set; }
        public string? ContactNumber { get; set; }
        public string? HouseNo { get; set; }
        public string? RoadNo { get; set; }
        public string? DetailAddress { get; set; }
        public string? DistrictName { get; set; }
        public string? DivisionName { get; set; }
        public string? ZoneName { get; set; }
        public int AddressId { get; set; }
        public int DistrictId { get; set; }
        public int DivisionId { get; set; }
        public int ZoneId { get; set; }
        public IEnumerable<ZonePoco> Zone { get; set; }
    }
}
