﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.POCO.Order
{
    public class OrderProductPoco
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public string? OrderCode { get; set; }
        public string? Weight { get; set; }
        public decimal Price { get; set; }
        public int Points { get; set; }
    }
}
