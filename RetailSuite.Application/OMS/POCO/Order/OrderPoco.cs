﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.POCO.Order
{
    public class OrderPoco
    {
        public int Id { get; set; }
        public string? OrderCode { get; set; }
        public string? TrnCode { get; set; }
        public DateTime OrderDate { get; set; }
        public string? CustomerId { get; set; }
        public string? CustomerName { get; set; }
        public string? PhoneNumber { get; set; }
        public decimal TobePaid { get; set; }
        public string? Status { get; set; }
        public int StatusId { get; set; }
        public string? VoucherCode { get; set; }
        public decimal TotalProductPrice { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal PromoPrice { get; set; }
        public decimal AtfrPromoPrice { get; set; }
        public string? PaymentType { get; set; }
        public int Payment { get; set; }
        public int ZoneId { get; set; }
        public string? ZoneName { get; set; }
        public int DeliveryAddressId { get; set; }
        public decimal DeliveryPrice { get; set; }
        public bool IsCampaign { get; set; }
        public bool IsCombo { get; set; }
        public bool IsOnlinePaid { get; set; }
        public int StoreId { get; set; }
        public string? StoreName { get; set; }
        public IEnumerable<OrderProductPoco>? OrderProducts { get; set; }
        public IEnumerable<OrderCampaignPoco>? OrderCampaignProducts { get; set; }
        public IEnumerable<DeliveryAddPoco> Addresses { get; set; }
        public IEnumerable<OrderComboPoco> OrderCombos { get; set; }
    }
}
