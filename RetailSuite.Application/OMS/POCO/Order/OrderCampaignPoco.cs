﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.POCO.Order
{
    public class OrderCampaignPoco
    {
        public int Id { get; set; }
        public int CampaignId { get; set;}
        public string? ProductName { get; set;}
        public string? Weight { get; set;}
        public int Qunatity { get; set;}
        public decimal CampaignPrice { get; set; }
        public string OrderCode { get; set; }
    }
}
