﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.POCO.Order
{
    public class OrderComboPoco
    {
        public int ComboId { get; set; }
        public decimal RegularPrice { get; set; }
        public decimal ComboPrice { get; set; }
        public int Quantity { get; set; }
        public string? ComboName { get; set; }

    }
}
