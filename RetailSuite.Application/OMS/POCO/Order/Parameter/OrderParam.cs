﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.POCO.Order.Parameter
{
    public class OrderParam
    {
        public int paymentType { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public int status { get; set; }
        public string OrderId { get; set; }
    }
}
