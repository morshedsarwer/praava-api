﻿using AutoMapper;
using RetailSuite.Application.OMS.DTOS.DistrictDTO;
using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Profiles
{
    public class DistrictProfile:Profile
    {
        public DistrictProfile()
        {
            CreateMap<District, ReadDistrictDTO>();
            CreateMap<CreateDistrictDTO, District>();
            CreateMap<UpdateDistrictDTO, District>();
        }
    }
}
