﻿using AutoMapper;
using RetailSuite.Application.OMS.DTOS.OrderCommentsDTO;
using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Profiles
{
    public class OrderCommentProfile : Profile
    {
        public OrderCommentProfile()
        {
            CreateMap<OrderComments, ReadOrderCommentDTO>();
            CreateMap<CreateOrderCommentDTO, OrderComments>();
            CreateMap<UpdateOrderCommentDTO, OrderComments>();
        }
    }
}
