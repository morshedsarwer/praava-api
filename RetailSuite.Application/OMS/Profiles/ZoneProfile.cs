﻿using AutoMapper;
using RetailSuite.Application.OMS.DTOS.ZoneDTO;
using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Profiles
{
    public class ZoneProfile : Profile
    {
        public ZoneProfile()
        {
            CreateMap<Zone, ReadZoneDTO>();
            CreateMap<CreateZoneDTO, Zone>();
            CreateMap<UpdateZoneDTO, Zone>();
        }
    }
}
