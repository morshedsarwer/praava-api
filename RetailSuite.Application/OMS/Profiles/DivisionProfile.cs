﻿using AutoMapper;
using RetailSuite.Application.OMS.DTOS.DivisionDTO;
using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Profiles
{
    public class DivisionProfile:Profile
    {
        public DivisionProfile()
        {
            CreateMap<Division, ReadDivisionDTO>();
            CreateMap<CreateDivisionDTO, Division>();
            CreateMap<UpdateDivisionDTO, Division>();
        }
    }
}
