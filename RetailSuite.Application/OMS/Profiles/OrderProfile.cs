﻿using AutoMapper;
using RetailSuite.Application.OMS.DTOS.OrderDTO;
using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Profiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Order, ReadOrderDTO>();
            CreateMap<ReadOrderDTO, Order>();
            CreateMap<CreateOrderDTO, Order>();
            CreateMap<Order, CreateOrderDTO>();
            CreateMap<RecurringOrder, CreateRecurringDTO>();
            CreateMap<CreateRecurringDTO,RecurringOrder>();

            CreateMap<UpdateOrderDTO, Order>();
            CreateMap<OrderProductsDTO, OrderProducts>();
            CreateMap<UpdateOrderProductsDTO, OrderProducts>();
            CreateMap<UpdateOrderCampaign, OrderCampaign>();
            CreateMap<OrderProducts, OrderProductsDTO>();
            CreateMap<OrderProducts, AddOrderProduct>();
            CreateMap<AddOrderProduct, OrderProducts>();
            CreateMap<OrderCombo, OrderComboDTO>();
            CreateMap<OrderComboDTO, OrderCombo>();
            CreateMap<UpdateOrderCombo, OrderCombo>();
            CreateMap<OrderCampaignDTO, OrderCampaign>();
            CreateMap<OrderCampaign, OrderCampaignDTO>();
            CreateMap<GatewayTransaction, GatewayTrnDTO>();
            CreateMap<GatewayTrnDTO, GatewayTransaction>();

        }
    }
}
