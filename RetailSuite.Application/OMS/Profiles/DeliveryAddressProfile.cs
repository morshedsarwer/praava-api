﻿using AutoMapper;
using RetailSuite.Application.OMS.DTOS.DeliveryAddressDTO;
using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Profiles
{
    public class DeliveryAddressProfile : Profile
    {
        public DeliveryAddressProfile()
        {
            CreateMap<DeliveryAddress, ReadDeliveryAddressDTO>();
            CreateMap<CreateDeliveryAddressDTO, DeliveryAddress>();
            CreateMap<UpdateDeliveryAddressDTO, DeliveryAddress>();
        }
    }
}
