﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.DivisionDTO
{
    public class UpdateDivisionDTO
    {
        public string DivisionName { get; set; }
    }
}
