﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.OrderCommentsDTO
{
    public class CreateOrderCommentDTO
    {
        public int OrderId { get; set; }
        public string Comment { get; set; }
    }
}
