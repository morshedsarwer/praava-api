﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.OrderDTO
{
    public class GatewayTrnDTO
    {
        public string? OrderTrnCode { get; set; }
        public string? GatewayTrnCode { get; set; }
        public decimal Amount { get; set; }
        public string? PaymentGateway { get; set; }
    }
}
