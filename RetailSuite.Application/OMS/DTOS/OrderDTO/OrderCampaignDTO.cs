﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.OrderDTO
{
    public class OrderCampaignDTO
    {
        public int ProductId { get; set; }
        public int ProductQuantity { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal ProductPrice { get; set; }
        public int CampaignId { get; set; }
    }
}
