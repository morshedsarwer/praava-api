﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.OrderDTO
{
    public class OrderProductsDTO
    {
        public int ProductId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal ProductPrice { get; set; }
        public int ProductQuantity { get; set; }
    }
}
