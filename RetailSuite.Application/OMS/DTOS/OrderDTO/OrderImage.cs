﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.OrderDTO
{
    public class OrderImage
    {
        public string TrnCode { get; set; }
        public IFormFile Image { get; set; }
    }
}
