﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.OrderDTO
{
    public class UpdateOrderDTO
    {
        //public string CustomerID { get; set; }
        public string VoucherCode { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal TotalPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal AfterPromoPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal PromoPrice { get; set; }
        public int PaymentType { get; set; }
        public int ZoneId { get; set; }
        public int DeliveryType { get; set; }
        public int DeliveryAddressID { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal DeliveryPrice { get; set; }
        public string OrderEditBy { get; set; }
        public DateTime EditDate { get; set; }
    }
}
