﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.OrderDTO
{
    public class AddOrderProduct
    {
        public int ProductId { get; set; }
        public int ProductQuantity { get; set; }
        public string OrderCode { get; set; }
    }
}
