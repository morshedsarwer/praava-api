﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.OrderDTO
{
    public class OrderComboDTO
    {
        public int ComboId { get; set; }
        public int ProductQuantity { get; set; }
        public decimal RegularPrice { get; set; }
        public int ComboPrice { get; set; }
    }
}
