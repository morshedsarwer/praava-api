﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.DistrictDTO
{
    public class CreateDistrictDTO
    {
        public string DistrictName { get; set; }
        public int DivisionId { get; set; }
    }
}
