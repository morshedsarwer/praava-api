﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.ZoneDTO
{
    public class CreateZoneDTO
    {
        
        public int DistrictId { get; set; }
        public string ZoneName { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal RegularPrice { get; set; }
        public int RegularDeliveryDay { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal ExpressPrice { get; set; }
        public int ExpressDeliveryDay { get; set; }
    }
}
