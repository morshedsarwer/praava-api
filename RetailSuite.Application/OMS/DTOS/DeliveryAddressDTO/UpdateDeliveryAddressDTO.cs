﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.DeliveryAddressDTO
{
    public class UpdateDeliveryAddressDTO
    {
        
        //public string CustomerID { get; set; }
        public string Name { get; set; }
        public string HouseNo { get; set; }
        public string DetailAddress { get; set; }
        public int District { get; set; }
        public string Zip { get; set; }
        public string ContactNumber { get; set; }
        public string RoadNo { get; set; }
        public int Zone { get; set; }
        public int Division { get; set; }
        public string SaveAddressAs { get; set; }
        //public bool IsDelete { get; set; }
    }
}
