﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.DTOS.DeliveryAddressDTO
{
    public class ReadDeliveryAddressDTO
    {
        public int AddressId { get; set; }
        public string CustomerID { get; set; }
        public string Name { get; set; }
        public string HouseNo { get; set; }
        public string DetailAddress { get; set; }
        public string District { get; set; }
        public string Zip { get; set; }
        public string ContactNumber { get; set; }
        public string RoadNo { get; set; }
        public string Thana { get; set; }
        public string Division { get; set; }
        public string SaveAddressAs { get; set; }
        public bool IsDelete { get; set; }
    }
}
