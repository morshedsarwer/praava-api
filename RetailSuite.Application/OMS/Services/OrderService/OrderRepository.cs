﻿using Microsoft.EntityFrameworkCore;
using Nancy;
using RetailSuite.Application.Common.SMS;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.OMS.DTOS.OrderDTO;
using RetailSuite.Application.OMS.PaymentGateway;
using RetailSuite.Application.OMS.POCO;
using RetailSuite.Application.OMS.POCO.Order;
using RetailSuite.Application.OMS.ViewModel;
using RetailSuite.Application.Voucher.Services.General;
using RetailSuite.Domain.OMS;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Services.OrderService
{
    public class OrderRepository : IOrderRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;
        private readonly IGeneralVoucherRepository _voucher;

        public OrderRepository(RetailDbContext context, ISaveContexts saveContexts, IGeneralVoucherRepository voucher)
        {
            _context = context;
            _saveContext = saveContexts;
            _voucher = voucher;
        }
        public async Task<bool> AddOrder(OrderContextViewModel orderContextViewModel, string username, string OrderTrnCode)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (orderContextViewModel.OrderProducts is not null || orderContextViewModel.Order is not null || orderContextViewModel.OrderCombos is not null || orderContextViewModel.OrderCampaigns is not null)
                    {
                        /*decimal toatalPrice = 0;
                        decimal totalProductPrice = 0;
                        decimal grandTotal = 0;
                        decimal afterPromoprice = 0;
                        decimal finarAftrPromo = 0;
                        decimal promoPrice = 0;
                        decimal campaignPrice = 0;
                        decimal comboPrice = 0;*/
                        // getting username
                        var user = _saveContext.GetUserFromUserName(username);
                        orderContextViewModel.Order.CustomerID = user.Id;
                        // code is already added in the constructor
                        var orderProducts = new List<OrderProducts>();
                        var comb = new List<OrderCombo>();
                        var camp = new List<OrderCampaign>();
                        //var deliveryPrice = _context.Zones.FirstOrDefault(x => x.ZoneID == orderContextViewModel.Order.ZoneId);
                        var recurring = new List<RecurringOrder>();
                        foreach (var item in orderContextViewModel.OrderProducts)
                        {
                            //var product = await _context.Products.FirstOrDefaultAsync(s => s.ProductId == item.ProductID);
                            item.OrderCode = orderContextViewModel.Order.OrderCode;
                            //item.ProductPoints = product.ProductPoints;
                            // Error in Line 60
                            //item.ProductPrice = (from p in _context.ProductPrices where (p.CreatedAt == (from pr in _context.ProductPrices where pr.ProductId == item.ProductID select pr.CreatedAt).Max()) select p.Price).Max();
                            //item.ProductPoints = (from p in _context.Products where p.ProductId == item.ProductID select p.ProductPoints).SingleOrDefault();
                            //item.ProductPrice = (from p in _context.ProductPrices where p.ProductId==item.ProductID select )
                            //totalProductPrice += (item.ProductPrice * item.ProductQuantity);
                            orderProducts.Add(item);
                        }
                        if (orderContextViewModel.Order.IsCombo == true)
                        {
                            foreach (var item in orderContextViewModel.OrderCombos)
                            {
                                item.OrderCode = orderContextViewModel.Order.OrderCode;
                                //comboPrice += (item.ComboPrice * item.ProductQuantity);
                                comb.Add(item);
                            }
                            _context.OrderCombos.AddRange(comb);
                            _context.SaveChanges();
                        }
                        if (orderContextViewModel.Order.IsCampaign == true)
                        {
                            foreach (var item in orderContextViewModel.OrderCampaigns)
                            {
                                item.OrderCode = orderContextViewModel.Order.OrderCode;
                                //campaignPrice += (item.ProductPrice * item.ProductQuantity);
                                camp.Add(item);
                            }
                            _context.OrderCampaigns.AddRange(camp);
                            _context.SaveChanges();
                        }
                        if (orderContextViewModel.Order.IsRecurring == true)
                        {
                            foreach (var item in orderContextViewModel.RecurringOrders)
                            {
                                item.OrderCode= orderContextViewModel.Order.OrderCode;
                                recurring.Add(item);
                            }
                            _context.RecurringOrders.AddRange(recurring);
                            _context.SaveChanges();

                        }

                        /*if (!string.IsNullOrWhiteSpace(orderContextViewModel.Order.VoucherCode))
                        {
                            var voucher = _context.GeneralVouchers.FirstOrDefault(x => x.Code == orderContextViewModel.Order.VoucherCode && x.EndDate <= DateTime.Now && x.NumOfVoucher <= x.NumberOfUsedVoucher);
                            if (voucher != null)
                            {
                                if (voucher.VoucherDiscType == "percent")
                                {
                                    //promoPrice = (voucher.DiscAmntOrPercent * totalProductPrice) / 100;
                                    //afterPromoprice = totalProductPrice - promoPrice;
                                }
                                else if (voucher.VoucherDiscType == "taka")
                                {
                                   // promoPrice = voucher.DiscAmntOrPercent;
                                    //afterPromoprice = totalProductPrice - promoPrice;
                                }
                                var count = voucher.NumberOfUsedVoucher + 1;
                                voucher.NumberOfUsedVoucher = count;
                                _context.SaveChanges();
                            }
                        }*/
                        //toatalPrice = totalProductPrice + campaignPrice + comboPrice;
                        //afterPromoprice = toatalPrice - promoPrice;
                        //grandTotal = afterPromoprice + deliveryPrice.RegularPrice;
                        //grandTotal = (deliveryPrice.RegularPrice+toatalPrice)-afterPromoprice;
                        orderContextViewModel.Order.OrderTrnId = OrderTrnCode;
                        //orderContextViewModel.Order.TotalPrice = toatalPrice;
                        //orderContextViewModel.Order.PromoPrice = promoPrice;
                        //orderContextViewModel.Order.GrandTotalPrice = grandTotal;
                        //orderContextViewModel.Order.AfterPromoPrice = afterPromoprice;
                       // orderContextViewModel.Order.DeliveryPrice = deliveryPrice.RegularPrice;
                        _context.Orders.Add(orderContextViewModel.Order);
                        _context.SaveChanges();
                        _context.OrderProducts.AddRange(orderProducts);
                        _context.SaveChanges();
                        //sms send             

                        await transaction.CommitAsync();
                        return true;
                    }
                    return false;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }
            }

        }

        public async Task DeleteOrderAsync(int Id)
        {
            try
            {
                var itemRemoved = await _context.Orders.FirstOrDefaultAsync(s => s.OrderId == Id);
                _context.Orders.Remove(itemRemoved);
                await _saveContext.SaveChangesAsync();
            }
            catch
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<IEnumerable<OrderPoco>> GetOrderbyCodeAsync(string code)
        {
            try
            {
                var orders = from order in _context.Orders
                             join st in _context.OrderStatuses on order.OrderStatus equals st.OrderStatusID
                             //join store in _context.Stores on order.StoreId equals store.StoreId
                             join zone in _context.Zones on order.ZoneId equals zone.ZoneID
                             join user in _context.Users on order.CustomerID equals user.Id
                             where order.OrderCode == code
                             select new OrderPoco
                             {
                                 Id = order.OrderId,
                                 CustomerId = order.CustomerID,
                                 AtfrPromoPrice = order.AfterPromoPrice,
                                 DeliveryAddressId = order.DeliveryAddressID,
                                 DeliveryPrice = order.DeliveryPrice,
                                 IsCampaign = order.IsCampaign,
                                 OrderCode = order.OrderCode,
                                 IsOnlinePaid = order.IsOnlinePaid,
                                 OrderDate = Convert.ToDateTime(order.OrderDate),
                                 PaymentType = (order.PaymentType == 1 ? "COD" : "SSL"),
                                 PromoPrice = order.PromoPrice,
                                 Status = st.StatusName,
                                 //StoreId = order.StoreId,
                                 //StoreName = store.StoreName,
                                 //CustomerName = _saveContext.GetUserFromCustomerId(order.CustomerID).ToString(),
                                 ZoneId = order.ZoneId,
                                 ZoneName = zone.ZoneName,
                                 GrandTotal = order.GrandTotalPrice,
                                 CustomerName = user.FirstName + " " + user.LastName,
                                 PhoneNumber = user.PhoneNumber,
                                 TrnCode = order.OrderTrnId,
                                 TotalProductPrice = order.TotalPrice,
                                 TobePaid = order.IsOnlinePaid == true && order.PaymentType == 2 ? 0 : order.GrandTotalPrice,
                                 VoucherCode = order.VoucherCode,
                                 OrderProducts = (from orderProduct in _context.OrderProducts
                                                  join product in _context.Products on orderProduct.ProductID equals product.ProductId
                                                  where order.OrderCode == orderProduct.OrderCode
                                                  select new OrderProductPoco
                                                  {
                                                      ProductName = product.ProductName,
                                                      ProductId = product.ProductId,
                                                      Points = orderProduct.ProductPoints,
                                                      OrderCode = orderProduct.OrderCode,
                                                      Price = orderProduct.ProductPrice,
                                                      Quantity = orderProduct.ProductQuantity,
                                                      Id = orderProduct.OrderProductsID,
                                                      Weight = product.Weight
                                                  }).ToList<OrderProductPoco>(),
                                 OrderCampaignProducts = (from campaign in _context.OrderCampaigns
                                                          join product in _context.Products on campaign.ProductID equals product.ProductId
                                                          where order.OrderCode == campaign.OrderCode
                                                          select new OrderCampaignPoco
                                                          {
                                                              ProductName = product.ProductName,
                                                              CampaignId = campaign.CampaignId,
                                                              CampaignPrice = campaign.ProductPrice,
                                                              Id = campaign.Id,
                                                              Qunatity = campaign.ProductQuantity,
                                                              OrderCode = order.OrderCode,
                                                              Weight = product.Weight
                                                          }).ToList<OrderCampaignPoco>(),

                                 Addresses = (from add in _context.DeliveryAddresses
                                              join dis in _context.Districts on add.District equals dis.DistrictId
                                              join div in _context.Divisions on dis.DivisionId equals div.DivisionID
                                              join z in _context.Zones on add.Zone equals z.ZoneID
                                              where add.AddressId == order.DeliveryAddressID
                                              select new DeliveryAddPoco
                                              {
                                                  AddressId = add.AddressId,
                                                  ContactNumber = add.ContactNumber,
                                                  DetailAddress = add.DetailAddress,
                                                  DistrictId = add.District,
                                                  DistrictName = dis.DistrictName,
                                                  DivisionName = div.DivisionName,
                                                  HouseNo = add.HouseNo,
                                                  DivisionId = div.DivisionID,
                                                  Name = add.Name,
                                                  RoadNo = add.RoadNo,
                                                  SaveAddressAs = add.SaveAddressAs,
                                                  ZoneName = z.ZoneName,
                                                  ZoneId = z.ZoneID
                                              }).ToList<DeliveryAddPoco>(),
                                 OrderCombos = (from com in _context.OrderCombos
                                                join comb in _context.ComboProducts on com.ComboId equals comb.ComboId
                                                where com.OrderCode == order.OrderCode
                                                select new OrderComboPoco
                                                {
                                                    ComboId = com.ComboId,
                                                    ComboPrice = com.ComboPrice,
                                                    Quantity = com.ProductQuantity,
                                                    RegularPrice = com.RegularPrice,
                                                    ComboName = comb.ComboProductName
                                                }).ToList<OrderComboPoco>()
                             };

                return orders;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> GetOrderByCustomerId(string customerId)
        {
            // get all the orders
            var orders = await _context.Orders.Where(s => s.CustomerID == customerId).ToListAsync();
            //var orderProducts = await _context.OrderProducts.Where(s => orders.Any(p => p.OrderCode == s.OrderCode)).ToListAsync();

            // and all  products of the orders
            var orderProducts = await (from op in _context.OrderProducts
                                       join ord in _context.Orders on op.OrderCode equals ord.OrderCode
                                       orderby ord.OrderDate descending
                                       where ord.CustomerID == customerId
                                       select op).ToListAsync();

            return new { orders = orders, orderProducts = orderProducts };
        }

        public async Task<OrderContextViewModel> GetOrderbyIdAsync(int Id)
        {
            try
            {
                var order = await _context.Orders.FirstOrDefaultAsync(s => s.OrderId == Id);
                var orderProducts = await _context.OrderProducts.Where(s => s.OrderCode == order.OrderCode).ToListAsync();
                var ocvm = new OrderContextViewModel { Order = order, OrderProducts = orderProducts };
                return ocvm;
                //return order;
            }
            catch
            {
                throw new ArgumentNullException();
            }

        }

        public async Task<IEnumerable<OrderProducts>> UpdateOrderProductsByCode(string Code, IEnumerable<OrderProducts> newOrderProducts)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    var orderProducts = await _context.OrderProducts.Where(s => s.OrderCode == Code).ToListAsync();
                    // delete them
                    _context.OrderProducts.RemoveRange(orderProducts);
                    _context.SaveChanges();
                    // setting the code for newly entered item
                    foreach (var item in newOrderProducts)
                    {
                        item.OrderCode = Code;
                    }
                    _context.OrderProducts.AddRange(newOrderProducts);
                    _context.SaveChanges();
                    await trans.CommitAsync();
                    var neworderProducts = await _context.OrderProducts.Where(s => s.OrderCode == Code).ToListAsync();
                    return newOrderProducts;

                }
                catch
                {
                    trans.Rollback();
                    throw new ArgumentNullException();
                }
            }
        }
        public async Task<dynamic> GetOrdersAsync()
        {
            try
            {
                var orders = await _context.Orders.ToListAsync();

                // get all the products of the order
                // with OrderCode
                var orderProducts = await (from op in _context.OrderProducts
                                           join ord in _context.Orders on op.OrderCode equals ord.OrderCode
                                           select op).ToListAsync();

                return new { Orders = orders, OrderProduct = orderProducts };
            }
            catch
            {
                throw new ArgumentNullException();
            }

        }

        public async Task<string> Checkout(OrderContextViewModel orderViewModel, string baseUrl, string username, string OrderTrnCode)
        {
            try
            {

                var result = await AddOrder(orderViewModel, username, OrderTrnCode);
                if (result == false)
                {
                    return null;
                }
                var user = _saveContext.GetUserFromUserName(username);
                //var productName = "HP Pavilion Series Laptop";
                //var price = 85000;

                //var baseUrl = Request.Scheme + "://" + Request.Host;
                //Console.WriteLine(baseUrl);
                // CREATING LIST OF POST DATA
                NameValueCollection PostData = new NameValueCollection();

                PostData.Add("total_amount", $"{orderViewModel.Order.GrandTotalPrice}");
                PostData.Add("tran_id", $"{OrderTrnCode}");
                PostData.Add("success_url", "https://farmroots.com.bd");
                PostData.Add("fail_url", "https://www.farmroots.com.bd/");
                PostData.Add("cancel_url", "https://www.farmroots.com.bd/");

                PostData.Add("version", "3.00");
                PostData.Add("cus_name", $"{user.UserName}");
                PostData.Add("cus_email", "abc.xyz@mail.co");
                PostData.Add("cus_add1", "Address Line On");
                PostData.Add("cus_add2", "Address Line Tw");
                PostData.Add("cus_city", "City Nam");
                PostData.Add("cus_state", "State Nam");
                PostData.Add("cus_postcode", "Post Cod");
                PostData.Add("cus_country", "Countr");
                PostData.Add("cus_phone", $"{user.PhoneNumber}");
                PostData.Add("cus_fax", "0171111111");
                PostData.Add("ship_name", "ABC XY");
                PostData.Add("ship_add1", "Address Line On");
                PostData.Add("ship_add2", "Address Line Tw");
                PostData.Add("ship_city", "City Nam");
                PostData.Add("ship_state", "State Nam");
                PostData.Add("ship_postcode", "Post Cod");
                PostData.Add("ship_country", "Countr");
                PostData.Add("value_a", "ref00");
                PostData.Add("value_b", "ref00");
                PostData.Add("value_c", "ref00");
                PostData.Add("value_d", "ref00");
                PostData.Add("shipping_method", "NO");
                PostData.Add("num_of_item", $"{orderViewModel.OrderProducts.Count() + orderViewModel.OrderCampaigns.Count()}");
                PostData.Add("product_name", $"{orderViewModel.OrderProducts}");
                PostData.Add("product_profile", "general");
                PostData.Add("product_category", "Demo");

                //we can get from email notificaton
                var storeId = "farmrootscombdlive";
                var storePassword = "6106508C6D41393429";
                var isSandboxMood = false;
                SSLCommerzGatewayProcessor sslcz = new SSLCommerzGatewayProcessor(storeId, storePassword, isSandboxMood);
                string response = sslcz.InitiateTransaction(PostData);
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void GatewayConfirmation(string TrnCode)
        {

            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    var order = _context.Orders.FirstOrDefault(x => x.OrderTrnId == TrnCode);
                    if (order != null)
                    {
                        order.IsOnlinePaid = true;
                        _context.SaveChanges();
                        trans.Commit();
                    }

                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }

        }

        public async Task<IEnumerable<OrderPoco>> Orders(int paymentType, DateTime fromDate, DateTime toDate, int status)
        {
            try
            {
                var orders = from order in _context.Orders
                             join st in _context.OrderStatuses on order.OrderStatus equals st.OrderStatusID
                             join zone in _context.Zones on order.ZoneId equals zone.ZoneID
                             join user in _context.Users on order.CustomerID equals user.Id
                             where (order.OrderDate >= fromDate && order.OrderDate <= toDate)
                             orderby order.OrderTrnId descending
                             //where (order.OrderDate>=fromDate)
                             //&& order.PaymentType == paymentType 
                             //&& (order.OrderDate>=fromDate && order.OrderDate<=toDate)
                             //&& user.PhoneNumber==phoneNumber
                             select new OrderPoco
                             {
                                 Id = order.OrderId,
                                 CustomerId = order.CustomerID,
                                 AtfrPromoPrice = order.AfterPromoPrice,
                                 DeliveryAddressId = order.DeliveryAddressID,
                                 DeliveryPrice = order.DeliveryPrice,
                                 IsCampaign = order.IsCampaign,
                                 OrderCode = order.OrderCode,
                                 IsOnlinePaid = order.IsOnlinePaid,
                                 OrderDate = Convert.ToDateTime(order.OrderDate),
                                 PaymentType = (order.PaymentType == 1 ? "COD" : "SSL"),
                                 Payment = order.PaymentType,
                                 PromoPrice = order.PromoPrice,
                                 Status = st.StatusName,
                                 StatusId = order.OrderStatus,
                                 //StoreId = order.StoreId,
                                 ZoneId = order.ZoneId,
                                 ZoneName = zone.ZoneName,
                                 GrandTotal = order.GrandTotalPrice,
                                 CustomerName = user.FirstName + " " + user.LastName,
                                 PhoneNumber = user.PhoneNumber,
                                 TrnCode = order.OrderTrnId,
                                 TobePaid = order.IsOnlinePaid == true && order.PaymentType == 2 ? 0 : order.GrandTotalPrice,
                                 TotalProductPrice = order.TotalPrice
                             };


                if (paymentType > 0 && status > 0)
                {
                    var data = from order in orders
                               where (order.Payment == paymentType && order.StatusId == status)
                               select order;
                    return data.ToList();
                }
                else if (paymentType == 0 && status > 0)
                {
                    var data = from order in orders
                               where (order.StatusId == status)
                               select order;
                    return data.ToList();
                }
                else if (paymentType > 0 && status == 0)
                {
                    var data = from order in orders
                               where (order.Payment == paymentType)
                               select order;
                    return data.ToList();
                }
                else
                {
                    var data = from order in orders
                               select order;
                    return data.ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<OrderPoco>> OrdersByCustomer(string customerId)
        {
            try
            {
                var orders = from order in _context.Orders
                             join st in _context.OrderStatuses on order.OrderStatus equals st.OrderStatusID
                             //join store in _context.Stores on order.StoreId equals store.StoreId
                             join zone in _context.Zones on order.ZoneId equals zone.ZoneID
                             //join user in _context.Users on order.CustomerID equals user.Id
                             where order.CustomerID == customerId
                             select new OrderPoco
                             {
                                 Id = order.OrderId,
                                 CustomerId = order.CustomerID,
                                 AtfrPromoPrice = order.AfterPromoPrice,
                                 DeliveryAddressId = order.DeliveryAddressID,
                                 DeliveryPrice = order.DeliveryPrice,
                                 IsCampaign = order.IsCampaign,
                                 OrderCode = order.OrderCode,
                                 IsOnlinePaid = order.IsOnlinePaid,
                                 OrderDate = Convert.ToDateTime(order.OrderDate),
                                 PaymentType = (order.PaymentType == 1 ? "COD" : "SSL"),
                                 PromoPrice = order.PromoPrice,
                                 Status = st.StatusName,
                                 StoreId = order.StoreId,
                                 //StoreName = store.StoreName,
                                 //CustomerName = _saveContext.GetUserFromCustomerId(order.CustomerID).ToString(),
                                 ZoneId = order.ZoneId,
                                 ZoneName = zone.ZoneName,
                                 GrandTotal = order.GrandTotalPrice,
                                 TrnCode = order.OrderTrnId,
                                 TobePaid = order.IsOnlinePaid == true && order.PaymentType == 2 ? 0 : order.GrandTotalPrice,
                                 TotalProductPrice = order.TotalPrice
                             };

                return orders.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<IEnumerable<OrderPoco>> Orders(string OrderId)
        {
            try
            {
                var orders = from order in _context.Orders
                             join st in _context.OrderStatuses on order.OrderStatus equals st.OrderStatusID
                             join zone in _context.Zones on order.ZoneId equals zone.ZoneID
                             join user in _context.Users on order.CustomerID equals user.Id
                             where order.OrderTrnId == OrderId
                             //where (order.OrderDate>=fromDate)
                             //&& order.PaymentType == paymentType 
                             //&& (order.OrderDate>=fromDate && order.OrderDate<=toDate)
                             //&& user.PhoneNumber==phoneNumber
                             select new OrderPoco
                             {
                                 Id = order.OrderId,
                                 CustomerId = order.CustomerID,
                                 AtfrPromoPrice = order.AfterPromoPrice,
                                 DeliveryAddressId = order.DeliveryAddressID,
                                 DeliveryPrice = order.DeliveryPrice,
                                 IsCampaign = order.IsCampaign,
                                 OrderCode = order.OrderCode,
                                 IsOnlinePaid = order.IsOnlinePaid,
                                 OrderDate = Convert.ToDateTime(order.OrderDate),
                                 PaymentType = (order.PaymentType == 1 ? "COD" : "SSL"),
                                 Payment = order.PaymentType,
                                 PromoPrice = order.PromoPrice,
                                 Status = st.StatusName,
                                 StatusId = order.OrderStatus,
                                 //StoreId = order.StoreId,
                                 ZoneId = order.ZoneId,
                                 ZoneName = zone.ZoneName,
                                 GrandTotal = order.GrandTotalPrice,
                                 CustomerName = user.FirstName + " " + user.LastName,
                                 PhoneNumber = user.PhoneNumber,
                                 TrnCode = order.OrderTrnId,
                                 TobePaid = order.IsOnlinePaid == true && order.PaymentType == 2 ? 0 : order.GrandTotalPrice,
                                 TotalProductPrice = order.TotalPrice
                             };
                return orders;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<OrderCampaign>> UpdateOrderCampaignByCode(string Code, IEnumerable<OrderCampaign> orderProducts)
        {
            //var campigns = _context.OrderCampaigns.Where(x => x.OrderCode == Code).ToList();
            //_context.OrderCampaigns.RemoveRange(campigns);
            //_context.SaveChanges();
            //return campigns;
            using var trans = _context.Database.BeginTransaction();

            try
            {
                var campigns = _context.OrderCampaigns.Where(x => x.OrderCode == Code).ToList();
                _context.OrderCampaigns.RemoveRange(campigns);
                _context.SaveChanges();
                foreach (var item in orderProducts)
                {
                    item.OrderCode = Code;
                }
                _context.OrderCampaigns.AddRange(orderProducts);
                _context.SaveChanges();
                trans.Commit();
                var result = await _context.OrderCampaigns.Where(x => x.OrderCode == Code).ToListAsync();
                return result;

            }
            catch (Exception)
            {
                trans.Rollback();
                throw;
            }

        }
        public async Task<IEnumerable<OrderCombo>> UpdateOrderComboByCode(string Code, IEnumerable<OrderCombo> orderCombos)
        {
            //var campigns = _context.OrderCampaigns.Where(x => x.OrderCode == Code).ToList();
            //_context.OrderCampaigns.RemoveRange(campigns);
            //_context.SaveChanges();
            //return campigns;
            using var trans = _context.Database.BeginTransaction();

            try
            {
                var combos = _context.OrderCombos.Where(x => x.OrderCode == Code).ToList();
                _context.OrderCombos.RemoveRange(combos);
                _context.SaveChanges();
                foreach (var item in orderCombos)
                {
                    item.OrderCode = Code;
                }
                _context.OrderCombos.AddRange(orderCombos);
                _context.SaveChanges();
                trans.Commit();
                var result = await _context.OrderCombos.Where(x => x.OrderCode == Code).ToListAsync();
                return result;

            }
            catch (Exception)
            {
                trans.Rollback();
                throw;
            }

        }

        public async Task<bool> AddOrderProdcut(OrderProducts orderProducts)
        {
            try
            {
                orderProducts.ProductPrice = (from p in _context.ProductPrices where (p.CreatedAt == (from pr in _context.ProductPrices where pr.ProductId == orderProducts.ProductID select pr.CreatedAt).Max()) select p.Price).Max();
                orderProducts.ProductPoints = (from p in _context.Products where p.ProductId == orderProducts.ProductID select p.ProductPoints).SingleOrDefault();
                await _context.OrderProducts.AddAsync(orderProducts);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public async Task SendMessage(string phone, int statusId, string OrderTrnId)
        {
            try
            {
                if (statusId == 1)
                {
                    await SendSMS.Send(phone, $"Your order {OrderTrnId} has been placed");
                }
                else if (statusId == 4)
                {
                    await SendSMS.Send(phone, $"Your order {OrderTrnId} ready for delivery");
                }
                else if (statusId == 6)
                {
                    await SendSMS.Send(phone, $"Your order {OrderTrnId} delivered");
                }
                else if (statusId == 7)
                {
                    await SendSMS.Send(phone, $"Your order {OrderTrnId} cancelled");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
