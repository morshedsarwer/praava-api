﻿using RetailSuite.Application.OMS.DTOS.OrderDTO;
using RetailSuite.Application.OMS.POCO.Order;
using RetailSuite.Application.OMS.ViewModel;
using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Services.OrderService
{
    public interface IOrderRepository
    {
        Task<dynamic> GetOrdersAsync();
        Task<OrderContextViewModel> GetOrderbyIdAsync(int Id);
        Task<bool> AddOrder(OrderContextViewModel orderContextViewModel,string username, string OrderTrnCode);
        Task DeleteOrderAsync(int Id);
        Task<dynamic> GetOrderByCustomerId(string customerId);
        Task<IEnumerable<OrderPoco>> GetOrderbyCodeAsync(string code);
        Task<IEnumerable<OrderProducts>> UpdateOrderProductsByCode(string Code, IEnumerable<OrderProducts> orderProducts);
        Task<IEnumerable<OrderCampaign>> UpdateOrderCampaignByCode(string Code, IEnumerable<OrderCampaign> orderProducts);
        Task<IEnumerable<OrderCombo>> UpdateOrderComboByCode(string Code, IEnumerable<OrderCombo> orderCombos);
        Task<string> Checkout(OrderContextViewModel orderViewModel, string baseUrl, string username, string OrderTrnCode);
        void GatewayConfirmation(string TrnCode);
        //Task<IEnumerable<OrderPoco>> Orders(int storeId, int paymentType,DateTime from, DateTime to, string phoneNumber);
        Task<IEnumerable<OrderPoco>> Orders(int paymentType, DateTime fromDate, DateTime toDate, int status );
        Task<IEnumerable<OrderPoco>> Orders(string OrderId);
        Task<IEnumerable<OrderPoco>> OrdersByCustomer(string customerId);
        Task<bool> AddOrderProdcut(OrderProducts orderProducts);
        Task SendMessage(string phone, int statusId, string OrderTrnId);
    }
}
