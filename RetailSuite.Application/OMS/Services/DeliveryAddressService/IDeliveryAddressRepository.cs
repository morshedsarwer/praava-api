﻿using RetailSuite.Application.OMS.POCO;
using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Services.DeliveryAddressService
{
    public interface IDeliveryAddressRepository
    {
        Task<DeliveryAddress> AddDeliveryAddress(DeliveryAddress address,string username);
        Task<DeliveryAddress> GetDeliveryAddressById(int Id);
        Task<IEnumerable<DivisionPoco>> GetAddress();
        Task<IEnumerable<ZonePoco>> GetZonesByDistrict(int id);
        void EditDeliveryAddress();
        Task DeleteDeliveryAddres(int AddressId);
        Task<IEnumerable<DeliveryAddPoco>> GetDeliveryAddresses(string customerId);

    }
}
