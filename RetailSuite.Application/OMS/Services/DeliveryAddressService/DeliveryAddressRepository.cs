﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.OMS.POCO;
using RetailSuite.Domain.OMS;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Services.DeliveryAddressService
{
    public class DeliveryAddressRepository : IDeliveryAddressRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public DeliveryAddressRepository(RetailDbContext context, ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;

        }
        public async Task<DeliveryAddress> AddDeliveryAddress(DeliveryAddress address, string username)
        {
            // get the user
            try
            {
                var user = _saveContext.GetUserFromUserName(username);
                address.CustomerID = user.Id;
                var output = await _context.DeliveryAddresses.AddAsync(address);
                return output.Entity;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task DeleteDeliveryAddres(int AddressId)
        {
            try
            {
                var itemRemoved = await GetDeliveryAddressById(AddressId);
                if (itemRemoved != null)
                {
                    itemRemoved.IsDelete = true;
                    await _saveContext.SaveChangesAsync();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void EditDeliveryAddress()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<DivisionPoco>> GetAddress()
        {
            try
            {
                var results = from d in _context.Divisions
                              select new DivisionPoco
                              {
                                  Id = d.DivisionID,
                                  Name = d.DivisionName,
                                  Districts = (from dis in _context.Districts
                                               where d.DivisionID == dis.DivisionId
                                               select new DistrictPoco
                                               {
                                                   Id = dis.DistrictId,
                                                   Name = dis.DistrictName,
                                                   DivisionId = dis.DivisionId,
                                                   Zones = (from z in _context.Zones
                                                            where dis.DistrictId == z.DistrictId
                                                            select new ZonePoco
                                                            {
                                                                Id = z.ZoneID,
                                                                DistricId = z.DistrictId,
                                                                DistricName = dis.DistrictName,
                                                                //DivisionName = d.DivisionName,
                                                                ExpressPrice = z.ExpressPrice,
                                                                RegulaPrice = z.RegularPrice,
                                                                DivisionId = dis.DivisionId,
                                                                ZoneName = z.ZoneName,
                                                                ExpressDeliDay = z.ExpressDeliveryDay,
                                                                RegularDeliveryDay = z.RegularDeliveryDay
                                                            }).ToList<ZonePoco>()
                                               }).ToList<DistrictPoco>()
                              };
                return results.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<DeliveryAddress> GetDeliveryAddressById(int Id)
        {
            try
            {
                var address = await _context.DeliveryAddresses.FirstOrDefaultAsync(s => s.AddressId == Id);
                return address;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<DeliveryAddPoco>> GetDeliveryAddresses(string customerId)
        {
            try
            {

                var result = from add in _context.DeliveryAddresses
                             join div in _context.Divisions on add.Division equals div.DivisionID
                             join dis in _context.Districts on add.District equals dis.DistrictId
                             join zone in _context.Zones on add.Zone equals zone.ZoneID
                             where add.CustomerID == customerId && add.IsDelete == false
                             select new DeliveryAddPoco
                             {
                                 SaveAddressAs = add.SaveAddressAs,
                                 ZoneName = zone.ZoneName,
                                 ZoneId = zone.ZoneID,
                                 AddressId = add.AddressId,
                                 ContactNumber = add.ContactNumber,
                                 DetailAddress = add.DetailAddress,
                                 DistrictId = dis.DistrictId,
                                 DistrictName = dis.DistrictName,
                                 DivisionId = dis.DivisionId,
                                 DivisionName = div.DivisionName,
                                 HouseNo = add.HouseNo,
                                 Name = add.Name,
                                 RoadNo = add.RoadNo,
                                 Zone = (from z in _context.Zones
                                         where z.ZoneID == zone.ZoneID
                                         select new ZonePoco
                                         {
                                             DistricId = z.DistrictId,
                                             DistricName = dis.DistrictName,
                                             DivisionId = dis.DivisionId,
                                             DivisionName = div.DivisionName,
                                             ExpressDeliDay = z.ExpressDeliveryDay,
                                             ExpressPrice = z.ExpressPrice,
                                             RegulaPrice = z.RegularPrice,
                                             RegularDeliveryDay = z.RegularDeliveryDay,
                                             ZoneName = z.ZoneName,
                                             Id = zone.ZoneID
                                         }).ToList<ZonePoco>()
                             };

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ZonePoco>> GetZonesByDistrict(int id)
        {
            try
            {
                var zones = (from zone in _context.Zones
                             where zone.DistrictId == id
                             select new ZonePoco
                             {
                                 DistricId = zone.DistrictId,
                                 ExpressDeliDay = zone.ExpressDeliveryDay,
                                 ExpressPrice = zone.ExpressPrice,
                                 Id = zone.ZoneID,
                                 RegulaPrice = zone.RegularPrice,
                                 RegularDeliveryDay = zone.RegularDeliveryDay,
                                 ZoneName = zone.ZoneName
                             }).ToList();

                return zones;
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
