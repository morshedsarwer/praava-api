﻿using RetailSuite.Application.OMS.POCO;
using RetailSuite.Domain.OMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Services.OmsService
{
    public interface IOmsRepository
    {
        Task<IEnumerable<Division>> GetAllDivision();
        Task<Division> GetDivisionById(int Id);
        Task<Division> AddDivision(Division division);


        Task<IEnumerable<District>> GetAllDistrict();
        Task<District> GetDistrictById(int Id);
        Task<District> AddDistrict(District district);

        
        Task<IEnumerable<ZonePoco>> GetAllZones();
        Task<Zone> GetZonesById(int Id);
        Task<Zone> AddZone(Zone zone);


        // delivery addres
        Task<IEnumerable<DeliveryAddress>> GetAllDeliveryAddress();
        Task<DeliveryAddress> GetDeliveryAddressById(int id);
        Task<DeliveryAddress> AddDeliveryAddress(DeliveryAddress delivery);

        // order Comments
        Task<IEnumerable<OrderComments>> GetAllOrderComments(int orderId);
        Task<OrderComments> GetOrderCommentsById(int Id);
        void AddOrderComments(OrderComments orderComments);
        Task DeleteOrderComments(int Id);
      
        
    
    }
}
