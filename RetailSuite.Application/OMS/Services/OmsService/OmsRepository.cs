﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.OMS.POCO;
using RetailSuite.Domain.OMS;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.OMS.Services.OmsService
{
    public class OmsRepository : IOmsRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public OmsRepository(RetailDbContext context, ISaveContexts saveContext)
        {
            _context = context;
            _saveContext = saveContext;
        }


        public async Task<Division> AddDivision(Division division)
        {
            var output = await _context.Divisions.AddAsync(division);
            await _saveContext.SaveChangesAsync();
            return output.Entity;
        }

        public async Task<IEnumerable<Division>> GetAllDivision()
        {
            var divisions = await _context.Divisions.ToListAsync();
            return divisions;
        }

        public async Task<Division> GetDivisionById(int Id)
        {
            var division = await _context.Divisions.FirstOrDefaultAsync(s => s.DivisionID == Id);
            return division;
        }

        public async Task<District> AddDistrict(District district)
        {
            var output = await _context.Districts.AddAsync(district);
            await _saveContext.SaveChangesAsync();
            return output.Entity;
        }

        public async Task<IEnumerable<District>> GetAllDistrict()
        {
            var districts = await _context.Districts.ToListAsync();
            return districts;
        }

        public async Task<District> GetDistrictById(int Id)
        {
            var district = await _context.Districts.FirstOrDefaultAsync(s => s.DistrictId == Id);
            return district;
        }

        public async Task<IEnumerable<ZonePoco>> GetAllZones()
        {
            try
            {
                var zones = from z in _context.Zones
                            join dis in _context.Districts
                            on z.DistrictId equals dis.DistrictId
                            join div in _context.Divisions
                            on dis.DivisionId equals div.DivisionID
                            select new ZonePoco()
                            {
                                Id=z.ZoneID,
                                ZoneName=z.ZoneName,
                                DistricId=dis.DistrictId,
                                DistricName=dis.DistrictName,
                                DivisionId=dis.DivisionId,
                                DivisionName=div.DivisionName,
                                ExpressDeliDay=z.ExpressDeliveryDay,
                                ExpressPrice=z.ExpressPrice,
                                RegulaPrice=z.RegularPrice,
                                RegularDeliveryDay=z.RegularDeliveryDay,

                            };
                return zones;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Zone> GetZonesById(int Id)
        {
            var zone = await _context.Zones.FirstOrDefaultAsync(s => s.ZoneID == Id);
            return zone;
        }

        public async Task<Zone> AddZone(Zone zone)
        {
            var output = await _context.Zones.AddAsync(zone);
            await _saveContext.SaveChangesAsync();
            return output.Entity;

        }

        public async Task<IEnumerable<DeliveryAddress>> GetAllDeliveryAddress()
        {
            var addresses = await _context.DeliveryAddresses.ToListAsync();
            return addresses;
        }

        public async Task<DeliveryAddress> GetDeliveryAddressById(int Id)
        {
            var address = await _context.DeliveryAddresses.FirstOrDefaultAsync(s => s.AddressId == Id);
            return address;
        }

        public async Task<DeliveryAddress> AddDeliveryAddress(DeliveryAddress deliveryAddress)
        {
            var output = await _context.AddAsync(deliveryAddress);
            await _saveContext.SaveChangesAsync();
            return output.Entity;
        }

        public async Task<IEnumerable<OrderComments>> GetAllOrderComments(int orderId)
        {
            try
            {
                var comments = await _context.OrderComments.Where(s => s.OrderId == orderId).ToListAsync();
                return comments;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<OrderComments> GetOrderCommentsById(int Id)
        {
            var comment = await _context.OrderComments.FirstOrDefaultAsync(s => s.Id == Id);
            return comment;
        }

        public void AddOrderComments(OrderComments orderComments)
        {
            _context.OrderComments.Add(orderComments);
            _context.SaveChanges();
        }

        public async Task DeleteOrderComments(int Id)
        {
            var itemRemoved = await GetOrderCommentsById(Id);
            if (itemRemoved != null)
            {
                _context.OrderComments.Remove(itemRemoved);
                await _context.SaveChangesAsync();


            }
        }
    }
}
