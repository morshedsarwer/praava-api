﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.POCO
{
    public class StockPoco
    {
        public int Id { get; set; }
        public string? SKU { get; set; }
        public string? ProductName { get; set; }
        public int Quantity { get; set; }
        public int ProductId { get; set; }
        public string? ProductCode { get; set; }
    }
}
