﻿using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.POCO
{
    public class UnimarStoreTransfer
    {
        public int StoreId { get; set; }
        public IEnumerable<StoreProduct> StoreProduct { get; set; }
    }
}
