﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.ViewModel.ConsignmentViewModel;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using RetailSuite.Infrastructure.Contexts;

namespace RetailSuite.Application.WHMS.Services.ConsignmentService
{
    public class ConsignmentRepository : IConsignmentRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public ConsignmentRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public void AddConsignment(ConsignmentContextViewModel consignmentContextViewModel,string username)
        {
            //_context.Consignments.Add(consignment);
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var user = _saveContext.GetUserFromUserName(username);
                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    consignmentContextViewModel.Consignment.ConsignCode = code;
                    consignmentContextViewModel.Consignment.CreatedBy = user.Id;
                    var conproducts = new List<ConsignmentProduct>();
                    foreach (var item in consignmentContextViewModel.ConsignmentProducts) 
                    {
                        item.ConsignCode = code;
                        conproducts.Add(item);
                    }
                    _context.Consignments.Add(consignmentContextViewModel.Consignment);
                    _context.SaveChanges();
                    _context.ConsignmentProducts.AddRange(conproducts);
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e) {
                    transaction.Rollback();
                }
            
            }
        }

        public async Task DeleteConsignmentAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetConsignmentByIdAsync(Id);
                itemRemoved.IsDelete = true;
            }
            catch {
                throw new ArgumentNullException();
            }
            

        }

        public async Task<IEnumerable<Consignment>> GetConsignmentAsync()
        {
            try
            {
                var consignments = await _context.Consignments.Where(s=>s.IsDelete == false).ToListAsync();
                return consignments;
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public Task<Consignment> GetConsignmentByIdAsync(int Id)
        {
            try
            {
                var consignment = _context.Consignments.Where(p=>p.IsDelete == false).FirstOrDefaultAsync(s => s.ConsignmentID == Id);
                return consignment;
            }
            catch 
            {
                throw new ArgumentNullException();
            }
            
        }

        public void UpdateConsignmentAsync()
        {
            // NOT IMPLEMENTED
        }
    }
}
