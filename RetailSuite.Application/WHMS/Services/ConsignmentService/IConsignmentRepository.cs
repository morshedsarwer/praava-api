﻿using RetailSuite.Application.WHMS.ViewModel.ConsignmentViewModel;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.ConsignmentService
{
    public interface IConsignmentRepository
    {
        Task<IEnumerable<Consignment>> GetConsignmentAsync();
        Task<Consignment> GetConsignmentByIdAsync(int Id);
        void AddConsignment(ConsignmentContextViewModel consignmentContextViewModel,string username);
        void UpdateConsignmentAsync();
        Task DeleteConsignmentAsync(int Id);
    }
}
