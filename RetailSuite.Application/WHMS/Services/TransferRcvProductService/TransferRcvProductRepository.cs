﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.ViewModel.TransferProductViewModel;
using RetailSuite.Domain.WHMS.Models.Stock;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.TransferRcvProductService
{
    public class TransferRcvProductRepository : ITransferRcvProductRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public TransferRcvProductRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }

        public void AddTransferRcvProduct(IEnumerable<TransferRcvProduct> transferRcvProducts,string username)
        {
            using (var transaction = _context.Database.BeginTransaction()) 
            {
                try
                {
                    var user = _saveContext.GetUserFromUserName(username);
                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    var transRcvProduct = new List<TransferRcvProduct>();
                    foreach (var item in transferRcvProducts)
                    {
                        item.CreatedBy = user.Id;
                        item.TransferCode = code;
                        transRcvProduct.Add(item);
                    }

                    _context.TransferRcvProducts.AddRange(transRcvProduct);
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex) 
                {
                    transaction.Rollback();
                }
            }
        }

        public async Task DeleteTransferRcvProductAsync(int Id)
        {
            var itemRemoved = await GetTransferRcvProductByIdAsync(Id);
            itemRemoved.IsDelete = true;
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<TransferRcvProduct>> GetTransferRcvProductAsync()
        {
            var transRcvProducts = await _context.TransferRcvProducts.Where(s=>s.IsDelete == false).ToListAsync();
            return transRcvProducts;
        }

        public async Task<TransferRcvProduct> GetTransferRcvProductByIdAsync(int Id)
        {
            var transRcvProduct = await _context.TransferRcvProducts.Where(p=>p.IsDelete == false).FirstOrDefaultAsync(s => s.TRProductId == Id);
            return transRcvProduct;
        }

        public void UpdateTransferRcvProductAsync()
        {
            // NOT IMPLEMENTED
        }
    }
}
