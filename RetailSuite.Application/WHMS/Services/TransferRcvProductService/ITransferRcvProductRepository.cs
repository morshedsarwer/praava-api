﻿using RetailSuite.Application.WHMS.ViewModel.TransferProductViewModel;
using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.TransferRcvProductService
{
    public interface ITransferRcvProductRepository
    {
        Task<IEnumerable<TransferRcvProduct>> GetTransferRcvProductAsync();
        Task<TransferRcvProduct> GetTransferRcvProductByIdAsync(int Id);
        void AddTransferRcvProduct(IEnumerable<TransferRcvProduct> transferRcvProducts,string username);
        void UpdateTransferRcvProductAsync();
        Task DeleteTransferRcvProductAsync(int Id);
    }
}
