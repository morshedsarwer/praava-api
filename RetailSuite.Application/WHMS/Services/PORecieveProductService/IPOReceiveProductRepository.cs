﻿using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.PORecieveProductService
{
    public interface IPOReceiveProductRepository
    {
        Task<IEnumerable<POReceiveProduct>> GetPOReceiveProductAsync();
        Task<POReceiveProduct> GetPOReceiveProductByIdAsync(int Id);
        Task<POReceiveProduct> AddPOReceiveProduct(POReceiveProduct pOReceiveProduct,string username);
        void UpdatePOReceiveProductAsync();
        Task DeletePOReceiveProductAsync(int Id);
    }
}
