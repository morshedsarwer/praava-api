﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.PORecieveProductService
{
    public class POReceiveProductRepository : IPOReceiveProductRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public POReceiveProductRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public async Task<POReceiveProduct> AddPOReceiveProduct(POReceiveProduct pOReceiveProduct,string username)
        {
            try
            {
                var user = _saveContext.GetUserFromUserName(username);
                pOReceiveProduct.CreatedBy = user.Id;
                var output = await _context.POReceiveProducts.AddAsync(pOReceiveProduct);
                await _context.SaveChangesAsync();
                return output.Entity;
            }
            catch (Exception ex) {
                throw new ArgumentNullException();
            }
            
        }

        public async Task DeletePOReceiveProductAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetPOReceiveProductByIdAsync(Id);
                itemRemoved.IsDelete = true;
            }
            catch (Exception ex) {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<IEnumerable<POReceiveProduct>> GetPOReceiveProductAsync()
        {
            try
            {
                var products = await _context.POReceiveProducts.Where(s=>s.IsDelete == false).ToListAsync();
                return products;
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<POReceiveProduct> GetPOReceiveProductByIdAsync(int Id)
        {
            try
            {
                var product = await _context.POReceiveProducts.Where(p=>p.IsDelete == false).FirstOrDefaultAsync(s => s.PORId == Id);
                return product;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
            
        }

        public void UpdatePOReceiveProductAsync()
        {
            throw new NotImplementedException();
        }
    }
}
