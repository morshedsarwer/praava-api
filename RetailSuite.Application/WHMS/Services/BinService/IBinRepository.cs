﻿using RetailSuite.Domain.WHMS.Models.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.BinService
{
    public interface IBinRepository
    {
        Task<IEnumerable<Bin>> GetBinAsync();
        Task<Bin> GetBinByIdAsync(int Id);
        Task<Bin> AddBin(Bin bin,string username);
        void UpdateBinAsync();
        Task DeleteBinAsync(int Id);
    }
}








