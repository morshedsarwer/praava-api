﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.WHMS.Models.Store;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.BinService
{
    public class BinRepository : IBinRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public BinRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
            
        }
        public async Task<Bin> AddBin(Bin bin,string username)
        {
            try
            {
                // get user from username
                var user = _saveContext.GetUserFromUserName(username);
                bin.CreatedBy = user.Id;
                var output = await _context.Bins.AddAsync(bin);
                await _context.SaveChangesAsync();
                return output.Entity;
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public async Task DeleteBinAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetBinByIdAsync(Id);
                itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<IEnumerable<Bin>> GetBinAsync()
        {
            try
            {
                var bins = await _context.Bins.Where(s=>s.IsDelete == false).ToListAsync();
                return bins;
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public Task<Bin> GetBinByIdAsync(int Id)
        {
            try
            {
                var bin = _context.Bins.Where(p=>p.IsDelete== false).FirstOrDefaultAsync(s => s.BinId == Id);
                return bin;
            }
            catch {

                throw new ArgumentNullException();
            }
            
        }

        public void UpdateBinAsync()
        {
            throw new NotImplementedException();
        }
    }
}
