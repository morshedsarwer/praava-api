﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.PODeliveryMethodService
{
    public class PODeliveryMethodRepository : IPODeliveryMethodRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public PODeliveryMethodRepository(RetailDbContext context,ISaveContexts saveContext)
        {
            _context = context;
            _saveContext = saveContext;

        }

        public async Task<PODeliveryMethod> AddPODeliveryMethod(PODeliveryMethod pODeliveryMethod,string username)
        {
            try
            {
                // get the user
                var user = _saveContext.GetUserFromUserName(username);
                pODeliveryMethod.CreatedBy = user.Id;
                var output = await _context.PODeliveryMethods.AddAsync(pODeliveryMethod);
                await _context.SaveChangesAsync();
                return output.Entity;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
            

        }

        public async Task DeletePODeliveryMethodAsync(int Id)
        {
            try 
            {
                var itemRemoved = await GetPODeliveryMethodByIdAsync(Id);
                itemRemoved.IsDelete = true;
            }catch (Exception ex)
            {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<IEnumerable<PODeliveryMethod>> GetPODeliveryMethodAsync()
        {
            try
            {
                var delivaries = await _context.PODeliveryMethods.Where(s=>s.IsDelete == false).ToListAsync();
                return delivaries;
            }
            catch (Exception ex) {

                throw new ArgumentNullException();
            }
            
        }

        public Task<PODeliveryMethod> GetPODeliveryMethodByIdAsync(int Id)
        {
            try
            {
                var delivary = _context.PODeliveryMethods.Where(p=>p.IsDelete == false).FirstOrDefaultAsync(s => s.PODeliveryId == Id);
                return delivary;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
            

        }

        public void UpdatePODeliveryMethodAsync()
        {
            throw new NotImplementedException();
        }
    }
}
