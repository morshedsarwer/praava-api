﻿using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.PODeliveryMethodService
{
    public interface IPODeliveryMethodRepository
    {
        Task<IEnumerable<PODeliveryMethod>> GetPODeliveryMethodAsync();
        Task<PODeliveryMethod> GetPODeliveryMethodByIdAsync(int Id);
        Task<PODeliveryMethod>  AddPODeliveryMethod(PODeliveryMethod pODeliveryMethod,string username);
        void UpdatePODeliveryMethodAsync();
        Task DeletePODeliveryMethodAsync(int Id);
    }
}
