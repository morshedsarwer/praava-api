﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.WHMS.Models.Stock;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.RequisitionRcvProductService
{
    public class RequisitionRcvProductRepository : IRequisitionRcvProductRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public RequisitionRcvProductRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
     
        public void AddRequisitionRcvProduct(IEnumerable<RequisitionRcvProduct> requisitionRcvProducts,string username)
        {
            using (var transaction = _context.Database.BeginTransaction()) 
            {
                try
                {
                    var user = _saveContext.GetUserFromUserName(username);
                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    var reqrecvproduct = new List<RequisitionRcvProduct>();
                    foreach (var item in requisitionRcvProducts)
                    {
                        item.ReqCode = code;
                        item.CreatedBy = user.Id;
                        reqrecvproduct.Add(item);
                    }
                    _context.RequitionRcvProducts.AddRange(reqrecvproduct);
                    _context.SaveChanges();
                    transaction.Commit();


                }
                catch (Exception ex) 
                {
                    transaction.Rollback();
                }
            }

            //_context.RequitionRcvProducts.AddRange(requisitionRcvProduct);
        }

        public async Task DeleteRequisitionRcvProductAsync(int Id)
        {
            var itemRemoved = await GetRequisitionRcvProductByIdAsync(Id);
            itemRemoved.IsDelete = true;
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<RequisitionRcvProduct>> GetRequisitionRcvProductAsync()
        {
            var requisionRcvProducts = await _context.RequitionRcvProducts.Where(s=>s.IsDelete == false).ToListAsync();
            return requisionRcvProducts;
        }

        public async Task<RequisitionRcvProduct> GetRequisitionRcvProductByIdAsync(int Id)
        {
            var requisioRcvProduct = await _context.RequitionRcvProducts.Where(p=>p.IsDelete == false).FirstOrDefaultAsync(s => s.RequitionRcvId == Id);
            return requisioRcvProduct;
        }

        public void UpdateRequisitionRcvProductAsync()
        {
            // NOT Implemented
        }
    }
}
