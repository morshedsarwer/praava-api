﻿using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.RequisitionRcvProductService
{
    public interface IRequisitionRcvProductRepository
    {
        Task<IEnumerable<RequisitionRcvProduct>> GetRequisitionRcvProductAsync();
        Task<RequisitionRcvProduct> GetRequisitionRcvProductByIdAsync(int Id);
        void AddRequisitionRcvProduct(IEnumerable<RequisitionRcvProduct> requisitionRcvProduct,string username);
        void UpdateRequisitionRcvProductAsync();
        Task DeleteRequisitionRcvProductAsync(int Id);
    }
}
