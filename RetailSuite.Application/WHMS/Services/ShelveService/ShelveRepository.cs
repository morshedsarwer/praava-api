﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.WHMS.Models.Store;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.ShelveService
{
    public class ShelveRepository : IShelveRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public ShelveRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public async Task<Shelve> AddShelve(Shelve shelve,string username)
        {
            try
            {
                var user = _saveContext.GetUserFromUserName(username);
                shelve.CreatedBy = user.Id;
                var output = await _context.Shelves.AddAsync(shelve);
                await _context.SaveChangesAsync();
                return output.Entity;

            }
            catch {
                throw new ArgumentNullException();
            }
            

        }

        public async Task DeleteShelveAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetShelveByIdAsync(Id);
                _context.Shelves.Remove(itemRemoved);
            }
            catch {
                throw new ArgumentNullException();
            }
            

        }

        public async Task<IEnumerable<Shelve>> GetShelveAsync()
        {
            try
            {
                var shelves = await _context.Shelves.ToListAsync();
                return shelves;
            }
            catch {
                throw new ArgumentNullException();
            }
        }

        public async Task<Shelve> GetShelveByIdAsync(int Id)
        {
            try
            {
                var shelve = await _context.Shelves.FirstOrDefaultAsync(s => s.ShelveId == Id);
                return shelve;
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public void UpdateShelveAsync()
        {
            // NOT IMPLEMENTED
        }
    }
}
