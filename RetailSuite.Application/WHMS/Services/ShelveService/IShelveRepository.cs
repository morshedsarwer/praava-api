﻿using RetailSuite.Domain.WHMS.Models.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.ShelveService
{
    public interface IShelveRepository
    {
        Task<IEnumerable<Shelve>> GetShelveAsync();
        Task<Shelve> GetShelveByIdAsync(int Id);
        Task<Shelve> AddShelve(Shelve shelve,string username);
        void UpdateShelveAsync();
        Task DeleteShelveAsync(int Id);
    }
}
