﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.POCO;
using RetailSuite.Application.WHMS.ViewModel.StockTransferViewModel;
using RetailSuite.Domain.WHMS.Models.Stock;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.StockTransferService
{
    public class StockTransferRepository : IStockTransferRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public StockTransferRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
            
        }
        public void AddStockTransfer(StockTransferContextViewModel stockTransferContextViewModel,string username)
        {
            using (var transaction = _context.Database.BeginTransaction()) 
            {
                try
                {
                    var user = _saveContext.GetUserFromUserName(username);
                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    stockTransferContextViewModel.StockTransfer.TransferCode = code;
                    stockTransferContextViewModel.StockTransfer.CreatedBy = user.Id;
                    var tproducts = new List<TransferProduct>();
                    foreach (var item in stockTransferContextViewModel.TransferProducts)
                    {
                        item.TransferCode = code;
                        tproducts.Add(item);
                    }
                    _context.StockTransfers.Add(stockTransferContextViewModel.StockTransfer);
                    _context.SaveChanges();
                    _context.TransferProducts.AddRange(tproducts);
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex) 
                {
                    transaction.Rollback();
                
                }
            
            }
        }

        public async Task DeleteStockTransferAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetStockTransferByIdAsync(Id);
                itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<StockTransfer>> GetStockTransferAsync()
        {
            try
            {
                var stocktransfers = await _context.StockTransfers.Where(s => s.IsDelete == false).ToListAsync();
                return stocktransfers;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Task<StockTransfer> GetStockTransferByIdAsync(int Id)
        {
            try
            {
                var stocktransfer = _context.StockTransfers.Where(s => s.IsDelete == false).FirstOrDefaultAsync(s => s.TransferId == Id);
                return stocktransfer;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public async Task<StockTransferContextViewModel> GetStockTransferById(int Id)
        {
            try
            {
                var p = new StockTransferContextViewModel();
                p.StockTransfer = await _context.StockTransfers.Where(p => p.IsDelete == false).FirstOrDefaultAsync(s => s.TransferId == Id);
                p.TransferProducts = await (_context.TransferProducts.Where(s => s.TransferCode == p.StockTransfer.TransferCode)).ToListAsync();
                return p;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void UpdateStockTransferAsync()
        {
            throw new NotImplementedException();
        }

        public async Task AddFarmrootsStock(FarmrootsStock farmrootsStock)
        {
            try
            {
                _context.FarmrootsStocks.Add(farmrootsStock);
                await _saveContext.SaveChangesAsync();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<FarmrootsStock> GetFarmrootsstockById(int Id)
        {
            try
            {
                var farmroot = await _context.FarmrootsStocks.FirstOrDefaultAsync(s => s.Id == Id);
                return farmroot;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task DeleteFarmrootsStock(int Id)
        {
            try
            {
                var itemRemoved = await GetFarmrootsstockById(Id);
                if (itemRemoved != null)
                {
                    _context.FarmrootsStocks.Remove(itemRemoved);
                    await _saveContext.SaveChangesAsync();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<StockPoco>> GetAllFarmrootsStock()
        {
            try
            {
                var stock = new List<StockPoco>();
                using (var con = _context.Database.GetDbConnection())
                {
                    if (con.State != System.Data.ConnectionState.Open)
                        con.Open();
                    var command = con.CreateCommand();
                    command.CommandText = "get_stock_storeid";                   
                    using (var result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            while(result.Read())
                            {
                                stock.Add(new StockPoco() 
                                {
                                    Id = Convert.ToInt32(result[0]),
                                    SKU= Convert.ToString(result[1]),
                                    ProductName= Convert.ToString(result[2]),
                                    Quantity= Convert.ToInt32(result[3]),                                    
                                    ProductId= Convert.ToInt32(result[4]),
                                    ProductCode = Convert.ToString(result[5]),
                                });
                            }
                        }
                    }
                        con.Close();
                }
                return stock;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
