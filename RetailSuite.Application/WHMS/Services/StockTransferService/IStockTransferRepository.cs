﻿using RetailSuite.Application.WHMS.POCO;
using RetailSuite.Application.WHMS.ViewModel.StockTransferViewModel;
using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.StockTransferService
{
    public interface IStockTransferRepository
    {
        Task<IEnumerable<StockTransfer>> GetStockTransferAsync();
        Task<StockTransfer> GetStockTransferByIdAsync(int Id);
        void AddStockTransfer(StockTransferContextViewModel stockTransferContextViewModel,string username);
        void UpdateStockTransferAsync();
        Task DeleteStockTransferAsync(int Id);
        Task<StockTransferContextViewModel> GetStockTransferById(int Id);
        Task AddFarmrootsStock(FarmrootsStock farmrootsStock);
        Task<IEnumerable<StockPoco>> GetAllFarmrootsStock();
        Task<FarmrootsStock> GetFarmrootsstockById(int Id);
        Task DeleteFarmrootsStock(int Id);

    }
}
