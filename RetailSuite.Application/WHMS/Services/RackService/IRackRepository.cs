﻿using RetailSuite.Domain.WHMS.Models.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.RackService
{
    public interface IRackRepository
    {
        Task<IEnumerable<Rack>> GetRackAsync();
        Task<Rack> GetRackByIdAsync(int Id);
        Task<Rack> AddRack(Rack rack,string username);
        void UpdateRackAsync();
        Task DeleteRackAsync(int Id);
    }
}
