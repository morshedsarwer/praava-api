﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.WHMS.Models.Store;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.RackService
{
    public class RackRepository : IRackRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public RackRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public async Task<Rack> AddRack(Rack rack,string username)
        {
            try
            {
                var user = _saveContext.GetUserFromUserName(username);
                rack.CreatedBy = user.Id;
                var output = await _context.Racks.AddAsync(rack);
                await _context.SaveChangesAsync();
                return output.Entity;
                
                
            }
            catch {

                throw new ArgumentNullException();
            }
            
        }

        public async Task DeleteRackAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetRackByIdAsync(Id);
                _context.Racks.Remove(itemRemoved);
            }
            catch {

                throw new ArgumentNullException();
            }
            
        }

        public async Task<IEnumerable<Rack>> GetRackAsync()
        {
            try
            {
                var racks = await _context.Racks.ToListAsync();
                return racks;
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<Rack> GetRackByIdAsync(int Id)
        {
            try
            {
                var rack = await _context.Racks.FirstOrDefaultAsync(s => s.RackId == Id);
                return rack;
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public void UpdateRackAsync()
        {
            // NOT IMPLEMENTED HERE
        }
    }
}
