﻿using RetailSuite.Application.WHMS.ViewModel.StoreRequisitionViewModel;
using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace RetailSuite.Application.WHMS.Services.RequisitionService
{
    public interface IStoreRequisionRepository
    {
        Task<IEnumerable<StoreRequisition>> GetStoreRequisitionAsync();
        Task<StoreRequisition> GetStoreRequisitionByIdAsync(int Id);
        void AddStoreRequisition(StoreRequisitionContextViewModel storeRequisitionContextViewModel,string username);
        void UpdateStoreRequisitionAsync();
        Task DeleteStoreRequisitionAsync(int Id);
        Task<StoreRequisitionContextViewModel> GetStoreRequisitionById(int Id);
    }
}
