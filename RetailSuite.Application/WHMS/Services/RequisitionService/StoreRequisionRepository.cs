﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.ViewModel.StoreRequisitionViewModel;
using RetailSuite.Domain.WHMS.Models.Stock;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.RequisitionService
{
    public class StoreRequisionRepository : IStoreRequisionRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public StoreRequisionRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;

        }
        public void AddStoreRequisition(StoreRequisitionContextViewModel storeRequisitionContextViewModel,string username)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var user = _saveContext.GetUserFromUserName(username);
                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    storeRequisitionContextViewModel.StoreRequisition.ReqCode = code;
                    storeRequisitionContextViewModel.StoreRequisition.CreatedBy = user.Id;
                    var tags = new List<RequisitionProduct>();
                    foreach (var item in storeRequisitionContextViewModel.RequisitionProducts) 
                    {
                        item.REQCode = code;
                        item.CreatedBy = user.Id;
                        tags.Add(item);
                    
                    }
                    
                    _context.StoreRequisitions.Add(storeRequisitionContextViewModel.StoreRequisition);
                    _context.SaveChanges();
                    _context.RequisitionProducts.AddRange(tags);
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch {
                    transaction.Rollback();
                
                }
            }
        }

        public async Task DeleteStoreRequisitionAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetStoreRequisitionByIdAsync(Id);
                itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<IEnumerable<StoreRequisition>> GetStoreRequisitionAsync()
        {
            try
            {
                var requisitions = await _context.StoreRequisitions.Where(s=>s.IsDelete == false).ToListAsync();
                return requisitions;
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<StoreRequisition> GetStoreRequisitionByIdAsync(int Id)
        {
            try
            {
                var requision = await _context.StoreRequisitions.Where(p=>p.IsDelete == false).FirstOrDefaultAsync(s => s.RequisitionId == Id);
                return requision;
            }
            catch 
            {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<StoreRequisitionContextViewModel> GetStoreRequisitionById(int Id)
        {
            var p = new StoreRequisitionContextViewModel();
            p.StoreRequisition = await _context.StoreRequisitions.FirstOrDefaultAsync(s => s.RequisitionId == Id);
            p.RequisitionProducts = await (_context.RequisitionProducts.Where(s => s.REQCode == p.StoreRequisition.ReqCode)).ToListAsync();
            return p;

        }



        public void UpdateStoreRequisitionAsync()
        {
            // NOT IMPLEMENTED
        }
    }
}
