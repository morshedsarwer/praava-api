﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.ConsignmentReturnService
{
    public class ConsignmentReturnRepository : IConsignmentReturnRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public ConsignmentReturnRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public async Task<ConsignmentProductReturn> AddConsignmentReturn(ConsignmentProductReturn consignmentReturn,string username)
        {
            try
            { 
                var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                var user = _saveContext.GetUserFromUserName(username);
                consignmentReturn.CreatedBy = user.Id;
                consignmentReturn.ConsignCode = code;
                var output = await _context.ConsignmentReturns.AddAsync(consignmentReturn);
                return output.Entity;

                
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public async Task DeleteConsignmentReturnAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetConsignmentReturnByIdAsync(Id);
                _context.ConsignmentReturns.Remove(itemRemoved);
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<IEnumerable<ConsignmentProductReturn>> GetConsignmentReturnAsync()
        {
            try
            {
                var conremoves = await _context.ConsignmentReturns.ToListAsync();
                return conremoves;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<ConsignmentProductReturn> GetConsignmentReturnByIdAsync(int Id)
        {
            try
            {
                var conremove = await _context.ConsignmentReturns.FirstOrDefaultAsync(s => s.Id == Id);
                return conremove;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public void UpdateConsignmentReturnAsync()
        {
            throw new NotImplementedException();
        }
    }
}
