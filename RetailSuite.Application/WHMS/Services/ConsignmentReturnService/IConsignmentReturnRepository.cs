﻿using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.ConsignmentReturnService
{
    public interface IConsignmentReturnRepository
    {
        Task<IEnumerable<ConsignmentProductReturn>> GetConsignmentReturnAsync();
        Task<ConsignmentProductReturn> GetConsignmentReturnByIdAsync(int Id);
        Task<ConsignmentProductReturn> AddConsignmentReturn(ConsignmentProductReturn consignmentReturn,string username);
        void UpdateConsignmentReturnAsync();
        Task DeleteConsignmentReturnAsync(int Id);
    }
}
