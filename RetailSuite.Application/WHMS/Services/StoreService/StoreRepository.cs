﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.WHMS.Models.Store;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.StoreService
{
    public class StoreRepository : IStoreRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public StoreRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public async Task<Store> AddStore(Store store,string username)
        {
            var user = _saveContext.GetUserFromUserName(username);
            store.CreatedBy = user.Id;
            var output = await _context.Stores.AddAsync(store);
            await _context.SaveChangesAsync();
            return output.Entity;
        }

        public async Task DeleteStoreAsync(int Id)
        {
            try
            {
                var itemremoved = await GetStoreByIdAsync(Id);
                _context.Stores.Remove(itemremoved);
            }
            catch {

                throw new ArgumentNullException();
            }
            
        }

        public async Task<IEnumerable<Store>> GetStoreAsync()
        {
            try
            {
                return await _context.Stores.ToListAsync();
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<Store> GetStoreByIdAsync(int Id)
        {
            try
            {
                return await _context.Stores.FirstOrDefaultAsync(s => s.StoreId == Id);
            }
            catch {
                throw new ArgumentNullException();
            }
            

        }

        public void UpdateStoreAsync()
        {
            throw new NotImplementedException();
        }
    }
}
