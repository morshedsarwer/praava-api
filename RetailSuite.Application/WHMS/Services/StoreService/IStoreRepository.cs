﻿using RetailSuite.Domain.WHMS.Models.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.StoreService
{
    public interface IStoreRepository
    {
        Task<IEnumerable<Store>> GetStoreAsync();
        Task<Store> GetStoreByIdAsync(int Id);
        Task<Store> AddStore(Store store,string username);
        void UpdateStoreAsync();
        Task DeleteStoreAsync(int Id);
    }
}
