﻿using RetailSuite.Application.WHMS.ViewModel;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.PurchaseOrderService
{
    public interface IPurchaseOrderRepository
    {
        Task<IEnumerable<PurchaseOrder>> GetPurchaseOrderAsync();
        Task<PurchaseOrder> GetPurchaseOrderByIdAsync(int Id);
        void AddPurchaseOrder(POContextViewModel pOContextViewModel,string username);
        void UpdatePurchaseOrder();
        Task DeletePurchaseOrderAsync(int Id);
        Task<POContextViewModel> GetPurchaseOrderById(int Id);

    }
}
