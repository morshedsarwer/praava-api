﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.ViewModel;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.PurchaseOrderService
{
    public class PurchaseOrderRepository : IPurchaseOrderRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public PurchaseOrderRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;

        }
        public void AddPurchaseOrder(POContextViewModel pOContextViewModel,string username)
        {
           
                using (var transaction=_context.Database.BeginTransaction()) 
                {
                try
                {
                    var user = _saveContext.GetUserFromUserName(username);
                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    pOContextViewModel.PurchaseOrder.POCode = code;
                    pOContextViewModel.PurchaseOrder.CreatedBy = user.Id;
                    var poProducts = new List<POProduct>();
                    foreach (var item in pOContextViewModel.POProducts) 
                    {
                        item.POCode = code;
                        item.CreatedBy = user.Id;
                        poProducts.Add(item);
                    
                    }

                    _context.PurchaseOrders.Add(pOContextViewModel.PurchaseOrder);
                    _context.SaveChanges();
                    _context.POProducts.AddRange(poProducts);
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e) { 
                    transaction.Rollback();
                }
                }                                         
            
            
            
        }

        public async Task DeletePurchaseOrderAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetPurchaseOrderByIdAsync(Id);
                itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<IEnumerable<PurchaseOrder>> GetPurchaseOrderAsync()
        {
            try
            {
                var purchaseOrders = await _context.PurchaseOrders.Where(s=>s.IsDelete == false).ToListAsync();
                return purchaseOrders;
            }
            catch {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<PurchaseOrder> GetPurchaseOrderByIdAsync(int Id)
        {
            // this method is used by the DeletePurchaseOrderAsync
            try
            {
                var purchaseOrder = await _context.PurchaseOrders.Where(s=>s.IsDelete == false).FirstOrDefaultAsync(p => p.POId == Id);
                return purchaseOrder;
            }
            catch
            {
                throw new ArgumentNullException();
            }

        }

        public async Task<POContextViewModel> GetPurchaseOrderById(int Id)
        {
            var p = new POContextViewModel();
            p.PurchaseOrder = await _context.PurchaseOrders.Where(P=>P.IsDelete == false).FirstOrDefaultAsync(s => s.POId == Id);
            p.POProducts = await (_context.POProducts.Where(s => s.POCode == p.PurchaseOrder.POCode)).ToListAsync();
            return p;

            
                ;

        }








        public void UpdatePurchaseOrder()
        {
            // TODO
        }
    }
}
