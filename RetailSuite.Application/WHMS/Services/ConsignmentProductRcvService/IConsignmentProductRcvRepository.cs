﻿using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.ConsignmentProductRcvService
{
    public interface IConsignmentProductRcvRepository
    {
        Task<IEnumerable<ConsignmentProductRcv>> GetConsignmentProductRcvAsync();
        Task<ConsignmentProductRcv> GetConsignmentProductRcvByIdAsync(int Id);
        Task<ConsignmentProductRcv> AddConsignmentProductRcv(ConsignmentProductRcv consignmentProductRcv,string username);
        void UpdateConsignmentProductRcvAsync();
        Task DeleteConsignmentProductRcvAsync(int Id);
    }
}
