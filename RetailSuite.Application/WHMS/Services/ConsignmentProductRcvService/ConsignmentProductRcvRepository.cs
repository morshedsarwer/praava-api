﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Services.ConsignmentProductRcvService
{
    public class ConsignmentProductRcvRepository : IConsignmentProductRcvRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public ConsignmentProductRcvRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;

        }
        public async Task<ConsignmentProductRcv> AddConsignmentProductRcv(ConsignmentProductRcv consignmentProductRcv,string username)
        {
            try
            {
                var user = _saveContext.GetUserFromUserName(username);
                consignmentProductRcv.CreatedBy = user.Id;
                var output = await _context.ConsignmentProductRcvs.AddAsync(consignmentProductRcv);
                await _context.SaveChangesAsync();
                return output.Entity;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public async Task DeleteConsignmentProductRcvAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetConsignmentProductRcvByIdAsync(Id);
                itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();


            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            
            }
            
        }

        public async Task<IEnumerable<ConsignmentProductRcv>> GetConsignmentProductRcvAsync()
        {
            try
            {
                var conproducts = await _context.ConsignmentProductRcvs.Where(s=>s.IsDelete == false).ToListAsync();
                return conproducts;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        
        }

        public async  Task<ConsignmentProductRcv> GetConsignmentProductRcvByIdAsync(int Id)
        {
            try
            {
                var conproduct = await _context.ConsignmentProductRcvs.Where(p=>p.IsDelete== false).FirstOrDefaultAsync(s => s.Id == Id);
                return conproduct;

            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public void UpdateConsignmentProductRcvAsync()
        {
            throw new NotImplementedException();
        }
    }
}
