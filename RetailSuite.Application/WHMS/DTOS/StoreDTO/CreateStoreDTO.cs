﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.StoreDTO
{
    public class CreateStoreDTO
    {
        public string StoreName { get; set; }
        public string Address { get; set; }
        public int StoreSize { get; set; }
        public int NumberOfRack { get; set; }
    }
}
