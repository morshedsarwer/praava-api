﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.ConsignmentReturnDTO
{
    public class CreateConsignmentReturnDTO
    {
        public string POCode { get; set; }
    }
}
