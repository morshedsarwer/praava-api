﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.StockTransferDTO
{
    public class ReadStockTransferDTO
    {
        public int TransferId { get; set; }
        // system generate code
        public int ToStoreId { get; set; }
        public int FromStoreId { get; set; }
        public int TransferStatusId { get; set; }
        public string Description { get; set; }
        public DateTime ExpectationDate { get; set; }
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        public int RiderId { get; set; }
        public DateTime SendingDate { get; set; }
        public DateTime ReceivingDate { get; set; }
    }
}
