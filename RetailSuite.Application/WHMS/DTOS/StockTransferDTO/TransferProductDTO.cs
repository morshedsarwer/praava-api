﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.StockTransferDTO
{
    public class TransferProductDTO
    {
        public int ProductId { get; set; }
        public int TransferQty { get; set; }
    }
}
