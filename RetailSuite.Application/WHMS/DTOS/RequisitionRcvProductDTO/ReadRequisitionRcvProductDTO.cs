﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.RequisitionRcvProductDTO
{
    public class ReadRequisitionRcvProductDTO
    {
        public int RequitionRcvId { get; set; }
        //public string ReqCode { get; set; }
        public int ProductId { get; set; }
        public int QCCateType { get; set; }
        public int RcvQuantity { get; set; }
        public int ReceivedBy { get; set; }
        public DateTime ReceivedDate { get; set; }
    }
}
