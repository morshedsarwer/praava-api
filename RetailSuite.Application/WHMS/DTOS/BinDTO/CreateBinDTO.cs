﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.BinDTO
{
    public class CreateBinDTO
    {
        public string BinName { get; set; }
        public int ShelveId { get; set; }
    }
}
