﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.BinDTO
{
    public class ReadBinDTO
    {
        public int BinId { get; set; }
        public string BinName { get; set; }
        public int ShelveId { get; set; }
    }
}
