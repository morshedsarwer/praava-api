﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.ShelveDTO
{
    public class CreateShelveDTO
    {
        public string ShelveName { get; set; }
        public int RackId { get; set; }
    }
}
