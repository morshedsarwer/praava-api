﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.ShelveDTO
{
    public class ReadShelveDTO
    {
        public int ShelveId { get; set; }
        public string ShelveName { get; set; }
        public int RackId { get; set; }
    }
}
