﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.TransferRcvProductDTO
{
    public class UpdateTransferRcvProductDTO
    {
        // public int TRProductId { get; set; }
        // public string TransferCode { get; set; }
        public int ProductId { get; set; }
        public int QCCateType { get; set; }
        public int RcvQuantity { get; set; }
        public int ReceivedBy { get; set; }
        public DateTime ReceivedDate { get; set; }
    }
}
