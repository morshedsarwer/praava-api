﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.ConsignmentProductRcvDTO
{
    public class ReadConsignmentProductRcvDTO
    {
        public int Id { get; set; }
        public int PORId { get; set; }
        public int ConsignProdId { get; set; }
        public int ConsignCode { get; set; }
        public int RcvQuantity { get; set; }
        public int QCCateType { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TPPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal SellingPrice { get; set; }
        public string DiscType { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal DiscAmount { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal AftrDiscSellingPrice { get; set; }
        public string BarCode { get; set; }
        public int RackId { get; set; }
        public int ShelveId { get; set; }
        public int BinId { get; set; }
    }
}
