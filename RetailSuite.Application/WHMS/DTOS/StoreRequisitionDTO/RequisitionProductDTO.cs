﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.StoreRequisitionDTO
{
    public class RequisitionProductDTO
    {
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public int ReqQuantity { get; set; }
        public int SendingQuantity { get; set; }
    }
}
