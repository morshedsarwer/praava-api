﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.StoreRequisitionDTO
{
    public class CreateStoreRequisitionDTO
    {
        
        public int ToStoreId { get; set; }
        public int FromStoreId { get; set; }
        public int RequitionStatusId { get; set; }
        public string Description { get; set; }
        public DateTime ExpectationDate { get; set; }
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        // employee id, responsible as rider
        [ForeignKey("Employee")]
        public int RiderId { get; set; }
        public DateTime SendingDate { get; set; }
        public DateTime ReceivingDate { get; set; }
    }
}
