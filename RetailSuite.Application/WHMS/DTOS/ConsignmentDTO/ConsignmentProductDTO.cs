﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.ConsignmentDTO
{
    public class ConsignmentProductDTO
    {
        //public string ConsignmentCode { get; set; }
        public int ProductId { get; set; }
        public int OrderQuantity { get; set; }
    }
}
