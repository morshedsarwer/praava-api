﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.RackDTO
{
    public class CreateRackDTO
    {
        public string RackName { get; set; }
    }
}
