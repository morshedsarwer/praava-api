﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.RackDTO
{
    public class ReadRackDTO
    {
        public int RackId { get; set; }
        public string RackName { get; set; }
    }
}
