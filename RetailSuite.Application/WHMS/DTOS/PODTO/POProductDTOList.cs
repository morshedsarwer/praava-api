﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.PODTO
{
    public class POProductDTOList
    {
        public IEnumerable<POProductDTO> POProducts { get; set; }
    }
}
