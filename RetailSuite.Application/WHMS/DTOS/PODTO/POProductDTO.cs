﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.PODTO
{
    public class POProductDTO
    {
        public int ProductId { get; set; }
       // public string POCode { get; set; }
        public int OrderQuantity { get; set; }
    }
}
