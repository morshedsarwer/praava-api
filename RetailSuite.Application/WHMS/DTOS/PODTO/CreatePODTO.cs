﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.DTOS.PODTO
{
    public class CreatePODTO
    {
        //enum
        public int POType { get; set; }
        // if POType credit 
        public DateTime CreditPayDay { get; set; }
        public DateTime OrderDate { get; set; }
        //enum
        public int POStatus { get; set; }
        public int VendorId { get; set; }
        public int PODevliveryStoreId { get; set; }
        //public string POCode { get; set; }
        public string Description { get; set; }
        public int ApprovedBy { get; set; }
        public DateTime ApprovalDate { get; set; }
        public int DeliveryType { get; set; }
    }
}
