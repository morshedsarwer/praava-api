﻿using RetailSuite.Application.WHMS.DTOS.StoreRequisitionDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.ViewModel.StoreRequisitionViewModel
{
    public class StoreRequisitionViewModel
    {
        public CreateStoreRequisitionDTO CreateStoreRequisitionDTO { get; set; }
        public IEnumerable<RequisitionProductDTO> RequisitionProductDTOs { get; set; }
    }
}
