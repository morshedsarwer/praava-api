﻿using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.ViewModel.StoreRequisitionViewModel
{
    public class StoreRequisitionContextViewModel
    {
        public StoreRequisition StoreRequisition { get; set; }
        public IEnumerable<RequisitionProduct> RequisitionProducts { get; set; }
    }
}
