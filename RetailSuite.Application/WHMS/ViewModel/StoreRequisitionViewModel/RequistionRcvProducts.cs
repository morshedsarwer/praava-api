﻿using RetailSuite.Application.WHMS.DTOS.RequisitionRcvProductDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.ViewModel.StoreRequisitionViewModel
{
    public class RequistionRcvProducts
    {
        public IEnumerable<CreateRequisitionRcvProductDTO> CreateRequisitionRcvProductDTOs { get; set; }
    }
}
