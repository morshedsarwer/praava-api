﻿using RetailSuite.Application.WHMS.DTOS.ConsignmentDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.ViewModel.ConsignmentViewModel
{
    public class ConsignmentViewModel
    {
        public CreateConsignmentDTO CreateConsignmentDTO { get; set; }
        public IEnumerable<ConsignmentProductDTO> ConsignmentProductDTOs { get; set; }
    }
}
