﻿using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.ViewModel.ConsignmentViewModel
{
    public class ConsignmentContextViewModel
    {
        public Consignment Consignment { get; set; }
        public IEnumerable<ConsignmentProduct> ConsignmentProducts { get; set; }
    }
}
