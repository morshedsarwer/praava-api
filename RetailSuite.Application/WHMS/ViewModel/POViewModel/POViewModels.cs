﻿using RetailSuite.Application.WHMS.DTOS.PODTO;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.ViewModel
{
    public class POViewModels
    {
        public CreatePODTO CreatePODTO { get; set; }
        public IEnumerable<POProductDTO> POProductDTOs { get; set; }
    }
}
