﻿using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.ViewModel
{
    public class POContextViewModel
    {
        public PurchaseOrder PurchaseOrder { get; set; }
        public IEnumerable<POProduct> POProducts { get; set; }
    }
}
