﻿using RetailSuite.Application.WHMS.DTOS.TransferRcvProductDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.ViewModel.TransferProductViewModel
{
    public class TransferRcvProducts
    {
        public IEnumerable<CreateTransferRcvProductDTO> CreateTransferRcvProductDTOs { get; set; } 
    }
}
