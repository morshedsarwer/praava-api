﻿using RetailSuite.Application.WHMS.DTOS.StockTransferDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.ViewModel.StockTransferViewModel
{
    public class StockTransferViewModel
    {
        public CreateStockTransferDTO CreateStockTransferDTO { get; set; }
        public IEnumerable<TransferProductDTO> TransferProductDTOs { get; set; }
    }
}
