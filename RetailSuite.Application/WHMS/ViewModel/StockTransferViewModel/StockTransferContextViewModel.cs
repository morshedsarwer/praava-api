﻿using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.ViewModel.StockTransferViewModel
{
    public class StockTransferContextViewModel
    {
        public StockTransfer StockTransfer { get; set; }
        public IEnumerable<TransferProduct> TransferProducts { get; set; }
    }
}
