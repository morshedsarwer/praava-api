﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.StoreDTO;
using RetailSuite.Domain.WHMS.Models.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class StoreProfile : Profile
    {
        public StoreProfile()
        {
            CreateMap<Store, ReadStoreDTO>();
            CreateMap<CreateStoreDTO, Store>();
            CreateMap<UpdateStoreDTO, Store>();
        }
    }
}
