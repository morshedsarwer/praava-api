﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.ConsignmentProductRcvDTO;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class ConsignmentProductRcvProfile:Profile
    {
        public ConsignmentProductRcvProfile()
        {
            CreateMap<ConsignmentProductRcv, ReadConsignmentProductRcvDTO>();
            CreateMap<CreateConsignmentProductRcvDTO, ConsignmentProductRcv>();
            CreateMap<UpdateConsignmentProductRcvDTO, ConsignmentProductRcv>();
        }
    }
}
