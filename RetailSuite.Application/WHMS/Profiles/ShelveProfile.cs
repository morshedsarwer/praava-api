﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.ShelveDTO;
using RetailSuite.Domain.WHMS.Models.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class ShelveProfile:Profile
    {
        public ShelveProfile()
        {
            CreateMap<Shelve, ReadShelveDTO>();
            CreateMap<CreateShelveDTO, Shelve>();
            CreateMap<UpdateShelveDTO, Shelve>();
        }

    }
}
