﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.StoreRequisitionDTO;
using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class StoreRquisitionProfile : Profile
    {
        public StoreRquisitionProfile()
        {
            CreateMap<StoreRequisition, ReadStoreRequisitionDTO>();
            CreateMap<CreateStoreRequisitionDTO, StoreRequisition>();
            CreateMap<UpdateStoreRequisitionDTO, StoreRequisition>();
            CreateMap<RequisitionProductDTO, RequisitionProduct>();
            CreateMap<StoreRequisition, CreateStoreRequisitionDTO>();
            CreateMap<RequisitionProduct, RequisitionProductDTO>();
        }

    }
}
