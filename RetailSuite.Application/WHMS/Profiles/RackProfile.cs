﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.RackDTO;
using RetailSuite.Domain.WHMS.Models.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class RackProfile:Profile
    {
        public RackProfile()
        {
            CreateMap<Rack, ReadRackDTO>();
            CreateMap<CreateRackDTO, Rack>();
            CreateMap<UpdateRackDTO, Rack>();
        }
    }
}
