﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.FarmRootsStockDTO;
using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class FarmrootsStockProfile : Profile
    {
        public FarmrootsStockProfile()
        {
            CreateMap<FarmrootsStock, ReadFarmrootsStockDTO>();
            CreateMap<CreateFarmrootsStockDTO, FarmrootsStock>();
            CreateMap<UpdateFarmrootsStockDTO, FarmrootsStock>();
        }
    }
}
