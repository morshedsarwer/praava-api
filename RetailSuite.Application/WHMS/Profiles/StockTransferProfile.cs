﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.StockTransferDTO;
using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class StockTransferProfile : Profile
    {
        public StockTransferProfile()
        {
            CreateMap<StockTransfer, ReadStockTransferDTO>();
            CreateMap<CreateStockTransferDTO, StockTransfer>();
            CreateMap<UpdateStockTransferDTO, StockTransfer>();
            CreateMap<StockTransfer, CreateStockTransferDTO>();
            CreateMap<TransferProduct, TransferProductDTO>();
            CreateMap<TransferProductDTO, TransferProduct>();
        }
    }
}
