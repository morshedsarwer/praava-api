﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.ConsignmentDTO;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class ConsignmentProfile : Profile
    {
        public ConsignmentProfile()
        {
            CreateMap<Consignment, ReadConsignmentDTO>();
            CreateMap<CreateConsignmentDTO, Consignment>();
            CreateMap<UpdateConsignmentDTO, Consignment>();
            CreateMap<ConsignmentProductDTO, ConsignmentProduct>();
        }
    }
}
