﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.POReceiveProductDTO;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class POReceiveProductProfile : Profile
    {
        public POReceiveProductProfile()
        {
            CreateMap<POReceiveProduct, ReadPOReceiveProductDTO>();
            CreateMap<CreatePOReceiveProductDTO, POReceiveProduct>();
            CreateMap<UpdatePOReceiveProductDTO, POReceiveProduct>();
        }
    }
}
