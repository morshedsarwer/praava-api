﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.PODTO;
using RetailSuite.Application.WHMS.ViewModel;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class PurchaseOrderProfile:Profile
    {
        public PurchaseOrderProfile()
        {
            CreateMap<PurchaseOrder, ReadPODTO>();
            CreateMap<CreatePODTO, PurchaseOrder>();
            CreateMap<UpdatePODTO, PurchaseOrder>();
            CreateMap<POProductDTO, POProduct>();
            //CreateMap<POViewModels, POContextViewModel>();
        }

    }
}
