﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.ConsignmentReturnDTO;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class ConsignmentReturnProfile:Profile
    {
        public ConsignmentReturnProfile()
        {
            CreateMap<ConsignmentProductReturn, ReadConsignmentReturnDTO>();
            CreateMap<CreateConsignmentReturnDTO, ConsignmentProductReturn>();
            CreateMap<UpdateConsignmentReturnDTO, ConsignmentProductReturn>();
        }
    }
}
