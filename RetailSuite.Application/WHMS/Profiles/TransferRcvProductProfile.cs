﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.TransferRcvProductDTO;
using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class TransferRcvProductProfile : Profile
    {
        public TransferRcvProductProfile()
        {
            CreateMap<TransferRcvProduct, ReadTransferRcvProductDTO>();
            CreateMap<CreateTransferRcvProductDTO, TransferRcvProduct>();
            CreateMap<UpdateTransferRcvProductDTO, TransferRcvProduct>();
        }
    }
}
