﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.RequisitionRcvProductDTO;
using RetailSuite.Domain.WHMS.Models.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class RequisitionRcvProductProfile:Profile
    {
        public RequisitionRcvProductProfile()
        {
            CreateMap<RequisitionRcvProduct, ReadRequisitionRcvProductDTO>();
            CreateMap<CreateRequisitionRcvProductDTO, RequisitionRcvProduct>();
            CreateMap<UpdateRequisitionRcvProductDTO, RequisitionRcvProduct>();
        }

    }
}
