﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.PODelivaryMethodDTO;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class PODeliveryMethodProfile:Profile
    {
        public PODeliveryMethodProfile()
        {
            CreateMap<PODeliveryMethod, ReadPODeliveryMethodDTO>();
            CreateMap<CreatePODeliveryMethodDTO, PODeliveryMethod>();
            CreateMap<UpdatePODeliveryMethodDTO, PODeliveryMethod>();

        }
    }
}
