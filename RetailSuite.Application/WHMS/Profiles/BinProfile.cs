﻿using AutoMapper;
using RetailSuite.Application.WHMS.DTOS.BinDTO;
using RetailSuite.Domain.WHMS.Models.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.WHMS.Profiles
{
    public class BinProfile:Profile
    {
        public BinProfile()
        {
            CreateMap<Bin, ReadBinDTO>();
            CreateMap<CreateBinDTO, Bin>();
            CreateMap<UpdateBinDTO, Bin>();
            CreateMap<ReadBinDTO, Bin>();
        }
    }
}
