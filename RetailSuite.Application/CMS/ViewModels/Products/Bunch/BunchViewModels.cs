﻿using RetailSuite.Application.CMS.DTOS.BunchProductDTO;
using RetailSuite.Application.CMS.DTOS.BunchProductTagDTO;
using RetailSuite.Application.DTOS.BunchProductDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.ViewModels.Products.Bunch
{
    public class BunchViewModels
    {
        public CreateBunchProductDTO BunchProductDTO { get; set; }
        public IEnumerable<BunchProductTagDTO> BunchProductTag { get; set; }
        public BunchPriceDTO BunchPriceDTO { get; set; }
    }
}
