﻿using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.ViewModels.Products.Bunch
{
    public class BunchContextViewModels
    {
        public BunchPrice BunchPrice { get; set; }
        public IEnumerable<BunchProductTag> BunchProductTags { get; set; }
        public BunchProduct BunchProduct { get; set; }
    }
}
