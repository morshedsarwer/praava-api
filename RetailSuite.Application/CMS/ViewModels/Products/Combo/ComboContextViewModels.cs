﻿using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.ViewModels.Products.Combo
{
    public class ComboContextViewModels
    {
        public ComboProduct ComboProduct { get; set; }
        public IEnumerable<ComboProductTag> ComboProductTags { get; set; }


    }
}
