﻿using RetailSuite.Application.CMS.DTOS.ComboProductDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.ViewModels.Products.Combo
{
    public class ComboViewModels
    {
        public CreateComboProductDTO ComboProductDTO { get; set; }
        public IEnumerable<CreateComboProductTagDTO> ComboProductTags { get; set; } 
    }
}
