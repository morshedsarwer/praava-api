﻿using RetailSuite.Application.CMS.DTOS.ProductDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.ViewModels.Products
{
    public class ProductContextViewModel
    {
        public Product Product { get; set; }
        public IEnumerable<ProductImage> ProductImages { get; set; }
        public IEnumerable<ProductBarcode> ProductBarcode { get; set; }
        public IEnumerable<FreeProduct> FreeProducts { get; set; }
        public IEnumerable<ProductCategories> ProductCategories { get; set; }
        public IEnumerable<ProductMetaTag> ProductMetaTags { get; set; }
        public IEnumerable<ProductAttribute> Attributes { get; set; }
    }
}
