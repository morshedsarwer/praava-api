﻿using RetailSuite.Application.CMS.DTOS.ProductCategoriesDTO;
using RetailSuite.Application.CMS.DTOS.ProductDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.ViewModels.Products
{
    public class ProductViewModelDTO
    {
        public CreateProductDTO ProductDTO { get; set; }
        //public IEnumerable<CreateImageDTO> ImageDTO { get; set; }
        //public IEnumerable<CreateFreeProduct> FreeProducts { get; set; }
        public IEnumerable<CreateProductCategoriesDTO> ProdCategories { get; set; }
        public IEnumerable<CreateProdMetaTag> ProdMetaTags { get; set; }
        public IEnumerable<AttributeDTO> Attributes { get; set; }
    }
}
