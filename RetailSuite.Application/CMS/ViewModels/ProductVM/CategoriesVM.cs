﻿using RetailSuite.Application.CMS.DTOS.ProductDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.ViewModels.ProductVM
{
    public class CategoriesVM
    {
        public ReadProductDTO ReadProduct { get; set; }
        public IEnumerable<ReadProductCategoriesDTO> Cats { get; set; }
    }
}
