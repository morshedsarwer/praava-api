﻿using RetailSuite.Application.CMS.DTOS.ProductDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.ViewModels.BarCode
{
    public class BarCodeViewModels
    {
        public IEnumerable<CreateProductBarcodeDTO> BarCodes { get; set; }
    }
}
