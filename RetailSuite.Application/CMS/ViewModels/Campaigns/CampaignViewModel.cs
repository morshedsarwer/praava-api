﻿using RetailSuite.Application.CMS.DTOS.CampaignDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.ViewModels.Campaigns
{
    public class CampaignViewModel
    {
        public CreateCampaignDTO CreateCampaignDTO { get; set; }
        public IEnumerable<CampaignProductsDTO> CampaignProductsDTOs { get; set; }
    }
}
