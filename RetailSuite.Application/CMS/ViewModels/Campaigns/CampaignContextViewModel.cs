﻿using RetailSuite.Domain.CMS.Models.Campaigns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.ViewModels.Campaigns
{
    public class CampaignContextViewModel
    {
        public Campaign Campaign { get; set; }
        public IEnumerable<CampaignProducts> CampaignProducts { get; set; }
    }
}
