﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Categories
{
    public class GGChildCategory
    {
        public int Id { get; set; }
        public int GChildId { get; set; }
        public string GGChildCat { get; set; }
    }
}
