﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Categories
{
    public class ParentCategory
    {
        public int Id { get; set; }
        public string ParentCat { get; set; }
        public string ImageUrl { get; set; }
        public IEnumerable<ChildCategory> Child { get; set; }
    }
}
