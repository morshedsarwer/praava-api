﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Categories
{
    public class GrandChildCategory
    {
        public int Id { get; set; }
        public int ChildId { get; set; }
        public string GChildCat { get; set; }
        public IEnumerable<GGChildCategory> GGChild { get; set; }
    }
}
