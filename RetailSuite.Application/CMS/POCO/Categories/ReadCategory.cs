﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Categories
{
    public class ReadCategory
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}
