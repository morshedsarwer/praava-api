﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Categories
{
    public class ChildCategory
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string ChildCat { get; set; }
        //public IEnumerable<GrandChildCategory> GrandChild { get; set; }
    }
}
