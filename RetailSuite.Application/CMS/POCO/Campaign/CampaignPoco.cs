﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Campaign
{
    public class CampaignPoco
    {
        public int Id { get; set; }
        public string CampaignName { get; set; }
        public string CampaignCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public IEnumerable<CampaignProductPoco> Products { get; set; }
    }
}
