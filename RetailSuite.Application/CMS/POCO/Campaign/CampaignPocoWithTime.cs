﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Campaign
{
    public class CampaignPocoWithTime
    {
        public int Id { get; set; }
        public string CampaignName { get; set; }
        public string CampaignCode { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string EndDate { get; set; }
        public string EndTime { get; set; }
        public string Description { get; set; }
    }
}
