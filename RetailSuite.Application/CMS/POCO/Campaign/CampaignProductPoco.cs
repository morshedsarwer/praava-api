﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Campaign
{
    public class CampaignProductPoco
    {
        public int Id { get; set; }
        public string? Code { get; set; }
        public string ProductName { get; set; }
        public int ProductId { get; set; }
        public decimal RegularPrice { get; set; }
        public decimal DiscAmount { get; set; }
        public decimal AtrDiscAmount { get; set; }
        public string DiscType { get; set; }
        public string Weight { get; set; }
        public IEnumerable<CampaignProductImage> Images { get; set; }
    }
    public class CampaignProductImage
    {
        public string Url { get; set; }
    }
}
