﻿using Castle.MicroKernel.SubSystems.Conversion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Combo
{
    public class ComboProductTagGetPOCO
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal DiscountAmount { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal RegularPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal AfterDiscAmount { get; set; }
        //public DiscountType discountType { get; set; }
        public string DiscountType { get; set; }
    }
}
