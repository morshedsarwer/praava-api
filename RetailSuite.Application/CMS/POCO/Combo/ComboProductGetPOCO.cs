﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Combo
{
    public class ComboProductGetPOCO
    {
        public int ComboId { get; set; }
        public string ComboProductName { get; set; }
        public int ProductId { get; set; }
        public string ComboCode { get; set; }
        //public int SellingPriceId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal RegularPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal ComboPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        //public Decimal TotalDiscount { get; set; }
        public string Description { get; set; }
        public string? ImageUrl { get; set; }
        public bool IsApproved { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<ComboProductTagGetPOCO> Products { get; set; }

    }
}
