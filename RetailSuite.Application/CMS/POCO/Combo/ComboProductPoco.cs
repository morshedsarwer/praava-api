﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Combo
{
    public class ComboProductPoco
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string ProductName { get; set; }
        public int ProductId { get; set; }
        public decimal RegularPrice { get; set; }
        public decimal DiscAmount { get; set; }
        public decimal AftDiscAmount { get; set; }
        public string DiscType { get; set; }
    }
}
