﻿using RetailSuite.Application.CMS.POCO.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Customer
{
    public class WishListPoco
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string? ProductName { get; set; }
        public DateTime? Created { get; set; }
        public string? SKU { get; set; }
        public Decimal Price  { get; set; }
        public IEnumerable<ProductImagePoco> Images { get; set; }
    }
}
