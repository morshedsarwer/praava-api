﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Customer
{
    public class CartPoco
    {
        public int Id { get; set; }
        public string? ProductName { get; set; }
        public string? Date { get; set; }
        public string? SKU { get; set; }
        public int Quantity { get; set; }
        public decimal Total { get; set; }
        public decimal  Price { get; set; }
    }
}
