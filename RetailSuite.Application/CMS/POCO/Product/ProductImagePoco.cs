﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Product
{
    public class ProductImagePoco
    {
        public int Id { get; set; }
        public string? Url { get; set; }
        public string? ImageName { get; set; }
    }
}
