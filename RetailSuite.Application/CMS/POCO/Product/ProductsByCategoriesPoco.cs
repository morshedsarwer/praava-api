﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Product
{
    public class ProductsByCategoriesPoco
    {
        public int ProductId { get; set; }
        public string? ProductName { get; set; }
        public string? ProductSKU { get; set; }
        public string? ProductCode { get; set; }
        public string? BrandName { get; set; }
        public string? ParentCatName { get; set; }
        public string? ChildCatName { get; set; }
        public decimal Price { get; set; }
        public int ParentCatId { get; set; }
        public string? Weight { get; set; }
        public string? Description { get; set; }
        public IEnumerable<ProductImagePoco> Images { get; set; }
        public IEnumerable<ChildCategoryPoco> Childs { get; set; }
        public IEnumerable<ProductMetaTagPoco> Tags { get; set; }

    }
}
