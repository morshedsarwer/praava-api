﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Product
{
    public class ProductPoco
    {
        public int ProductId { get; set; }
        public string? ProductName { get; set; }
        public string? ProductSKU { get; set; }
        public string? ProductCode { get; set; }
        public bool IsPreOrder { get; set; }
        public bool IsPublish { get; set; }
        public bool IsMArketPlace { get; set; }
        public bool IsNewArrival { get; set; }
        public int ProductPoints { get; set; }
        public string? BrandName { get; set; }
        public string? CategoryName { get; set; }
        public decimal Price { get; set; }
    }
}
