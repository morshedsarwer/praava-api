﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Product
{
    public class ChildCategoriesProductsPoco
    {
        public string? ChildCat { get; set; }
        public int ChildCatId { get; set; }
        public IEnumerable<ProductsByChildCategoriesPoco> ProductsList { get; set; }
    }
}
