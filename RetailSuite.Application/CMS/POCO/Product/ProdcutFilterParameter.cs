﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Product
{
    public class ProdcutFilterParameter
    {
        public decimal From { get; set; }
        public decimal To { get; set; }
        public int isPublish { get; set; }
        public IEnumerable<ProductCategory> ProductCatsId { get; set; }
    }
}
