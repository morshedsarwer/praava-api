﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Product
{
    public class ProductFiltersData
    {
        public int ProductId { get; set; }
        public string? ProductCode { get; set; }
        public string? SKU { get; set; }
        public string? ProductName { get; set; }
        public string? BrandName { get; set; }
        public string? ParentCat { get; set; }
        public decimal Price { get; set; }
        public string? Ispublished { get; set; }
        public bool IsPublish { get; set; }
        public int ParentCatId { get; set; }

    }
}
