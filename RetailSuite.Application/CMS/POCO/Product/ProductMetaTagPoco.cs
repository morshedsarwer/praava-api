﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Product
{
    public class ProductMetaTagPoco
    {
        public int Id { get; set; }
        public string? Tags { get; set; }
    }
}
