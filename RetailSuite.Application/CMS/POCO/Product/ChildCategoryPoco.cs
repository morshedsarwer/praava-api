﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.POCO.Product
{
    public class ChildCategoryPoco
    {
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}
