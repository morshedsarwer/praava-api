﻿using RetailSuite.Application.CMS.DTOS.ProductDTO;
using RetailSuite.Application.CMS.POCO;
using RetailSuite.Application.CMS.POCO.Product;
using RetailSuite.Application.CMS.ViewModels.BarCode;
using RetailSuite.Application.CMS.ViewModels.Products;
using RetailSuite.Application.CMS.ViewModels.ProductVM;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Domain.CMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.ProductServices
{
    public interface IProductRepository
    {
        Task<Product> GetProductByIdAsync(int Id);
        Task<string> AddProduct(ProductContextViewModel product);
        Task<dynamic> AddProducPricetAsync(ProductPriceDTO productPrice);
        Task<IEnumerable<ProductPoco>> GetProducts();
        Task<IEnumerable<dynamic>> GetProductImages(string productCode);
        Task<IEnumerable<Product>> GetAllProducts();
        void UpdateProductAsync();
        void AddMetaTag(string productCode,CreateProdMetaTag createProdMetaTag);
        Task DeleteProductAsync(int Id);
        Task<bool> AddBarCode(BarCodeViewModels barCode); 
        Task<IEnumerable<ProductImage>> GetProductImagesByProductCode(string ProductCode);
        Task<ProductImage> GetProductImageByIdAsync(int Id);

        Task<IEnumerable<FreeProduct>> GetFreeProductsByProductCode(string ProductCode);
        Task<FreeProduct> GetFreeProductByIdAsync(int Id);
        Task DeleteFreeProduct(int Id);
        Task<IEnumerable<ProductAttribute>> GetProductAttributeByProductCode(string ProductCode);
        Task<ProductAttribute> GetProductAttributeById(int Id);
        Task DeleteProductAttribute(int Id);
        Task<IEnumerable<ProductCategories>> GetProductCategoriesByProductCode(string ProductCode);
        Task<ProductCategories> GetProductCategoriesById(int Id);
        Task DeleteProductCategories(int Id);
        Task<IEnumerable<ProductMetaTag>> GetProductMetatagByProductCode(string ProductCode);
        Task<ProductMetaTag> GetProductMetaTagById(int Id);
        Task DeleteProductMetaTag(int Id);
        Task<ProductPrice> GetProductPriceById(int Id);
        Task DeleteProductPrice(int Id);
        Task<ProductBarcode> GetProductBarcodeById(int Id);
        Task DeleteProductBarcode(int Id);
        Task<bool> DeleteAddedProducts(string ProductCode);
        Task<object> GetProductsByCategory();
        Task<IEnumerable<ProductFiltersData>>GetProductsFilterData(ProdcutFilterParameter prodcutFilter);
        Task<IEnumerable<object>> GetProductsBYCategories();
        Task<IEnumerable<object>> GetProductsByChildCategories(int ChildId);
        Task<IEnumerable<object>> GetProductsByParentCategories(int parentId);
        Task<CategoriesVM> ProductCategories(int Id);
        Task<IEnumerable<object>> GetProductsBYCategories(string search);

    }
}
