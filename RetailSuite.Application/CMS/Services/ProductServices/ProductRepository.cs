﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CMS.DTOS.ProductDTO;
using RetailSuite.Application.CMS.POCO;
using RetailSuite.Application.CMS.POCO.Product;
using RetailSuite.Application.CMS.ViewModels.BarCode;
using RetailSuite.Application.CMS.ViewModels.Products;
using RetailSuite.Application.CMS.ViewModels.ProductVM;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Domain.WHMS.Models.Stock;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.ProductServices
{
    public class ProductRepository : IProductRepository
    {
        private readonly RetailDbContext _context;
        private readonly IMapper _mapper;

        public ProductRepository(RetailDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> AddBarCode(BarCodeViewModels barCode)
        {
            try
            {
                var barcodes = _mapper.Map<IEnumerable<ProductBarcode>>(barCode.BarCodes);
                await _context.ProductBarcodes.AddRangeAsync(barcodes);
                var result = await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> AddProducPricetAsync(ProductPriceDTO productPrice)
        {
            try
            {
                var product = await _context.Products.FirstOrDefaultAsync(p => p.ProductId == productPrice.ProductId);
                if (product == null)
                    return "Product not exists";
                using (var transaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        var price = _mapper.Map<ProductPrice>(productPrice);
                        var pri = await _context.ProductPrices.AddAsync(price);
                        await _context.SaveChangesAsync();
                        product.IsPublish = productPrice.IsPublish;
                        //product.IsNewArrival = productPrice.IsNewArrival;
                        //product.ProductPoints = productPrice.ProductPoints;
                        await _context.SaveChangesAsync();
                        await transaction.CommitAsync();
                        return new ProductPriceDTO { Price = pri.Entity.Price, ProductId = pri.Entity.ProductId };
                    }
                    catch (Exception)
                    {
                        await transaction.RollbackAsync();
                        throw;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<string> AddProduct(ProductContextViewModel product)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //List<ProductImage> ProductImageList = new List<ProductImage>();
                    //var freeProducts = new List<FreeProduct>();
                    var categories = new List<ProductCategories>();
                    var metaTags = new List<ProductMetaTag>();
                    var attr = new List<ProductAttribute>();
                    foreach (var item in product.Attributes)
                    {
                        item.ProductCode = product.Product.ProductCode;
                        attr.Add(item);
                    }
                    /*foreach (var item in product.ProductImages)
                    {
                        item.ProductCode = product.Product.ProductCode;
                        ProductImageList.Add(item);
                    }*/
                    foreach (var item in product.ProductCategories)
                    {
                        item.ProductCode = product.Product.ProductCode;
                        categories.Add(item);
                    }
                    foreach (var item in product.ProductMetaTags)
                    {
                        item.ProductCode = product.Product.ProductCode;
                        metaTags.Add(item);
                    }
                    var prods = await _context.Products.AddAsync(product.Product);
                    await _context.SaveChangesAsync();


                    // only for farmroots
                    var farm = new FarmrootsStock()
                    {
                        Quantity = 0,
                        StoreId = 1,
                        ProductCode = product.Product.ProductCode,
                    };

                    await _context.FarmrootsStocks.AddAsync(farm);
                    await _context.SaveChangesAsync();

                    //await _context.ProductImages.AddRangeAsync(ProductImageList);
                    //await _context.SaveChangesAsync();
                    await _context.ProductCategories.AddRangeAsync(categories);
                    await _context.SaveChangesAsync();
                    await _context.ProductMetaTags.AddRangeAsync(metaTags);
                    await _context.SaveChangesAsync();
                    await _context.ProductAttributes.AddRangeAsync(attr);
                    await _context.SaveChangesAsync();
                    /*if (product.Product.IsHasFreeProd == true)
                    {
                        foreach (var free in product.FreeProducts)
                        {
                            free.ProductCode = product.Product.ProductCode;
                            freeProducts.Add(free);
                        }
                        await _context.FreeProducts.AddRangeAsync(freeProducts);
                        await _context.SaveChangesAsync();
                    }*/
                    transaction.Commit();
                    return product.Product.ProductCode;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return "failed";
                }

            }

        }
        public async Task DeleteProductAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetProductByIdAsync(Id);
                itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<Product> GetProductByIdAsync(int Id)
        {
            try
            {
                var product = await _context.Products.Where(p => p.IsDelete == false).FirstOrDefaultAsync(s => s.ProductId == Id);
                return product;
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<IEnumerable<dynamic>> GetProductImages(string productCode)
        {
            try
            {
                var images = from image in _context.ProductImages
                             where (image.ProductCode == productCode)
                             select new
                             {
                                 Id = image.ProductImageId,
                                 Url = image.Url,
                                 ImageName = image.ImageName,
                                 AltImageName = image.AltImageName
                             };
                return images;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<ProductPoco>> GetProducts()
        {
            var products = new List<ProductPoco>();
            try
            {
                using (var con = _context.Database.GetDbConnection())
                {
                    if (con.State != System.Data.ConnectionState.Open)
                        con.Open();
                    var command = con.CreateCommand();
                    // need to close db connection
                    command.CommandText = "usp_get_products_with_brand_and_price";

                    using (var result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            while (result.Read())
                            {
                                products.Add(new ProductPoco
                                {
                                    ProductId = Convert.ToInt32(result[0]),
                                    ProductName = Convert.ToString(result[1]),
                                    ProductCode = Convert.ToString(result[2]),
                                    IsPreOrder = Convert.ToBoolean(result[3]),
                                    IsPublish = Convert.ToBoolean(result[4]),
                                    IsMArketPlace = Convert.ToBoolean(result[5]),
                                    IsNewArrival = Convert.ToBoolean(result[6]),
                                    ProductPoints = Convert.ToInt32(result[7]),
                                    BrandName = Convert.ToString(result[8]),
                                    Price = Convert.ToDecimal(result[9]),
                                    ProductSKU = Convert.ToString(result[10]),
                                    CategoryName = Convert.ToString(result[11])
                                });
                            }
                        }
                        con.Close();
                    }
                }
                return products;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public void UpdateProductAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ProductImage>> GetProductImagesByProductCode(string ProductCode)
        {
            try
            {
                var productImages = await _context.ProductImages.Where(s => s.ProductCode == ProductCode).ToListAsync();
                return productImages;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ProductImage> GetProductImageByIdAsync(int Id)
        {
            try
            {
                var productImage = await _context.ProductImages.FirstOrDefaultAsync(s => s.ProductImageId == Id);
                return productImage;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<IEnumerable<FreeProduct>> GetFreeProductsByProductCode(string ProductCode)
        {
            try
            {
                var freeproducts = await _context.FreeProducts.Where(s => s.ProductCode == ProductCode).ToListAsync();
                return freeproducts;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<FreeProduct> GetFreeProductByIdAsync(int Id)
        {
            try
            {
                var freeproduct = await _context.FreeProducts.FirstOrDefaultAsync(s => s.FreeProductId == Id);
                return freeproduct;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task DeleteFreeProduct(int Id)
        {
            try
            {
                var itemRemoved = await GetFreeProductByIdAsync(Id);
                if (itemRemoved != null)
                {
                    itemRemoved.IsDelete = true;
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<ProductAttribute>> GetProductAttributeByProductCode(string ProductCode)
        {
            try
            {
                var attributes = await _context.ProductAttributes.Where(s => s.ProductCode == ProductCode).ToListAsync();
                return attributes;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ProductAttribute> GetProductAttributeById(int Id)
        {
            try
            {
                var attribute = await _context.ProductAttributes.FirstOrDefaultAsync(s => s.Id == Id);
                return attribute;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task DeleteProductAttribute(int Id)
        {
            try
            {
                var itemRemoved = await GetProductAttributeById(Id);
                if (itemRemoved != null)
                {
                    itemRemoved.IsDelete = true;
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ProductCategories>> GetProductCategoriesByProductCode(string ProductCode)
        {
            try
            {
                var productCategories = await _context.ProductCategories.Where(s => s.ProductCode == ProductCode).ToListAsync();
                return productCategories;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ProductCategories> GetProductCategoriesById(int Id)
        {
            try
            {
                var productCategory = await _context.ProductCategories.FirstOrDefaultAsync(s => s.Id == Id);
                return productCategory;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task DeleteProductCategories(int Id)
        {
            try
            {
                var itemRemoved = await GetProductCategoriesById(Id);
                if (itemRemoved != null)
                {
                    itemRemoved.IsDelete = true;
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ProductMetaTag>> GetProductMetatagByProductCode(string ProductCode)
        {
            try
            {
                var tags = await _context.ProductMetaTags.Where(s => s.ProductCode == ProductCode).ToListAsync();
                return tags;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ProductMetaTag> GetProductMetaTagById(int Id)
        {
            try
            {
                var tag = await _context.ProductMetaTags.FirstOrDefaultAsync(s => s.Id == Id);
                return tag;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task DeleteProductMetaTag(int Id)
        {
            try
            {
                var itemRemoved = await GetProductMetaTagById(Id);
                if (itemRemoved != null)
                {
                    _context.ProductMetaTags.Remove(itemRemoved);
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ProductPrice> GetProductPriceById(int Id)
        {
            try
            {
                var price = await _context.ProductPrices.FirstOrDefaultAsync(s => s.Id == Id);
                return price;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task DeleteProductPrice(int Id)
        {
            try
            {
                var itemRemoved = await GetProductPriceById(Id);
                if (itemRemoved != null)
                {
                    itemRemoved.IsDelete = true;
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ProductBarcode> GetProductBarcodeById(int Id)
        {
            try
            {
                var barcode = await _context.ProductBarcodes.FirstOrDefaultAsync(s => s.ProductBarCodeId == Id);
                return barcode;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task DeleteProductBarcode(int Id)
        {
            try
            {
                var itemRemoved = await GetProductBarcodeById(Id);
                if (itemRemoved != null)
                {
                    itemRemoved.IsDelete = true;
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public async Task<bool> DeleteAddedProducts(string ProductCode)
        {
            try
            {
                var attribute = _context.ProductAttributes.Where(s => s.ProductCode == ProductCode).ToList();
                var categories = _context.ProductCategories.Where(s => s.ProductCode == ProductCode).ToList();
                var metatag = _context.ProductMetaTags.Where(s => s.ProductCode == ProductCode).ToList();
                var products = _context.Products.FirstOrDefault(s => s.ProductCode == ProductCode);

                using (var transaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        if (attribute != null)
                        {
                            _context.ProductAttributes.RemoveRange(attribute);
                            _context.SaveChanges();
                        }
                        if (categories != null)
                        {
                            _context.ProductCategories.RemoveRange(categories);
                            _context.SaveChanges();
                        }
                        if (metatag != null)
                        {
                            _context.ProductMetaTags.RemoveRange(metatag);
                            _context.SaveChanges();
                        }
                        if (products != null)
                        {
                            _context.Products.Remove(products);
                            _context.SaveChanges();
                        }
                        transaction.Commit();
                        return true;
                    }
                    catch
                    {
                        transaction.Rollback();
                        return false;
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            try
            {
                var products = await _context.Products.ToListAsync();
                return products;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<object> GetProductsByCategory()
        {
            throw new NotImplementedException();
        }

        public void AddMetaTag(string productCode, CreateProdMetaTag createProdMetaTag)
        {
            try
            {
                var tag = new ProductMetaTag()
                {
                    ProductCode = productCode,
                    MetaTag = createProdMetaTag.MetaTag
                };
                _context.ProductMetaTags.Add(tag);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ProductFiltersData>> GetProductsFilterData(ProdcutFilterParameter prodcutFilter)
        {
            try
            {
                var filters = new List<ProductFiltersData>();
                using (var con = _context.Database.GetDbConnection())
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Columns.Add(new DataColumn("ProductId", typeof(int)));
                    foreach (var item in prodcutFilter.ProductCatsId)
                    {
                        dataTable.Rows.Add(item.Id);
                    }
                    if (con.State != System.Data.ConnectionState.Open)
                        con.Open();
                    var command = con.CreateCommand();
                    // need to close db connection
                    command.CommandText = "get_products_by_parameters @from,@to,@ispublsh,@ProductsId";
                    command.Parameters.Add(new SqlParameter("@from", prodcutFilter.From));
                    command.Parameters.Add(new SqlParameter("@to", prodcutFilter.To));
                    command.Parameters.Add(new SqlParameter("@ispublsh", prodcutFilter.isPublish));
                    var param = new SqlParameter("ProductsId", dataTable);
                    param.SqlDbType = SqlDbType.Structured;
                    param.TypeName = "ProductCategoriesId";
                    command.Parameters.Add(param);
                    using (var result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                            while (result.Read())
                            {
                                filters.Add(new ProductFiltersData()
                                {
                                    ProductId = Convert.ToInt32(result[0]),
                                    ProductCode = Convert.ToString(result[1]),
                                    SKU = Convert.ToString(result[2]),
                                    ProductName = Convert.ToString(result[3]),
                                    BrandName = Convert.ToString(result[4]),
                                    ParentCat = Convert.ToString(result[5]),
                                    Ispublished = Convert.ToString(result[6]),
                                    IsPublish = Convert.ToBoolean(result[7]),
                                    ParentCatId = Convert.ToInt32(result[8]),
                                    Price = Convert.ToDecimal(result[9]),
                                });
                            }
                    }
                    con.Close();
                }
                return filters;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public async Task<IEnumerable<object>> GetProductsBYCategories()
        {
            try
            {
                var images = _context.ProductImages.ToList();
                var tags = _context.ProductMetaTags.ToList();
                var childCats = _context.Cats.ToList();
                var parents = from p in childCats
                              where !string.IsNullOrWhiteSpace(p.ParentCat)
                              select p;
                var parentcatIds = _context.ProductCategories.ToList();
                var products = new List<ProductsByCategoriesPoco>();
                /*var results = new List<ProductsByCategoriesPoco>();
                var vm = new List<CategoriesProductsPoco>();*/
                List<List<CategoriesProductsPoco>> vm2 = new List<List<CategoriesProductsPoco>>();
                var list = new List<object>();
                using (var con = _context.Database.GetDbConnection())
                {
                    if (con.State != System.Data.ConnectionState.Open)
                        con.Open();
                    var command = con.CreateCommand();
                    // need to close db connection
                    command.CommandText = "get_product_by_categories";
                    using (var result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            while (result.Read())
                            {
                                products.Add(new ProductsByCategoriesPoco()
                                {
                                    ProductId = Convert.ToInt32(result[0]),
                                    Price = Convert.ToDecimal(result[1]),
                                    ProductSKU = Convert.ToString(result[2]),
                                    ProductName = Convert.ToString(result[3]),
                                    ProductCode = Convert.ToString(result[4]),
                                    BrandName = Convert.ToString(result[5]),
                                    ParentCatId = Convert.ToInt32(result[6]),
                                    ParentCatName = Convert.ToString(result[7]),
                                    Weight = Convert.ToString(result[8]),
                                    Description = Convert.ToString(result[9])
                                });
                            }
                        }
                    }
                    con.Close();
                }
                var data = from pro in products
                           group pro by pro.ParentCatId
                               into ep
                           orderby ep.Key
                           select ep;
                var parentCat = "";
                var catId = 0;
                foreach (var p in data)
                {
                    var results = new List<ProductsByCategoriesPoco>();
                    var vm = new List<CategoriesProductsPoco>();
                    if (p != null)
                    {
                        foreach (var item in p)
                        {
                            if (p.Key == item.ParentCatId)
                            {
                                parentCat = item.ParentCatName;
                                catId = item.ParentCatId;

                                results.Add(new ProductsByCategoriesPoco()
                                {
                                    BrandName = item.BrandName,
                                    Price = item.Price,
                                    ProductSKU = item.ProductSKU,
                                    ProductCode = item.ProductCode,
                                    ProductId = item.ProductId,
                                    ProductName = item.ProductName,
                                    ParentCatId = item.ParentCatId,
                                    ParentCatName = item.ParentCatName,
                                    Weight = item.Weight,
                                    Description = item.Description,
                                    Childs = from cat in childCats
                                             where cat.ParentId == item.ParentCatId
                                             select new ChildCategoryPoco()
                                             {
                                                 Id = cat.Id,
                                                 Name = cat.ChildCat
                                             },
                                    Images = from img in images
                                             where img.ProductCode == item.ProductCode
                                             select new ProductImagePoco()
                                             {
                                                 Id = img.ProductImageId,
                                                 ImageName = img.ImageName,
                                                 Url = img.Url,
                                             },
                                    Tags = from tag in tags
                                           where tag.ProductCode == item.ProductCode
                                           select new ProductMetaTagPoco()
                                           {
                                               Id = tag.Id,
                                               Tags = tag.MetaTag
                                           }
                                });
                            }
                        }
                        vm.Add(new CategoriesProductsPoco()
                        {
                            ProductsList = results,
                            ParentCat = parentCat,
                            ParentCatId = catId
                        });
                        vm2.Add(vm.ToList());
                    }

                }
                return vm2;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<CategoriesVM> ProductCategories(int Id)
        {
            try
            {
                //var vm = new CategoriesVM();
                var product = _context.Products.FirstOrDefault(x => x.ProductId == Id);
                var p = _mapper.Map<ReadProductDTO>(product);
                var categories = from cat in _context.ProductCategories
                                 where cat.ProductCode == product.ProductCode
                                 select new ReadProductCategoriesDTO()
                                 {
                                     ProductCode = cat.ProductCode,
                                     ChildIdId = cat.ChildCatId,
                                     ParentId = cat.ParentCatId
                                 };
                var vm = new CategoriesVM()
                {
                    Cats = categories,
                    ReadProduct = p
                };
                return vm;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<object>> GetProductsByChildCategories(int ChildId)
        {
            try
            {
                var images = _context.ProductImages.ToList();
                var tags = _context.ProductMetaTags.ToList();
                var childCats = _context.Cats.ToList();
                var parentcatIds = _context.ProductCategories.ToList();
                var parent = from p in _context.ProductCategories
                             group p by p.ParentCatId
                            into g
                             select new { Id = g.Key };
                var products = new List<ProductsByChildCategoriesPoco>();
                List<List<ChildCategoriesProductsPoco>> vm2 = new List<List<ChildCategoriesProductsPoco>>();
                /*var results = new List<ProductsByCategoriesPoco>();
                var vm = new List<CategoriesProductsPoco>();*/
                using (var con = _context.Database.GetDbConnection())
                {
                    if (con.State != System.Data.ConnectionState.Open)
                        con.Open();
                    var command = con.CreateCommand();
                    // need to close db connection
                    command.CommandText = "get_products_by_child_categories @childcatId";
                    command.Parameters.Add(new SqlParameter("@childcatId", ChildId));
                    using (var result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            while (result.Read())
                            {
                                products.Add(new ProductsByChildCategoriesPoco()
                                {
                                    ProductId = Convert.ToInt32(result[0]),
                                    Price = Convert.ToDecimal(result[1]),
                                    ProductSKU = Convert.ToString(result[2]),
                                    ProductName = Convert.ToString(result[3]),
                                    ProductCode = Convert.ToString(result[4]),
                                    BrandName = Convert.ToString(result[5]),
                                    ParentCatId = Convert.ToInt32(result[6]),
                                    ChildCatName = Convert.ToString(result[7]),
                                    ChildId = Convert.ToInt32(result[8]),
                                    Weight = Convert.ToString(result[9]),
                                    Description = Convert.ToString(result[10]),
                                });
                            }
                        }
                    }
                    con.Close();

                }

                var data = from pro in products
                           group pro by pro.ParentCatId
                               into ep
                           orderby ep.Key
                           select ep;
                var childCat = "";
                var catId = 0;
                foreach (var p in data)
                {
                    var results = new List<ProductsByChildCategoriesPoco>();
                    var vm = new List<ChildCategoriesProductsPoco>();
                    if (p != null)
                    {
                        foreach (var item in p)
                        {
                            if (p.Key == item.ParentCatId)
                            {
                                childCat = item.ChildCatName;
                                catId = item.ChildId;

                                results.Add(new ProductsByChildCategoriesPoco()
                                {
                                    BrandName = item.BrandName,
                                    Price = item.Price,
                                    ProductSKU = item.ProductSKU,
                                    ProductCode = item.ProductCode,
                                    ProductId = item.ProductId,
                                    ProductName = item.ProductName,
                                    ParentCatId = item.ParentCatId,
                                    ChildCatName = item.ChildCatName,
                                    Weight = item.Weight,
                                    Description = item.Description,
                                    ChildId = item.ChildId,
                                    Images = from img in images
                                             where img.ProductCode == item.ProductCode
                                             select new ProductImagePoco()
                                             {
                                                 Id = img.ProductImageId,
                                                 ImageName = img.ImageName,
                                                 Url = img.Url,
                                             },
                                    Tags = from tag in tags
                                           where tag.ProductCode == item.ProductCode
                                           select new ProductMetaTagPoco()
                                           {
                                               Id = tag.Id,
                                               Tags = tag.MetaTag
                                           }
                                });
                            }
                        }
                        vm.Add(new ChildCategoriesProductsPoco()
                        {
                            ProductsList = results,
                            ChildCat = childCat,
                            ChildCatId = catId
                        });
                        vm2.Add(vm.ToList());
                    }

                }
                return vm2;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public async Task<IEnumerable<object>> GetProductsByParentCategories(int parentId)
        {
            try
            {
                var images = _context.ProductImages.ToList();
                var tags = _context.ProductMetaTags.ToList();
                var childCats = _context.Cats.ToList();
                var parents = from p in childCats
                              where !string.IsNullOrWhiteSpace(p.ParentCat)
                              select p;
                var parentcatIds = _context.ProductCategories.ToList();
                var products = new List<ProductsByCategoriesPoco>();
                /*var results = new List<ProductsByCategoriesPoco>();
                var vm = new List<CategoriesProductsPoco>();*/
                List<List<CategoriesProductsPoco>> vm2 = new List<List<CategoriesProductsPoco>>();
                var list = new List<object>();
                using (var con = _context.Database.GetDbConnection())
                {
                    if (con.State != System.Data.ConnectionState.Open)
                        con.Open();
                    var command = con.CreateCommand();
                    // need to close db connection
                    command.CommandText = "get_products_by_parent_categories @parentcatId";
                    command.Parameters.Add(new SqlParameter("@parentcatId", parentId));
                    using (var result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            while (result.Read())
                            {
                                products.Add(new ProductsByCategoriesPoco()
                                {
                                    ProductId = Convert.ToInt32(result[0]),
                                    Price = Convert.ToDecimal(result[1]),
                                    ProductSKU = Convert.ToString(result[2]),
                                    ProductName = Convert.ToString(result[3]),
                                    ProductCode = Convert.ToString(result[4]),
                                    BrandName = Convert.ToString(result[5]),
                                    ParentCatId = Convert.ToInt32(result[6]),
                                    ParentCatName = Convert.ToString(result[7]),
                                    Weight = Convert.ToString(result[8]),
                                    Description = Convert.ToString(result[9])
                                });
                            }
                        }
                    }
                    con.Close();
                }
                var data = from pro in products
                           group pro by pro.ParentCatId
                               into ep
                           orderby ep.Key
                           select ep;
                var parentCat = "";
                var catId = 0;
                foreach (var p in data)
                {
                    var results = new List<ProductsByCategoriesPoco>();
                    var vm = new List<CategoriesProductsPoco>();
                    if (p != null)
                    {
                        foreach (var item in p)
                        {
                            if (p.Key == item.ParentCatId)
                            {
                                parentCat = item.ParentCatName;
                                catId = item.ParentCatId;

                                results.Add(new ProductsByCategoriesPoco()
                                {
                                    BrandName = item.BrandName,
                                    Price = item.Price,
                                    ProductSKU = item.ProductSKU,
                                    ProductCode = item.ProductCode,
                                    ProductId = item.ProductId,
                                    ProductName = item.ProductName,
                                    ParentCatId = item.ParentCatId,
                                    ParentCatName = item.ParentCatName,
                                    Weight = item.Weight,
                                    Description = item.Description,
                                    Images = from img in images
                                             where img.ProductCode == item.ProductCode
                                             select new ProductImagePoco()
                                             {
                                                 Id = img.ProductImageId,
                                                 ImageName = img.ImageName,
                                                 Url = img.Url,
                                             },
                                    Tags = from tag in tags
                                           where tag.ProductCode == item.ProductCode
                                           select new ProductMetaTagPoco()
                                           {
                                               Id = tag.Id,
                                               Tags = tag.MetaTag
                                           }
                                });
                            }
                        }
                        vm.Add(new CategoriesProductsPoco()
                        {
                            ProductsList = results,
                            ParentCat = parentCat,
                            ParentCatId = catId
                        });
                        vm2.Add(vm.ToList());
                    }

                }
                return vm2;
            }
            catch (Exception)
            {
                throw;
            }
        }



        public async Task<IEnumerable<object>> GetProductsBYCategories(string search)
        {
            try
            {
                var images = _context.ProductImages.ToList();
                var tags = _context.ProductMetaTags.ToList();
                var childCats = _context.Cats.ToList();
                var parents = from p in childCats
                              where !string.IsNullOrWhiteSpace(p.ParentCat)
                              select p;
                var parentcatIds = _context.ProductCategories.ToList();
                var products = new List<ProductsByCategoriesPoco>();
                /*var results = new List<ProductsByCategoriesPoco>();
                var vm = new List<CategoriesProductsPoco>();*/
                List<List<CategoriesProductsPoco>> vm2 = new List<List<CategoriesProductsPoco>>();
                var list = new List<object>();
                using (var con = _context.Database.GetDbConnection())
                {
                    if (con.State != System.Data.ConnectionState.Open)
                        con.Open();
                    var command = con.CreateCommand();
                    // need to close db connection
                    command.CommandText = "get_product_by_search @search";
                    command.Parameters.Add(new SqlParameter("@search", search));
                    using (var result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            while (result.Read())
                            {
                                products.Add(new ProductsByCategoriesPoco()
                                {
                                    ProductId = Convert.ToInt32(result[0]),
                                    Price = Convert.ToDecimal(result[1]),
                                    ProductSKU = Convert.ToString(result[2]),
                                    ProductName = Convert.ToString(result[3]),
                                    ProductCode = Convert.ToString(result[4]),
                                    BrandName = Convert.ToString(result[5]),
                                    ParentCatId = Convert.ToInt32(result[6]),
                                    ParentCatName = Convert.ToString(result[7]),
                                    Weight = Convert.ToString(result[8]),
                                    Description = Convert.ToString(result[9])
                                });
                            }
                        }
                    }
                    con.Close();
                }
                var data = from pro in products
                           group pro by pro.ParentCatId
                               into ep
                           orderby ep.Key
                           select ep;
                var parentCat = "";
                var catId = 0;
                foreach (var p in data)
                {
                    var results = new List<ProductsByCategoriesPoco>();
                    var vm = new List<CategoriesProductsPoco>();
                    if (p != null)
                    {
                        foreach (var item in p)
                        {
                            if (p.Key == item.ParentCatId)
                            {
                                parentCat = item.ParentCatName;
                                catId = item.ParentCatId;

                                results.Add(new ProductsByCategoriesPoco()
                                {
                                    BrandName = item.BrandName,
                                    Price = item.Price,
                                    ProductSKU = item.ProductSKU,
                                    ProductCode = item.ProductCode,
                                    ProductId = item.ProductId,
                                    ProductName = item.ProductName,
                                    ParentCatId = item.ParentCatId,
                                    ParentCatName = item.ParentCatName,
                                    Weight = item.Weight,
                                    Description = item.Description,
                                    Childs = from cat in childCats
                                             where cat.ParentId == item.ParentCatId
                                             select new ChildCategoryPoco()
                                             {
                                                 Id = cat.Id,
                                                 Name = cat.ChildCat
                                             },
                                    Images = from img in images
                                             where img.ProductCode == item.ProductCode
                                             select new ProductImagePoco()
                                             {
                                                 Id = img.ProductImageId,
                                                 ImageName = img.ImageName,
                                                 Url = img.Url,
                                             },
                                    Tags = from tag in tags
                                           where tag.ProductCode == item.ProductCode
                                           select new ProductMetaTagPoco()
                                           {
                                               Id = tag.Id,
                                               Tags = tag.MetaTag
                                           }
                                });
                            }
                        }
                        vm.Add(new CategoriesProductsPoco()
                        {
                            ProductsList = results,
                            ParentCat = parentCat,
                            ParentCatId = catId
                        });
                        vm2.Add(vm.ToList());
                    }

                }
                return vm2;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
