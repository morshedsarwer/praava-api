﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CMS.POCO.Combo;
using RetailSuite.Application.CMS.ViewModels.Products.Combo;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.ComboProductServices
{
    public class ComboProductRepository : IComboProductRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public ComboProductRepository(RetailDbContext context, ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public async Task<string> AddComboProduct(ComboContextViewModels contextViewModels, string username)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    // get the user from the username

                    var user = _saveContext.GetUserFromUserName(username);

                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    contextViewModels.ComboProduct.ComboCode = code;
                    contextViewModels.ComboProduct.CreatedBy = user.Id;

                    Console.WriteLine(contextViewModels.ComboProduct.CreatedBy);


                    List<ComboProductTag> tags = new List<ComboProductTag>();
                    foreach (var item in contextViewModels.ComboProductTags)
                    {
                        item.ComboCode = code;
                        item.CreatedBy = user.Id;
                        tags.Add(item);
                    }
                    _context.ComboProducts.Add(contextViewModels.ComboProduct);
                    _context.SaveChanges();
                    _context.ComboProductTags.AddRange(tags);
                    _context.SaveChanges();
                    transaction.Commit();
                    return contextViewModels.ComboProduct.ComboCode;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return "failed";

                }
            }
        }

        public async Task DeleteComboProductAsync(int Id)
        {
            try
            {
                var itemRemoved = await _context.ComboProducts.FirstOrDefaultAsync(s => s.ComboId == Id);
                if (itemRemoved != null)
                {
                    itemRemoved.IsDelete = true;
                    var comboProductTags = _context.ComboProductTags.Where(s => s.ComboCode == itemRemoved.ComboCode).ToList();
                    foreach(var item in comboProductTags)
                    {
                        item.IsDelete = true;
                    }

                    //   _context.ComboProducts.Remove(itemRemoved);
                    
                    //_context.ComboProductTags.Remove(itemRemoved);
                    await _context.SaveChangesAsync();
                }
            }
            catch
            {

                throw new ArgumentNullException();
            }
        }

        public async Task<IEnumerable<dynamic>> GetComboProductAsync()
        {
            try
            {
                //var comboProducts = await _context.ComboProducts.Where(s => s.IsDelete == false).ToListAsync();                
                //return comboProducts;
                var data = from combo in _context.ComboProducts

                           where combo.IsDelete == false
                           select new ComboProductGetPOCO
                           {
                               ComboId = combo.ComboId,
                               ComboPrice = combo.ComboPrice,
                               ComboProductName = combo.ComboProductName,
                               ComboCode = combo.ComboCode,
                               Description = combo.Description,
                               ImageUrl = combo.ImageUrl,
                               IsApproved = combo.IsApproved,
                               RegularPrice = combo.RegularPrice,
                               ProductId = combo.ProductId,
                               StoreId = combo.StoreId,
                               StoreName = _context.Stores.FirstOrDefault(s => s.StoreId == combo.StoreId).StoreName,
                               StartDate = combo.StartDate,
                               EndDate = combo.EndDate,
                               Products = (from tag in _context.ComboProductTags
                                           where (tag.ComboCode == combo.ComboCode && tag.IsDelete == false)
                                           select new ComboProductTagGetPOCO
                                           {
                                               Id = tag.Id,
                                               AfterDiscAmount = tag.AfterDiscAmount,
                                               ProductId = tag.ProductId,
                                               ProductName = _context.Products.FirstOrDefault(s => s.ProductId == tag.ProductId).ProductName,
                                               DiscountAmount = tag.DiscountAmount,
                                               DiscountType = tag.DiscountType,
                                               RegularPrice = tag.RegularPrice
                                           }).ToList()



                           };

                return data;
            }
            catch
            {

                throw new ArgumentNullException();
            }
        }
        public async Task<IEnumerable<ComboProductPoco>> GetComboProducts(string ComboCode)
        {
            try
            {
                // this is used by the delete method
                //var comboProduct = await _context.ComboProducts.Where(c=>c.ComboCode==ComboCode).ToListAsync();
                var data = from combo in _context.ComboProductTags
                           where combo.IsDelete == false
                           join product in _context.Products on combo.ProductId equals product.ProductId
                           where (combo.ComboCode == ComboCode )
                           select new ComboProductPoco()
                           {
                               Id = combo.Id,
                               ProductName = product.ProductName,
                               Code = combo.ComboCode,
                               DiscAmount = combo.DiscountAmount,
                               RegularPrice = combo.RegularPrice,
                               DiscType = combo.DiscountType,
                               AftDiscAmount = combo.AfterDiscAmount,
                               ProductId = combo.ProductId

                           };
                return await data.ToListAsync();
            }
            catch
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<ComboContextViewModels> GetComboProductById(int Id)
        {
            var p = new ComboContextViewModels();
            p.ComboProduct = await _context.ComboProducts.Where(p => p.IsDelete == false).FirstOrDefaultAsync(s => s.ComboId == Id);
            p.ComboProductTags = await (_context.ComboProductTags.Where(s => s.ComboCode == p.ComboProduct.ComboCode)).ToListAsync();
            return p;
        }
        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync() > 0);
        }
        public void UpdateComboProductTagAsync()
        {
            // not implemented here
        }

        public bool DeleteAddedProducts(string ComboCode)
        {
            var product = _context.ComboProducts.FirstOrDefault(s => s.ComboCode == ComboCode);
            var tags = _context.ComboProductTags.Where(s => s.ComboCode == ComboCode).ToList();
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (tags != null)
                    {
                        _context.ComboProductTags.RemoveRange(tags);
                        _context.SaveChanges();
                    }
                    if (product != null)
                    {
                        _context.ComboProducts.Remove(product);
                        _context.SaveChanges();
                    }
                    transaction.Commit();
                    return true;
                }
                catch
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public async Task<ComboProductTag> GetComboProductTagById(int Id)
        {
            var tag = await _context.ComboProductTags.FirstOrDefaultAsync(s => (s.Id == Id && s.IsDelete == false));
            return tag;
        }

        public async Task DeleteComboProductTag(int Id)
        {
            var itemRemoved = await GetComboProductTagById(Id);
            if (itemRemoved != null)
            {
                itemRemoved.IsDelete = true;
                //_context.ComboProductTags.Remove(itemRemoved);
                await _saveContext.SaveChangesAsync();
            }
        }

        public async Task AddComboProductTag(ComboProductTag comboProductTag)
        {
            await _context.ComboProductTags.AddAsync(comboProductTag);
            await _saveContext.SaveChangesAsync();
        }

        public Task<IEnumerable<ComboProductPoco>> GetComboProductByIdAsync(string ComboCode)
        {
            throw new NotImplementedException();
        }

        public async Task<ComboProduct> GetComboProductByIdAsync(int Id)
        {
            try
            {
                // this is used by the delete method
                var comboProduct = await _context.ComboProducts.Where(s => s.IsDelete == false).FirstOrDefaultAsync(c => c.ComboId == Id);
                return comboProduct;
            }
            catch
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<dynamic> GetComboById(int Id)
        {
            try
            {
                //var comboProducts = await _context.ComboProducts.Where(s => s.IsDelete == false).ToListAsync();                
                //return comboProducts;
                var data = from combo in _context.ComboProducts

                           where (combo.IsDelete == false && combo.ComboId == Id)
                           select new ComboProductGetPOCO
                           {
                               ComboId = combo.ComboId,
                               ComboPrice = combo.ComboPrice,
                               ComboProductName = combo.ComboProductName,
                               ComboCode = combo.ComboCode,
                               Description = combo.Description,
                               ImageUrl = combo.ImageUrl,
                               IsApproved = combo.IsApproved,
                               RegularPrice = combo.RegularPrice,
                               ProductId = combo.ProductId,
                               StoreId = combo.StoreId,
                               StoreName = _context.Stores.FirstOrDefault(s => s.StoreId == combo.StoreId).StoreName,
                               StartDate = combo.StartDate,
                               EndDate = combo.EndDate,
                               Products = (from tag in _context.ComboProductTags
                                           where (tag.ComboCode == combo.ComboCode && tag.IsDelete == false)
                                           select new ComboProductTagGetPOCO
                                           {
                                               Id = tag.Id,
                                               AfterDiscAmount = tag.AfterDiscAmount,
                                               ProductId = tag.ProductId,
                                               ProductName = _context.Products.FirstOrDefault(s => s.ProductId == tag.ProductId).ProductName,
                                               DiscountAmount = tag.DiscountAmount,
                                               DiscountType = tag.DiscountType,
                                               RegularPrice = tag.RegularPrice
                                           }).ToList()



                           };

                return data;
            }
            catch
            {

                throw new ArgumentNullException();
            }
        }
    }
}
