﻿using RetailSuite.Application.CMS.POCO.Combo;
using RetailSuite.Application.CMS.ViewModels.Products.Combo;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.ComboProductServices
{
    public interface IComboProductRepository
    {
        Task<IEnumerable<dynamic>> GetComboProductAsync();
        Task<IEnumerable<ComboProductPoco>> GetComboProducts(string ComboCode);
        Task<ComboProduct> GetComboProductByIdAsync(int Id);
        Task<dynamic> GetComboById(int Id);
        Task<string> AddComboProduct(ComboContextViewModels contextViewModels,string username);
        Task<bool> SaveChangesAsync();
        void UpdateComboProductTagAsync();
        Task DeleteComboProductAsync(int Id);
        Task<ComboContextViewModels> GetComboProductById(int Id);
        bool DeleteAddedProducts(string ComboCode);

        Task<ComboProductTag> GetComboProductTagById(int Id);
        Task DeleteComboProductTag(int Id);
        Task AddComboProductTag(ComboProductTag comboProductTag);
    }
}
