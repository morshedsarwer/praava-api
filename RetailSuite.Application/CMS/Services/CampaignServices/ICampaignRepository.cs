﻿using RetailSuite.Application.CMS.DTOS.CampaignDTO;
using RetailSuite.Application.CMS.POCO.Campaign;
using RetailSuite.Application.CMS.ViewModels.Campaigns;
using RetailSuite.Domain.CMS.Models.Campaigns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.CampaignServices
{
    public interface ICampaignRepository
    {
        Task<IEnumerable<CampaignPoco>> GetAllCampaignAsync();
        Task<IEnumerable<CampaignProductPoco>> GetCampaignProducts(string campaignCode);
        Task<object> GetCampaignByIdAsync(int Id);
        string AddCampaign(CampaignContextViewModel campaignContextViewModel);
        Task DeleteCampaign(int Id);
        Task<CampaignProducts> GetCampaignProductById(int Id);
        Task DeleteCampaignProduct(int Id);
        Task AddCampaignProduct(CampaignProducts campaignProducts);
    }
}
