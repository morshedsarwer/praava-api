﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CMS.DTOS.CampaignDTO;
using RetailSuite.Application.CMS.POCO.Campaign;
using RetailSuite.Application.CMS.ViewModels.Campaigns;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Campaigns;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.CampaignServices
{
    public class CampaignRepository : ICampaignRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public CampaignRepository(RetailDbContext context, ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;

        }
        public string AddCampaign(CampaignContextViewModel campaignContextViewModel)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    campaignContextViewModel.Campaign.CampaignCode = code;
                    var campaignProducts = new List<CampaignProducts>();
                    var productPrice = new List<ProductPrice>();
                    foreach (var item in campaignContextViewModel.CampaignProducts)
                    {
                        item.CampaignCode = code;
                        var price = new ProductPrice() 
                        {
                            Price=item.DiscPrice,
                            ProductId=item.ProductId,
                            CreatedAt=DateTime.Now,                           

                        };
                        campaignProducts.Add(item);
                        productPrice.Add(price);
                    }
                    _context.Campaigns.Add(campaignContextViewModel.Campaign);
                    _context.SaveChanges();
                    _context.CampaignProducts.AddRange(campaignProducts);
                    _context.SaveChanges();
                    _context.ProductPrices.AddRange(productPrice);
                    _context.SaveChanges();
                    transaction.Commit();
                    return campaignContextViewModel.Campaign.CampaignCode;
                }
                catch
                {
                    transaction.Rollback();
                    return "failed";
                }

            }
        }

        public async Task AddCampaignProduct(CampaignProducts campaignProducts)
        {
            await _context.CampaignProducts.AddAsync(campaignProducts);
            await _saveContext.SaveChangesAsync();
        }

        public async Task DeleteCampaign(int Id)
        {
            var itemRemoved = _context.Campaigns.FirstOrDefault(x=>x.Id == Id);
            if (itemRemoved != null)
            {
               itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();
            }
        }

        public async Task DeleteCampaignProduct(int Id)
        {
            var itemRemoved = await GetCampaignProductById(Id);
            if (itemRemoved != null)
            {
                _context.CampaignProducts.Remove(itemRemoved);
                await _saveContext.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<CampaignPoco>> GetAllCampaignAsync()
        {
            try
            {
                var toDay = DateTime.Now;
                var result = from campaign in _context.Campaigns
                             //where campaign.EndDate <= toDay
                             where campaign.IsDelete==false
                             select new CampaignPoco
                             {
                                 CampaignCode = campaign.CampaignCode,
                                 CampaignName = campaign.CampaignName,
                                 Description = campaign.Description,
                                 EndDate = campaign.EndDate,
                                 StartDate = campaign.StartDate,
                                 Id = campaign.Id,
                                 Products = (from prod in _context.CampaignProducts
                                             join p in _context.Products on prod.ProductId equals p.ProductId
                                             where campaign.CampaignCode == prod.CampaignCode
                                             select new CampaignProductPoco
                                             {
                                                 ProductId = prod.ProductId,
                                                 AtrDiscAmount = prod.DiscPrice,
                                                 DiscType = prod.DiscType,
                                                 DiscAmount = prod.DiscAmount,
                                                 Code = prod.CampaignCode,
                                                 Id = prod.Id,
                                                 ProductName = p.ProductName,
                                                 RegularPrice = prod.RegularPrice,
                                                 Weight = p.Weight,
                                                 Images = (from img in _context.ProductImages
                                                           where p.ProductCode == img.ProductCode
                                                           select new CampaignProductImage
                                                           {
                                                               Url = img.Url
                                                           }).ToList<CampaignProductImage>(),

                                             }).ToList<CampaignProductPoco>()
                             };

                return result;

            }
            catch
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<object> GetCampaignByIdAsync(int Id)
        {
            try
            {
                var campaign = (from c in _context.Campaigns
                                where c.Id == Id
                                select new CampaignPocoWithTime
                                {
                                    Id = c.Id,
                                    CampaignCode = c.CampaignCode,
                                    CampaignName = c.CampaignName,
                                    Description = c.Description,
                                    EndDate = c.EndDate.ToString("yyyy-MM-dd"),
                                    EndTime = c.EndDate.ToString("HH:mm"),
                                    StartDate = c.StartDate.ToString("yyyy-MM-dd"),
                                    StartTime = c.StartDate.ToString("HH:mm"),
                                });
                return campaign;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<CampaignProducts> GetCampaignProductById(int Id)
        {
            try
            {
                var campaignproduct = await _context.CampaignProducts.FirstOrDefaultAsync(s => s.Id == Id);
                return campaignproduct;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<CampaignProductPoco>> GetCampaignProducts(string campaignCode)
        {
            try
            {
                var query = from camp in _context.CampaignProducts
                            join product in _context.Products
                            on camp.ProductId equals product.ProductId
                            where camp.CampaignCode == campaignCode
                            select new CampaignProductPoco()
                            {
                                Id = camp.Id,
                                Code = camp.CampaignCode,
                                ProductName = product.ProductName,
                                DiscType = camp.DiscType,
                                DiscAmount = camp.DiscAmount,
                                RegularPrice = camp.RegularPrice,
                                AtrDiscAmount = camp.DiscPrice,
                                ProductId = camp.ProductId,
                            };
                return query;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
