﻿using RetailSuite.Application.CMS.DTOS.CategoryDTO;
using RetailSuite.Application.CMS.POCO.Categories;
using RetailSuite.Application.CMS.ViewModels.Categories;
using RetailSuite.Domain.CMS.Models.Categories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.CategoryServices
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<ParentCategory>> GetParentCategoriesAsync();
        Task<IEnumerable<POCO.Categories.GrandChildCategory>> GetGChildAndGGchildsAsync(int childId);
        Task<dynamic> AddParentCategory(Category category);
        
        Task<dynamic> AddChildCategory(Category category);
        Task<dynamic> AddGChildCategory(Category category);
        Task<dynamic> AddGGChildCategory(Category category);
        Task<ReadCategory> GetCategoryByIdAsync(int id);
        Task<dynamic> GetChildCategories();
        Task<dynamic> GetGrandChildCategories();
        void UpdateCategoryAsync();
        Task DeleteCategoryAsync(int Id);
        
    }
}
