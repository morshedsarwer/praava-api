﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CMS.DTOS.CategoryDTO;
using RetailSuite.Application.CMS.POCO.Categories;
using RetailSuite.Application.CMS.ViewModels.Categories;
using RetailSuite.Domain.CMS.Models.Categories;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChildCategory = RetailSuite.Application.CMS.POCO.Categories.ChildCategory;
using GrandChildCategory = RetailSuite.Application.CMS.POCO.Categories.GrandChildCategory;

namespace RetailSuite.Application.CMS.Services.CategoryServices
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly RetailDbContext _context;

        public CategoryRepository(RetailDbContext context)
        {
            _context = context;
        }
        public async Task<dynamic> AddParentCategory(Category category)
        {
            try
            {
                var exist = await _context.Cats.AnyAsync(x => x.ParentCat == category.ParentCat);
                if (!exist)
                {
                    var result = await _context.Cats.AddAsync(category);
                    await _context.SaveChangesAsync();
                    var parent = new ParentCategoryDTO
                    {
                        ParentCat = result.Entity.ParentCat
                    };
                    return parent;
                }
                return "parent category already exist";
            }
            catch (Exception)
            {

                throw;
            }

        }
        public async Task<IEnumerable<ParentCategory>> GetParentCategoriesAsync()

        {
            try
            {
                var parentList = new List<ParentCategory>();
                var chilList = new List<ChildCategory>();
                var categories = from p in _context.Cats select p;
                var parents = from p in categories
                              where !string.IsNullOrWhiteSpace(p.ParentCat)
                              select p;
                foreach (var parent in parents)
                {
                    var childs = from p in categories
                                 where !string.IsNullOrWhiteSpace(p.ChildCat) && p.ParentId == parent.Id
                                 select (new ChildCategory
                                 {
                                     Id = p.Id,
                                     ParentId = parent.Id,
                                     ChildCat = p.ChildCat
                                 });
                    parentList.Add(new ParentCategory()
                    {
                        ParentCat = parent.ParentCat,
                        Id = parent.Id,
                        ImageUrl = parent.ImageUrl,
                        Child = childs
                    });
                }
                return parentList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<POCO.Categories.GrandChildCategory>> GetGChildAndGGchildsAsync(int childId)
        {
            try
            {
                var gchildsList = new List<GrandChildCategory>();
                var categories = from p in _context.Cats select p;
                var GChilds = from p in categories
                              where p.ChildCatId == childId
                              select p;

                foreach (var gchild in GChilds)
                {
                    var ggchild = from c in categories
                                  where c.GChildCatId == gchild.Id
                                  select (new GGChildCategory
                                  {
                                      Id = c.Id,
                                      GChildId = c.GChildCatId,
                                      GGChildCat = c.GGChildCat
                                  });

                    gchildsList.Add(
                        new GrandChildCategory
                        {
                            Id = gchild.Id,
                            ChildId = gchild.ChildCatId,
                            GChildCat = gchild.GChildCat,
                            GGChild = ggchild
                        });
                }

                return gchildsList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async void UpdateCategoryAsync()
        {
            
          
        }

        public async Task DeleteCategoryAsync(int Id)
        {
            try
            {
                var itemRemoved = await _context.Cats.FirstOrDefaultAsync(s => s.Id == Id);
                if (itemRemoved != null)
                {
                    itemRemoved.IsDelete = true;
                    await _context.SaveChangesAsync();
                }
            }catch (Exception e)
            {
                throw new ArgumentNullException();
            }
            

        }

        public async Task<dynamic> AddChildCategory(Category category)
        {
            try
            {
                var exists = await _context.Cats.AnyAsync(x => x.ChildCat == category.ChildCat);
                if (!exists)
                {
                    var result = await _context.Cats.AddAsync(category);
                    await _context.SaveChangesAsync();
                    return new ChildCategoryDTO { ParentId = result.Entity.ParentId, ChildCat = result.Entity.ChildCat }; ;
                }
                return "child category already exists";
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<dynamic> AddGChildCategory(Category category)
        {
            try
            {
                var exists = await _context.Cats.AnyAsync(x=>x.GChildCat==category.GChildCat);
                if (!exists)
                {
                    var result = await _context.Cats.AddAsync(category);
                    await _context.SaveChangesAsync();
                    return new GChildCategoryDTO { ChildCatId = result.Entity.ChildCatId, GChildCat = result.Entity.GChildCat }; ;
                }
                return "grand child ctegory already exists";
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> AddGGChildCategory(Category category)
        {
            try
            {
                var result = await _context.Cats.AddAsync(category);
                await _context.SaveChangesAsync();
                return result.Entity;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ReadCategory> GetCategoryByIdAsync(int id)
        {
            var result = await _context.Cats.FirstOrDefaultAsync(x => x.Id == id);
            if (result != null)
            {
                if (!string.IsNullOrWhiteSpace(result.ParentCat))
                    return new ReadCategory { CategoryID = result.Id, CategoryName = result.ParentCat };
                if (!string.IsNullOrWhiteSpace(result.ChildCat))
                    return new ReadCategory { CategoryID = result.Id, CategoryName = result.ChildCat };
                if (!string.IsNullOrWhiteSpace(result.GChildCat))
                    return new ReadCategory { CategoryID = result.Id, CategoryName = result.GChildCat };
                if (!string.IsNullOrWhiteSpace(result.GGChildCat))
                    return new ReadCategory { CategoryID = result.Id, CategoryName = result.GGChildCat };
            }
            return null;

        }

        public async Task<dynamic> GetChildCategories()
        {
            var list = new List<dynamic>();
            var result = from c in _context.Cats
                         where !string.IsNullOrWhiteSpace(c.ChildCat)
                         select (new
                         {
                             ChilId = c.Id,
                             ChildCatName = c.ChildCat
                         });
            foreach (var item in result)
            {
                list.Add(item);
            }
            return new ReadCategoryViewModels { ReadCategories=list};
        }

        public async Task<dynamic> GetGrandChildCategories()
        {
            var list = new List<dynamic>();
            var grandChilds = from c in _context.Cats
                              where !string.IsNullOrWhiteSpace(c.GChildCat)
                              select( new 
                              {
                                  id = c.Id,
                                  GrandChildCatName = c.GChildCat
                              });

            foreach (var item in grandChilds)
            {
                list.Add(item);
            }
            return new ReadCategoryViewModels { ReadCategories = list};
        }
    }
}
