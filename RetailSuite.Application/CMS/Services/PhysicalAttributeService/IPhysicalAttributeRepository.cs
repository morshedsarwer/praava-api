﻿using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.PhysicalAttributeService
{
    public interface IPhysicalAttributeRepository
    {
        Task<IEnumerable<PhysicalAttribute>> GetPhysicalAttributeAsync();
        Task<PhysicalAttribute> GetPhysicalAttributeByIdAsync(int Id);
        Task<PhysicalAttribute> AddPhysicalAttribute(PhysicalAttribute physicalAttribute,string username);
        void UpdatePhysicalAttributeAsync();
        Task DeletePhysicalAttributeAsync(int Id);
    }
}
