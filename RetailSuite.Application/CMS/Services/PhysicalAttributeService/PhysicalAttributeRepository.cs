﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.PhysicalAttributeService
{
    public class PhysicalAttributeRepository : IPhysicalAttributeRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public PhysicalAttributeRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public async Task<PhysicalAttribute> AddPhysicalAttribute(PhysicalAttribute physicalAttribute,string username)
        {
            try
            {
                var user = _saveContext.GetUserFromUserName(username);
                physicalAttribute.CreatedBy = user.Id;
                var output = await _context.PhysicalAttributes.AddAsync(physicalAttribute);
                await _context.SaveChangesAsync();
                return output.Entity;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
            
        }

        public async Task DeletePhysicalAttributeAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetPhysicalAttributeByIdAsync(Id);
                itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }
            

        }

        public async Task<IEnumerable<PhysicalAttribute>> GetPhysicalAttributeAsync()
        {
            try
            {
                var attributes = await _context.PhysicalAttributes.Where(s=>s.IsDelete == false).ToListAsync();
                return attributes;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<PhysicalAttribute> GetPhysicalAttributeByIdAsync(int Id)
        {
            try
            {
                var attribute = await _context.PhysicalAttributes.Where(p=>p.IsDelete == false).FirstOrDefaultAsync(s => s.AttributeId == Id);
                return attribute;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
            
        }

        public void UpdatePhysicalAttributeAsync()
        {
            throw new NotImplementedException();
        }
    }
}
