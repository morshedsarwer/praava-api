﻿using RetailSuite.Application.CMS.ViewModels.Products.Bunch;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Services.BunchProductServices
{
    public interface IBunchProductRepository
    {
        Task<IEnumerable<BunchProduct>> GetBunchProductsAsync();
        Task<BunchProduct> GetBunchProductByIdAsync(int Id);
        void AddBunchProduct(BunchContextViewModels contextViewModels);
        Task<bool> SaveChangesAsync();
        void UpdateBunchProductAsync();
        Task DeleteBunchProductAsync(int Id);
        Task<BunchContextViewModels> GetBunchProductById(int Id);
    }
}
