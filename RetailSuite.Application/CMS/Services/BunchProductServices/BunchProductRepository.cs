﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CMS.ViewModels.Products.Bunch;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Services.BunchProductServices
{
    public class BunchProductRepository : IBunchProductRepository
    {
        private readonly RetailDbContext _context;

        public BunchProductRepository(RetailDbContext context)
        {
            _context = context;
        }        

        public   void AddBunchProduct(BunchContextViewModels contextViewModels )
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    contextViewModels.BunchPrice.BunchProductCode = code;
                    contextViewModels.BunchProduct.BunchProductCode = code;
                    List<BunchProductTag> tags = new List<BunchProductTag>();
                    foreach (var item in contextViewModels.BunchProductTags)
                    {
                        item.BunchProductCode = code;
                        tags.Add(item);
                    }
                    _context.BunchPrices.Add(contextViewModels.BunchPrice);
                    _context.SaveChanges();
                    _context.BunchProducts.Add(contextViewModels.BunchProduct);
                    _context.SaveChanges();
                    _context.BunchProductTags.AddRange(tags);
                    transaction.Commit();
                }
                catch (Exception e)
                {

                    transaction.Rollback();
                }
            }
        }




        public async Task DeleteBunchProductAsync(int Id)
        {
            try
            {
                // Get the item
                var itemRemoved = await GetBunchProductByIdAsync(Id);
                if (itemRemoved != null)
                {
                    itemRemoved.IsDelete = true;
                    await _context.SaveChangesAsync();
                    await SaveChangesAsync();
                }

            }
            catch {

                throw new ArgumentNullException();
            }
        }

        public async Task<IEnumerable<BunchProduct>> GetBunchProductsAsync()
        {
            try
            {
                var bunchProducts = await _context.BunchProducts.Where(s=>s.IsDelete == false).ToListAsync();
                return bunchProducts;
            }
            catch {

                throw new ArgumentNullException();
            }
        }

        public async Task<BunchProduct> GetBunchProductByIdAsync(int Id)
        {
            try
            {
                return await _context.BunchProducts.FirstOrDefaultAsync(s => s.BunchProductId == Id);
            }
            catch {
                throw new ArgumentNullException();

            }
        }

        public async Task<BunchContextViewModels> GetBunchProductById(int Id)
        {
            try
            {
                var p = new BunchContextViewModels();
                p.BunchProduct = await _context.BunchProducts.Where(p=>p.IsDelete==false).FirstOrDefaultAsync(s => s.BunchProductId == Id);
                p.BunchPrice = await _context.BunchPrices.FirstOrDefaultAsync(s => s.BunchProductCode == p.BunchProduct.BunchProductCode);
                p.BunchProductTags = await (_context.BunchProductTags.Where(s => s.BunchProductCode == p.BunchProduct.BunchProductCode)).ToListAsync();
                return p;                
                
            }
            catch {
                throw new ArgumentNullException();
            }            
            
        }


        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync() > 0);
        }

        public void UpdateBunchProductAsync()
        {
           // all work is in the controller
        }
    }
}
