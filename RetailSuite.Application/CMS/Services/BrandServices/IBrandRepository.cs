﻿using RetailSuite.Domain.CMS.Models.Brands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Services.BrandServices
{
    public interface IBrandRepository
    {
        Task<IEnumerable<Brand>> GetBrandsAsync();
        Task<Brand> GetBrandByIdAsync(int Id);
        Task<Brand> AddBrand(Brand brand,string username);
        Task<bool> SaveChangesAsync();
        void UpdateBrandAsync();
        Task DeleteBrandAsync(int Id);

    }
}
