﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Brands;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Services.BrandServices
{
    public class BrandRepository : IBrandRepository
    {
        private RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public BrandRepository(RetailDbContext context, ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;

        }


        public async Task<Brand> AddBrand(Brand brand, string username)
        {
            try
            {
                //var user = _saveContext.GetUserFromUserName(username);

                //brand.CreatedBy = user.Id;
                var result = await _context.Brands.AddAsync(brand);
                await _context.SaveChangesAsync();
                return result.Entity;
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<IEnumerable<Brand>> GetBrandsAsync()
        {
            try
            {
                var brands = await _context.Brands.Where(s => s.IsDelete == false).ToListAsync();
                return brands;
            }
            catch
            {
                throw new ArgumentNullException();
            }

        }

        public async Task<Brand> GetBrandByIdAsync(int Id)
        {
            return await _context.Brands.Where(p => p.IsDelete == false).FirstOrDefaultAsync(s => s.BrandId == Id);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync() > 0);
        }

        public void UpdateBrandAsync()
        {
            throw new NotImplementedException();
        }

        public async Task DeleteBrandAsync(int Id)
        {
            try
            {
                // Get the Item
                var itemRemoved = await GetBrandByIdAsync(Id);

                if (itemRemoved != null)
                {
                    //_context.Brands.Remove(itemRemoved);
                    itemRemoved.IsDelete = true;
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

                throw new ArgumentNullException();
            }
        }
    }
}
