﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.ProductShadeService
{
    public class ProductShadeRepository : IProductShadeRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public ProductShadeRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public async Task<ProductShade> AddProductShade(ProductShade productShade,string username)
        {
            try
            {
                var user = _saveContext.GetUserFromUserName(username);
                productShade.CreatedBy = user.Id;
                var output = await _context.ProductShades.AddAsync(productShade);
                await _context.SaveChangesAsync();
                return output.Entity;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public async Task DeleteProductShadeAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetProductShadeByIdAsync(Id);
                itemRemoved.IsDelete = true;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<IEnumerable<ProductShade>> GetProductShadeAsync()
        {
            try
            {
                var products = await _context.ProductShades.Where(p=>p.IsDelete == false).ToListAsync();
                return products;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<ProductShade> GetProductShadeByIdAsync(int Id)
        {
            try
            {
                var product = await _context.ProductShades.Where(p=>p.IsDelete == false).FirstOrDefaultAsync(s => s.ProductShadeId == Id);
                return product;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public void UpdateProductShadeAsync()
        {
            throw new NotImplementedException();
        }
    }
}

