﻿using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.ProductShadeService
{
    public interface IProductShadeRepository
    {
        Task<IEnumerable<ProductShade>> GetProductShadeAsync();
        Task<ProductShade> GetProductShadeByIdAsync(int Id);
        Task<ProductShade> AddProductShade(ProductShade productShade,string usernamae);
        void UpdateProductShadeAsync();
        Task DeleteProductShadeAsync(int Id);

    }
}
