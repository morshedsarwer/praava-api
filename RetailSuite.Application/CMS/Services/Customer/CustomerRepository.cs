﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CMS.DTOS.CustomersDTO.WishListDTO;
using RetailSuite.Application.CMS.POCO.Customer;
using RetailSuite.Application.CMS.POCO.Product;
using RetailSuite.Domain.CMS.Models.Customer;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.Customer
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly RetailDbContext _context;

        public CustomerRepository(RetailDbContext context)
        {
            _context = context;
        }

        public async Task<bool> AddCart(Cart products)
        {
            try
            {
                var cart = _context.Carts.FirstOrDefault(x => x.CustomerId == products.CustomerId);
                if (cart != null)
                {
                    if (cart.ProductId == products.ProductId)
                    {
                        return false;
                    }
                    await _context.Carts.AddAsync(products);
                    await _context.SaveChangesAsync();
                    return true;
                }
                else
                {
                    await _context.Carts.AddAsync(products);
                    await _context.SaveChangesAsync();
                    return true;
                }
            }
            catch (System.Exception)
            {
                throw new NotImplementedException();
            }
        }

        public async Task<bool> AddWishList(WishListProducts products)
        {
            try
            {
                var wish = _context.WishListProducts.FirstOrDefault(x => x.CustomerId == products.CustomerId);
                if (wish != null)
                {
                    if (wish.ProductId == products.ProductId)
                    {
                        return false;
                    }
                    await _context.WishListProducts.AddAsync(products);
                    await _context.SaveChangesAsync();
                    return true;
                }
                else
                {
                    await _context.WishListProducts.AddAsync(products);
                    await _context.SaveChangesAsync();
                    return true;
                }
            }
            catch (Exception)
            {
                throw new NotImplementedException();
            }
        }

        public async Task<bool> DeleteCart(int id)
        {
            try
            {
                var cart = await _context.Carts.FindAsync(id);
                if (cart != null)
                {
                    _context.Carts.Remove(cart);
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (System.Exception)
            {

                throw new NotImplementedException();
            }
        }

        public async Task<bool> DeleteishList(int id)
        {
            try
            {
                var wish = _context.WishListProducts.FirstOrDefault(X => X.Id == id);
                if (wish != null)
                {
                    _context.WishListProducts.Remove(wish);
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                throw new NotImplementedException();
            }
        }

        public async Task<IEnumerable<CartPoco>> GetCarts(string customerId)
        {
            var carts = new List<CartPoco>();
            try
            {
                using (var con = _context.Database.GetDbConnection())
                {

                    if (con.State != System.Data.ConnectionState.Open)
                        con.Open();
                    var command = con.CreateCommand();
                    command.CommandText = "get_cart_by_customer @customerId";
                    command.Parameters.Add(new SqlParameter("@customerId", customerId));
                    //command.Parameters.Add()
                    using (var result = await command.ExecuteReaderAsync())
                    {
                        if (result.HasRows)
                        {
                            while (await result.ReadAsync())
                            {
                                carts.Add(new CartPoco()
                                {
                                    Id = Convert.ToInt32(result[0]),
                                    ProductName = Convert.ToString(result[1]),
                                    Date = Convert.ToString(result[2]),
                                    SKU = Convert.ToString(result[3]),
                                    Price = Convert.ToDecimal(result[4]),
                                    Quantity = Convert.ToInt32(result[5]),
                                    Total = Convert.ToDecimal(result[6])
                                });
                            }
                        }
                    }
                    con.Close();
                }
                return carts;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<IEnumerable<WishListPoco>> GetWishList(string customerId)
        {
            try
            {
                var result = from w in _context.WishListProducts
                             join p in _context.Products
                             on w.ProductId equals p.ProductId
                             where w.CustomerId == customerId && w.IsDelete == false
                             select new WishListPoco
                             {
                                 ProductId = w.ProductId,
                                 ProductName = p.ProductName,
                                 Created = p.CreatedAt,
                                 SKU = p.productsku,
                                 //Price=(from p in _context.ProductPrices where p.ProductId==w.ProductId select p.Price).Max(),
                                 Price = (from p in _context.ProductPrices where (p.CreatedAt == (from pr in _context.ProductPrices where pr.ProductId == w.ProductId select pr.CreatedAt).Max()) select p.Price).Max(),
                                 Id = w.Id,
                                 Images = (from img in _context.ProductImages
                                           where img.ProductCode == p.ProductCode
                                           select new ProductImagePoco
                                           {
                                               Id = img.ProductImageId,
                                               ImageName = img.ImageName,
                                               Url = img.Url,
                                           }).ToList<ProductImagePoco>(),

                             };
                /*using (var con = _context.Database.GetDbConnection())
                {
                    if (con.State != System.Data.ConnectionState.Open)
                        con.Open();
                    var command = con.CreateCommand();
                    command.CommandText = "get_Wishlist_by_customer @customerId";
                    command.Parameters.Add(new SqlParameter("@customerId",customerId));
                    using (var result = await command.ExecuteReaderAsync())
                    {
                        if (result.HasRows)
                        {
                            while (await result.ReadAsync())
                            {
                                wish.Add(new WishListPoco()
                                {
                                    Id = Convert.ToInt32(result[0]),
                                    ProductName = Convert.ToString(result[1]),
                                    Created = Convert.ToString(result[2]),
                                    SKU = Convert.ToString(result[3])
                                });
                            }
                        }
                    }
                    con.Close();
                }*/

                return result;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<bool> UpdateCart(WishListCartProductsDTO cart, int Id)
        {
            try
            {
                var data = _context.Carts.FirstOrDefault(c => c.Id == Id);
                if (data != null)
                {
                    data.Quantity = cart.Quantity;
                    data.Price = (from p in _context.ProductPrices where (p.CreatedAt == (from pr in _context.ProductPrices where pr.ProductId == data.ProductId select pr.CreatedAt).Max()) select p.Price).Max();
                    data.CreatedAt = DateTime.Now;
                    _context.SaveChanges();
                    return true;

                }
                return false;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }

}
