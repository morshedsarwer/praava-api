﻿using RetailSuite.Application.CMS.DTOS.CustomersDTO.WishListDTO;
using RetailSuite.Application.CMS.POCO.Customer;
using RetailSuite.Domain.CMS.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Services.Customer
{
    public interface ICustomerRepository
    {
        Task<bool> AddWishList(WishListProducts products);
        Task<bool> AddCart(Cart products);
        Task<bool> UpdateCart(WishListCartProductsDTO cart,int Id);
        Task<bool> DeleteishList(int id);
        Task<bool> DeleteCart(int id);
        Task<IEnumerable<WishListPoco>> GetWishList(string customerId);
        Task<IEnumerable<CartPoco>> GetCarts(string customerId);
    }
}
