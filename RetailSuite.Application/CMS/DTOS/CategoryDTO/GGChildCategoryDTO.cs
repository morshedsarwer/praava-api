﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.CategoryDTO
{
    public class GGChildCategoryDTO
    {
        public int GChildCatId { get; set; }
        public string GGChildCat { get; set; }
    }
}
