﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.CategoryDTO
{
    public class ParentCategoryDTO
    {
        //[Required]
        public string ParentCat { get; set; }
    }
}
