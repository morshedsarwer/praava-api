﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.CategoryDTO
{
    public class ChildCategoryDTO
    {
        public int ParentId { get; set; }
        public string ChildCat { get; set; }
    }
}
