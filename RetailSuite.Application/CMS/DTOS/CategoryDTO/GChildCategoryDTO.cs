﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.CategoryDTO
{
    public class GChildCategoryDTO
    {
        public int ChildCatId { get; set; }
        public string GChildCat { get; set; }
    }
}
