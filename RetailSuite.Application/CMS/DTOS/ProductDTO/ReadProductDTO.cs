﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductDTO
{
    public class ReadProductDTO
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string productsku { get; set; }
        public string ProductCode { get; set; }
        public string Weight { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public string ProductDimension { get; set; }
        //public string ProductCode { get; set; }
        public int BrandId { get; set; }
        public bool IsPreOrder { get; set; }
        //public bool IsmarketPlace { get; set; }
        public bool IsPublish { get; set; }
        public bool IsHasFreeProd { get; set; }
        public int DelivaryDayPreorder { get; set; }
        //public int ProductPoints { get; set; }
        //public int ProductShadeId { get; set; }
        public string Description { get; set; }
    }
}
