﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductDTO
{
    public class ReadProductCategoriesDTO
    {
        public string ProductCode { get; set; }
        public int ParentId { get; set; }
        public int ChildIdId { get; set; }
    }
}
