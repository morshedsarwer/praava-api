﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductDTO
{
    public class UpdateProductDTO
    {
        
        public string ProductName { get; set; }
        public string Weight { get; set; }
        public string ProductDimension { get; set; }
        public string productsku { get; set; }
        public int BrandId { get; set; }
        public bool IsPreOrder { get; set; }
        public int DelivaryDayPreorder { get; set; }
        
        public string Descriotion { get; set; }
    }
}
