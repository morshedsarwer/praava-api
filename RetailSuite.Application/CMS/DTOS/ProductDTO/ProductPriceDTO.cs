﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductDTO
{
    public class ProductPriceDTO
    {
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public bool IsPublish { get; set; }
        public bool IsNewArrival { get; set; }
        public int ProductPoints { get; set; }
    }
}
