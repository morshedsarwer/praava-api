﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductDTO
{
    public class CreateProductDTO
    {
        
        [ForeignKey("ParentProduct")]
        public string ProductName { get; set; }
        //public string ProductCode { get; set; }
        //public int ParentProductId { get; set; }
        //public ParentProduct ParentProduct { get; set; }
        //[Column(TypeName = "decimal(18,2)")]
        public string Weight { get; set; }
        public string productsku { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public string ProductDimension { get; set; }
        //public Vendor Vendor { get; set; }
        [ForeignKey("Brand")]
        public int BrandId { get; set; }
        public bool IsPreOrder { get; set; }
        //public bool IsmarketPlace { get; set; }
        public bool IsPublish { get; set; }
        public bool IsHasFreeProd { get; set; }
        //public bool IsNewArrival { get; set; }
        /* if IsPreOrder is checked then Delivery day will show and set days */
        public int DelivaryDayPreorder { get; set; }
        //public int ProductPoints { get; set; }
        //[ForeignKey("ProductShade")]
        //public int ProductShadeId { get; set; }
        public string Description { get; set; }
    }
}
