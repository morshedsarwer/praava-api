﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductDTO
{
    public class CreateImageDTO
    {
        public string ImageName { get; set; }
        public string AltImageName { get; set; }
        public string Url { get; set; }
    }
}
