﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductDTO
{
    public class CreateProdMetaTag
    {
        public string MetaTag { get; set; }
    }
}
