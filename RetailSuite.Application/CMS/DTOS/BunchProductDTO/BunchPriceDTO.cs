﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.BunchProductDTO
{
    public class BunchPriceDTO
    {
        public decimal TotalRegularPrice { get; set; }
        public decimal TotalBunchPrice { get; set; }
    }
}
