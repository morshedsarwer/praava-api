﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.BunchProductDTO
{
    public class BunchProductTagDTO
    {
        public int ProductId { get; set; }
        public decimal RegularPrice { get; set; }
        public decimal SellingPrice { get; set; }
    }
}
