﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductCategoriesDTO
{
    public class UpdateProductCategoriesDTO
    {
        public int ParentCatId { get; set; }
        public int ChildCatId { get; set; }
    }
}
