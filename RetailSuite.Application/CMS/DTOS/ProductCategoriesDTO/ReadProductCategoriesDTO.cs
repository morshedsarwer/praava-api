﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductCategoriesDTO
{
    public class ReadProductCategoriesDTO
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
    }
}
