﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.PhysicalAttributeDTO
{
    public class ReadPhysicalAttributeDTO
    {
        public int AttributeId { get; set; }
        public string Type { get; set; }
    }
}
