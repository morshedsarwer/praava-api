﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.PhysicalAttributeDTO
{
    public class CreatePhysicalAttributeDTO
    {
        public string Type { get; set; }
    }
}
