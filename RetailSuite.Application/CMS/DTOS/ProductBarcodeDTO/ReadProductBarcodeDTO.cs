﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductBarcodeDTO
{
    public class ReadProductBarcodeDTO
    {
        public int ProductBarCodeId { get; set; }
        public string Barcode { get; set; }
        public int ProductId { get; set; }
    }
}
