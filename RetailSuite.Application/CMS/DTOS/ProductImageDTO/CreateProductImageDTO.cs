﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductImageDTO
{
    public class CreateProductImageDTO
    {
        
        //public string Url { get; set; }
        //public string ImageName { get; set; }
        public string AltImageName { get; set; }
        public string ProductCode { get; set; }
        public IFormFile ImageFile { get; set; }
    }
}
