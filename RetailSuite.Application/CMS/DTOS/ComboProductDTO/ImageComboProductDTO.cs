﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ComboProductDTO
{
    public class ImageComboProductDTO
    {
        //public string ComboProductName { get; set; }
        //public int ProductId { get; set; }
 
        //[Column(TypeName = "decimal(18,2)")]
        //public decimal RegularPrice { get; set; }
        //[Column(TypeName = "decimal(18,2)")]
        //public Decimal ComboPrice { get; set; }
        //public string Description { get; set; }

        [NotMapped]
        public IFormFile ImageFile { get; set; }
    }
}
