﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ComboProductDTO
{
    public class UpdateComboProductDTO
    {
        public string ComboProductName { get; set; }
        public int ProductId { get; set; }
 
        [Column(TypeName = "decimal(18,2)")]
        public decimal RegularPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal ComboPrice { get; set; }
        public string Description { get; set; }
        public bool IsApproved { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
