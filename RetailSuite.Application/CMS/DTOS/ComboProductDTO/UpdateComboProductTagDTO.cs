﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ComboProductDTO
{
    public class UpdateComboProductTagDTO
    {
        public int ProductId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal DiscountAmount { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal RegularPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal AfterDiscAmount { get; set; }
        //public DiscountType discountType { get; set; }
        public string DiscountType { get; set; }
    }
}
