﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductAttributeDTO
{
    public class UpdateProductAttributeDTO
    {
        public int AttributeId { get; set; }
    }
}
