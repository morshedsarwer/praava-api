﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductPriceDTO
{
    public class UpdateProductPriceDTO
    {

        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }
        public bool IsApproved { get; set; }

    }
}
