﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.BunchProductTagDTO
{
    public class UpdateBunchProductTagDTO
    {

        public int ProductId { get; set; }
        public int RegularPriceId { get; set; }
        public int SellingPriceId { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public Decimal BunchPrice { get; set; }
        public int BunchProductId { get; set; }
    }
}
