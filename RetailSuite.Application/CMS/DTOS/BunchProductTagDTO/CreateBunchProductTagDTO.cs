﻿using RetailSuite.Application.CMS.DTOS.ProductDTOs;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Domain.CMS.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.BunchProductTagDTO
{
    public class CreateBunchProductTagDTO
    {

        public List<ProductViewModels> Products { get; set; }
        public int RegularPriceId { get; set; }
        //public RegularPrice RegularPrice { get; set; }
        public int SellingPriceId { get; set; }
        //public SellingPrice SellingPrice { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public Decimal BunchPrice { get; set; }
        public int BunchProductId { get; set; }

    }
}
