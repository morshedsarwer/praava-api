﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.DTOS.BrandDTO
{
    public class CreateBrandDTO
    {
        
        public string Name { get; set; }
        public string Description { get; set; }
        
    }
}
