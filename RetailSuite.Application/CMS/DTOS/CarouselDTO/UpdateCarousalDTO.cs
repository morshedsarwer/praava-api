﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.CarouselDTO
{
    public class UpdateCarousalDTO
    {
        public string Name { get; set; }
        public string Url { get; set; }
        //public string CarouselCode { get; set; }
        //public string? ImageName { get; set; }
        //public string? ImageUrl { get; set; }
        public string Description { get; set; }
        public bool IsPublished { get; set; }

        [NotMapped]
        public IFormFile ImageFile { get; set; }
    }
}
