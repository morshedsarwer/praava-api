﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.CarouselDTO
{
    public class ReadCarouselDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string CarouselCode { get; set; }
        public string ImageName { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public bool IsPublished { get; set; }
    }
}
