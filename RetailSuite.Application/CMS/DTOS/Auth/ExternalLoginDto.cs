﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.Auth
{
    public class ExternalLoginDto
    {
        public string Provider { get; set; }
        public string Token { get; set; }
        public string? Name { get; set; }
    }
}
