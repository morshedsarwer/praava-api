﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.CampaignDTO
{
    public class ReadCampaignDTO
    {
        public int Id { get; set; }
        public string CampaignName { get; set; }
        public string CampaignCode { get; set; }
        //public string CampaignCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
    }
}
