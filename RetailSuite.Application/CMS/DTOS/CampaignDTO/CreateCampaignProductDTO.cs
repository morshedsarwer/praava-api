﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.CampaignDTO
{
    public class CreateCampaignProductDTO
    {
        public int ProductId { get; set; }
        public string CampaignCode { get; set; }
        public string DiscType { get; set; }
        public int DiscAmount { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal RegularPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal DiscPrice { get; set; }
    }
}
