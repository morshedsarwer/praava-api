﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductShadeDTO
{
    public class CreateProductShadeDTO
    {
        public string ColorCode { get; set; }
        public string ShadeName { get; set; }

    }
}
