﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.FreeProductDTO
{
    public class UpdateFreeProductDTO
    {
        public string Name { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal Price { get; set; }
        public int NumbOfFreeProd { get; set; }
        public string ImgUrl { get; set; }
        public string Description { get; set; }
    }
}
