﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.ProductMetaTagDTO
{
    public class ReadProductMetaTagDTO
    {
        public int Id { get; set; }
        public string MetaTag { get; set; }
    }
}
