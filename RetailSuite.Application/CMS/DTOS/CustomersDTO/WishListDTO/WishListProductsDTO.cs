﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.CustomersDTO.WishListDTO
{
    public class WishListCartProductsDTO
    {
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public int Quantity  { get; set; }
    }
}
