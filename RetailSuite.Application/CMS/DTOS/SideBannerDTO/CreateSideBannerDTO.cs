﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.SideBannerDTO
{
    public class CreateSideBannerDTO
    {

        public string Link { get; set; }
        public string Position { get; set; }
        
        [NotMapped]
        public IFormFile ImageFile { get; set; }
    }
}
