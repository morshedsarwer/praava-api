﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.DTOS.SideBannerDTO
{
    public class ReadSideBannerDTO
    {
        public int Id { get; set; }
        public string? ImageName { get; set; }
        public string? ImageUrl { get; set; }
        public string Link { get; set; }
        public string Position { get; set; }
    }
}
