﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.ProductShadeDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class ProductShadeProfile : Profile
    {
        public ProductShadeProfile()
        {
            CreateMap<ProductShade, ReadProductShadeDTO>();
            CreateMap<CreateProductShadeDTO, ProductShade>();
            CreateMap<UpdateProductShadeDTDO, ProductShade>();
        }

    }
}
