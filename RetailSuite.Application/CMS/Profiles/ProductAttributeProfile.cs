﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.ProductAttributeDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class ProductAttributeProfile:Profile
    {
        public ProductAttributeProfile()
        {
            CreateMap<ProductAttribute, ReadProductAttributeDTO>();
            CreateMap<CreateProductAttributeDTO, ProductAttribute>();
            CreateMap<UpdateProductAttributeDTO, ProductAttribute>();
        }
    }
}
