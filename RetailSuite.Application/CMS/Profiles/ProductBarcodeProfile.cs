﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.ProductBarcodeDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class ProductBarcodeProfile:Profile
    {
        public ProductBarcodeProfile()
        {
            CreateMap<ProductBarcode, ReadProductBarcodeDTO>();
            CreateMap<CreateProductBarcodeDTO, ProductBarcode>();
            CreateMap<UpdateProductBarcodeDTO, ProductBarcode>();
        }

    }
}
