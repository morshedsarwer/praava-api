﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.CategoryDTO;
using RetailSuite.Domain.CMS.Models.Categories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class CategoryProfile:Profile
    {
        public CategoryProfile()
        {
            CreateMap<ParentCategoryDTO, Category>();
            CreateMap<ChildCategoryDTO, Category>();
            CreateMap<GChildCategoryDTO, Category>();
            CreateMap<GGChildCategoryDTO, Category>();
            CreateMap<Category, ParentCategoryDTO>();
        }
    }
}
