﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.ProductDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, ReadProductDTO>();
            CreateMap<CreateProductDTO, Product>();
            CreateMap<CreateImageDTO, ProductImage>();
            CreateMap<CreateProductBarcodeDTO, ProductBarcode>();
            CreateMap<CreateFreeProduct, FreeProduct>();
            CreateMap<CreateProdMetaTag, ProductMetaTag>();
            CreateMap<UpdateProductDTO, Product>();
            CreateMap<AttributeDTO, ProductAttribute>();
            CreateMap<ProductPriceDTO, ProductPrice>();
            CreateMap<CreateProductBarcodeDTO, ProductBarcode>();
        }
    }
}
