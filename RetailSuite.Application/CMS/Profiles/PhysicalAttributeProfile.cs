﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.PhysicalAttributeDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class PhysicalAttributeProfile:Profile
    {
        public PhysicalAttributeProfile()
        {
            CreateMap<PhysicalAttribute, ReadPhysicalAttributeDTO>();
            CreateMap<CreatePhysicalAttributeDTO, PhysicalAttribute>();
            CreateMap<UpdatePhysicalAttributeDTO, PhysicalAttribute>();
        }
    }
}
