﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.ComboProductDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class ComboProductProfile:Profile
    {
        public ComboProductProfile()
        {
            CreateMap<ComboProduct, ReadComboProductDTO>();
            CreateMap<CreateComboProductDTO, ComboProduct>();
            CreateMap<ImageComboProductDTO, ComboProduct>();
            CreateMap<UpdateComboProductDTO, ComboProduct>();
            CreateMap<ComboProductTagDTO, ComboProductTag>();
            CreateMap<CreateComboProductTagDTO, ComboProductTag>();
            CreateMap<ComboProduct, CreateComboProductDTO>();
            CreateMap<ComboProductTag, ComboProductTagDTO>();
            CreateMap<ComboProductTag, UpdateComboProductTagDTO>();
            CreateMap<UpdateComboProductTagDTO, ComboProductTag>();
            CreateMap<AddSingleComboProductTag, ComboProductTag>();

        }

    }
}
