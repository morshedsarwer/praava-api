﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.ProductPriceDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class ProductPriceProfile : Profile
    {
        public ProductPriceProfile()
        {
            CreateMap<ProductPrice, ReadProductPriceDTO>();
            CreateMap<CreateProductPriceDTO, ProductPrice>();
            CreateMap<UpdateProductPriceDTO, ProductPrice>();
        }
    }
}
