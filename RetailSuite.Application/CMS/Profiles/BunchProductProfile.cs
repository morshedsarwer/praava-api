﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.BunchProductDTO;
using RetailSuite.Application.DTOS.BunchProductDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Profiles
{
    public class BunchProductProfile:Profile
    {
        public BunchProductProfile()
        {
            CreateMap<BunchProduct, ReadBunchProductDTO>();
            CreateMap<CreateBunchProductDTO, BunchProduct>();
            CreateMap<UpdateBunchProductDTO, BunchProduct>();
            CreateMap<BunchPriceDTO, BunchPrice>();
            CreateMap<BunchProductTagDTO, BunchProductTag>();
        }
    }
}
