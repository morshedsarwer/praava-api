﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.CarouselDTO;
using RetailSuite.Domain.CMS.Models.Carousel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class CarouselProfile : Profile
    {
        public CarouselProfile()
        {
            CreateMap<Carousel, ReadCarouselDTO>();
            CreateMap<CreateCarouselDTO, Carousel>();
            CreateMap<UpdateCarousalDTO, Carousel>();
        }
    }
}
