﻿using AutoMapper;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RetailSuite.Application.CMS.DTOS.BunchProductTagDTO;

namespace RetailSuite.Application.CMS.Profiles
{
    public class BunchProductTagProfile:Profile
    {
        public BunchProductTagProfile()
        {
            CreateMap<BunchProductTag, ReadBunchProductTagDTO>();
            CreateMap<CreateBunchProductTagDTO, BunchProductTag>();
            CreateMap<UpdateBunchProductTagDTO, BunchProductTag>();
        }

    }
}
