﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.SideBannerDTO;
using RetailSuite.Domain.CMS.Models.SideBanner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class SideBannerProfile : Profile
    {
        public SideBannerProfile()
        {
            CreateMap<SideBanner, ReadSideBannerDTO>();
            CreateMap<CreateSideBannerDTO, SideBanner>();
        }

    }
}
