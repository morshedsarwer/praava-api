﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.ProductMetaTagDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class ProductMetaTagProfile : Profile
    {
        public ProductMetaTagProfile()
        {
            CreateMap<ProductMetaTag, ReadProductMetaTagDTO>();
            CreateMap<CreateProductMetaTagDTO, ProductMetaTag>();
            CreateMap<UpdateProductMetaTagDTO, ProductMetaTag>();
        }
    }
}
