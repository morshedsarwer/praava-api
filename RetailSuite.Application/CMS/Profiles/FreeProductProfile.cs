﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.FreeProductDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class FreeProductProfile : Profile
    {
        public FreeProductProfile()
        {
            CreateMap<FreeProduct, ReadFreeProductDTO>();
            CreateMap<CreateFreeProductDTO, FreeProduct>();
            CreateMap<UpdateFreeProductDTO, FreeProduct>();
        }
    }
}
