﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.CampaignDTO;
using RetailSuite.Domain.CMS.Models.Campaigns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class CampaignProfile : Profile
    {
        public CampaignProfile()
        {
            CreateMap<Campaign, ReadCampaignDTO>();
            CreateMap<CreateCampaignDTO, Campaign>();
            CreateMap<UpdateCampaignDTO, Campaign>();
            CreateMap<CampaignProductsDTO, CampaignProducts>();
            CreateMap<CreateCampaignProductDTO, CampaignProducts>();
        }
    }
}
