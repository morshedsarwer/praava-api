﻿using AutoMapper;
using RetailSuite.Application.CMS.DTOS.ProductCategoriesDTO;
using RetailSuite.Domain.CMS.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CMS.Profiles
{
    public class ProductCategoriesProfile:Profile
    {
        public ProductCategoriesProfile()
        {
            CreateMap<ProductCategories, ReadProductCategoriesDTO>();
            CreateMap<CreateProductCategoriesDTO, ProductCategories>();
            CreateMap<UpdateProductCategoriesDTO, ProductCategories>();
        }
    }
}
