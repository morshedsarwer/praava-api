﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using RetailSuite.Application.DTOS;
using RetailSuite.Application.DTOS.BrandDTO;
using RetailSuite.Domain.CMS.Models.Brands;

namespace RetailSuite.Application.Profiles;
    public class BrandProfile : Profile
    {
    public BrandProfile()
    {
        CreateMap<Brand,ReadBrandDTO>();
        CreateMap<Brand, ReadBrandByIdDTO>();
        CreateMap<CreateBrandDTO, Brand>();
        CreateMap<UpdateBrandDTO, Brand>();
        CreateMap<ReadBrandByIdDTO, Brand>();

    }

    }

