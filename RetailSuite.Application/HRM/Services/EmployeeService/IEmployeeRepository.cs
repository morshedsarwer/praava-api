﻿using RetailSuite.Domain.HRM.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.HRM.Services.EmployeeService
{
    public interface IEmployeeRepository
    {
        Task<IEnumerable<Employee>> GetEmployeesAsync();
        Task<Employee> GetEmployeeByIdAsync(int Id);
        Task<Employee> AddEmployee(Employee employee,string username);
        void UpdateEmployeeAsync();
        Task DeleteEmployeeAsync(int Id);
    }
}
