﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.HRM.Employee;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.HRM.Services.EmployeeService
{
    public class EmployeeRepositoey : IEmployeeRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public EmployeeRepositoey(RetailDbContext context,ISaveContexts saveContext)
        {
            _context = context;
            _saveContext = saveContext;
        }
        public async Task<Employee> AddEmployee(Employee employee,string username)
        {
            var user = _saveContext.GetUserFromUserName(username);
            employee.CreatedBy = user.Id;
            var output = await _context.Employees.AddAsync(employee);
            await _context.SaveChangesAsync();
            return output.Entity;

        }

        public async Task DeleteEmployeeAsync(int Id)
        {
            var itemDeleted = await GetEmployeeByIdAsync(Id);
            itemDeleted.IsDelete = true;
            await _context.SaveChangesAsync();
        }

        public async Task<Employee> GetEmployeeByIdAsync(int Id)
        {
            var employee = await _context.Employees.Where(p=>p.IsDelete == false).FirstOrDefaultAsync(s => s.EmployeeId == Id);
            return employee;

        }

        public async Task<IEnumerable<Employee>> GetEmployeesAsync()
        {
            var employees = await _context.Employees.Where(p=>p.IsDelete == false).ToListAsync();
            return employees;
        }

        public void UpdateEmployeeAsync()
        {
            // NOT IMPLEMENTED
        }
    }
}
