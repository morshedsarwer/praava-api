﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.HRM.DTOS.EmployeeDTO
{
    public class UpdateEmployeeDTO
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }
}
