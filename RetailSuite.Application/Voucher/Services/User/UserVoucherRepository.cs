﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.Voucher.CommonVEntity;
using RetailSuite.Domain.Voucher.User;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RetailSuite.Application.Voucher.Services.User
{
    public class UserVoucherRepository : IUserVoucherRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public UserVoucherRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public void AddUserVoucher(UserVoucher userVoucher,string UVCode,string username)
        {
            try
            {
                using (var transaction = _context.Database.BeginTransaction()) 
                {
                    try
                    {

                        // Add to the uservoucher code
                        var user = _saveContext.GetUserFromUserName(username);
                        var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                        userVoucher.Code = code;
                        userVoucher.UVCode = UVCode;
                        _context.UserVouchers.Add(userVoucher);
                        _context.SaveChanges();

                        // Add a central code
                        var voucherCode = new VoucherCode { Code = code, VCode = UVCode,customerId = user.Id };
                        _context.VoucherCodes.Add(voucherCode);
                        _context.SaveChanges();
                        transaction.Commit();

                    }
                    catch (Exception ex) 
                    {
                        transaction.Rollback();
                    }
                }

            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<dynamic> CheckVoucher(string UVCode)
        {
            var isvoucher = await _context.UserVouchers.AnyAsync(s => s.UVCode == UVCode && !string.IsNullOrWhiteSpace(s.ApprovedBy));
            if (isvoucher)
            {
                var voucher = await _context.UserVouchers.FirstOrDefaultAsync(s => s.UVCode == UVCode);
                if ((voucher.NumOfVoucher - voucher.NumberOfUsedVoucher) > 0) 
                {
                    var currentDate = DateTime.Now;
                    var start = voucher.StartDate;
                    var end = voucher.EndDate;
                    if ((currentDate >= start) && (currentDate <= end))
                    {
                        return new { minCartAmmount = voucher.MinCartAmount, discountType = voucher.VoucherDiscType, PercentageOrAmount = voucher.DiscAmntOrPercent, UptoDiscountAmount = voucher.UpToDiscAmnt, VoucherCode = voucher.UVCode };
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async Task DeleteUserVoucher(int Id)
        {
            try
            {
                var itemRemoved = await GetUserVoucherByIdAsync(Id);
                _context.UserVouchers.Remove(itemRemoved);
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<IEnumerable<UserVoucher>> GetUserVoucherAsync()
        {
            try {
                var uservouchers = await _context.UserVouchers.ToListAsync();
                return uservouchers;
            }catch (Exception ex)
            {
                throw new ArgumentNullException();
            }
        }

        public async Task<UserVoucher> GetUserVoucherByIdAsync(int Id)
        {
            var uservoucher = await _context.UserVouchers.FirstOrDefaultAsync(s => s.UserVId == Id);
            return uservoucher;
        }

        public void UpdateUserVoucherAsync()
        {
           // NOT IMPLEMENTED
        }
    }
}
