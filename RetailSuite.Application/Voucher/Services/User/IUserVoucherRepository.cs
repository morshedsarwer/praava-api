﻿using RetailSuite.Domain.Voucher.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.User
{
    public interface IUserVoucherRepository
    {
        Task<IEnumerable<UserVoucher>> GetUserVoucherAsync();
        Task<UserVoucher> GetUserVoucherByIdAsync(int Id);
        void AddUserVoucher(UserVoucher userVoucher,string UVCode,string username);
        void UpdateUserVoucherAsync();
        Task DeleteUserVoucher(int Id);
        Task<dynamic> CheckVoucher(string UVCode);
    }
}
