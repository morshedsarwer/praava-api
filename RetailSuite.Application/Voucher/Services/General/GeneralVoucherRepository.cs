﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Voucher.POCO;
using RetailSuite.Domain.Voucher.CommonVEntity;
using RetailSuite.Domain.Voucher.General;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.General
{
    public class GeneralVoucherRepository : IGeneralVoucherRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public GeneralVoucherRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;

        }
        public void AddGeneralVoucher(GeneralVoucher generalVoucher, string GVCode,string username)
        {
            try
            {
                //_context.GeneralVouchers.Add(generalVoucher);
                using (var transaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        // get the user from username
                        var user = _saveContext.GetUserFromUserName(username);
                        var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                        generalVoucher.Code = code;
                        generalVoucher.GVCode = GVCode;
                        generalVoucher.ApprovedBy = "nafis123";
                        _context.GeneralVouchers.Add(generalVoucher);
                        _context.SaveChanges();

                        // adding in the central Voucher data store
                        var voucherCode = new VoucherCode { Code = code, VCode = GVCode,customerId = user.Id };
                        _context.VoucherCodes.Add(voucherCode);
                        _context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }

                }


            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }

        }



        public async Task<dynamic> CheckVoucher(string GVCode)
        {
            // try to get the voucher First
            var isvoucher = await _context.GeneralVouchers.AnyAsync(s => s.GVCode == GVCode && !string.IsNullOrWhiteSpace(s.ApprovedBy));
            // if there is voucher
            if (isvoucher)
            {

                var voucher = await _context.GeneralVouchers.FirstOrDefaultAsync(s => s.GVCode == GVCode);
                // get the approval Date


                if ((voucher.NumOfVoucher - voucher.NumberOfUsedVoucher) > 0)
                {
                    var currentDate = DateTime.Now;
                    var start = voucher.StartDate;
                    var end = voucher.EndDate;


                    if ((currentDate >= start) && (currentDate <= end))
                    {

                        return new { minCartAmmount = voucher.MinCartAmount,discountType = voucher.VoucherDiscType, PercentageOrAmount = voucher.DiscAmntOrPercent,UptoDiscountAmount=voucher.UpToDiscAmnt,VoucherCode = voucher.GVCode };
                    }
                    else
                    {

                        return new { };
                    }
                }
                else
                {
                    return new { };
                }
            }
            else
            {

                return new { };
            }
        }

        public async Task DeleteGeneralVoucherAsync(int Id)
        {
            try
            {
                var general = _context.GeneralVouchers.FirstOrDefault(x=>x.GeneralVId==Id);
                if (general != null)
                {
                    general.IsDelete = true;
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }

        }

        public async Task<IEnumerable<GeneralVoucher>> GetGeneralVoucherAsync()
        {
            try
            {
                var gvs = await _context.GeneralVouchers.ToListAsync();
                return gvs;
            }
            catch
            {
                throw new ArgumentNullException();
            }

        }

        public async Task<object> GetGeneralVoucherByIdAsync(int Id)
        {
            try
            {
                var gv = (from v in _context.GeneralVouchers where v.GeneralVId==Id select new GeneralVPoco
                {
                    GeneralVId = v.GeneralVId,
                    description = v.Description,
                    discAmntOrPercent = v.DiscAmntOrPercent,
                    EndDate = v.EndDate.ToString("yyyy-MM-dd"),
                    EndTime = v.EndDate.ToString("HH:mm"),
                    GVCode = v.GVCode,
                    minCartAmount = v.MinCartAmount,
                    numberOfUsedVoucher = v.NumberOfUsedVoucher,
                    numOfVoucher=v.NumOfVoucher,
                    upToDiscAmnt=v.UpToDiscAmnt,
                    voucherDiscType=v.VoucherDiscType,
                    startDate=v.StartDate.ToString("yyyy-MM-dd"),
                    startTime =v.StartDate.ToString("HH:mm"),
                });
                return gv;
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException();
            }

        }

        public void UpdateGeneralVoucherAsync()
        {
            // NOT IMPLEMENTED
        }

        public bool IsVoucherAvilable(string GVcode)
        {
            // get the voucher from the coucher code
            var isvoucher = _context.GeneralVouchers.FirstOrDefault(s => s.GVCode == GVcode);
            if (isvoucher == null)
            {
                return false;
            }
            else
            {
                return true;                
            }
        }

        public async Task<GeneralVoucher> GetGeneralVoucherByGVCode(string GVCode)
        {
            try
            {
                var voucher = await _context.GeneralVouchers.FirstOrDefaultAsync(s => s.GVCode == GVCode);
                return voucher;
            }
            catch
            {
                throw new ArgumentException();
            }
        }
    }
}
