﻿using RetailSuite.Domain.Voucher.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.General
{
    public interface IGeneralVoucherRepository
    {
        Task<IEnumerable<GeneralVoucher>> GetGeneralVoucherAsync();
        Task<object> GetGeneralVoucherByIdAsync(int Id);
        void AddGeneralVoucher(GeneralVoucher generalVoucher,string GVCode,string username);
        void UpdateGeneralVoucherAsync();
        Task DeleteGeneralVoucherAsync(int Id);
        Task<dynamic> CheckVoucher(string GvCode);
        bool IsVoucherAvilable(string GVcode);
        Task<GeneralVoucher> GetGeneralVoucherByGVCode(string GVCode);

    }
}
