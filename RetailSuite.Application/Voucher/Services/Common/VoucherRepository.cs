﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.Voucher.CommonVEntity;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.Common
{
    public class VoucherRepository : IVoucherRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public VoucherRepository(RetailDbContext context, ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;
        }

        public async Task<IEnumerable<VoucherCode>> GetVoucherByCustomerId(string customerId)
        {
            var vouchers = await _context.VoucherCodes.Where(s => s.customerId == customerId).ToListAsync();
            return vouchers;
        }

        public bool IsVoucherAvilable(string Vcode)
        {
            // get the voucher from the coucher code
            var isvoucher = _context.VoucherCodes.FirstOrDefault(s => s.VCode == Vcode);
            if (isvoucher == null)
            {
                // no voucher avilable in this name
                // free to save it
                return false;
            }
            else
            {
                //return true;

                // check if it is active or not
                if (isvoucher.IsActive == true)
                {
                    // already active not save
                    return true;
                }
                else
                {
                    // not active so add it
                    return false;

                }
            }


        }

    }
}
