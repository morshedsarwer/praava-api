﻿using RetailSuite.Domain.Voucher.CommonVEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.Common
{
    public interface IVoucherRepository
    {
        bool IsVoucherAvilable(string Vcode);
        Task<IEnumerable<VoucherCode>> GetVoucherByCustomerId(string customerId);
    }
}
