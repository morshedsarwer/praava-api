﻿using RetailSuite.Application.Voucher.ViewModels.AppUserVoucherViewModel;
using RetailSuite.Domain.Voucher.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.App
{
    public interface IAppUserVoucherRepository
    {
        Task<IEnumerable<AppUserVoucher>> GetAppUserVoucher();
        Task<AppUserVoucher> GetAppUserVoucherById(int Id);
        void AddAppUserVoucher(AppUserVoucherContextViewModel appUserVoucherContextViewModel,string AppVoucher,string username);
        Task DeleteAppUserVoucher(int Id);
        Task<dynamic> CheckVoucher(string AppVoucher);
    }
}
