﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Voucher.ViewModels.AppUserVoucherViewModel;
using RetailSuite.Domain.Voucher.App;
using RetailSuite.Domain.Voucher.CommonVEntity;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.App
{
    public class AppUserVoucherRepository : IAppUserVoucherRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public AppUserVoucherRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;

        }
        public void AddAppUserVoucher(AppUserVoucherContextViewModel appUserVoucherContextViewModel,string AppVoucher,string username)
        {

            using (var transaction = _context.Database.BeginTransaction()) 
            {
                try
                {
                    // get the user from username
                    var user = _saveContext.GetUserFromUserName(username);
                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    appUserVoucherContextViewModel.AppUserVoucher.Code = code;
                    appUserVoucherContextViewModel.AppUserVoucher.AppVoucher = AppVoucher;

                    var vusers = new List<AppVoucherUsers>();
                    foreach (var item in appUserVoucherContextViewModel.AppVoucherUsers)
                    {
                        item.Code = code;
                        vusers.Add(item);
                    }
                    _context.AppUserVouchers.Add(appUserVoucherContextViewModel.AppUserVoucher);
                    _context.SaveChanges();
                    _context.AppVoucherUsers.AddRange(vusers);
                    _context.SaveChanges();

                    // you can configure isAvilable here
                    var voucherCode = new VoucherCode { Code = code, VCode = AppVoucher,customerId = user.Id };
                    _context.VoucherCodes.Add(voucherCode);
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                }

            }
                
            

        }

        public async Task<dynamic> CheckVoucher(string AppVoucher)
        {
            // check if there is a voucher
            var isvoucher = await _context.AppUserVouchers.AnyAsync(s => s.AppVoucher == AppVoucher && !string.IsNullOrWhiteSpace(s.ApprovedBy));
            if (isvoucher) 
            {
                // if there is then get the voucher
                var voucher = await _context.AppUserVouchers.FirstOrDefaultAsync(s => s.AppVoucher == AppVoucher);
                // is avalilabke or not
                if ((voucher.NumOfVoucher - voucher.NumberOfUsedVoucher) > 0) 
                {
                    var currentDate = DateTime.Now;
                    var start = voucher.StartDate;
                    var end = voucher.EndDate;
                    if ((currentDate >= start) && (currentDate <= end))
                    {
                        return new { minCartAmmount = voucher.MinCartAmount, discountType = voucher.VoucherDiscType, PercentageOrAmount = voucher.DiscAmntOrPercent, UptoDiscountAmount = voucher.UpToDiscAmnt, VoucherCode = voucher.AppVoucher };
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async Task DeleteAppUserVoucher(int Id)
        {
            var itemRemoved = await GetAppUserVoucherById(Id);
            _context.AppUserVouchers.Remove(itemRemoved);   
        }

        public async Task<IEnumerable<AppUserVoucher>> GetAppUserVoucher()
        {
            var uservouchers = await _context.AppUserVouchers.ToListAsync();
            return uservouchers;
        }

        public Task<AppUserVoucher> GetAppUserVoucherById(int Id)
        {
            var uservoucher = _context.AppUserVouchers.FirstOrDefaultAsync(s => s.AppVId == Id);
            return uservoucher;
        }
    }
}
