﻿using RetailSuite.Domain.Voucher.Vendor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.Vendor
{
    public interface ISellerVoucherRepository
    {
        Task<IEnumerable<SellerVoucher>> GetSellerVoucherAsync();
        Task<SellerVoucher> GetSellerVoucherByIdAsync(int Id);
        void AddSellerVoucher(SellerVoucher sellerVoucher,string SVcode,string username);
        void UpdateSellerVoucherAsync();
        Task DeleteSellerVoucherAsync(int Id);
        Task<dynamic> CheckVoucher(string SVCode);
    }
}
