﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.Voucher.CommonVEntity;
using RetailSuite.Domain.Voucher.Vendor;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.Vendor
{
    public class SellerVoucherRepository : ISellerVoucherRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public SellerVoucherRepository(RetailDbContext context,ISaveContexts saveContexts)
        {
            _context = context;
            _saveContext = saveContexts;

        }
        public void AddSellerVoucher(SellerVoucher sellerVoucher,string SVCode,string username)
        {
            try
            {
                // Voucher code hold the redundant voucher data
                // just for searching 
                using (var transaction = _context.Database.BeginTransaction()) 
                {
                    try
                    {
                        // get the user
                        var user = _saveContext.GetUserFromUserName(username);
                        var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                        sellerVoucher.Code = code;
                        sellerVoucher.SVCode = SVCode;
                        _context.SellerVouchers.Add(sellerVoucher);
                        _context.SaveChanges();

                        // now add the same info to voucher data
                        // VoucherCode entity for searching
                        var voucherCode = new VoucherCode { Code = code,VCode = SVCode,customerId = user.Id};
                        _context.VoucherCodes.Add(voucherCode);
                        _context.SaveChanges();
                        transaction.Commit();

                    }
                    catch (Exception ex) 
                    {
                        transaction.Rollback();
                    }
                }



            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
            
            

        }

        public async Task<dynamic> CheckVoucher(string SVCode)
        {
            var isvoucher = await _context.SellerVouchers.AnyAsync(s => s.SVCode == SVCode && !string.IsNullOrWhiteSpace(s.ApprovedBy));
            if (isvoucher) 
            {
                var voucher = await _context.SellerVouchers.FirstOrDefaultAsync(s => s.SVCode == SVCode);
                if ((voucher.NumOfVoucher - voucher.NumberOfUsedVoucher) > 0) 
                {
                    var currentDate = DateTime.Now;
                    var start = voucher.StartDate;
                    var end = voucher.EndDate;
                    if ((currentDate >= start) && (currentDate <= end)) 
                    {
                        return new { minCartAmmount = voucher.MinCartAmount, discountType = voucher.VoucherDiscType, PercentageOrAmount = voucher.DiscAmntOrPercent, UptoDiscountAmount = voucher.UpToDiscAmnt, VoucherCode = voucher.SVCode };

                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public async Task DeleteSellerVoucherAsync(int Id)
        {
            try
            {
                var itemRemoved = await GetSellerVoucherByIdAsync(Id);
                _context.SellerVouchers.Remove(itemRemoved);
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<IEnumerable<SellerVoucher>> GetSellerVoucherAsync()
        {
            try 
            {
                var sv = await _context.SellerVouchers.ToListAsync();
                return sv;
            }catch (Exception ex)
            {
                throw new ArgumentNullException();
            }
            
        }

        public async Task<SellerVoucher> GetSellerVoucherByIdAsync(int Id)
        {
            try
            {
                var sv = await _context.SellerVouchers.FirstOrDefaultAsync(s => s.SellerVId == Id);
                return sv;
            }
            catch (Exception ex) 
            {
                throw new ArgumentNullException();
            }
            
        }

        public void UpdateSellerVoucherAsync()
        {
            // NOT IMPLEMENTED
        }
    }
}
