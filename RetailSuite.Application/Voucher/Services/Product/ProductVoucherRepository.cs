﻿using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Voucher.ViewModels.ProductVoucherViewModel;
using RetailSuite.Domain.Voucher.CommonVEntity;
using RetailSuite.Domain.Voucher.Product;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.Product
{
    public class ProductVoucherRepository : IProductVoucherRepository
    {
        private readonly RetailDbContext _context;
        private readonly ISaveContexts _saveContext;

        public ProductVoucherRepository(RetailDbContext context,ISaveContexts saveContexts )
        {
            _context = context;
            _saveContext = saveContexts;
        }
        public void AddProductVoucher(ProductVoucherContextViewModel productVoucherContextViewModel,string PVCode,string username)
        {
            using (var transaction = _context.Database.BeginTransaction()) 
            {
                try
                {
                    // get user
                    var user = _saveContext.GetUserFromUserName(username);
                    var code = DateTime.Now.ToString("yyyyMMddHHmmss");
                    productVoucherContextViewModel.ProductVoucher.Code = code;
                    productVoucherContextViewModel.ProductVoucher.PVCode = PVCode;
                    var pvProducts = new List<ProdVoucherProducts>();
                    foreach (var item in productVoucherContextViewModel.ProdVoucherProducts)
                    {
                        item.Code = code;
                        pvProducts.Add(item);
                    }
                    _context.ProductVouchers.Add(productVoucherContextViewModel.ProductVoucher);
                    _context.SaveChanges();
                    _context.ProdVoucherProducts.AddRange(pvProducts);
                    _context.SaveChanges();

                    // you can configure isAvilable here
                    var voucherCode = new VoucherCode { Code = code, VCode = PVCode,customerId = user.Id };
                    _context.VoucherCodes.Add(voucherCode);
                    _context.SaveChanges();
                    transaction.Commit();



                }
                catch (Exception ex) 
                {
                    transaction.Rollback();
                }



            }
            
        }

        public async Task<dynamic> CheckVoucher(string PVCode)
        {
            var isvoucher = await _context.ProductVouchers.AnyAsync(s => s.PVCode == PVCode && !string.IsNullOrWhiteSpace(s.ApprovedBy));
            if (isvoucher)
            {
                // get the voucher
                var voucher = await _context.ProductVouchers.FirstOrDefaultAsync(s => s.PVCode == PVCode);
                if ((voucher.NumOfVoucher = voucher.NumberOfUsedVoucher) > 0)
                {
                    var currentDate = DateTime.Now;
                    var start = voucher.StartDate;
                    var end = voucher.EndDate;
                    if ((currentDate >= start) && (currentDate <= end)) 
                    {
                        return new { minCartAmmount = voucher.MinCartAmount, discountType = voucher.VoucherDiscType, PercentageOrAmount = voucher.DiscAmntOrPercent, UptoDiscountAmount = voucher.UpToDiscAmnt, VoucherCode = voucher.PVCode };
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async Task DeleteProductVoucherAsync(int Id)
        {
            var itemRemoved = await GetProductVoucherByIdAsync(Id);
            _context.ProductVouchers.Remove(itemRemoved);
        }

        public async Task<IEnumerable<ProductVoucher>> GetProductVoucherAsync()
        {
            var productVouchers = await _context.ProductVouchers.ToListAsync();
            return productVouchers;

        }

        public async Task<ProductVoucher> GetProductVoucherByIdAsync(int Id)
        {
            var productVoucher = await _context.ProductVouchers.FirstOrDefaultAsync(s=>s.ProductVId == Id);
            return productVoucher;
        }

        public void UpdateProductVoucherAsync()
        {
            // NOT IMPLEMENTED
        }
    }
}
