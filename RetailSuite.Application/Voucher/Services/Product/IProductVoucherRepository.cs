﻿using RetailSuite.Application.Voucher.ViewModels.ProductVoucherViewModel;
using RetailSuite.Domain.Voucher.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Services.Product
{
    public interface IProductVoucherRepository
    {
        Task<IEnumerable<ProductVoucher>> GetProductVoucherAsync();
        Task<ProductVoucher> GetProductVoucherByIdAsync(int Id);
        void AddProductVoucher(ProductVoucherContextViewModel productVoucherContextViewModel,string PVCode,string username);
        void UpdateProductVoucherAsync();
        Task DeleteProductVoucherAsync(int Id);
        Task<dynamic> CheckVoucher(string PVCode);
    }
}
