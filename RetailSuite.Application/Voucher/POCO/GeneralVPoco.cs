﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.POCO
{
    public class GeneralVPoco
    {
        public int GeneralVId { get; set; }
        public string GVCode { get; set; }
        public int numOfVoucher { get; set; }
        public int numberOfUsedVoucher { get; set; }
        public string startDate { get; set; }
        public string startTime { get; set; }
        public string EndTime { get; set; }
        public string EndDate { get; set; }
        public decimal minCartAmount { get; set; }
        public string voucherDiscType { get; set; }
        public decimal discAmntOrPercent { get; set; }
        public decimal upToDiscAmnt { get; set; }
        public string description { get; set; }
    }
}
