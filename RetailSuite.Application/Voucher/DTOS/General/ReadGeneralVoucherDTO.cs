﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.DTOS.General
{
    public class ReadGeneralVoucherDTO
    {
        public int GeneralVId { get; set; }
        
        public string GVCode { get; set; }
        //public string Code { get; set; }
        // how many voucher issued for this voucher profile
        public int NumOfVoucher { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal MinCartAmount { get; set; }
        // like taka of or percentage
        public string VoucherDiscType { get; set; }
        // final amount after voucher used
        [Column(TypeName = "decimal(18,2)")]
        public decimal DiscAmntOrPercent { get; set; }
        // example upto 100 taka can dicount amount
        [Column(TypeName = "decimal(18,2)")]
        public decimal UpToDiscAmnt { get; set; }
        public DateTime ApprovalDate { get; set; }
        public string ApprovedBy { get; set; }
        public string Description { get; set; }
     //   public int CreatedBy { get; set; }
    }
}
