﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.DTOS.Products
{
    public class UpdateProductVoucherDTO
    {
        //public int ProductVId { get; set; }
        // voucher code for client which is unique// user generate
        public string PVCode { get; set; }
        // has relationship with prodvoucherproducts table using system
        // system generate code
        // public string Code { get; set; }
        // how many voucher issued for this voucher profile
        public int NumOfVoucher { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal MinCartAmount { get; set; }
        // like taka of or percentage
        public string VoucherDiscType { get; set; }
        // final amount after voucher used
        [Column(TypeName = "decimal(18,2)")]
        public decimal DiscAmntOrPercent { get; set; }
        // example upto 100 taka can dicount amount
        [Column(TypeName = "decimal(18,2)")]
        public decimal UpToDiscAmnt { get; set; }
        public int ApprovalDate { get; set; }
        public int ApprovedBy { get; set; }
        public string Description { get; set; }
    }
}
