﻿using AutoMapper;
using RetailSuite.Application.Voucher.DTOS.App;
using RetailSuite.Domain.Voucher.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Profiles
{
    public class AppUserVoucherProfile : Profile
    {
        public AppUserVoucherProfile()
        {
            CreateMap<AppUserVoucher, ReadAppUserVoucherDTO>();
            CreateMap<CreateAppUserVoucherDTO, AppUserVoucher>();
            CreateMap<UpdateAppUserVoucherDTO, AppUserVoucher>();
            CreateMap<AppVoucherUsersDTO, AppVoucherUsers>();
        }
    }
}
