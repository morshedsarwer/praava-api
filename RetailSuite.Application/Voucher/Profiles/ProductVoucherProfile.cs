﻿using AutoMapper;
using RetailSuite.Application.Voucher.DTOS.Products;
using RetailSuite.Domain.Voucher.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Profiles
{
    public class ProductVoucherProfile : Profile
    {
        public ProductVoucherProfile()
        {
            CreateMap<ProductVoucher, ReadProductVoucherDTO>();
            CreateMap<CreateProductVoucherDTO, ProductVoucher>();
            CreateMap<UpdateProductVoucherDTO, ProductVoucher>();
            CreateMap<ProdVoucherProductsDTO, ProdVoucherProducts>();
        }

    }
}
