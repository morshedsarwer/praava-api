﻿using AutoMapper;
using RetailSuite.Application.Voucher.DTOS.General;
using RetailSuite.Domain.Voucher.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Profiles
{
    public class GeneralVoucherProfile:Profile
    {
        public GeneralVoucherProfile()
        {

            CreateMap<GeneralVoucher, ReadGeneralVoucherDTO>();
            CreateMap<ReadGeneralVoucherDTO, GeneralVoucher>();
            CreateMap<CreateGeneralVoucherDTO, GeneralVoucher>();
            CreateMap<GeneralVoucher, CreateGeneralVoucherDTO>();
            CreateMap<UpdateGeneralVoucherDTO, GeneralVoucher>();
            CreateMap<GeneralVoucher, UpdateGeneralVoucherDTO>();
        }
    }
}
