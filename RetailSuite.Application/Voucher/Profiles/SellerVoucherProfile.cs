﻿using AutoMapper;
using RetailSuite.Application.Voucher.DTOS.Vendors;
using RetailSuite.Domain.Voucher.Vendor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Profiles
{
    public class SellerVoucherProfile : Profile
    {
        public SellerVoucherProfile()
        {
            CreateMap<SellerVoucher, ReadSellerVoucherDTO>();
            CreateMap<ReadSellerVoucherDTO, SellerVoucher>();
            CreateMap<CreateSellerVoucherDTO, SellerVoucher>();
            CreateMap<SellerVoucher, CreateSellerVoucherDTO>();
            CreateMap<UpdateSellerVoucherDTO, SellerVoucher>();
            CreateMap<SellerVoucher, UpdateSellerVoucherDTO>();
        }

    }
}
