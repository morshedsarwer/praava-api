﻿using AutoMapper;
using RetailSuite.Application.Voucher.DTOS.User;
using RetailSuite.Domain.Voucher.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.Profiles
{
    public class UserVoucherProfile : Profile
    {
        public UserVoucherProfile()
        {
            CreateMap<UserVoucher, ReadUserVoucherDTO>();
            CreateMap<ReadUserVoucherDTO, UserVoucher>();
            CreateMap<CreateUserVoucherDTO, UserVoucher>();
            CreateMap<UpdateUserVoucherDTO, UserVoucher>();
        }
    }
}
