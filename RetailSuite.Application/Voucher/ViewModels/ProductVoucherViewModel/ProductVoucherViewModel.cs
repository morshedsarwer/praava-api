﻿using RetailSuite.Application.Voucher.DTOS.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.ViewModels.ProductVoucherViewModel
{
    public class ProductVoucherViewModel
    {
        public CreateProductVoucherDTO CreateProductVoucherDTO { get; set; }
        public IEnumerable<ProdVoucherProductsDTO> ProdVoucherProductsDTOs { get; set; }

    }
}
