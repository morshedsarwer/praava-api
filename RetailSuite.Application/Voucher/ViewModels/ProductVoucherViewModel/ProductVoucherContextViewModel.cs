﻿using RetailSuite.Domain.Voucher.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.ViewModels.ProductVoucherViewModel
{
    public class ProductVoucherContextViewModel
    {
        public ProductVoucher ProductVoucher { get; set; }
        public IEnumerable<ProdVoucherProducts> ProdVoucherProducts { get; set; }
    }
}
