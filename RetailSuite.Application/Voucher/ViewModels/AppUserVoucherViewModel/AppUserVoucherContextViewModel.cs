﻿using RetailSuite.Domain.Voucher.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.ViewModels.AppUserVoucherViewModel
{
    public class AppUserVoucherContextViewModel
    {
        public AppUserVoucher AppUserVoucher { get; set; }
        public IEnumerable<AppVoucherUsers> AppVoucherUsers { get; set; }
    }
}
