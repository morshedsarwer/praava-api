﻿using RetailSuite.Application.Voucher.DTOS.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Voucher.ViewModels.AppUserVoucherViewModel
{
    public class AppUserVoucherViewModel
    {
        public CreateAppUserVoucherDTO CreateAppUserVoucherDTO { get; set; }
        public IEnumerable<AppVoucherUsersDTO> AppVoucherUsersDTOs { get; set; }
    }
}
