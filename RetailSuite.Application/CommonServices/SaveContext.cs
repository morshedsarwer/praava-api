﻿using Microsoft.AspNetCore.Identity;
using RetailSuite.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CommonServices
{
    public class SaveContexts:ISaveContexts
    {
        private readonly RetailDbContext _context;

        public SaveContexts(RetailDbContext context)
        {
            _context = context;

        }

        public IdentityUser GetUserFromCustomerId(string customerId)
        {
            return _context.Users.FirstOrDefault(s => s.Id == customerId);
        }

        public IdentityUser GetUserFromUserName(string username)
        {
            return _context.Users.FirstOrDefault(s => s.UserName == username);
        }
        public async Task<bool> SaveChangesAsync() {

            return (await _context.SaveChangesAsync() > 0);
        }


    }
}
