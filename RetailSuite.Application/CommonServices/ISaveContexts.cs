﻿
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.CommonServices
{
    public interface ISaveContexts
    {
        public Task<bool> SaveChangesAsync();
        public IdentityUser GetUserFromUserName(string username);
        public IdentityUser GetUserFromCustomerId(string customerId);
    }
}
