﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Common.SMS
{
    public static class SendSMS
    {
        public static async Task<bool> Send(string phone, string message)
        {
            /*var sms = new OrderSms()
            {
                CustomerName = "Morshed",
                DeliveryDate = DateTime.Now,
                OrderDate = DateTime.Now,
                OrderTrn = "FR0014521",
                TotalAmount = 1200
            };*/
            var respon = new ApiResponse();
            using (var httpClient = new HttpClient())
            {
                //string message = "Your Order FR000125 has been placed";
                //string phone = "01894840889";
                //StringContent content = new StringContent(JsonConvert.SerializeObject(sms), Encoding.UTF8, "application/json");
                using (var response = await httpClient.GetAsync($"http://202.164.208.212/smsnet/bulk/api?api_key=d65b96c32bfce31ac69ba2c035676c30255&mask=FARMROOTS&recipient={phone}&message={message}"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    respon = JsonConvert.DeserializeObject<ApiResponse>(apiResponse);
                    //Console.WriteLine(respon);
                }
            }
            if (respon != null)
            {
                return true;
            }
            return false;
        }
    }
}
