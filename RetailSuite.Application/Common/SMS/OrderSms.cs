﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Common.SMS
{
    public class OrderSms
    {
        public string OrderTrn { get; set; }
        public string CustomerName { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public decimal TotalAmount { get; set; }
    }

    public class ApiResponse
    {
        public string messageid { get; set; }
        public string status { get; set; }
        public string message { get; set; }
    }
}
