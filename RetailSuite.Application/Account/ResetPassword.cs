﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Application.Account
{
    public class ResetPassword
    {
        public string Phone { get; set; }
        public string Password { get; set; }
    }
}
