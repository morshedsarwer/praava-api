﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.OMS
{
    public class OrderStatus
    {
        [Key]
        public int OrderStatusID { get; set; }
        public string StatusName { get; set; }
    }
}
