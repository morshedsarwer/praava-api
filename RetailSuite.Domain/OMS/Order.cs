﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.OMS
{
    public class Order
    {
        public Order()
        {
            this.OrderDate = DateTime.Now;
            this.OrderCode = DateTime.Now.ToString("yyyyMMddHHmmss");
            this.OrderStatus = (int)Enums.OrderStatus.OrderPlaced;
            this.IsOnlinePaid = false;
        }
        [Key]
        public int OrderId { get; set; }
        public string OrderCode { get; set; }
        public string OrderTrnId { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerID { get; set; }
        public int OrderStatus { get; set; }
        public string VoucherCode { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal TotalPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal GrandTotalPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal AfterPromoPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal PromoPrice { get; set; }
        public int PaymentType { get; set; }
        public int ZoneId { get; set; }
        public int DeliveryType { get; set; }
        public int DeliveryAddressID { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal DeliveryPrice { get; set; }
        public string? OrderEditBy { get; set; }
        public DateTime EditDate { get; set; }
        public bool IsOnlinePaid { get; set; }
        public bool IsCombo { get; set; }
        public bool IsCampaign { get; set; }
        public bool IsRecurring { get; set; }
        public int StoreId { get; set; }
        public string? ImageName { get; set; }
        public string? ImageUrl { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }


    }
}
