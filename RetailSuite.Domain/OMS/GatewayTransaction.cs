﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.OMS
{
    public class GatewayTransaction : AuditEntity
    {
        public int Id { get; set; }
        public string? OrderTrnCode { get; set; }
        public string? GatewayTrnCode { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }
        public string? PaymentGateway { get; set; }
    }
}
