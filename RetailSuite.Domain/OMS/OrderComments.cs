﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.OMS
{
    public class OrderComments : AuditEntity
    {
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string Comment { get; set; }
    }
}
