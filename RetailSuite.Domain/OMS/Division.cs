﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.OMS
{
    public class Division
    {
        [Key]
        public int DivisionID { get; set; }
        public string DivisionName { get; set; }
    }
}
