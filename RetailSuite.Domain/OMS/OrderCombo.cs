﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.OMS
{
    public class OrderCombo
    {
        [Key]
        public int Id { get; set; }
        public string OrderCode { get; set; }
        public int ComboId { get; set; }
        public int ProductQuantity { get; set; }
        public decimal RegularPrice { get; set; }
        public int ComboPrice { get; set; }
    }
}
