﻿using Castle.MicroKernel.SubSystems.Conversion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.OMS
{
    public class OrderProducts
    {
        [Key]
        public int OrderProductsID { get; set; }
        public int ProductID { get; set; }
        public int ProductQuantity { get; set; }
        public string OrderCode { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal ProductPrice { get; set; }
        public int ProductPoints { get; set; }
    }
}
