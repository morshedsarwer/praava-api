﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.OMS
{
    public class District
    {
        [Key]
        public int DistrictId { get; set;}
        public string DistrictName { get; set; }
        public int DivisionId { get; set; }
    }
}
