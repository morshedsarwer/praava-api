﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.OMS
{
    public class RecurringOrder
    {
        public int Id { get; set; }
        public string IsMonthOrWeek { get; set; }
        public int MonthOrDateValue { get; set; }
        public int Continuity { get; set; }
        public string? OrderCode { get; set; }

    }
}
