﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.OMS
{
    public class DeliveryAddress
    {
        [Key]
        public int AddressId { get; set; }
        public string CustomerID { get; set; }
        public string Name { get; set; }
        public string HouseNo { get; set; }
        public string DetailAddress { get; set; }
        public int District { get; set; }
        public string Zip { get; set; }
        public string ContactNumber { get; set; }
        public string RoadNo { get; set; }
        public int Zone { get; set; }
        public int Division { get; set; }
        public string SaveAddressAs { get; set; }
        public bool IsDelete { get; set; }

/*      public string Address { get; set; }
        public int DivisionId { get; set; }
        public int DistrictId { get; set; }
        public int ZoneId { get; set; }
*/    }
}
