﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Enums
{
    public enum DeliveryType
    {
        Regular = 1,
        Express = 2
    }
}
