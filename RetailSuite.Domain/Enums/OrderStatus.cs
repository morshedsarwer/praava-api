﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Enums
{
    public enum OrderStatus
    {
        OrderPlaced = 1,
        OrderAuthorize = 2,
        OrderProcessed = 3,
        OrderOutForDelivery = 4,
        OrderDelivered = 5
    }
}
