﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Stock
{
    public class UnimartStoreProducts
    {
        public UnimartStoreProducts()
        {
            this.IsPusblish = false;
        }
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int StoreId { get; set; }
        public bool IsPusblish { get; set; }
    }
}
