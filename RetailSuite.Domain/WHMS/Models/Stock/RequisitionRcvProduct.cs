﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Stock
{
    public class RequisitionRcvProduct : AuditEntity
    {
        [Key]
        public int RequitionRcvId { get; set; }
        public string ReqCode { get; set; }
        public int ProductId { get; set; }
        public int QCCateType { get; set; }
        public int RcvQuantity { get; set; }
        public int ReceivedBy { get; set; }
        public DateTime ReceivedDate { get; set; }
    }
}
