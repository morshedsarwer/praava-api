﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Stock
{
    public class StockTransfer:AuditEntity
    {
        [Key]
        public int TransferId { get; set; }
        // system generate code
        public string TransferCode { get; set; }
        public int ToStoreId { get; set; }
        public int FromStoreId { get; set; }
        public int TransferStatusId { get; set; }
        public string Description { get; set; }
        public DateTime ExpectationDate { get; set; }
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        public int RiderId { get; set; }
        public DateTime SendingDate { get; set; }
        public DateTime ReceivingDate { get; set; }
    }
}
