﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Stock
{
    public class FarmrootsStock : ApprovalEntity
    {
        [Key]
        public int Id { get; set; }
        public int StoreId { get; set; }
        public string ProductCode { get; set; }
        public int Quantity { get; set; }
    }
}
