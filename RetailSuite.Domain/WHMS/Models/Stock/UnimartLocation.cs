﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Stock
{
    public class UnimartLocation
    {
        [Key]
        public int Id { get; set; }
        public int StoreId { get; set; }
        public string? Area { get; set; }
    }
}
