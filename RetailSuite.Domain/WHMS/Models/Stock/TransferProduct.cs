﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Stock
{
    public class TransferProduct
    {
        [Key]
        public int TProductId { get; set; }
        public string TransferCode { get; set; }
        public int ProductId { get; set; }
        public int TransferQty { get; set; }
    }
}
