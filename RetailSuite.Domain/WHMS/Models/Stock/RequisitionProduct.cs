﻿ using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Stock
{
    public class RequisitionProduct : AuditEntity
    {
        [Key]
        public int RProductId { get; set; }
        public string REQCode { get; set; }
        public int ProductId { get; set; }
        public int ReqQuantity { get; set; }
        public int SendingQuantity { get; set; }
    }
}
