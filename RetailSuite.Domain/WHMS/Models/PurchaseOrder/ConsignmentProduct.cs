﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.PurchaseOrder
{
    public class ConsignmentProduct
    {
        [Key]
        public int ConsignProdId { get; set; }
        public string ConsignCode { get; set; }
        public int ProductId { get; set; }
        public int OrderQuantity { get; set; }
    }
}
