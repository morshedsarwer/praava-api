﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.PurchaseOrder
{
    public class POProduct:AuditEntity
    {
        [Key]
        public int POProductID { get; set;}
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public string POCode { get; set;}
        public int OrderQuantity { get; set;}
        
    }
}
