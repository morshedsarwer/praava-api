﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.PurchaseOrder
{
    public class ConsignmentProductReturn:AuditEntity
    {
        [Key]
        public int Id { get; set; }
        public string ConsignCode { get; set; }
        public int ReturnQuantity { get; set; }
        public int ProductId { get; set; }
        public string POCode { get; set; }
    }
}
