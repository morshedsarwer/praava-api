﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.PurchaseOrder
{
    public class Consignment : ApprovalEntity
    {
        [Key]
        public int ConsignmentID { get; set; }
        public DateTime OrderDate { get; set; }
        public int VendorId { get; set; }
        public int StoreId { get; set; }
        public int DeliveryType { get; set; }
        public string ConsignCode { get; set; }
        // store
        public int PODevliveryStoreId { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime ProductReturnDate { get; set; }
        public string Description { get; set; }
        //public int ApprovedBy { get; set; }
        //public DateTime ApprovalDate { get; set; }
    }
}