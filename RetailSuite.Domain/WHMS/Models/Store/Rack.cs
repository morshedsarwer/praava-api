﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Store
{
    public class Rack : AuditEntity
    {
        public int RackId { get; set; }
        public string RackName { get; set;}
        //public List<Shelve> Shelves { get; set; }
    }
}
