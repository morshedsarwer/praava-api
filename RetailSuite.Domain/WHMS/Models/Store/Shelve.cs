﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Store
{
    public class Shelve: AuditEntity
    {
        [Key]
        public int ShelveId { get; set; }
        public string ShelveName { get; set; }
        public int RackId { get; set; }

    }
}
