﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Store
{
    public class Store:AuditEntity
    {
        [Key]
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public string Address { get; set; }
        public int StoreSize { get; set; }
        public int NumberOfRack { get; set; }

    }
}
