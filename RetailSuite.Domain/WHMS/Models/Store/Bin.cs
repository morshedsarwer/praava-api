﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Models.Store
{
    public class Bin :AuditEntity
    {
        [Key]
        public int BinId { get; set; }
        public string BinName { get; set; }
        public int ShelveId { get; set; }
    }
}
