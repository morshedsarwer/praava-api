﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Enum
{
    public enum POStatus
    {
        Pending=0,
        Approved=1,
        StockInTransit=2,
        Received=3,
        Returned=4,
        Cancelled=5
    }
}
