﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Enum
{
    public enum ReqTransStatus
    {
        Transit = 0,
        Received = 1

    }
}
