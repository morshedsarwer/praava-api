﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.WHMS.Enum
{
    public enum QCCategory
    {
        OK=0,
        Saleable=1,
        Broken=2,
        Faulty=3,
        Expired=4
    }
}
