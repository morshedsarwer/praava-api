﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Vendor.Models
{
    public class VendorPayment:AuditEntity
    {
        [Key]
        public int VendorPaymentId { get; set; }
        [ForeignKey("Vendor")]
        public int VendorId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }
        public string PaymentDetails { get; set; }
        public DateTime PaymentDate { get; set; }
        public string POCode { get; set; }
        public string Description { get; set; }


    }
}
