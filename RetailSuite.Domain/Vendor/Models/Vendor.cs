﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Vendor.Models
{
    public class Vendor: AuditEntity
    {
        [Key]
        public int VendorId { get; set; }
        public string Name { get; set; }
        public string Initial { get; set; }
        public string Owner { get; set; }
        public string BankAccountDetails { get; set; }
        public string ManagerName { get; set; }
        public string PhoneNumber { get; set; }

    }
}
