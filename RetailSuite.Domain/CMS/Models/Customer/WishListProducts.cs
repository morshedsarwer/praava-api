﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.Customer
{
    public class WishListProducts : AuditEntity
    {
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string CustomerId { get; set; }
    }
}
