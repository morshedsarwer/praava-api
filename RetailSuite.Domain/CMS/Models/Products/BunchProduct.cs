﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RetailSuite.Domain.Common;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class BunchProduct : ApprovalEntity
    {
        public BunchProduct()
        {
            this.IsApproved = false;
        }

        [Key]
        public int BunchProductId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string BunchProductCode { get; set; }
        public string Description { get; set; }
        public bool IsApproved { get; set; }
    }
}
