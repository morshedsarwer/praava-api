﻿
using RetailSuite.Domain.Common;
using System.ComponentModel.DataAnnotations;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class PhysicalAttribute : AuditEntity
    {
        [Key]
        public int AttributeId { get; set; }
        public string Type { get; set; }
    }


}
