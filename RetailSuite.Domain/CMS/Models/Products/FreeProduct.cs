﻿using Microsoft.AspNetCore.Http;
using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class FreeProduct : AuditEntity
    {
        [Key]
        public int FreeProductId { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "decimal(18,2)")]        
        public Decimal Price { get; set; }
        public int NumbOfFreeProd { get; set; }
        public string ImgUrl { get; set; }
        public string ImageName { get; set; }
        public string Description { get; set; }

        [NotMapped]
        public IFormFile ImageFile { get; set; }
        
        


    }
}
