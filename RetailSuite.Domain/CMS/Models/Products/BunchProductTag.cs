﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class BunchProductTag : AuditEntity
    {
        [Key]
        public int BunchProductTagId { get; set; }    
        public int ProductId { get; set; }
        public string BunchProductCode { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal RegularPrice { get; set; } 
        [Column(TypeName = "decimal(18,2)")]
        public decimal SellingPrice { get; set; } 

    }
}
