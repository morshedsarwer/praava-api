﻿using RetailSuite.Domain.Common;
using RetailSuite.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class ComboProductTag : ApprovalEntity
    {
        public ComboProductTag()
        {
            this.IsApproved = true;
            //this.discountType = DiscountType.percent;
        }
        [Key]
        public int Id { get; set; }
        public string ComboCode { get; set; }
        public int ProductId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal DiscountAmount { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal RegularPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal AfterDiscAmount { get; set; }
        public bool IsApproved { get; set; }
        //public DiscountType discountType { get; set; }
        public string DiscountType { get; set; }
    }
}
