﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class ProductMetaTag : AuditEntity
    {
        [Key]
        public int Id { get; set; }
        public string ProductCode { get; set; }
        public string MetaTag { get; set; }
    }
}
