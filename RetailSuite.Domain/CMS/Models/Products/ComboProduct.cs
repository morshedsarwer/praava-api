﻿using Castle.MicroKernel.SubSystems.Conversion;
using Microsoft.AspNetCore.Http;
using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class ComboProduct : ApprovalEntity
    {
        public ComboProduct()
        {
            this.IsApproved = false;
        }
        [Key]
        public int ComboId { get; set; }
        public string ComboProductName { get; set; }
        public int ProductId { get; set; }
        public string ComboCode { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal RegularPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public Decimal ComboPrice { get; set; }
        public string Description { get; set; }
        public bool IsApproved { get; set; }
        public string? ImageName { get; set; }
        public string? ImageUrl { get; set; }
        public int StoreId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }


        [NotMapped]
        public IFormFile ImageFile { get; set; }
        //public int ApprovalID { get; set; }
        //public DateTime ApprovalDate { get; set; }


    }
}
