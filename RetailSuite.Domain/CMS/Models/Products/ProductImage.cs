﻿using Microsoft.AspNetCore.Http;
using RetailSuite.Domain.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class ProductImage : AuditEntity
    {

        [Key]
        public int ProductImageId { get; set; }
        public string Url { get; set; }
        public string ImageName { get; set; }
        public string? AltImageName { get; set; }
        public string ProductCode { get; set; }
        
        [NotMapped]
        public IFormFile ImageFile { get; set; }

    }
}
