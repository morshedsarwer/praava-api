﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class ProductShade : AuditEntity
    {
        [Key]
        public int ProductShadeId { get; set; }
        public string ColorCode { get; set; }
        public string ShadeName { get; set; }

    }
}
