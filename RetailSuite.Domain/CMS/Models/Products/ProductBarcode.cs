﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class ProductBarcode : AuditEntity
    {
        [Key]
        public int ProductBarCodeId { get; set; }
        public string Barcode { get; set; }
        public int ProductId { get; set; }
    }
}
