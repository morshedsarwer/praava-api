﻿using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.Products
{
    public class BunchPrice: ApprovalEntity
    {
        public BunchPrice()
        {
            this.IsApproved = false;
        }
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalRegularPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalBunchPrice { get; set; }
        public string BunchProductCode { get; set; }
        public bool IsApproved { get; set; }
    }
}
