﻿using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Domain.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RetailSuite.Domain.CMS.Models.Brands
{
    public class Brand : AuditEntity
    {
        [Key]
        public int BrandId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        //public List<Product> Products { get; set; }
    }


}
