﻿using Microsoft.AspNetCore.Http;
using RetailSuite.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.Categories
{
    public class Category : AuditEntity
    {
        [Key]
        public int Id { get; set; }
        public string? ParentCat { get; set; }
        public int ParentId { get; set; }
        public string? ChildCat { get; set; }
        public int ChildCatId { get; set; }
        public string? GChildCat { get; set; }
        public int GChildCatId { get; set; }
        public string? GGChildCat { get; set; }
        public string? ImageName { get; set; }
        public string? ImageUrl { get; set; }

        [NotMapped]
        public IFormFile ImageFile { get;set; }
        
    }
}
