using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RetailSuite.Domain.CMS.Models.Campaigns
{
    public class CampaignProducts
    {
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string CampaignCode { get; set; }
        public string DiscType { get; set; }
        public int DiscAmount { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal RegularPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal DiscPrice { get; set; }
    }
}