using System.ComponentModel.DataAnnotations;
using RetailSuite.Domain.Common;

namespace RetailSuite.Domain.CMS.Models.Campaigns
{
    public class Campaign : ApprovalEntity
    {
        public Campaign()
        {
            this.IsPublish=false;
        }
        [Key]
        public int Id { get; set; }
        public string CampaignName { get; set; }
        public string CampaignCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public int StoreId { get; set; }
        public bool IsPublish { get; set; }

    }
}