﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.CMS.Models.SideBanner
{
    public class SideBanner
    {
        [Key]
        public int Id { get; set; }
        public string? ImageName { get; set; }
        public string? ImageUrl { get; set; }
        public string Link { get; set; }
        public string Position { get; set; }
    }
}
