﻿using RetailSuite.Domain.Voucher.CommonVEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Voucher.App
{
    public class AppUserVoucher: VoucherEntity
    {
        [Key]
        public int AppVId { get; set; }
        public string AppVoucher { get; set; }

        // 1 = android, 2=ios, 3= both
        public int AppType { get; set; }
    }
}
