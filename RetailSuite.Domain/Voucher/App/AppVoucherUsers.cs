﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Voucher.App
{
    public class AppVoucherUsers
    {
        [Key]
        public int Id { get; set; }
        public int CustomerId { get; set; }
        // system generate code
        public string Code { get; set; }
    }
}
