﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Voucher.CommonVEntity
{
    public class VoucherCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string VCode { get; set; }
        public bool IsActive { get; set; }
        public string customerId { get; set; }
    
    }
}
