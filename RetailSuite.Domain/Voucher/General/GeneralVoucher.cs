﻿using RetailSuite.Domain.Common;
using RetailSuite.Domain.Voucher.CommonVEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Voucher.General
{
    public class GeneralVoucher : VoucherEntity
    {
        [Key]
        public int GeneralVId { get; set; }
        public string GVCode { get; set; }

    }
}
