﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Voucher.Product
{
    public class ProdVoucherProducts
    {
        public int Id { get; set; }
        // relationship with product voucher system generate code
        public string Code { get; set; }
        public int ProductId { get; set; }
    }
}
