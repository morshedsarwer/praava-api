﻿using RetailSuite.Domain.Voucher.CommonVEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Voucher.Product
{
    public class ProductVoucher : VoucherEntity
    {
        [Key]
        public int ProductVId { get; set; }
        // voucher code for client which is unique// user generate
        public string PVCode { get; set; }
        // has relationship with prodvoucherproducts table using system generate code
    }
}
