﻿using RetailSuite.Domain.Voucher.CommonVEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Voucher.Vendor
{
    public class SellerVoucher : VoucherEntity
    {
        [Key]
        public int SellerVId { get; set; }
        public string SVCode { get; set; }
        public int VendorId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal MimVendorProdAmnt { get; set; }
    }
}
