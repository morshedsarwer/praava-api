﻿using RetailSuite.Domain.Voucher.CommonVEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Voucher.User
{
    public class UserVoucher : VoucherEntity
    {
        // waiting for crm decision, till wait 
        [Key]
        public int UserVId { get; set; }
        public string UVCode { get; set; }
    }
}
