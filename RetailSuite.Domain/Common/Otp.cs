﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Common
{
    public class Otp
    {
        [Key]
        public int Id { get; set; }
        public int OTP { get; set; }
        public string Phone { get; set; }
        public DateTime Expiration { get; set; }
    }
}
