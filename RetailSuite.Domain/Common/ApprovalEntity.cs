﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailSuite.Domain.Common
{
    public class ApprovalEntity: AuditEntity
    {
        public string? ApprovedBy { get; set; }
        public DateTime ApprovelDate { get; set; }
        public string? ApprovalNote { get; set; }


        public ApprovalEntity()
        {
            //this.ApprovedBy = null;
            // for testing give any name

            this.ApprovedBy = "test";
        }
    }
}
