﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;


using RetailSuite.Domain.CMS.Models.Brands;
using RetailSuite.Domain.CMS.Models.Categories;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Domain.HRM.Employee;
using RetailSuite.Domain.Vendor.Models;
using RetailSuite.Domain.Voucher.App;
using RetailSuite.Domain.Voucher.CommonVEntity;
using RetailSuite.Domain.Voucher.General;
using RetailSuite.Domain.Voucher.Product;
using RetailSuite.Domain.Voucher.Vendor;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;
using RetailSuite.Domain.WHMS.Models.Stock;
using RetailSuite.Domain.WHMS.Models.Store;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RetailSuite.Domain.Account.Models;
using RetailSuite.Domain.CMS.Models.Customer;
using RetailSuite.Domain.Voucher.User;
using RetailSuite.Domain.OMS;
using RetailSuite.Domain.CMS.Models.Campaigns;
using RetailSuite.Domain.CMS.Models.Carousel;
using RetailSuite.Domain.CMS.Models.SideBanner;
using RetailSuite.Domain.Common;

namespace RetailSuite.Infrastructure.Contexts
{
    public class RetailDbContext : IdentityDbContext<ApplicationUser>
    {
        public RetailDbContext(DbContextOptions<RetailDbContext> opt) : base(opt)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
        //cms
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Cats { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<BunchProduct> BunchProducts { get; set; }
        public DbSet<ProductAttribute> ProductAttributes { get; set; }
        public DbSet<BunchProductTag> BunchProductTags { get; set; }
        public DbSet<BunchPrice> BunchPrices { get; set; }
        public DbSet<ComboProduct> ComboProducts { get; set; }
        public DbSet<ComboProductTag> ComboProductTags { get; set; }
        public DbSet<FreeProduct> FreeProducts { get; set; }
        public DbSet<PhysicalAttribute> PhysicalAttributes { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<ProductBarcode> ProductBarcodes { get; set; }
        public DbSet<ProductCategories> ProductCategories { get; set; }
        public DbSet<ProductMetaTag> ProductMetaTags { get; set; }
        public DbSet<ProductShade> ProductShades { get; set; }
        public DbSet<ProductPrice> ProductPrices { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<VendorPayment> VendorPayments { get; set; }
        public DbSet<WishListProducts> WishListProducts { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<CampaignProducts> CampaignProducts { get; set; }
        public DbSet<Carousel> Carousels { get; set; }
        public DbSet<SideBanner> SideBanners { get; set; }

        //WHMS
        public DbSet<Bin> Bins { get; set; }
        public DbSet<Rack> Racks { get; set; }
        public DbSet<Shelve> Shelves { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<UnimartStoreProducts> UnimartStoreProducts { get; set; }
        public DbSet<UnimartLocation> UnimartLocations { get; set; }
        public DbSet<FarmrootsStock> FarmrootsStocks { get; set; }
        public DbSet<StockTransfer> StockTransfers { get; set; }
        public DbSet<TransferProduct> TransferProducts { get; set; }
        public DbSet<TransferRcvProduct> TransferRcvProducts { get; set; }
        public DbSet<RequisitionRcvProduct> RequitionRcvProducts { get; set; }


        // Purchase Order
        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public DbSet<POProduct> POProducts { get; set; }

        public DbSet<Consignment> Consignments { get; set; }
        public DbSet<ConsignmentProduct> ConsignmentProducts { get; set; }
        public DbSet<ConsignmentProductRcv> ConsignmentProductRcvs { get; set; }
        public DbSet<ConsignmentProductReturn> ConsignmentReturns { get; set; }
        public DbSet<StoreRequisition> StoreRequisitions { get; set; }
        public DbSet<RequisitionProduct> RequisitionProducts { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<POReceiveProduct> POReceiveProducts { get; set; }
        public DbSet<PODeliveryMethod> PODeliveryMethods { get; set; }

        // Voucher

        public DbSet<GeneralVoucher> GeneralVouchers { get; set; }
        public DbSet<AppUserVoucher> AppUserVouchers { get; set; }
        public DbSet<AppVoucherUsers> AppVoucherUsers { get; set; }
        public DbSet<ProductVoucher> ProductVouchers { get; set; }
        public DbSet<ProdVoucherProducts> ProdVoucherProducts { get; set; }
        public DbSet<SellerVoucher> SellerVouchers { get; set; }
        public DbSet<VoucherCode> VoucherCodes { get; set; }
        public DbSet<UserVoucher> UserVouchers { get; set; }


        // OMS
        public DbSet<DeliveryAddress> DeliveryAddresses { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderProducts> OrderProducts { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<OrderCombo> OrderCombos { get; set; }
        public DbSet<RecurringOrder> RecurringOrders { get; set; }
        public DbSet<OrderCampaign> OrderCampaigns { get; set; }
        public DbSet<GatewayTransaction> GatewayTransactions { get; set; }
        public DbSet<OrderComments> OrderComments { get; set; }
        public DbSet<Otp> Otps { get; set; }

    }
}
