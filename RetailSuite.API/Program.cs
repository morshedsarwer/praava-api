using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Identity.Web;
using Microsoft.EntityFrameworkCore;
using RetailSuite.Infrastructure.Contexts;
using Microsoft.Extensions.Configuration;
using RetailSuite.Application.Services.BrandServices;
using AutoMapper;
using RetailSuite.Application.Services.BunchProductServices;
using RetailSuite.Application.CMS.Services.ComboProductServices;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.CMS.Services.CategoryServices;
using RetailSuite.Application.WHMS.Services.StoreService;
using RetailSuite.Application.WHMS.Services.ShelveService;
using RetailSuite.Application.WHMS.Services.RackService;
using RetailSuite.Application.WHMS.Services.BinService;
using RetailSuite.Application.WHMS.Services.PurchaseOrderService;
using RetailSuite.Application.WHMS.Services.ConsignmentService;
using RetailSuite.Application.HRM.Services.EmployeeService;
using RetailSuite.Application.WHMS.Services.RequisitionService;
using RetailSuite.Application.Vendors.Services.VendorService;
using RetailSuite.Application.CMS.Services.ProductServices;
using RetailSuite.Application.CMS.Services.ProductShadeService;
using RetailSuite.Application.CMS.Services.PhysicalAttributeService;
using RetailSuite.Application.WHMS.Services.PORecieveProductService;
using RetailSuite.Application.WHMS.Services.PODeliveryMethodService;
using RetailSuite.Application.WHMS.Services.ConsignmentProductRcvService;
using RetailSuite.Application.WHMS.Services.ConsignmentReturnService;
using RetailSuite.Application.Vendors.Services.VendorPaymentService;
using RetailSuite.Application.WHMS.Services.StockTransferService;
using RetailSuite.Application.WHMS.Services.RequisitionRcvProductService;
using RetailSuite.Application.WHMS.Services.TransferRcvProductService;
using RetailSuite.Application.Voucher.Services.General;
using RetailSuite.Application.Voucher.Services.Vendor;
using RetailSuite.Application.Voucher.Services.Common;
using RetailSuite.Application.Voucher.Services.App;
using RetailSuite.Application.Voucher.Services.Product;
using Microsoft.AspNetCore.Identity;
using System.Text;
using RetailSuite.Application.CMS.Services.Customer;
using RetailSuite.Application.Voucher.Services.User;
using RetailSuite.Domain.Account.Models;
using RetailSuite.Application.OMS.Services.OmsService;
using RetailSuite.Application.OMS.Services.OrderService;
using RetailSuite.Application.OMS.Services.DeliveryAddressService;
using Microsoft.Extensions.FileProviders;
using RetailSuite.Application.CMS.Services.CampaignServices;
using Microsoft.OpenApi.Models;

//var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers(); ;
/*builder.Services.AddControllers().AddNewtonsoftJson(options =>
    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
);*/
builder.Services.AddCors(options=>options.AddDefaultPolicy(builder =>builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{

    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please Bearer and then token in the field",
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                   {
                     new OpenApiSecurityScheme
                     {
                       Reference = new OpenApiReference
                       {
                         Type = ReferenceType.SecurityScheme,
                         Id = "Bearer"
                       }
                      },
                      new string[] { }
                    }
                });
});

// for entity framework
var conf = builder.Configuration.GetConnectionString("retail");
builder.Services.AddDbContext<RetailDbContext>(opt => opt.UseSqlServer(conf));

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

// for identity
builder.Services.AddIdentity<ApplicationUser, IdentityRole>(options => 
                {
                    options.SignIn.RequireConfirmedEmail = true;
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 4;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;
                    
                })
                .AddEntityFrameworkStores<RetailDbContext>()
                .AddDefaultTokenProviders();

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(options =>
    {
        options.SaveToken = true;
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidAudience =builder.Configuration["JWT:ValidAudience"],
            ValidIssuer = builder.Configuration["JWT:ValidIssuer"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["JWT:Secret"]))
        };
    });

/* Add Brands Scoped starts*/
builder.Services.AddScoped<IBrandRepository, BrandRepository>();
/*End*/

/*Add BunchProductRepository */
builder.Services.AddScoped<IBunchProductRepository, BunchProductRepository>();
/*End*/

/* Add BunchProductTags Repository*/
//builder.Services.AddScoped<IBunchProductTagRepository, BunchProductTagRepository>();
/**/

/*Add Combo Product Repository */
builder.Services.AddScoped<IComboProductRepository, ComboProductRepository>();

/*Add Category Repository*/
builder.Services.AddScoped<ICategoryRepository, CategoryRepository>();
builder.Services.AddScoped<ICustomerRepository, CustomerRepository>();
/*Add Store Repository*/
builder.Services.AddScoped<IStoreRepository, StoreRepository>();
/**/


/*Add Shelve Repository*/
builder.Services.AddScoped<IShelveRepository,ShelveRepository>();
/**/

/*Add Rack Repository*/
builder.Services.AddScoped<IRackRepository, RackRepository>();
/**/

/*Add Bin Repository*/
builder.Services.AddScoped<IBinRepository, BinRepository>();
/**/

/*Add Bin Repository*/
builder.Services.AddScoped<IPurchaseOrderRepository, PurchaseOrderRepository>();
/**/

/*Add Consignment Repository*/
builder.Services.AddScoped<IConsignmentRepository, ConsignmentRepository>();
/**/

/*Add Employee Repository*/
builder.Services.AddScoped<IEmployeeRepository, EmployeeRepositoey>();
/**/

/*Add StoreRequision Repository*/
builder.Services.AddScoped<IStoreRequisionRepository, StoreRequisionRepository>();
/**/


/*Add Vendor Repository*/
builder.Services.AddScoped<IVendorRepository, VendorRepository>();
/**/

/*Add Product Repository*/
builder.Services.AddScoped<IProductRepository, ProductRepository>();
/**/

/* Add Products Shades Repository*/
builder.Services.AddScoped<IProductShadeRepository, ProductShadeRepository>();
/**/
/* Add physical Attribute Repository*/
builder.Services.AddScoped<IPhysicalAttributeRepository, PhysicalAttributeRepository>();
/**/

/* Add IPOReceiveProductRepository */
builder.Services.AddScoped<IPOReceiveProductRepository, POReceiveProductRepository>();

/*add IPODeliveryMethodRepository*/
builder.Services.AddScoped<IPODeliveryMethodRepository, PODeliveryMethodRepository>();
/**/

/* add IConsignmentProductRcvRepository*/
builder.Services.AddScoped<IConsignmentProductRcvRepository, ConsignmentProductRcvRepository>();
/**/

/* add IConsignmentReturnRepository*/
builder.Services.AddScoped<IConsignmentReturnRepository, ConsignmentReturnRepository>();
/**/


/* add IVendorPayment Repository*/
builder.Services.AddScoped<IVendorPaymentRepository, VendorPaymentRepository>();
/**/


/* add IVendorPayment Repository*/
builder.Services.AddScoped<IStockTransferRepository, StockTransferRepository>();
/**/

/* add IRequisitionRcvProductRepository Repository*/
builder.Services.AddScoped<IRequisitionRcvProductRepository, RequisitionRcvProductRepository>();
/**/

/* add ITransferRcvProductRepository Repository*/
builder.Services.AddScoped<ITransferRcvProductRepository, TransferRcvProductRepository>();
/**/


/* add IGeneralVoucherRepository Repository*/
builder.Services.AddScoped<IGeneralVoucherRepository, GeneralVoucherRepository>();
/**/
builder.Services.AddHttpContextAccessor();

/* Add Seller voucher*/
builder.Services.AddScoped<ISellerVoucherRepository, SellerVoucherRepository>();
/**/
/*https://localhost:7090;http://localhost:5090 */

/* Add Voucher Repository */
builder.Services.AddScoped<IVoucherRepository, VoucherRepository>();
/**/

/*Add SaveContext Repository*/
builder.Services.AddScoped<ISaveContexts, SaveContexts>();

/**/

/**/
builder.Services.AddScoped<IAppUserVoucherRepository, AppUserVoucherRepository>();
/**/

/**/
builder.Services.AddScoped<IProductVoucherRepository, ProductVoucherRepository>();
/**/

/**/
builder.Services.AddScoped<IUserVoucherRepository, UserVoucherRepository>();
/**/


/**/
builder.Services.AddScoped<IOmsRepository, OmsRepository>();
/**/

/**/
builder.Services.AddScoped<IOrderRepository, OrderRepository>();
/**/

builder.Services.AddScoped<IDeliveryAddressRepository, DeliveryAddressRepository>();

builder.Services.AddScoped<ICampaignRepository, CampaignRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();
app.UseStaticFiles(new StaticFileOptions{
    FileProvider = new PhysicalFileProvider(
        Path.Combine(Directory.GetCurrentDirectory(), "Images")),
    RequestPath = "/Images"
});
app.UseRouting();
app.UseCors();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
app.Run();

