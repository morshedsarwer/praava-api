﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Vendors.DTOS;
using RetailSuite.Application.Vendors.Services.VendorService;
using RetailSuite.Domain.Vendor.Models;

namespace RetailSuite.API.Controllers.Vendors
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorsController : ControllerBase
    {
        private readonly IVendorRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public VendorsController(IVendorRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadVendorDTO>>> GetAllVendors() 
        {
            try
            {
                var vendors = await _repository.GetVendorsAsync();
                return Ok(_mapper.Map<IEnumerable<ReadVendorDTO>>(vendors));
            }
            catch (Exception ex) {
                return NotFound("Vendors Not Found");
            }
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadVendorDTO>> GetVendorById(int Id) {
            try
            {
                var vendor = await _repository.GetVendorByIdAsync(Id);
                return Ok(_mapper.Map<ReadVendorDTO>(vendor));

            }
            catch (Exception ex) {
                return NotFound("Vendor Not Found");
            }
            
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateVendor(CreateVendorDTO createVendorDTO) 
        {
            try
            {
                // get the username
                var username = HttpContext.User.Identity.Name;
                var vendor = _mapper.Map<Vendor>(createVendorDTO);
                var vndr = await _repository.AddVendor(vendor,username);
                await _saveContext.SaveChangesAsync();
                return Ok(_mapper.Map<ReadVendorDTO>(vndr));
            }
            catch (Exception ex) {
                return BadRequest("Vendor Creation Failed");
            }

        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteVendor(int Id)
        {
            try
            {
                var item = await _repository.GetVendorByIdAsync(Id);
                await _repository.DeleteVendorAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }
            catch (Exception ex){
                return BadRequest("Vendr Delete Failed");
            }
        }

    }
}
