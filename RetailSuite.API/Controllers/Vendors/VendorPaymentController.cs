﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Vendors.DTOS.VendorPaymentDTO;
using RetailSuite.Application.Vendors.Services.VendorPaymentService;
using RetailSuite.Domain.Vendor.Models;

namespace RetailSuite.API.Controllers.Vendors
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorPaymentController : ControllerBase
    {
        private readonly IVendorPaymentRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public VendorPaymentController(IVendorPaymentRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;

        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GetVendorPaymentDTO>>> GetAllVendorPayments() 
        {
            try
            {
                var payments = await _repository.GetVendorPayment();
                return Ok(_mapper.Map<IEnumerable<GetVendorPaymentDTO>>(payments));
            }
            catch (Exception ex) 
            {
                return NotFound("Vendor payment Not Found");
            }
            
        }

        



        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadVendorPaymentDTO>> GetVendorPaymentById(int Id) 
        {
            try
            {
                var payment = await _repository.GetVendorPaymentByIdAsync(Id);
                return Ok(_mapper.Map<ReadVendorPaymentDTO>(payment));
            }
            catch (Exception ex) 
            {
                return NotFound("Vendor Payment Not Found");
            }
            
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateVendorPayment(CreateVendorPaymentDTO createVendorPaymentDTO) 
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var vendorPayment = _mapper.Map<VendorPayment>(createVendorPaymentDTO);
                var vndrpaymnt = await _repository.AddVendorPayment(vendorPayment,username);
                await _saveContext.SaveChangesAsync();
                return Ok(vndrpaymnt);
            }
            catch (Exception ex) 
            {
                return BadRequest("Vendor Payment Creation Failed");
            }
            
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteVendorPayment(int Id) 
        {
            try
            {
                var item = await _repository.GetVendorPaymentByIdAsync(Id);
                await _repository.DeleteVendorPaymentAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }
            catch (Exception ex) 
            {
                return BadRequest("Delete Failed");
            }
            
            
        }

    }
}
