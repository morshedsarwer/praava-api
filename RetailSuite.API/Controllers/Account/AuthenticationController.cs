﻿using Google.Apis.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using RetailSuite.Application.Account;
using RetailSuite.Application.CMS.DTOS.Auth;
using RetailSuite.Application.CMS.POCO.Account;
using RetailSuite.Application.Common.SMS;
using RetailSuite.Domain.Account.Models;
using RetailSuite.Domain.Common;
using RetailSuite.Infrastructure.Contexts;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace RetailSuite.API.Controllers.Account
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly RetailDbContext _context;

        public AuthenticationController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration, RetailDbContext context)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _context = context;

        }
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            var userExistsByUserName = await _userManager.FindByNameAsync(model.Username);
            var userExistsByEmail = await _userManager.FindByEmailAsync(model.Email);

            if (userExistsByUserName != null || userExistsByEmail != null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Failed", Message = "There is already a user" });
            }
            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                UserName = model.Username,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                UserType = UserRoles.User,
                SecurityStamp = Guid.NewGuid().ToString(),
                IsActive = true
            };

            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                return Ok(new Response { Status = "Success", Message = "User Created Successfully" });

            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Failed", Message = "User Creation Failed" });

            }
        }
        [HttpPost]
        [Route("customerlogin")]
        public async Task<IActionResult> CustomerLogin([FromBody] CustomerLoginModel model)
        {

            // put mobile number insted of the username
            var user = await _context.Users.FirstOrDefaultAsync(s => s.PhoneNumber == model.PhoneNumber);
            //user = await _userManager.FindByNameAsync(model.Username);

            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password) && user.UserType == UserRoles.Customer)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,user.UserName),
                    new Claim(ClaimTypes.Sid,user.Id),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));

                }
                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });

            }
            return Unauthorized();

        }
        [HttpPost]
        [Route("gmaillogin")]
        public async Task<IActionResult> GmailLogin([FromBody] ExternalLoginDto externalAuth)
        {
            /*var settings = new GoogleJsonWebSignature.ValidationSettings()
            {
                Audience = new List<string>() { "" }
            };*/
            var payload = await GoogleJsonWebSignature.ValidateAsync(externalAuth.Token);
            if (payload == null)
                return BadRequest("Invalid External Authentication.");
            var info = new UserLoginInfo(externalAuth.Provider, payload.Subject, externalAuth.Provider);
            var user = await _userManager.FindByLoginAsync(info.LoginProvider, info.ProviderKey);
            if (user == null)
            {
                user = await _userManager.FindByEmailAsync(payload.Email);
                if (user == null)
                {
                    user = new ApplicationUser { Email = payload.Email, UserName = payload.Email,UserType="Customer",FirstName=externalAuth.Name,LastName="" };
                    await _userManager.CreateAsync(user);
                    //prepare and send an email for the email confirmation
                    await _userManager.AddToRoleAsync(user, UserRoles.Customer);
                    await _userManager.AddLoginAsync(user, info);
                }
                else
                {
                    await _userManager.AddLoginAsync(user, info);
                }
            }
            if (user == null)
                return BadRequest("Invalid External Authentication.");

            var userData = await _context.Users.FirstOrDefaultAsync(s => s.Email == user.Email);
            if (user != null && user.UserType == UserRoles.Customer)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,user.UserName),
                    new Claim(ClaimTypes.Sid,user.Id),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));

                }
                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }

            return Unauthorized();


        }
        [HttpPost]
        [Route("facebooklogin")]
        public async Task<IActionResult> FacebbokLogin([FromBody] ExternalLoginDto externalAuth)
        {
            /*var settings = new GoogleJsonWebSignature.ValidationSettings()
            {
                Audience = new List<string>() { "" }
            };*/
            var payload = await GoogleJsonWebSignature.ValidateAsync(externalAuth.Token);
            if (payload == null)
                return BadRequest("Invalid External Authentication.");
            var info = new UserLoginInfo(externalAuth.Provider, payload.Subject, externalAuth.Provider);
            var user = await _userManager.FindByLoginAsync(info.LoginProvider, info.ProviderKey);
            if (user == null)
            {
                user = await _userManager.FindByEmailAsync(payload.Email);
                if (user == null)
                {
                    user = new ApplicationUser { Email = payload.Email, UserName = payload.Email, UserType = "Customer", FirstName = externalAuth.Name, LastName = "" };
                    await _userManager.CreateAsync(user);
                    //prepare and send an email for the email confirmation
                    await _userManager.AddToRoleAsync(user, UserRoles.Customer);
                    await _userManager.AddLoginAsync(user, info);
                }
                else
                {
                    await _userManager.AddLoginAsync(user, info);
                }
            }
            if (user == null)
                return BadRequest("Invalid External Authentication.");

            var userData = await _context.Users.FirstOrDefaultAsync(s => s.Email == user.Email);
            if (user != null && user.UserType == UserRoles.Customer)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,user.UserName),
                    new Claim(ClaimTypes.Sid,user.Id),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));

                }
                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }

            return Unauthorized();


        }

        [HttpPost]
        [Route("userlogin")]
        public async Task<IActionResult> UserLogin([FromBody] LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);

            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password) && (user.UserType == UserRoles.User || user.UserType == UserRoles.Admin) && user.IsActive == true)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,user.UserName),
                    new Claim(ClaimTypes.Sid,user.Id),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));

                }
                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });

            }
            return Unauthorized();

        }

        [HttpPost]
        [Route("register-admin")]
        public async Task<IActionResult> RegisterAdmin([FromBody] RegisterModel model)
        {
            var userExistsByUserName = await _userManager.FindByNameAsync(model.Username);
            var userExistsByEmail = await _userManager.FindByEmailAsync(model.Email);

            if (userExistsByUserName != null || userExistsByEmail != null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Failed", Message = "User Already Exists" });
            }

            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                UserName = model.Username,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                UserType = UserRoles.Admin,
                SecurityStamp = Guid.NewGuid().ToString(),
                IsActive = true
            };

            var result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User Creation Failed" });
            }


            // create user and the admin roles
            if (!await _roleManager.RoleExistsAsync(UserRoles.Admin))
            {
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
            }
            if (!await _roleManager.RoleExistsAsync(UserRoles.User))
            {
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.User));
            }

            // now add with the user make them admin
            if (await _roleManager.RoleExistsAsync(UserRoles.Admin))
            {
                await _userManager.AddToRoleAsync(user, UserRoles.Admin);

            }

            return Ok(new Response { Status = "Success", Message = "User Created Successfully" });
        }

        [HttpPost]
        [Route("register-customer")]
        public async Task<IActionResult> RegisterCustomer(CustomerRegistrationModel model)
        {
            var date = DateTime.Now.ToString("yyyyMMddHHmmss");
            var UserName = "FR" + date;
            //var userExistsByUserName = await _userManager.FindByNameAsync(UserName);
            //var userExistsByEmail = await _userManager.FindByEmailAsync(model.Email);
            var phoneNumber = await _context.Users.FirstOrDefaultAsync(s => s.PhoneNumber == model.PhoneNumber);
            if (/*userExistsByEmail != null &&*/ phoneNumber != null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User Already Exists" });

            }
            // other wise create the user with all the information
            ApplicationUser user = new ApplicationUser()
            {
                //Email = model.Email,
                UserName = UserName,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                UserType = UserRoles.Customer,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            // now create the user
            var result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User Creation Failed" });
            }

            // Create the Role First
            if (!await _roleManager.RoleExistsAsync(UserRoles.Customer))
            {
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.Customer));
            }
            // now assign the role to the user
            if (await _roleManager.RoleExistsAsync(UserRoles.Customer))
            {
                var d = await _userManager.AddToRoleAsync(user, UserRoles.Customer);
                if (d != null)
                {

                    var output = await CustomerLoginAfterRegistration(model.PhoneNumber, model.Password);
                    return Ok(output);
                }
                return BadRequest(new Response { Status = "Failed", Message = "Problem Creating User" });
            }
            return BadRequest(new Response { Status = "Failed", Message = "Problem Creating User" });
            // send the confirmation message
            /*var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            //var confirmationLink = Url.Action("ConfirmEmail", "Email", new { token, email = user.Email }, Request.Scheme);
            //var emailHelper = new EmailHelper();
            //bool emailResponse = emailHelper.SendMail(user.Email, confirmationLink);
            if (token !=null)
            {
                return Ok(new Response { Status = "Success", Message = "User Created Successfullt" });

            }
            else
            {
                return BadRequest(new Response { Status = "Failed", Message = "Problem Creating User" });
            }*/

        }

        [HttpPut("customerprofileupdate/{Id}")]
        public async Task<ActionResult> UpdateCustomerProfile(string Id, CustomerUpdateModel customerUpdateModel)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(Id);
                //var user = _userManager.Users.FirstOrDefault();
                user.FirstName = customerUpdateModel.FirstName;
                user.LastName = customerUpdateModel.LastName;
                user.Email = customerUpdateModel.Email;
                user.Address = customerUpdateModel.Address;
                user.PhoneNumber = customerUpdateModel.PhoneNumber;
                await _userManager.UpdateAsync(user);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest("Error Updating The Profile");
            }
        }
        [Authorize]
        [HttpPut("profileupdate")]
        public async Task<ActionResult> UpdateCustomerProfileByPhoneNumber(CustomerUpdateModel customerUpdateModel)
        {
            try
            {
                //var user = await _userManager.FindByIdAsync(Id);
                var userName = HttpContext.User.Identity.Name;
                var user = await _userManager.Users.FirstOrDefaultAsync(s => s.UserName == userName);
                user.FirstName = customerUpdateModel.FirstName;
                user.LastName = customerUpdateModel.LastName;
                user.Email = customerUpdateModel.Email;
                user.Address = customerUpdateModel.Address;
                user.PhoneNumber = customerUpdateModel.PhoneNumber;
                await _userManager.UpdateAsync(user);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest("Error Updating The Profile");
            }
        }
        [Authorize]
        [HttpGet("getprofile")]
        public async Task<ActionResult> GetProfile()
        {
            try
            {
                var userName = HttpContext.User.Identity.Name;
                var user = await _userManager.Users.FirstOrDefaultAsync(s => s.UserName == userName);
                var profile = new CustomerUpdateModel()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    Address = user.Address,
                    PhoneNumber = user.PhoneNumber
                };
                return Ok(profile);
            }
            catch (Exception ex)
            {
                return BadRequest("Error Updating The Profile");
            }
        }
        [HttpPut("passwordreset")]
        public async Task<ActionResult> ResetPassword(ResetPassword resetPassword)
        {
            try
            {
                var user = _userManager.Users.FirstOrDefault(s => s.PhoneNumber == resetPassword.Phone);
                if(user != null)
                {
                    if (user.UserType == "Customer")
                    {
                        await _userManager.RemovePasswordAsync(user);
                        await _userManager.AddPasswordAsync(user, resetPassword.Password);
                        return Ok(user);
                    }
                    return BadRequest("Error reseting password");
                }
                return BadRequest("Error reseting password");
            }
            catch (Exception ex)
            {
                return BadRequest("Error reseting password");
            }
        }
        [HttpGet("otp")]
        public async Task<ActionResult> Otp(string Phone)
        {
            try
            {
                var user = _context.Users.FirstOrDefault(s => s.PhoneNumber == Phone);
                if (user != null)
                {
                    if (user.UserType == "Customer")
                    {
                        var time = DateTime.Now;
                        int num = new Random().Next(1000, 9999);
                        var otp = $"your otp is {num}";
                        var newOtp = new Otp
                        {
                            OTP = num,
                            Phone = Phone,
                            Expiration = time.AddMinutes(2),
                        };
                        _context.Otps.Add(newOtp);
                        _context.SaveChanges();
                        await SendSMS.Send(Phone, otp);
                        return Ok(true);
                    }
                    return BadRequest(false);
                }
                return BadRequest(false);
            }
            catch (Exception ex)
            {
                return BadRequest("error while generate otp");
            }
        }
        [HttpGet("checkotp")]
        public async Task<ActionResult> CheckOtp(int otp)
        {
            try
            {
                var result = _context.Otps.FirstOrDefault(s => s.OTP == otp);
                var number = result.Phone;
                if (result != null && result.Expiration >= DateTime.Now)
                {
                    _context.Otps.Remove(result);
                    _context.SaveChangesAsync();
                    return Ok(number);
                }
                return BadRequest("does not match");
            }
            catch (Exception ex)
            {
                return BadRequest("error while generate otp");
            }
        }

        [HttpGet("users")]
        public async Task<ActionResult<IEnumerable<IEnumerable<UserPoco>>>> Users()
        {
            try
            {
                var user = from u in _context.Users
                           where u.IsActive == true && (u.UserType == UserRoles.User || u.UserType == UserRoles.Admin)
                           select new UserPoco()
                           {
                               IsActive = u.IsActive,
                               Email = u.Email,
                               Name = u.FirstName + " " + u.LastName,
                               Phone = u.PhoneNumber,
                               UserType = u.UserType,
                               Id = u.Id,
                           };
                return Ok(user);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [HttpPost("userstatus")]
        public async Task<ActionResult> UserStatus(string email)
        {
            try
            {
                var user = _context.Users.FirstOrDefault(x => x.Email == email);
                if (user != null)
                {
                    user.IsActive = false;
                    _context.SaveChanges();
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpGet("customers")]
        public async Task<ActionResult<IEnumerable<IEnumerable<UserPoco>>>> Customers()
        {
            try
            {
                var user = from u in _context.Users
                           where u.UserType == UserRoles.Customer
                           select new UserPoco()
                           {
                               IsActive = u.IsActive,
                               Email = u.Email,
                               Name = u.FirstName + " " + u.LastName,
                               Phone = u.PhoneNumber,
                               UserType = u.UserType,
                               Id = u.Id,
                           };
                return Ok(user);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [NonAction]
        public async Task<dynamic> CustomerLoginAfterRegistration(string PhoneNumber, string Password)
        {

            // put mobile number insted of the username
            var loginuser = await _context.Users.FirstOrDefaultAsync(s => s.PhoneNumber == PhoneNumber);
            //user = await _userManager.FindByNameAsync(model.Username);

            if (loginuser != null && await _userManager.CheckPasswordAsync(loginuser, Password) && loginuser.UserType == UserRoles.Customer)
            {
                var userRoles = await _userManager.GetRolesAsync(loginuser);
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,loginuser.UserName),
                    new Claim(ClaimTypes.Sid,loginuser.Id),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));

                }
                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

                return new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                };
            }
            return Unauthorized("problem login In");
        }
    }
}
