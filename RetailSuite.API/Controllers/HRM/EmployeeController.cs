﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.HRM.DTOS.EmployeeDTO;
using RetailSuite.Application.HRM.Services.EmployeeService;
using RetailSuite.Domain.HRM.Employee;

namespace RetailSuite.API.Controllers.HRM
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public EmployeeController(IEmployeeRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadEmployeeDTO>>> GetAllEmployees() 
        {
            try
            {
                var employees = await _repository.GetEmployeesAsync();
                return Ok(_mapper.Map<IEnumerable<ReadEmployeeDTO>>(employees));
            }
            catch 
            {
                return NotFound("Employees Not Found");   
            }
            
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<Employee>> GetEmployeeById(int Id) 
        {
            try
            {
                var employee = await _repository.GetEmployeeByIdAsync(Id);
                return Ok(_mapper.Map<ReadEmployeeDTO>(employee));
            }
            catch {
                return NotFound("Employee Not Found");
            }
            

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateEmployee(CreateEmployeeDTO createEmployeeDTO)
        {
            try {
                var username = HttpContext.User.Identity.Name;
                var employee = _mapper.Map<Employee>(createEmployeeDTO);
                var emp = await _repository.AddEmployee(employee,username);
                await _saveContext.SaveChangesAsync();
                return Ok(_mapper.Map<ReadEmployeeDTO>(emp));
            }catch{
                return BadRequest("Employee Creation Failed");
            }
            
        
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteEmployee(int Id) 
        {
            try
            {
                var item = await _repository.GetEmployeeByIdAsync(Id);
                await _repository.DeleteEmployeeAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }
            catch { 
            
                return BadRequest("Delete Failed");
            }
            
        }


    }
}
