﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.OMS.DTOS.DeliveryAddressDTO;
using RetailSuite.Application.OMS.POCO;
using RetailSuite.Application.OMS.Services.DeliveryAddressService;
using RetailSuite.Domain.OMS;

namespace RetailSuite.API.Controllers.OMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveryAddressController : ControllerBase
    {
        private readonly IDeliveryAddressRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public DeliveryAddressController(IDeliveryAddressRepository repository, IMapper mapper, ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadDeliveryAddressDTO>> GetDeliveryAddressById(int Id)
        {
            try
            {
                var address = await _repository.GetDeliveryAddressById(Id);
                return Ok(_mapper.Map<ReadDeliveryAddressDTO>(address));
            }
            catch (Exception)
            {
                return BadRequest("Not Found");
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<DeliveryAddress>> CreateDeliveryAddress(CreateDeliveryAddressDTO createDeliveryAddressDTO)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var address = _mapper.Map<DeliveryAddress>(createDeliveryAddressDTO);
                var output = await _repository.AddDeliveryAddress(address, username);
                await _saveContext.SaveChangesAsync();
                return Ok(output);
            }
            catch (Exception)
            {
                return BadRequest("Delivery Address Creation Failed");
            }
        }

        [HttpPut("{Id}")]
        public async Task<ActionResult> UpdateDeliveryAddress(int Id, UpdateDeliveryAddressDTO updateDeliveryAddressDTO)
        {
            try
            {
                var oldAddress = await _repository.GetDeliveryAddressById(Id);
                // mapp the data
                _mapper.Map(updateDeliveryAddressDTO, oldAddress);
                await _saveContext.SaveChangesAsync();
                return Ok(oldAddress);
            }

            catch (Exception ex)
            {
                return BadRequest("Update Failed");
            }
        }
        [HttpGet("getaddress")]
        public async Task<ActionResult<IEnumerable<DivisionPoco>>> GetAddress()
        {
            try
            {
                var data = _repository.GetAddress();
                return Ok(data);
            }
            catch (Exception)
            {
                return BadRequest("Not Found");
            }
        }
        [Authorize]
        [HttpGet("getaddressbycustomer")]
        public async Task<ActionResult<IEnumerable<DeliveryAddPoco>>> GetAddressByCustomer()
        {
            try
            {
                var username = HttpContext.User.Identity.Name;                
                var user = _saveContext.GetUserFromUserName(username);
                return Ok(_repository.GetDeliveryAddresses(user.Id));
            }
            catch (Exception)
            {
                return BadRequest("Not Found");
            }
        }
        [HttpGet("customeraddress/{customerId}")]
        public async Task<ActionResult<IEnumerable<DeliveryAddPoco>>> GetCustomerAddressById(string customerId)
        {
            try
            {
                return Ok(_repository.GetDeliveryAddresses(customerId));
            }
            catch (Exception)
            {
                return BadRequest("Customer Address Not Found");
            }
        }
        [HttpGet("getzones/{districId}")]
        public async Task<ActionResult<IEnumerable<DivisionPoco>>> GetZones(int districId)
        {
            try
            {
                if (districId > 0)
                {
                    return Ok(_repository.GetZonesByDistrict(districId));
                }
                return BadRequest("check your id");
            }
            catch (Exception)
            {
                return BadRequest("Zone Not Found");
            }
        }

        [HttpDelete("{AddressId}")]
        public async Task<ActionResult> DeleteDeliveryAddress(int AddressId)
        {
            try
            {
                await _repository.DeleteDeliveryAddres(AddressId);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch (Exception)
            {
                return BadRequest("Delete Failed");
            }
        }

    }
}
