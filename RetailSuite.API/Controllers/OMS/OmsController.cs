﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.OMS.DTOS.DeliveryAddressDTO;
using RetailSuite.Application.OMS.DTOS.DistrictDTO;
using RetailSuite.Application.OMS.DTOS.DivisionDTO;
using RetailSuite.Application.OMS.DTOS.OrderCommentsDTO;
using RetailSuite.Application.OMS.DTOS.ZoneDTO;
using RetailSuite.Application.OMS.POCO;
using RetailSuite.Application.OMS.Services.OmsService;
using RetailSuite.Domain.OMS;

namespace RetailSuite.API.Controllers.OMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class OmsController : ControllerBase
    {
        private readonly IOmsRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public OmsController(IOmsRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;


        }

        [HttpGet("division")]
        public async Task<ActionResult<IEnumerable<ReadDivisionDTO>>> GetAllDivision()
        {
            try
            {
                var divisions = await _repository.GetAllDivision();
                return Ok(_mapper.Map<IEnumerable<ReadDivisionDTO>>(divisions));
            }
            catch
            {
                return BadRequest("Division Not Found");
            }
        }

        [HttpGet("district")]
        public async Task<ActionResult<IEnumerable<ReadDistrictDTO>>> GetAllDistrict()
        {
            try
            {
                var district = await _repository.GetAllDistrict();
                return Ok(_mapper.Map<IEnumerable<ReadDistrictDTO>>(district));

            }
            catch
            {
                return BadRequest("District Not Found");
            }
        }

        [HttpGet("zone")]
        public async Task<ActionResult<IEnumerable<ZonePoco>>> GetAllZone()
        {
            try
            {
                var zone = await _repository.GetAllZones();
                return Ok(zone);
            }
            catch (Exception)
            {

                return BadRequest("Zone Not Found");
            }
        }
        
        [HttpGet("deliveryaddress")]
        public async Task<ActionResult<IEnumerable<ReadDeliveryAddressDTO>>> GetAllDeliveryAddress()
        {
            try
            {
                var addresses = await _repository.GetAllDeliveryAddress();
                return Ok(_mapper.Map<IEnumerable<ReadDeliveryAddressDTO>>(addresses));

            }
            catch
            {
                return BadRequest("Delivery address Not Found");
            }
        }

        [HttpGet("division/{Id}")]
        public async Task<ActionResult<ReadDivisionDTO>>GetDivisionById(int Id)
        {
            try
            {
                var division = await _repository.GetDivisionById(Id);
                return Ok(_mapper.Map<ReadDivisionDTO>(division));
            }
            catch
            {
                return BadRequest("Division Not Found");
            }
            

        }

        [HttpGet("district/{Id}")]
        public async Task<ActionResult<ReadDistrictDTO>> GetDistrictById(int Id)
        {
            try
            {
                var district = await _repository.GetDistrictById(Id);
                return Ok(_mapper.Map<ReadDistrictDTO>(district));
            }
            catch
            {
                return BadRequest("District Not Found"); ;
            }
            

        }

        [HttpGet("zone/{Id}")]
        public async Task<ActionResult<ReadZoneDTO>> GetZoneById(int Id)
        {
            try
            {
                var zone = await _repository.GetZonesById(Id);
                return Ok(_mapper.Map<ReadZoneDTO>(zone));
            }
            catch
            {
                return BadRequest("Zone Not Found");
            }
            

        }

        [HttpGet("deliveryaddress/{Id}")]
        public async Task<ActionResult<ReadDeliveryAddressDTO>> GetDeliveryAddressById(int Id)
        {
            try 
            {
                var address = await _repository.GetDeliveryAddressById(Id);
                return Ok(_mapper.Map<ReadDeliveryAddressDTO>(address));
            }
            catch
            {
                return BadRequest("Delivery Address Not Found");
            }
            

        }

        [HttpPost("division/new")]
        public async Task<ActionResult> CreateDivision(CreateDivisionDTO createDivisionDTO)
        {
            try
            {
                var division = _mapper.Map<Division>(createDivisionDTO);
                var output = await _repository.AddDivision(division);
                return Ok(output);
            }
            catch
            {
                return BadRequest("Division Creation Failed");
            }
            
        }

        [HttpPost("district/new")]
        public async Task<ActionResult> CreateDistrict(CreateDistrictDTO createDistrictDTO)
        {
            try
            {
                var district = _mapper.Map<District>(createDistrictDTO);
                var output = await _repository.AddDistrict(district);
                return Ok(output);
            }
            catch
            {
                return BadRequest("district Creation Failed");
            }
            
        }

        [HttpPost("zone/new")]
        public async Task<ActionResult> CreateZone(CreateZoneDTO createZoneDTO)
        {
            try
            {
                var zone = _mapper.Map<Zone>(createZoneDTO);
                var output = await _repository.AddZone(zone);
                return Ok(output);
            }
            catch
            {
                return BadRequest("Zone Creation Failed");
            }
            
        }

        [HttpPost("deliveryaddress/new")]
        public async Task<ActionResult> CreateZone(CreateDeliveryAddressDTO createDeliveryAddressDTO)
        {
            try
            {
                var deliveryAddress = _mapper.Map<DeliveryAddress>(createDeliveryAddressDTO);
                var output = await _repository.AddDeliveryAddress(deliveryAddress);
                return Ok(output);
            }
            catch
            {
                return BadRequest("Delivery Address Creation Failed");
            }
            
        }

        [HttpPut("UpdateDivision/{Id}")]
        public async Task<ActionResult> UpdateDivision(int Id,UpdateDivisionDTO updateDivisionDTO)
        {
            try
            {
                var oldDivision = await _repository.GetDivisionById(Id);
                _mapper.Map(updateDivisionDTO, oldDivision);
                await _saveContext.SaveChangesAsync();
                return Ok(oldDivision);
            }
            catch
            {
                return BadRequest("Update Failed");
            }           

        }

        [HttpPut("UpdateDistrict/{Id}")]
        public async Task<ActionResult> UpdateDistrict(int Id,UpdateDistrictDTO updateDistrictDTO)
        {
            try
            {
                var oldDistrict = await _repository.GetDistrictById(Id);
                _mapper.Map(updateDistrictDTO, oldDistrict);
                await _saveContext.SaveChangesAsync();
                return Ok(oldDistrict);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            
        }

        [HttpPut("UpdateZone/{Id}")]
        public async Task<ActionResult> UpdateZone(int Id,UpdateZoneDTO updateZoneDTO)
        {
            try
            {
                var oldzone = await _repository.GetZonesById(Id);
                _mapper.Map(updateZoneDTO, oldzone);
                await _saveContext.SaveChangesAsync();
                return Ok(oldzone);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            

        }

        [HttpPost("createComments")]
        public async Task<ActionResult>CreateComments(CreateOrderCommentDTO createOrderCommentDTO)
        {
            try
            {
                var orderComments = _mapper.Map<OrderComments>(createOrderCommentDTO);
                _repository.AddOrderComments(orderComments);
                await _saveContext.SaveChangesAsync();
                return Ok("Coments Created");
            }
            catch
            {
                return BadRequest("Comments Creation Failed");
            }
        }
       
        [HttpGet("getallcomments/{orderId}")]
        public async Task<ActionResult> GetAllComments(int orderId) 
        {
            try
            {
                var comments = await _repository.GetAllOrderComments(orderId);
                return Ok(_mapper.Map<IEnumerable<ReadOrderCommentDTO>>(comments));
            }
            catch
            {
                return BadRequest("Comments Not Found");
            }
            
        }

        [HttpDelete("deleteComment/{Id}")]
        public async Task<ActionResult>DeleteComment(int Id)
        {
            try
            {
                await _repository.DeleteOrderComments(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
            
        }
    }
}
