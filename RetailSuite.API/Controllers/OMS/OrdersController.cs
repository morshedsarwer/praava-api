﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RetailSuite.Application.Common.SMS;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.OMS.DTOS.OrderDTO;
using RetailSuite.Application.OMS.PaymentGateway;
using RetailSuite.Application.OMS.Services.OrderService;
using RetailSuite.Application.OMS.ViewModel;
using RetailSuite.Application.WHMS.POCO;
using RetailSuite.Domain.Account.Models;
using RetailSuite.Domain.OMS;
using RetailSuite.Infrastructure.Contexts;
using System.Collections.Specialized;
using System.Text;

namespace RetailSuite.API.Controllers.OMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        private readonly RetailDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;

        public OrderController(IOrderRepository repository, IMapper mapper, ISaveContexts saveContexts, RetailDbContext context,IWebHostEnvironment hostEnvironment)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
            _context = context;
            _hostEnvironment = hostEnvironment;
        }
        [HttpGet("test")]
        public async Task<ActionResult<dynamic>> Test()
        {
            try
            {
                var orders = await _repository.GetOrdersAsync();
                //return Ok(_mapper.Map<IEnumerable<ReadOrderDTO>>(orders));
                return Ok(_context.Districts.ToList());
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }
        [HttpGet]
        public async Task<ActionResult<dynamic>> GetAllOrders()
        {
            try
            {
                var orders = await _repository.GetOrdersAsync();
                //return Ok(_mapper.Map<IEnumerable<ReadOrderDTO>>(orders));
                return Ok(orders);
            }
            catch (Exception ex)
            {
                return NotFound("Orders Not Found");
            }

        }
        [HttpGet("OrderStatues")]
        public async Task<ActionResult> OrderStatues()
        {
            try
            {
                var status = _context.OrderStatuses.ToList();
                return Ok(status);
            }
            catch (Exception)
            {
                return BadRequest("Order Status Not Found");
            }
        }
        [HttpPost("changestatus/{oderId}")]
        public async Task<ActionResult> ChangeStatus(int oderId, ChangeOrderStatus changeOrder)
        {
            try
            {
                var status = _context.Orders.FirstOrDefault(x => x.OrderId == oderId);
                if (status != null)
                {
                    status.OrderStatus = changeOrder.StatusId;
                    var phone = _context.Users.FirstOrDefault(x => x.Id == status.CustomerID);
                    await _repository.SendMessage(phone.PhoneNumber, changeOrder.StatusId, status.OrderTrnId);
                    await _context.SaveChangesAsync();
                    return Ok(status);
                }
                return BadRequest("Status Not Changed");
            }
            catch (Exception)
            {
                return BadRequest("Status Change Failed");
            }
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<OrderContextViewModel>> GetOrderById(int Id)
        {
            try
            {
                var order = await _repository.GetOrderbyIdAsync(Id);
                return order;
                //return Ok(_mapper.Map<ReadOrderDTO>(order));
            }
            catch (Exception ex)
            {
                return NotFound("Not Found");
            }
        }

        [HttpPut("{Id}")]
        public async Task<ActionResult> UpdateOrderByOrderId(int Id, UpdateOrderDTO updateOrderDTO)
        {
            try
            {
                var oldOrder = await _repository.GetOrderbyIdAsync(Id);
                _mapper.Map(updateOrderDTO, oldOrder.Order);
                await _saveContext.SaveChangesAsync();
                return Ok(oldOrder.Order);
            }
            catch
            {
                return BadRequest("Update Failed");
            }

        }

        //[Authorize]
        [HttpGet("getorderbycode/{code}")]
        public async Task<ActionResult<object>> GetOrderByCode(string code)
        {
            try
            {
                var data = await _repository.GetOrderbyCodeAsync(code);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return NotFound("Order Not Found");
            }
        }
        [Authorize]
        [HttpGet("getorderbycustomer")]
        public async Task<ActionResult<object>> OrdersByCustomer()
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var userId = _context.Users.FirstOrDefault(x => x.UserName == username);
                //var id = "1f223748-b0ea-4fec-96ad-c3133ff89fff";
                if (userId != null)
                {
                    var data = await _repository.OrdersByCustomer(userId.Id);
                    return Ok(data);
                }
                return NotFound("Orders not found");
            }
            catch (Exception ex)
            {
                return NotFound("Order not found");
            }
        }

        [HttpGet("GetByCustomerId/{customerId}")]
        public async Task<ActionResult<dynamic>> GetOrderByCustomerId(string customerId)
        {
            try
            {
                var order = await _repository.GetOrderByCustomerId(customerId);
                return Ok(order);
            }
            catch
            {
                return BadRequest("Order Not Found");
            }


        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateOrder(OrderViewModel orderView)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                //var username = "FR20220220145601";
                var order = _mapper.Map<Order>(orderView.CreateOrderDTO);
                var orderproducts = _mapper.Map<IEnumerable<OrderProducts>>(orderView.OrderProductsDTOs);
                var orderCombo = _mapper.Map<IEnumerable<OrderCombo>>(orderView.OrderComboDTO);
                var orderCampaign = _mapper.Map<IEnumerable<OrderCampaign>>(orderView.CampaignDTOs);
                //var recuring = _mapper.Map<IEnumerable<RecurringOrder>>(orderView.CreateRecurringDTOs);
                order.OrderEditBy = username;
                order.EditDate = DateTime.Now;
                var context = new OrderContextViewModel { Order = order, OrderProducts = orderproducts, OrderCampaigns = orderCampaign, OrderCombos = orderCombo };
                //string demo = "";
                int lastShowPieceId = 0;
                var orderData = _context.Orders.FirstOrDefault();
                if (orderData != null)
                {
                    lastShowPieceId = _context.Orders.Max(x => x.OrderId);
                }
                var demo = Convert.ToString(lastShowPieceId + 1).PadLeft(4, '0');
                //int lastId = _context.Orders.FirstOrDefault(x => x.OrderId == lastShowPieceId);

                string TrnCode = "FR" + demo;
                var result = await _repository.AddOrder(context, username, TrnCode);
                if (result == false)
                    return BadRequest();
                var customerPhone = _context.Users.FirstOrDefault(x => x.Id == context.Order.CustomerID);
                await SendSMS.Send(customerPhone.PhoneNumber, $"Your order {context.Order.OrderTrnId} has been placed");
                return Ok(new { TrnCode = TrnCode, SslUrl = string.Empty });
            }
            catch (Exception ex)
            {
                return BadRequest("Order Creation Failed");
            }
        }
        [Authorize(Roles = UserRoles.Customer)]
        [HttpPost("checkout")]
        public async Task<ActionResult> Checkout(OrderViewModel orderView)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                //var username = "FR20220314224133";
                var order = _mapper.Map<Order>(orderView.CreateOrderDTO);
                //order.ImageName = await SaveImage(order.ImageFile);
                //order.ImageUrl = Path.Combine("Images", order.ImageName);

                var orderproducts = _mapper.Map<IEnumerable<OrderProducts>>(orderView.OrderProductsDTOs);
                var orderCombo = _mapper.Map<IEnumerable<OrderCombo>>(orderView.OrderComboDTO);
                var orderCampaign = _mapper.Map<IEnumerable<OrderCampaign>>(orderView.CampaignDTOs);
                var recuring = _mapper.Map<IEnumerable<RecurringOrder>>(orderView.CreateRecurringDTOs);
                var context = new OrderContextViewModel { Order = order, OrderProducts = orderproducts, OrderCampaigns = orderCampaign, OrderCombos = orderCombo,RecurringOrders = recuring };
                //string demo = "";
                int lastShowPieceId = 0;
                var orderData = _context.Orders.FirstOrDefault();
                if (orderData != null)
                {
                    lastShowPieceId = _context.Orders.Max(x => x.OrderId);
                }
                var demo = Convert.ToString(lastShowPieceId + 1).PadLeft(4, '0');
                //int lastId = _context.Orders.FirstOrDefault(x => x.OrderId == lastShowPieceId);

                string TrnCode = "FR" + demo;
                if (orderView.CreateOrderDTO.PaymentType == 2)
                {
                    string baseUrl = Request.Scheme + "://" + Request.Host;
                    var result = await _repository.Checkout(context, baseUrl, username, TrnCode);
                    if (result == null)
                    {
                        return BadRequest();
                    }
                    
                    return Ok(new { TrnCode = TrnCode, SslUrl = result });
                }
                else if (orderView.CreateOrderDTO.PaymentType == 1)
                {
                    var result = await _repository.AddOrder(context, username, TrnCode);
                    if (result == false)
                    {
                        return BadRequest();
                    }
                    var customerPhone = _context.Users.FirstOrDefault(x => x.Id == context.Order.CustomerID);
                    await SendSMS.Send(customerPhone.PhoneNumber, $"Your order {context.Order.OrderTrnId} has been placed");
                    return Ok(new { TrnCode = TrnCode, SslUrl = string.Empty });
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return BadRequest("Order Creation Failed");
            }
        }

        [HttpPost("OrderImage")]
        public async Task<ActionResult>OrderImage([FromForm] OrderImage orderImage)
        {
            try
            {
                var order = await _context.Orders.FirstOrDefaultAsync(s => s.OrderTrnId == orderImage.TrnCode);
                order.ImageName = await SaveImage(orderImage.Image);
                order.ImageUrl = Path.Combine("Images", order.ImageName);
                await _saveContext.SaveChangesAsync();
                return Ok("Image Uploaded Successfully");
            }
            catch
            {
                return BadRequest("Image Upload Failed");
            }
            
            
        }

        [NonAction]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            try
            {
                string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
                imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
                var path = Path.Combine(_hostEnvironment.ContentRootPath, "Images");
                System.IO.Directory.CreateDirectory(path);
                var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images", imageName);
                using (var fileStream = new FileStream(imagePath, FileMode.Create))
                {
                    await imageFile.CopyToAsync(fileStream);
                }
                return imageName;
            }
            catch (Exception)
            {

                throw;
            }

        }



        [HttpGet("gatewayconfirmation/{TrnCode}")]
        public async Task<ActionResult> GatewayConfirmation(string TrnCode)
        {
            try
            {
                var customer = _context.Orders.FirstOrDefault(z=>z.OrderTrnId == TrnCode);
                var customerPhone = _context.Users.FirstOrDefault(x => x.Id == customer.CustomerID);
                await SendSMS.Send(customerPhone.PhoneNumber, $"Your order {customer.OrderTrnId} has been placed");
                //var gateway = _mapper.Map<GatewayTransaction>(gatewayTrn);
                _repository.GatewayConfirmation(TrnCode);
                return Redirect("https://farmroots.com.bd");
            }
            catch (Exception)
            {
                return BadRequest("Failed");
            }
        }
        /*[HttpGet("orderstatus")]
        public async Task<ActionResult> OrderStatus()
        {
            return Ok();
        }
        [HttpPost("changeorderstatus")]
        public async Task<ActionResult> ChangeOrderStatus(GatewayTrnDTO gatewayTrn)
        {
        }*/
        [HttpGet("sms")]
        public async Task<IActionResult> SendSms()
        {
            /*var sms = new OrderSms()
            {
                CustomerName = "Morshed",
                DeliveryDate = DateTime.Now,
                OrderDate = DateTime.Now,
                OrderTrn = "FR0014521",
                TotalAmount = 1200
            };
            var respon = new ApiResponse();
            using (var httpClient = new HttpClient())
            {
                string message = "Your Order FR000125 has been placed";
                string phone = "01894840889";
                StringContent content = new StringContent(JsonConvert.SerializeObject(sms), Encoding.UTF8, "application/json");
                using (var response = await httpClient.GetAsync($"http://202.164.208.212/smsnet/bulk/api?api_key=d65b96c32bfce31ac69ba2c035676c30255&mask=FARMROOTS&recipient={phone}&message={message}"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    respon = JsonConvert.DeserializeObject<ApiResponse>(apiResponse);
                    //Console.WriteLine(respon);
                }
            }*/
            var response = await SendSMS.Send("01833252133", "new method");
            return Ok(response);
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteOrder(int Id)
        {
            try
            {
                await _repository.DeleteOrderAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch (Exception ex)
            {
                return BadRequest("Delete Failed");
            }
        }

        [HttpPut("UpdateOrderProducts/{Code}")]
        public async Task<ActionResult<OrderProducts>> UpdateOrderProducts(string Code, IEnumerable<UpdateOrderProductsDTO> OrderProductsList)
        {
            try
            {
                var orderProducts = _mapper.Map<IEnumerable<OrderProducts>>(OrderProductsList);
                var newOrderProducts = await _repository.UpdateOrderProductsByCode(Code, orderProducts);
                return Ok(newOrderProducts);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
        }
        [HttpPut("updateordercampaign/{Code}")]
        public async Task<ActionResult<OrderCampaign>> UpdateOrderCampaign(string Code, IEnumerable<UpdateOrderCampaign> orderCampaigns)
        {
            try
            {
                var campaigns = _mapper.Map<IEnumerable<OrderCampaign>>(orderCampaigns);
                var newOrderProducts = await _repository.UpdateOrderCampaignByCode(Code, campaigns);
                return Ok(newOrderProducts);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
        }

        [HttpPut("updateordercombo/{Code}")]
        public async Task<ActionResult<OrderCampaign>> UpdateOrderCombo(string Code, IEnumerable<OrderComboDTO> orderCombos)
        {
            try
            {
                var combos = _mapper.Map<IEnumerable<OrderCombo>>(orderCombos);
                var newOrderComboProducts = await _repository.UpdateOrderComboByCode(Code, combos);
                return Ok(newOrderComboProducts);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
        }

        //[HttpGet("orders/{storeId}/{fromDate}/{DateTime toDate}/{phoneNumber}/{paymentType}")]
        //[HttpGet("orders/{paymentType}/{fromDate}/{toDate}/{status}/{OrderId}")]
        //[Authorize]
        [HttpGet("orders")]
        public async Task<ActionResult<object>> Orders(int paymentType, DateTime fromDate, DateTime toDate, int status, string? OrderId = null)
        {
            try
            {
                if (string.IsNullOrEmpty(OrderId))
                {
                    var data = await _repository.Orders(paymentType, fromDate, toDate, status);
                    return Ok(data);
                }
                return Ok(await _repository.Orders(OrderId));
            }
            catch (Exception)
            {
                return BadRequest("Not Found");
            }
        }
        [HttpGet("defaultorders")]
        public async Task<ActionResult<object>> DefaultOrders()
        {
            try
            {
                int paymentType = 0;
                var fromDate = DateTime.Today.AddDays(-5);
                var toDate = DateTime.Now;
                var status = 0;
                var data = await _repository.Orders(paymentType, fromDate, toDate, status);
                return Ok(data);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [Authorize]
        [HttpPost("addorderproduct")]
        public async Task<ActionResult> AddOrderProduct(AddOrderProduct orderProduct)
        {
            try
            {
                var product = _mapper.Map<OrderProducts>(orderProduct);
                var result = await _repository.AddOrderProdcut(product);
                if (result == true)
                {
                    return Ok("added new product");
                }
                return BadRequest("error in adding new product");
            }
            catch (Exception)
            {
                return BadRequest("error in adding new product");
            }
        }
    }
}
