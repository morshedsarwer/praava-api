﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.ConsignmentDTO;
using RetailSuite.Application.WHMS.Services.ConsignmentService;
using RetailSuite.Application.WHMS.ViewModel.ConsignmentViewModel;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsignmentsController : ControllerBase
    {
        private readonly IConsignmentRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public ConsignmentsController(IConsignmentRepository repository,IMapper mapper,ISaveContexts saveContexts )
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadConsignmentDTO>>> GetAllConsignments() 
        {
            try
            {
                var consignments = await _repository.GetConsignmentAsync();
                return Ok(_mapper.Map<IEnumerable<ReadConsignmentDTO>>(consignments));
            }
            catch {
                return NotFound("consignment Not found");
            }
            
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadConsignmentDTO>> GetConsignmentById(int Id) 
        {
            try
            {
                var consignment = await _repository.GetConsignmentByIdAsync(Id);
                return Ok(_mapper.Map<ReadConsignmentDTO>(consignment));
            }
            catch 
            {
                return NotFound("Consignment Not Found");
            }
            

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateConsignment(ConsignmentViewModel consignmentViewModel) 
        {
            try
            {
                // get the logged in username
                var username = HttpContext.User.Identity.Name;
                var consignment = _mapper.Map<Consignment>(consignmentViewModel.CreateConsignmentDTO);
                var consignmentProducts = _mapper.Map<IEnumerable<ConsignmentProduct>>(consignmentViewModel.ConsignmentProductDTOs);
                var context = new ConsignmentContextViewModel { Consignment = consignment, ConsignmentProducts = consignmentProducts };
                _repository.AddConsignment(context,username);
                await _saveContext.SaveChangesAsync();
                return Ok();
            }
            catch {
                return BadRequest("Consignment Creation Failed");
            }
            
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteConsignment(int Id)
        {
            try
            {
                var item = await _repository.GetConsignmentByIdAsync(Id);
                await _repository.DeleteConsignmentAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch 
            {
                return BadRequest("Delete Failed");
            }
            
        
        }
       
    }
}
