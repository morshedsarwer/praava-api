﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.PODTO;
using RetailSuite.Application.WHMS.Services.PurchaseOrderService;
using RetailSuite.Application.WHMS.ViewModel;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchaseOrdersController : ControllerBase
    {
        private readonly IPurchaseOrderRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public PurchaseOrdersController(IPurchaseOrderRepository repository,IMapper mapper,ISaveContexts saveContext)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContext;

        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadPODTO>>> GetAllPurchaseOrders() {

            try {
                var purchaseOrders = await _repository.GetPurchaseOrderAsync();
                return Ok(_mapper.Map<IEnumerable<ReadPODTO>>(purchaseOrders));
            }catch{
                return NotFound("Purchase Order Not Found");
            }
            
        }



        [HttpGet("{Id}")]
        public async Task<ActionResult<POContextViewModel>> GetPurchaseOrderById(int Id)
        {
            try
            {
                var poContext = await _repository.GetPurchaseOrderById(Id);
                return Ok(poContext);
            }
            catch 
            {
                return NotFound("Purchase Order Not Found");
            }
            


        }




        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreatePurchaseOrder(POViewModels pOViewModels)
        {
            try
            {
                // get the user
                var username = HttpContext.User.Identity.Name;
                var purchaseOrder = _mapper.Map<PurchaseOrder>(pOViewModels.CreatePODTO);
                var poProducts = _mapper.Map<IEnumerable<POProduct>>(pOViewModels.POProductDTOs);
                var pocvm = new POContextViewModel { PurchaseOrder = purchaseOrder, POProducts = poProducts };
                _repository.AddPurchaseOrder(pocvm, username);
                await _saveContext.SaveChangesAsync();
                return Ok();
            }
            catch 
            {

                return BadRequest("Purchase Order Creation Failed");
            }
            


        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeletePurchaseOrder(int Id) 
        {
            try
            {
                var item = await _repository.GetPurchaseOrderById(Id);
                await _repository.DeletePurchaseOrderAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch {

                return BadRequest("Delete Failed");
            }
            
        }

    }
}
