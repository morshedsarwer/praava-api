﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.StoreRequisitionDTO;
using RetailSuite.Application.WHMS.Services.RequisitionService;
using RetailSuite.Application.WHMS.ViewModel.StoreRequisitionViewModel;
using RetailSuite.Domain.WHMS.Models.Stock;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreRequisitionsController : ControllerBase
    {
        private readonly IStoreRequisionRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public StoreRequisitionsController(IStoreRequisionRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadStoreRequisitionDTO>>> GetALlStoreRequisitions()
        {
            try
            {

                var requisitions = await _repository.GetStoreRequisitionAsync();
                return Ok(_mapper.Map<IEnumerable<ReadStoreRequisitionDTO>>(requisitions));
            }
            catch {
                return NotFound("StoreRequisitions Not Found");
            }
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<StoreRequisitionViewModel>> GetStoreRequisitionById(int Id) 
        {
            try
            {
                var requisition = await _repository.GetStoreRequisitionById(Id);
                var p = new StoreRequisitionViewModel();
                p.CreateStoreRequisitionDTO = _mapper.Map<CreateStoreRequisitionDTO>(requisition.StoreRequisition);
                p.RequisitionProductDTOs = _mapper.Map<IEnumerable<RequisitionProductDTO>>(requisition.RequisitionProducts);
                return p;
               
            }
            catch 
            {
                return NotFound("StoreRequisition Not Found");
            }
            
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateStoreRequisition(StoreRequisitionViewModel storeRequisitionViewModel) 
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var storeRequisition = _mapper.Map<StoreRequisition>(storeRequisitionViewModel.CreateStoreRequisitionDTO);
                var requisitionProducts = _mapper.Map<IEnumerable<RequisitionProduct>>(storeRequisitionViewModel.RequisitionProductDTOs);
                var context = new StoreRequisitionContextViewModel { StoreRequisition = storeRequisition, RequisitionProducts = requisitionProducts };
                _repository.AddStoreRequisition(context,username);
                await _saveContext.SaveChangesAsync();
                return Ok("StoreRequisition Created");
            }
            catch 
            {
                return BadRequest("StoreRequisition creation Failed");

            }
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteStoreRequisition(int Id) 
        {
            try
            {
                var item1 = await _repository.GetStoreRequisitionByIdAsync(Id);
                var item = _mapper.Map<ReadStoreRequisitionDTO>(item1);
                await _repository.DeleteStoreRequisitionAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("StoreRequisition Deleted");
            }
            catch 
            {
                return BadRequest("Delete Failed");
            }
            
        
        }
        
        
        // PUT OPERATION NOT IMPLEMENTED
/**/
    }
}
