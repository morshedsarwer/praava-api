﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.RequisitionRcvProductDTO;
using RetailSuite.Application.WHMS.Services.RequisitionRcvProductService;
using RetailSuite.Application.WHMS.ViewModel.StoreRequisitionViewModel;
using RetailSuite.Domain.WHMS.Models.Stock;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequisitionRcvProductController : ControllerBase
    {
        private readonly IRequisitionRcvProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public RequisitionRcvProductController(IRequisitionRcvProductRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadRequisitionRcvProductDTO>>> GetAllRequisitionRcvProduct() 
        {
            try
            {
                var rrproducts = await _repository.GetRequisitionRcvProductAsync();
                return Ok(_mapper.Map<IEnumerable<ReadRequisitionRcvProductDTO>>(rrproducts));
            }
            catch
            {
                return BadRequest("RequisitionRcvProduct Not Found");
            }
            
            
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadRequisitionRcvProductDTO>> GetRequisitionRcvProductbyId(int Id) 
        {
            try
            {
                var rrproduct = await _repository.GetRequisitionRcvProductByIdAsync(Id);
                return Ok(_mapper.Map<ReadRequisitionRcvProductDTO>(rrproduct));
            }
            catch
            {
                return BadRequest("RequisitionRcvProduct Not Found");
            }
            
        
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateRequisitionRcvProduct(RequistionRcvProducts requistionRcvProducts) 
        {
            try
            {
                // get the username
                var username = HttpContext.User.Identity.Name;
                var requisitionRcvProduct = _mapper.Map<IEnumerable<RequisitionRcvProduct>>(requistionRcvProducts.CreateRequisitionRcvProductDTOs);
                _repository.AddRequisitionRcvProduct(requisitionRcvProduct, username);
                await _saveContext.SaveChangesAsync();
                return Ok("RequisitionRcvProduct Creation Successful");
            }
            catch
            {
                return BadRequest("RequisitionRcvProduct Creation failed");
            }
            
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteRequisitionRcvProduct(int Id) 
        {
            try
            {
                var item1 = await _repository.GetRequisitionRcvProductByIdAsync(Id);
                var item = _mapper.Map<ReadRequisitionRcvProductDTO>(item1);
                await _repository.DeleteRequisitionRcvProductAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
            
            
        }

    }
}
