﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.ShelveDTO;
using RetailSuite.Application.WHMS.Services.ShelveService;
using RetailSuite.Domain.WHMS.Models.Store;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShelvesController : ControllerBase
    {
        private IShelveRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public ShelvesController(IShelveRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
           
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadShelveDTO>>> GetAllShelves() 
        {
            try
            {
                var shelves = await _repository.GetShelveAsync();
                return Ok(_mapper.Map<IEnumerable<ReadShelveDTO>>(shelves));
            }
            catch {

                return NotFound("Shelve Not found");
            }
            
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadShelveDTO>> GetShelveById(int Id) {

            try
            {
                var shelve = await _repository.GetShelveByIdAsync(Id);
                return Ok(_mapper.Map<ReadShelveDTO>(shelve));
            }
            catch {
                return NotFound("Shelve Not Found");
            }
            
        
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateShelve(CreateShelveDTO createShelveDTO) {
            try {
                // get username
                var username = HttpContext.User.Identity.Name;
                var shelve = _mapper.Map<Shelve>(createShelveDTO);
                var shlv = await _repository.AddShelve(shelve,username);
                await _saveContext.SaveChangesAsync();
                return Ok(_mapper.Map<ReadShelveDTO>(shlv));
            }catch{
                return BadRequest("Shelve Creation failed");
            }
            
        
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteShelve(int Id) {
            try
            {
                _repository.DeleteShelveAsync(Id);
                await _saveContext.SaveChangesAsync();
                return NoContent();
            }
            catch { 
                return BadRequest("Delete Failed");
            }

            
        }

    }
}
