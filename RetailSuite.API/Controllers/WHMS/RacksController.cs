﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.RackDTO;
using RetailSuite.Application.WHMS.Services.RackService;
using RetailSuite.Domain.WHMS.Models.Store;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class RacksController : ControllerBase
    {
        private IRackRepository _repository;
        private IMapper _mapper;
        private ISaveContexts _saveContext;

        public RacksController(IRackRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadRackDTO>>> GetAllRacks()
        {
            try
            {
                var racks = await _repository.GetRackAsync();
                return Ok(_mapper.Map<IEnumerable<ReadRackDTO>>(racks));
            }
            catch {

                return NotFound("Racks Not Found");
            }
            
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadRackDTO>> GetRackById(int Id) {

            try
            {
                var rack = await _repository.GetRackByIdAsync(Id);
                return Ok(_mapper.Map<ReadRackDTO>(rack));
            }
            catch {
                return NotFound("Rack Not Found");
            }
            
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateRack(CreateRackDTO createRackDTO) {

            try
            {
                // get the username
                var username = HttpContext.User.Identity.Name;
                var rack = _mapper.Map<Rack>(createRackDTO);
                var rck = await _repository.AddRack(rack,username);
                await _saveContext.SaveChangesAsync();
                return Ok(_mapper.Map<ReadRackDTO>(rck));
            }
            catch {
                return BadRequest("Rack Creation Failed");
            }
            
        
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteRacks(int Id)
        {
            try
            {
                await _repository.DeleteRackAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch {
                return BadRequest("Delete Failed");
            }
            
        }

    }
}
