﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.FarmRootsStockDTO;
using RetailSuite.Application.WHMS.DTOS.StockTransferDTO;
using RetailSuite.Application.WHMS.POCO;
using RetailSuite.Application.WHMS.Services.StockTransferService;
using RetailSuite.Application.WHMS.ViewModel.StockTransferViewModel;
using RetailSuite.Domain.WHMS.Models.Stock;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockTransfersController : ControllerBase
    {
        private readonly IStockTransferRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public StockTransfersController(IStockTransferRepository repository,IMapper mapper,ISaveContexts saveContexts )
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
        }



        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadStockTransferDTO>>> GetAllStockTransfer() 
        {
            try
            {
                var stockTransfers = await _repository.GetStockTransferAsync();
                return Ok(_mapper.Map<IEnumerable<ReadStockTransferDTO>>(stockTransfers));
            }
            catch (Exception ex)
            {
                return NotFound("Stock Transfer Not found");
            }
            
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<StockTransferViewModel>> GetStockTransferbyId(int Id) 
        {
            try
            {
                var stockTransfer = await _repository.GetStockTransferById(Id);
                var p = new StockTransferViewModel();
                p.CreateStockTransferDTO = _mapper.Map<CreateStockTransferDTO>(stockTransfer.StockTransfer);
                p.TransferProductDTOs = _mapper.Map<IEnumerable<TransferProductDTO>>(stockTransfer.TransferProducts);
                return p;
            }
            catch (Exception ex)
            {
                return NotFound("Stock Transfer Not Found");
            }            
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateStockTransfer(StockTransferViewModel stockTransferViewModel) 
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var stockTransfer = _mapper.Map<StockTransfer>(stockTransferViewModel.CreateStockTransferDTO);
                var transferProducts = _mapper.Map<IEnumerable<TransferProduct>>(stockTransferViewModel.TransferProductDTOs);
                var context = new StockTransferContextViewModel { StockTransfer = stockTransfer, TransferProducts = transferProducts };
                _repository.AddStockTransfer(context,username);
                await _saveContext.SaveChangesAsync();
                return Ok("Stock Transfer is created");
            }
            catch (Exception ex) 
            {
                return BadRequest("Stock Transfer Creation failed");
            }
            
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteStockTransfer(int Id) 
        {
            try
            {
                var item1 = await _repository.GetStockTransferByIdAsync(Id);
                var item = _mapper.Map<ReadStockTransferDTO>(item1);
                await _repository.DeleteStockTransferAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch (Exception ex) 
            {
                return BadRequest("Delete Failed");
            }
        
        }

        [HttpPost("createFarmrootsStock")]
        public async Task<ActionResult> CreateFarmrootsStock(CreateFarmrootsStockDTO createFarmrootsStockDTO)
        {
            try
            {
                var farmroots = _mapper.Map<FarmrootsStock>(createFarmrootsStockDTO);
                await _repository.AddFarmrootsStock(farmroots);
                await _saveContext.SaveChangesAsync();
                return Ok("Farmroots Stock Created");
            }
            catch
            {
                return BadRequest("Farmroots Creation Failed");
            }
            
        }

        [HttpGet("getallfarmrootsstock")]
        public async Task<ActionResult<IEnumerable<StockPoco>>> GetAllFarmrootsStock()
        {
            try
            {
                return Ok(_repository.GetAllFarmrootsStock());                
            }
            catch
            {
                return BadRequest("Farmroots Stock Not Found");
            }
        }

        [HttpGet("getFarmrootsStockbyId/{Id}")]
        public async Task<ActionResult>GetfarmrootsStockById(int Id)
        {
            try
            {
                var farmroot = await _repository.GetFarmrootsstockById(Id);
                return Ok(_mapper.Map<ReadFarmrootsStockDTO>(farmroot));
            }
            catch
            {
                return BadRequest("farmroots Stock Not Found");
            }
        }
        [HttpPut("updateFarmrootsStock/{Id}")]
        public async Task<ActionResult>UpdateFarmrootsStock(int Id,UpdateFarmrootsStockDTO updateFarmrootsStockDTO)
        {
            try
            {
                var oldfarmroots = await _repository.GetFarmrootsstockById(Id);
                _mapper.Map(updateFarmrootsStockDTO, oldfarmroots);
                await _saveContext.SaveChangesAsync();
                return Ok(oldfarmroots);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            
        }

        [HttpDelete("deleteFarmrootsStock/{Id}")]
        public async Task<ActionResult>DeleteFarmrootsStock(int Id)
        {
            try
            {
                await _repository.DeleteFarmrootsStock(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
        }
    }
}
