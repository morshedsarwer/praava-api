﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.PODelivaryMethodDTO;
using RetailSuite.Application.WHMS.Services.PODeliveryMethodService;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class PODeliveryMethodsController : ControllerBase
    {
        private readonly IPODeliveryMethodRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public PODeliveryMethodsController(IPODeliveryMethodRepository repository,IMapper mapper,ISaveContexts saveContexts) 
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadPODeliveryMethodDTO>>> GetAllPODeliveryMethods() 
        {
            try
            {
                var delivaries = await _repository.GetPODeliveryMethodAsync();
                return Ok(_mapper.Map<IEnumerable<ReadPODeliveryMethodDTO>>(delivaries));
            }
            catch (Exception ex) 
            {
                return NotFound("PODeliveryMethod not Found");
            }
            
        
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadPODeliveryMethodDTO>> GetPODeliveryMethodsById(int Id)
        {
            try
            {
                var delivary = await _repository.GetPODeliveryMethodByIdAsync(Id);
                return Ok(_mapper.Map<ReadPODeliveryMethodDTO>(delivary));
            }
            catch (Exception ex) {
                return NotFound("PODeliveryMethod not found");
            }
            
            

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreatePODeliveryMethod(CreatePODeliveryMethodDTO createPODeliveryMethodDTO) 
        {
            try
            {
                // get the username
                var username = HttpContext.User.Identity.Name;
                var pODeliveryMethod = _mapper.Map<PODeliveryMethod>(createPODeliveryMethodDTO);
                var poDeliMethod = await _repository.AddPODeliveryMethod(pODeliveryMethod,username);
                await _saveContext.SaveChangesAsync();
                return Ok(poDeliMethod);
            }
            catch (Exception ex) {

                return BadRequest("PODeliveryMethod creation failed");
            }
            
            
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeletePODeliveryMethod(int Id) 
        {
            try
            {
                var item = await _repository.GetPODeliveryMethodByIdAsync(Id);
                await _repository.DeletePODeliveryMethodAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch (Exception ex) 
            {
                return BadRequest("Delete Failed");
            }
            
        }

    }
}
