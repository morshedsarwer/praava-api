﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.ConsignmentReturnDTO;
using RetailSuite.Application.WHMS.Services.ConsignmentReturnService;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsignmentReturnsController : ControllerBase
    {
        private readonly IConsignmentReturnRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public ConsignmentReturnsController(IConsignmentReturnRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;

        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadConsignmentReturnDTO>>> GetAllConsignmentReturns() 
        {
            try
            {
                var conreturns = await _repository.GetConsignmentReturnAsync();
                return Ok(_mapper.Map<IEnumerable<ReadConsignmentReturnDTO>>(conreturns));
            }
            catch (Exception ex) 
            {
                return NotFound("Consignment returns Not Found");
            }
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadConsignmentReturnDTO>> GetConsignmentReturnById(int Id) 
        {
            try
            {
                var conreturn = await _repository.GetConsignmentReturnByIdAsync(Id);
                return Ok(_mapper.Map<ReadConsignmentReturnDTO>(conreturn));

            }
            catch (Exception ex) 
            {
                return NotFound("Consignment Return Not Found");
            }
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateConsignmentReturn(CreateConsignmentReturnDTO createConsignmentReturnDTO) 
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var consignmentReturn = _mapper.Map<ConsignmentProductReturn>(createConsignmentReturnDTO);
                var conReturn = await _repository.AddConsignmentReturn(consignmentReturn,username);
                await _saveContext.SaveChangesAsync();
                return Ok(conReturn);
            }
            catch (Exception ex) 
            {
                return BadRequest("Consignment return creation failed");
            }
        }
      



        
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteConsignmentReturn(int Id) 
        {
            try
            {
                await _repository.DeleteConsignmentReturnAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch (Exception ex) 
            {
                return BadRequest("Delete failed");
            }
        }


    }
}
