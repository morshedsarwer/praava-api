﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.POReceiveProductDTO;
using RetailSuite.Application.WHMS.Services.PORecieveProductService;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class POReceiveProductsController : ControllerBase
    {
        private readonly IPOReceiveProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public POReceiveProductsController(IPOReceiveProductRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {

            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadPOReceiveProductDTO>>> GetAllPORecieveProducts() 
        {
            try 
            {
                var products = await _repository.GetPOReceiveProductAsync();
                return Ok(_mapper.Map<IEnumerable<ReadPOReceiveProductDTO>>(products));
            }
            catch (Exception ex)
            {
                return NotFound("POReceiveProduct not Found");
            }
            
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadPOReceiveProductDTO>> GetPOReceiveProductById(int Id) 
        {
            try
            {
                var product = await _repository.GetPOReceiveProductByIdAsync(Id);
                return Ok(_mapper.Map<ReadPOReceiveProductDTO>(product));
            }
            catch (Exception ex) {
                return NotFound("POReceiveProduct Not Found");
            }
            
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreatePOReceiveProduct(CreatePOReceiveProductDTO createPOReceiveProductDTO) 
        {
            try
            {
                // get the username
                var username = HttpContext.User.Identity.Name;
                var pOReceiveProduct = _mapper.Map<POReceiveProduct>(createPOReceiveProductDTO);
                var porcvproduct = await  _repository.AddPOReceiveProduct(pOReceiveProduct,username);
                await _saveContext.SaveChangesAsync();
                return Ok(_mapper.Map<ReadPOReceiveProductDTO>(porcvproduct));
            }
            catch (Exception ex) 
            {
                return BadRequest("POReceiveProduct Creation Failed");
            }

            
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeletePOReceiveProduct(int Id)
        {
            try 
            {
                var item = await _repository.GetPOReceiveProductByIdAsync(Id);
                await _repository.DeletePOReceiveProductAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }catch (Exception ex)
            {
                return BadRequest("Delete Failed");
            }
            
        
        }

    }
}
