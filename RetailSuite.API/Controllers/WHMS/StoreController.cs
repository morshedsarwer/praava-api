﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.StoreDTO;
using RetailSuite.Application.WHMS.POCO;
using RetailSuite.Application.WHMS.Services.StoreService;
using RetailSuite.Domain.WHMS.Models.Stock;
using RetailSuite.Domain.WHMS.Models.Store;
using RetailSuite.Infrastructure.Contexts;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoresController : ControllerBase
    {
        private readonly IStoreRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        private readonly RetailDbContext _context;

        public StoresController(IStoreRepository repository, IMapper mapper, ISaveContexts saveContext, RetailDbContext context)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContext;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadStoreDTO>>> GetAllStores()
        {

            try
            {
                var stores = await _repository.GetStoreAsync();
                return Ok(_mapper.Map<IEnumerable<ReadStoreDTO>>(stores));

            }
            catch
            {
                return BadRequest("Store Not found");

            }
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadStoreDTO>> GetStoreById(int Id)
        {

            try
            {
                var store = await _repository.GetStoreByIdAsync(Id);
                return Ok(_mapper.Map<ReadStoreDTO>(store));

            }
            catch
            {

                return NotFound("Store Not Found");
            }
        }
        //[Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateStore(CreateStoreDTO createStoreDTO)
        {
            try
            {
                //var username = HttpContext.User.Identity.Name;
                var store = _mapper.Map<Store>(createStoreDTO);
                var str = await _repository.AddStore(store, "nafis123");
                await _saveContext.SaveChangesAsync();
                return Ok(_mapper.Map<ReadStoreDTO>(str));
            }
            catch
            {
                return BadRequest("Store Creation Failed");
            }

        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteStore(int Id)
        {
            try
            {
                await _repository.DeleteStoreAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successfull");
            }
            catch
            {

                return BadRequest("Delete Failed");
            }

        }

        [HttpPost("storeproducts")]
        public async Task<ActionResult> UnimartStoreProducts(UnimarStoreTransfer unimarStore)
        {
            try
            {
                var data = new List<UnimartStoreProducts>();
                foreach (var item in unimarStore.StoreProduct)
                {
                    data.Add(new UnimartStoreProducts() 
                    {
                        IsPusblish= false,
                        ProductId= item.ProductId,
                        StoreId=unimarStore.StoreId
                    });
                }
                _context.UnimartStoreProducts.AddRange(data);
                _context.SaveChanges();
                return Ok("Store Product Created");
            }
            catch
            {
                return BadRequest("Store Products Creation Failed");
            }

        }

        [HttpPost("storeproductpublish/{id}/{isPublish}")]
        public async Task<ActionResult> UnimartStoreProductsPublish(int id, bool isPublish)
        {
            try
            {
                var products = _context.UnimartStoreProducts.FirstOrDefault(x=>x.Id==id);
                if (products != null)
                {
                    products.IsPusblish = isPublish;
                    _context.SaveChanges();
                    return Ok(products);
                }
                return BadRequest("Published Status Changed");
                
            }
            catch
            {
                return BadRequest("Publish Status Change Failed");
            }

        }

        [HttpGet("storeproducts/{Id}")]
        public async Task<ActionResult<object>> GetUnimartStoreProducts(int Id)
        {
            try
            {
                var stores = from store in _context.UnimartStoreProducts
                             join product in _context.Products on store.ProductId equals product.ProductId
                             join s in _context.Stores on store.StoreId equals s.StoreId
                             where store.StoreId == Id
                             select new
                             {
                                 store.Id,
                                 store.StoreId,
                                 product.ProductId,
                                 product.ProductName,
                                 product.ProductCode,
                                 s.StoreName,
                                 store.IsPusblish
                             };
                return Ok(stores);
            }
            catch
            {
                return BadRequest("Store Products Not Found");
            }
        }

        [HttpGet("getlocations")]
        public async Task<ActionResult<object>> GetUnimartgLocations()
        {

            try
            {
                var data = _context.UnimartLocations.ToList();
                return Ok(data);
            }
            catch (Exception)
            {
                return NotFound("Locations Not Found");
            }

        }
    }
}
