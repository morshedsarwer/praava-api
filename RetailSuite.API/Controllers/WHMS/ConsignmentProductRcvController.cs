﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.ConsignmentProductRcvDTO;
using RetailSuite.Application.WHMS.Services.ConsignmentProductRcvService;
using RetailSuite.Domain.WHMS.Models.PurchaseOrder;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsignmentProductRcvController : ControllerBase
    {
        private readonly IConsignmentProductRcvRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public ConsignmentProductRcvController(IConsignmentProductRcvRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadConsignmentProductRcvDTO>>> GetAllConsignmentProductRcvs() 
        {
            try
            {
                var conproducts = await _repository.GetConsignmentProductRcvAsync();
                return Ok(_mapper.Map<IEnumerable<ReadConsignmentProductRcvDTO>>(conproducts));
            }
            catch (Exception ex) 
            {
                return NotFound("Consignment Product Not Found");
            }

        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadConsignmentProductRcvDTO>> GetConsignmentProductRcvById(int Id)
        {
            try
            {

                var conproduct = await _repository.GetConsignmentProductRcvByIdAsync(Id);
                return Ok(_mapper.Map<ReadConsignmentProductRcvDTO>(conproduct));
            }
            catch (Exception ex) 
            {
                return NotFound("Consignment Product Not Found");
            }
            
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateConsignmentProductRcv(CreateConsignmentProductRcvDTO createConsignmentProductRcvDTO) 
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var consignmentProductRcv = _mapper.Map<ConsignmentProductRcv>(createConsignmentProductRcvDTO);
                var conProdRcv = await _repository.AddConsignmentProductRcv(consignmentProductRcv,username);
                await _saveContext.SaveChangesAsync();
                return Ok(_mapper.Map<ReadConsignmentProductRcvDTO>(conProdRcv));
            }
            catch (Exception ex) 
            {
                return BadRequest("Consignment Product Creation Failed");
            }
            
        
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteConsignmentProductRcv(int Id) 
        {
            try
            {
                var item = await _repository.GetConsignmentProductRcvByIdAsync(Id);
                await _repository.DeleteConsignmentProductRcvAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }
            catch (Exception ex) 
            {
                return BadRequest("Delete Failed");
            }
        }
    }
}
