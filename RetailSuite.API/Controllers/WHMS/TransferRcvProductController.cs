﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.WHMS.DTOS.TransferRcvProductDTO;
using RetailSuite.Application.WHMS.Services.TransferRcvProductService;
using RetailSuite.Application.WHMS.ViewModel.TransferProductViewModel;
using RetailSuite.Domain.WHMS.Models.Stock;

namespace RetailSuite.API.Controllers.WHMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransferRcvProductController : ControllerBase
    {
        private readonly ITransferRcvProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        public TransferRcvProductController(ITransferRcvProductRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadTransferRcvProductDTO>>> GetAllTransferRcvProduct() 
        {
            try
            {
                var transferRcvProducts = await _repository.GetTransferRcvProductAsync();
                return Ok(_mapper.Map<IEnumerable<ReadTransferRcvProductDTO>>(transferRcvProducts));
            }
            catch (Exception ex)
            {
                return NotFound("TransferRcvProduct Not Found");
            }
            
        
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadTransferRcvProductDTO>> GetTransferRcvProductById(int Id) 
        {
            try
            {
                var transferRcvProduct = await _repository.GetTransferRcvProductByIdAsync(Id);
                return Ok(_mapper.Map<ReadTransferRcvProductDTO>(transferRcvProduct));
            }
            catch (Exception ex)
            {
                return NotFound("TransferRcvProduct Not Found");
            }
            
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateTransferRcvProduct(TransferRcvProducts transferRcvProducts) 
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var transferRcvProduct = _mapper.Map<IEnumerable<TransferRcvProduct>>(transferRcvProducts.CreateTransferRcvProductDTOs);
                _repository.AddTransferRcvProduct(transferRcvProduct,username);
                _saveContext.SaveChangesAsync();
                return Ok("TransferRcvProduct Created");
            }
            catch (Exception ex)
            {
                return BadRequest("TransferRcvProduct Creation Failed");
            }
            
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteTransferRcvProduct(int Id) 
        {
            try
            {
                var item1 = await _repository.GetTransferRcvProductByIdAsync(Id);
                var item = _mapper.Map<ReadTransferRcvProductDTO>(item1);
                await _repository.DeleteTransferRcvProductAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }
            catch (Exception ex)
            {
                return BadRequest("Delete Failed");
            }
            
        
        }
    }
}
