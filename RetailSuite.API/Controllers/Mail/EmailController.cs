﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using RetailSuite.Application.Account;
using RetailSuite.Domain.Account.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace RetailSuite.API.Controllers.Mail
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private UserManager<ApplicationUser> _userManager;

        public EmailController(UserManager<ApplicationUser> userManager)
        {

            _userManager = userManager;
        }
        
        [HttpGet]
        public async Task<ActionResult> ConfirmEmail([FromQuery]string token, string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            
            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                return Ok(result);
            }
            else {
                return BadRequest("Email Confirmation Failed");
            }
        }
    }
}
