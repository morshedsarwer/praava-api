﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Voucher.DTOS.Vendors;
using RetailSuite.Application.Voucher.Services.Common;
using RetailSuite.Application.Voucher.Services.Vendor;
using RetailSuite.Domain.Voucher.Vendor;

namespace RetailSuite.API.Controllers.Vouchers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SellerVouchersController : ControllerBase
    {
        private readonly ISellerVoucherRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        private readonly IVoucherRepository _vrepository;

        public SellerVouchersController(ISellerVoucherRepository repository,IMapper mapper,ISaveContexts saveContexts,IVoucherRepository vrepository)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
            _vrepository = vrepository;
           
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadSellerVoucherDTO>>> GetAllSellervoucher() 
        {
            try
            {
                var vouchers = await _repository.GetSellerVoucherAsync();
                return Ok(_mapper.Map<IEnumerable<ReadSellerVoucherDTO>>(vouchers));
            }
            catch (Exception ex) 
            {
                return NotFound("Seller voucher Not Found");
            }
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadSellerVoucherDTO>> GetSellerVoucherById(int Id) 
        {
            try
            {
                var voucher = await _repository.GetSellerVoucherByIdAsync(Id);
                return Ok(_mapper.Map<ReadSellerVoucherDTO>(voucher));
            }
            catch (Exception ex) 
            {
                return NotFound("Seller Voucher Not Found");
            }
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateSellerVoucher(CreateSellerVoucherDTO createSellerVoucherDTO) 
        {
            try 
            {
                var username = HttpContext.User.Identity.Name;
                var isalready = _vrepository.IsVoucherAvilable(createSellerVoucherDTO.SVCode);
                if (isalready == false) 
                {
                    var sellerVoucher = _mapper.Map<SellerVoucher>(createSellerVoucherDTO);
                    _repository.AddSellerVoucher(sellerVoucher,createSellerVoucherDTO.SVCode,username);
                    await _saveContext.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return BadRequest("voucher code is already exists");
                }
                
            }catch (Exception ex)
            {
                return BadRequest("Seller Voucher Creation Failed");
            }
            
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteSellerVoucher(int Id) 
        {
            try
            {
                await _repository.DeleteSellerVoucherAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch (Exception ex) 
            {
                return BadRequest("Delete Failed");
            }
            
        }

        [HttpPost("check")]
        public async Task<ActionResult> Check(string SVCode) 
        {
            try
            {
                var status = await _repository.CheckVoucher(SVCode);
                return Ok(status);
            }
            catch
            {
                return BadRequest("Check Failed");
            }
            
        }
    }
}
