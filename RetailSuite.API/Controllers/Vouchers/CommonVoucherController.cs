﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.Voucher.Services.Common;
using RetailSuite.Domain.Voucher.CommonVEntity;

namespace RetailSuite.API.Controllers.Vouchers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonVoucherController : ControllerBase
    {
        private readonly IVoucherRepository _repository;

        public CommonVoucherController(IVoucherRepository repository)
        {
            _repository = repository;

        }
        [HttpPost]
        public ActionResult IsVoucherAvilable(string VCode) 
        {
            var isAvilable = _repository.IsVoucherAvilable(VCode);
            if (isAvilable == false) {
                return NotFound("Avilable For Entering");            
            }
            else
            {
                return Ok("Voucher is already active not avilable for entering");
            }
        }

        [HttpPost("GetVoucherbyCustomerId/{customerId}")]
        public async Task<ActionResult<IEnumerable<VoucherCode>>>GetVoucherByCustomerId(string customerId)
        {
            var vouchers = await _repository.GetVoucherByCustomerId(customerId);
            return Ok(vouchers); 
        } 
    }
}
