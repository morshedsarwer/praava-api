﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Voucher.DTOS.App;
using RetailSuite.Application.Voucher.Services.App;
using RetailSuite.Application.Voucher.Services.Common;
using RetailSuite.Application.Voucher.ViewModels.AppUserVoucherViewModel;
using RetailSuite.Domain.Voucher.App;

namespace RetailSuite.API.Controllers.Vouchers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppUserVouchersController : ControllerBase
    {
        private readonly IAppUserVoucherRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        private readonly IVoucherRepository _vrepository;

        public AppUserVouchersController(IAppUserVoucherRepository repository,IMapper mapper,ISaveContexts saveContexts,IVoucherRepository vrepository)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
            _vrepository = vrepository;

        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadAppUserVoucherDTO>>> GetAllAppUserVoucher() {
            try
            {
                var appUserVouchers = await _repository.GetAppUserVoucher();
                return Ok(_mapper.Map<IEnumerable<ReadAppUserVoucherDTO>>(appUserVouchers));
            }
            catch (Exception ex) 
            {
                return NotFound("AppUser Voucher Not Found");
            }
            

        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadAppUserVoucherDTO>> GetAppUserVoucherById(int Id) 
        {
            try
            {
                var appUserVoucher = await _repository.GetAppUserVoucherById(Id);
                return Ok(_mapper.Map<ReadAppUserVoucherDTO>(appUserVoucher));
            }
            catch (Exception ex) 
            {
                return NotFound("AppUser Voucher Not Found");
            }
            
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateAppUserVoucher(AppUserVoucherViewModel appUserVoucherViewModel) 
        {
            try {
                // get username
                var username = HttpContext.User.Identity.Name;
                var isalready = _vrepository.IsVoucherAvilable(appUserVoucherViewModel.CreateAppUserVoucherDTO.AppVoucher);
                if (isalready == false) {
                    var appUserVoucher = _mapper.Map<AppUserVoucher>(appUserVoucherViewModel.CreateAppUserVoucherDTO);
                    var appVoucherUsers = _mapper.Map<IEnumerable<AppVoucherUsers>>(appUserVoucherViewModel.AppVoucherUsersDTOs);
                    var context = new AppUserVoucherContextViewModel { AppUserVoucher = appUserVoucher, AppVoucherUsers = appVoucherUsers };
                    _repository.AddAppUserVoucher(context, appUserVoucherViewModel.CreateAppUserVoucherDTO.AppVoucher,username);
                    await _saveContext.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return BadRequest("Voucher Code Is already exists");
                }
                
            }catch (Exception ex)
            {
                return BadRequest("AppUser Voucher Creation Failed");
            }
            
        
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteAppUserVoucher(int Id) 
        {
            try 
            {
                await _repository.DeleteAppUserVoucher(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete successful");
            }catch(Exception ex)
            {
                return BadRequest("Delete Failed");
            }
            
        }

        [HttpPost("check")]
        public async Task<ActionResult> Check(string AppVoucher) 
        {
            try
            {
                var status = await _repository.CheckVoucher(AppVoucher);
                return Ok(status);
            }
            catch
            {
                return BadRequest("check failed");
            }
            
        }

    }
}
