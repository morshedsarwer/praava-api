﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Voucher.DTOS.General;
using RetailSuite.Application.Voucher.Services.Common;
using RetailSuite.Application.Voucher.Services.General;
using RetailSuite.Domain.Voucher.General;
using RetailSuite.Infrastructure.Contexts;

namespace RetailSuite.API.Controllers.Vouchers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeneralVouchersController : ControllerBase
    {
        private readonly IGeneralVoucherRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        private readonly IVoucherRepository _vrepository;
        private readonly RetailDbContext _context;

        public GeneralVouchersController(IGeneralVoucherRepository repository,IMapper mapper,ISaveContexts saveContexts,IVoucherRepository vrepository, RetailDbContext context)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
            _vrepository = vrepository;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadGeneralVoucherDTO>>> GetAllGeneralVoucher() 
        {
            try
            {
                var vouchers = await _repository.GetGeneralVoucherAsync();                
                return Ok(_mapper.Map<IEnumerable<ReadGeneralVoucherDTO>>(vouchers));
            }
            catch (Exception ex) 
            {
                return NotFound("General Voucher Not Found");
            }
            
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<object>> GetGeneralVoucherById(int Id) 
        {
            try
            {
                var voucher = await _repository.GetGeneralVoucherByIdAsync(Id);
                return Ok(voucher);
            }
            catch (Exception ex) 
            {
                return NotFound("General Voucher Not Found");
            }
            
        }


        [HttpPut("{Id}")]
        public async Task<ActionResult> UpdateGeneralVoucher(int Id,UpdateGeneralVoucherDTO updateGeneralVoucherDTO)
        {
            try
            {
                var voucherFromRepo = _context.GeneralVouchers.FirstOrDefault(x=>x.GeneralVId==Id);
                if (voucherFromRepo != null)
                {
                    _mapper.Map(updateGeneralVoucherDTO, voucherFromRepo);
                    await _saveContext.SaveChangesAsync();
                    return Ok("Updated Successfully");
                }
                return NotFound("Voucher not found");
            }
            catch
            {
                return BadRequest("Update failed");
            }
            
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateGeneralVoucher(CreateGeneralVoucherDTO createGeneralVoucherDTO) 
        {
            try
            {
                // get the username 
                var username = HttpContext.User.Identity.Name;
                //var username = "nafis123";
                //var isalready = _vrepository.IsVoucherAvilable(createGeneralVoucherDTO.GVCode);
                var isalready = _repository.IsVoucherAvilable(createGeneralVoucherDTO.GVCode);
                if (isalready == false) 
                { 

                    var generalVoucher = _mapper.Map<GeneralVoucher>(createGeneralVoucherDTO);
                    _repository.AddGeneralVoucher(generalVoucher,createGeneralVoucherDTO.GVCode,username);
                    await _saveContext.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    // there is already a voucher 
                    // fetch it
                    var voucher = await _repository.GetGeneralVoucherByGVCode(createGeneralVoucherDTO.GVCode);
                    // check if it is avalilable
                    var currentDate = DateTime.Now;
                    var start = voucher.StartDate;
                    var end = voucher.EndDate;
                    if(currentDate > end)
                    {
                        // ready for renew
                        var generalVoucher = _mapper.Map<GeneralVoucher>(createGeneralVoucherDTO);
                        _repository.AddGeneralVoucher(generalVoucher, createGeneralVoucherDTO.GVCode, username);
                        await _saveContext.SaveChangesAsync();
                        return Ok();
                    }
                    else
                    {
                        return BadRequest("Voucher Code is already exists");
                    }
                    
                }
                
            }
            catch (Exception ex) 
            {
                return BadRequest("Voucher Creation Failed");
            }
            
        }
        
        [HttpGet("check/{GVCode}")]
        public async Task<ActionResult<object>>Check(string GVCode)
        {
            try
            {
                var status = await _repository.CheckVoucher(GVCode);
                return Ok(status);
            }
            catch (Exception)
            {
                return BadRequest("Check Failed");
            }
        }       
        
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteGeneralVoucher(int Id) 
        {
            try
            {
                await _repository.DeleteGeneralVoucherAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch (Exception ex)
            {
                return BadRequest("Delete Failed");
            }
            
        }
    }
}
