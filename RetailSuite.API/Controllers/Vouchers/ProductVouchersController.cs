﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Voucher.DTOS.Products;
using RetailSuite.Application.Voucher.Services.Common;
using RetailSuite.Application.Voucher.Services.Product;
using RetailSuite.Application.Voucher.ViewModels.ProductVoucherViewModel;
using RetailSuite.Domain.Voucher.Product;

namespace RetailSuite.API.Controllers.Vouchers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductVouchersController : ControllerBase
    {
        private readonly IProductVoucherRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        private readonly IVoucherRepository _vrepository;

        public ProductVouchersController(IProductVoucherRepository repository,IMapper mapper,ISaveContexts saveContexts,IVoucherRepository vrepository)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
            _vrepository = vrepository;

        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadProductVoucherDTO>>> GetAllproductVoucher()
        {
            try
            {
                var productVouchers = await _repository.GetProductVoucherAsync();
                return Ok(_mapper.Map<IEnumerable<ReadProductVoucherDTO>>(productVouchers));
            }
            catch (Exception ex) 
            {
                return NotFound("Product Voucher Not Found");
            }
            
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadProductVoucherDTO>> GetProductVoucherById(int Id) 
        {
            try
            {
                var productVoucher = await _repository.GetProductVoucherByIdAsync(Id);
                return Ok(_mapper.Map<ReadProductVoucherDTO>(productVoucher));
            }
            catch (Exception ex) 
            {
                return NotFound("Product Voucher Not Found");
            }
            
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateProductVoucher(ProductVoucherViewModel productVoucherViewModel) 
        {
            try
            {
                // get the username
                var username = HttpContext.User.Identity.Name;
                var isalready = _vrepository.IsVoucherAvilable(productVoucherViewModel.CreateProductVoucherDTO.PVCode);
                if (isalready == false) 
                {
                    var productVoucher = _mapper.Map<ProductVoucher>(productVoucherViewModel.CreateProductVoucherDTO);
                    var pvproducts = _mapper.Map<IEnumerable<ProdVoucherProducts>>(productVoucherViewModel.ProdVoucherProductsDTOs);
                    var context = new ProductVoucherContextViewModel { ProductVoucher = productVoucher, ProdVoucherProducts = pvproducts };
                    _repository.AddProductVoucher(context, productVoucherViewModel.CreateProductVoucherDTO.PVCode,username);
                    await _saveContext.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return BadRequest("Product Voucher Already Exists");
                }
                
            }
            catch (Exception ex) 
            {
                return BadRequest("Product Voucher Creation Failed");
            }
            
        }



        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteProductVoucher(int Id) 
        {
            try
            {
                await _repository.DeleteProductVoucherAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch (Exception ex) 
            {
                return BadRequest("Delete Failed");
            }
            
        }

        [HttpPost("check")]
        public async Task<ActionResult>Check(string PVCode)
        {
            try
            {
                var status = await _repository.CheckVoucher(PVCode);
                return Ok(status);
            }
            catch
            {
                return BadRequest("Check Failed");
            }
            
        }
    }
}
