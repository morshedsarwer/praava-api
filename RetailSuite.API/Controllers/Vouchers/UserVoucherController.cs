﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.Voucher.DTOS.User;
using RetailSuite.Application.Voucher.Services.Common;
using RetailSuite.Application.Voucher.Services.User;
using RetailSuite.Domain.Voucher.User;

namespace RetailSuite.API.Controllers.Vouchers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserVoucherController : ControllerBase
    {
        private IUserVoucherRepository _repository;
        private IMapper _mapper;
        private ISaveContexts _saveContext;
        private IVoucherRepository _vrepository;

        public UserVoucherController(IUserVoucherRepository repository,IMapper mapper,ISaveContexts saveContexts,IVoucherRepository vrepository)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
            _vrepository = vrepository;

            
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadUserVoucherDTO>>>GetAllUserVoucher()
        {
            try
            {
                var vouchers = await _repository.GetUserVoucherAsync();
                return Ok(_mapper.Map<IEnumerable<ReadUserVoucherDTO>>(vouchers));
            }
            catch
            {
                return BadRequest("User Voucher Not Found");
            }
            
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadUserVoucherDTO>>GetUserVoucherById(int Id)
        {
            try
            {
                var voucher = await _repository.GetUserVoucherByIdAsync(Id);
                return Ok(_mapper.Map<ReadUserVoucherDTO>(voucher));
            }
            catch
            {
                return BadRequest("User Voucher Not Found");
            }
            
        }
        
        [Authorize]
        [HttpPost]
        public async Task<ActionResult>CreateUserVoucher(CreateUserVoucherDTO createUserVoucherDTO)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var isalready = _vrepository.IsVoucherAvilable(createUserVoucherDTO.UVCode);
                if (isalready == false)
                {
                    var uservoucher = _mapper.Map<UserVoucher>(createUserVoucherDTO);
                    _repository.AddUserVoucher(uservoucher, createUserVoucherDTO.UVCode, username);
                    await _saveContext.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return BadRequest("Voucher Code is Already exists");
                }
            }
            catch
            {
                return BadRequest("User Voucher Creation failed");
            }
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteUserVoucher(int Id)
        {
            try
            {
                await _repository.DeleteUserVoucher(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
            
        }

        [HttpPost("check")]
        public async Task<ActionResult> CheckVoucher(string UVCode) 
        {
            try
            {
                var status = await _repository.CheckVoucher(UVCode);
                return Ok(status);
            }
            catch
            {
                return BadRequest("check Failed");
            }
            
        }
    }
}
