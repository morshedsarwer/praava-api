﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CMS.DTOS.PhysicalAttributeDTO;
using RetailSuite.Application.CMS.Services.PhysicalAttributeService;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Products;

namespace RetailSuite.API.Controllers.CMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhysicalAttributesController : ControllerBase
    {
        private readonly IPhysicalAttributeRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public PhysicalAttributesController(IPhysicalAttributeRepository repository,IMapper mapper,ISaveContexts saveContexts )
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadPhysicalAttributeDTO>>> GetAllPhysicalAttributeAsync() 
        {
            try
            {
                var attributes = await _repository.GetPhysicalAttributeAsync();
                return Ok(_mapper.Map<IEnumerable<ReadPhysicalAttributeDTO>>(attributes));
            } catch (Exception ex)
            
            {
                return NotFound("Attribute Not Found");
            }
            
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadPhysicalAttributeDTO>> GetPhysicalAttributeById(int Id)
        {
            try
            {
                var attribute = await _repository.GetPhysicalAttributeByIdAsync(Id);
                return Ok(_mapper.Map<ReadPhysicalAttributeDTO>(attribute));
            }
            catch (Exception ex)
            {
                return NotFound("Attribute Not Found");
            
            }

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreatePhysicalAttribute(CreatePhysicalAttributeDTO createPhysicalAttributeDTO) 
        {
            try
            {
                // get the user username
                var username = HttpContext.User.Identity.Name;
                var physicalAttribute = _mapper.Map<PhysicalAttribute>(createPhysicalAttributeDTO);
                var phyattr = await _repository.AddPhysicalAttribute(physicalAttribute,username);
                await _saveContext.SaveChangesAsync();
                return Ok(_mapper.Map<ReadPhysicalAttributeDTO>(phyattr));
            }
            catch (Exception ex) 
            {
                return BadRequest("Physical Attribute Failed");
            }
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeletePhysicalAttribute(int Id) 
        {
            try
            {
                var item = await _repository.GetPhysicalAttributeByIdAsync(Id);
                await _repository.DeletePhysicalAttributeAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }
            catch (Exception ex)
            {
                return BadRequest("Delete Failed");
                
            }
        }

        [HttpPut("{Id}")]
        public async Task<ActionResult> UpdatePhysicalAttribute(int Id,UpdatePhysicalAttributeDTO updatePhysicalAttributeDTO)
        {
            try
            {
                var oldattribute = await _repository.GetPhysicalAttributeByIdAsync(Id);
                _mapper.Map(updatePhysicalAttributeDTO, oldattribute);
                await _saveContext.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            
        }
    }
}
