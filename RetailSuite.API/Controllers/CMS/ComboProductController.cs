﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CMS.DTOS.ComboProductDTO;
using RetailSuite.Application.CMS.Services.ComboProductServices;
using RetailSuite.Application.CMS.ViewModels.Products.Combo;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Infrastructure.Contexts;

namespace RetailSuite.API.Controllers.CMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComboProductController : ControllerBase
    {
        private readonly IComboProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly RetailDbContext _context;

        public ComboProductController(IComboProductRepository repository, IMapper mapper, ISaveContexts saveContexts, IWebHostEnvironment hostEnvironment, RetailDbContext context)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
            _hostEnvironment = hostEnvironment;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadComboProductDTO>>> GetAllComboProduct()
        {
            try
            {
                var comboproducts = await _repository.GetComboProductAsync();
                //return Ok(_mapper.Map<IEnumerable<ReadComboProductDTO>>(comboproducts));
                return Ok(comboproducts);
            }
            catch
            {
                return NotFound("Combo Products Not Found");

            }
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<ComboViewModels>> GetComboProductById(int Id)
        {

            try
            {
                var comboproduct = await _repository.GetComboById(Id);
                //var comboProduct = await _repository.GetComboProductById(Id);
                //var p = new ComboViewModels();
                //p.ComboProductDTO = _mapper.Map<CreateComboProductDTO>(comboProduct.ComboProduct);
                //p.ComboProductTags = _mapper.Map<IEnumerable<ComboProductTagDTO>>(comboProduct.ComboProductTags);
                /*var combo = _context.ComboProducts.FirstOrDefault(x=>x.ComboId==Id);
                var data = _mapper.Map<ReadComboProductDTO>(combo);*/
                return Ok(comboproduct);
            }
            catch
            {
                return NotFound("Combo Product not Found");
            }

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<ComboProduct>> CreateComboProduct(ComboViewModels comboViewModels)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var comboProduct = _mapper.Map<ComboProduct>(comboViewModels.ComboProductDTO);
                var tags = _mapper.Map<IEnumerable<ComboProductTag>>(comboViewModels.ComboProductTags);
                var ComboContextViewModels = new ComboContextViewModels { ComboProduct = comboProduct, ComboProductTags = tags };
                var result = await _repository.AddComboProduct(ComboContextViewModels, username);
                await _saveContext.SaveChangesAsync();
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Combo Product Post Failed");
            }
        }

        // HTTP PUT OPERATION PAUSED UNTILL FURTHER COMMAND


        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteComboProduct(int Id)
        {
            try
            {
                var item = await _repository.GetComboProductByIdAsync(Id);
                await _repository.DeleteComboProductAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }
            catch
            {
                return BadRequest("Delete Failed");

            }
        }

        [HttpPut("{Id}")]
        public async Task<ActionResult> UpdateComboProduct(int Id, UpdateComboProductDTO updateComboProductDTO)
        {
            // set the isapproved to false 
            // when editing it
            try
            {
                updateComboProductDTO.IsApproved = false;
                var oldComboProduct = await _repository.GetComboProductByIdAsync(Id);
                _mapper.Map(updateComboProductDTO, oldComboProduct);
                await _saveContext.SaveChangesAsync();
                return Ok(oldComboProduct);
            }
            catch (Exception)
            {
                return BadRequest("Update Failed");
            }

        }

        //[Authorize]
        [HttpPost("comboProductImage")]
        public async Task<ActionResult> UploadComboImage([FromForm] ImageComboProductDTO imageComboProductDTO, [FromForm] string ComboCode)
        {
            try
            {
                var comboproduct = _context.ComboProducts.FirstOrDefault(s => s.ComboCode == ComboCode);
                if (comboproduct != null)
                {
                    comboproduct.ImageName = await SaveImage(imageComboProductDTO.ImageFile);
                    comboproduct.ImageUrl = Path.Combine("Images", comboproduct.ImageName);
                    if (await _saveContext.SaveChangesAsync())
                    {
                        return Ok(comboproduct);
                    }
                    else
                    {
                        _repository.DeleteAddedProducts(ComboCode);
                        return BadRequest();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return BadRequest("Image Upload Failed");
            }

        }
        [HttpGet("getcomboproducts/{ComboCode}")]
        public async Task<ActionResult> GetComboProducts(string ComboCode)
        {
            try
            {
                var results = await _repository.GetComboProducts(ComboCode);
                return Ok(results);
            }
            catch (Exception)
            {
                return NotFound("Combo Product Not Found");
            }
        }

        [HttpGet("getComboProductTag/{Id}")]
        public async Task<ActionResult> GetComboProductTagById(int Id)
        {
            try
            {
                var comboproductTag = await _repository.GetComboProductTagById(Id);
                return Ok(comboproductTag);
            }
            catch
            {
                return BadRequest("Combo Product Tag Not Found");
            }
        }

        [HttpDelete("deleteComboProductTag/{Id}")]
        public async Task<ActionResult> DeleteComboProductTag(int Id)
        {
            try
            {
                await _repository.DeleteComboProductTag(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
        }

        [HttpPut("updateComboProductTag/{Id}")]
        public async Task<ActionResult> UpdateComboProductTag(int Id, UpdateComboProductTagDTO comboProductTagDTO)
        {
            try
            {
                var oldComboTag = await _repository.GetComboProductTagById(Id);
                _mapper.Map(comboProductTagDTO, oldComboTag);
                await _saveContext.SaveChangesAsync();
                return Ok(oldComboTag);

            }
            catch
            {
                return BadRequest("Update Failed");
            }
        }

        [HttpPost("addComboProductTag")]
        public async Task<ActionResult> AddComboProductTag(AddSingleComboProductTag createComboProductTagDTO)
        {
            try
            {
                var comboProductTag = _mapper.Map<ComboProductTag>(createComboProductTagDTO);
                await _repository.AddComboProductTag(comboProductTag);
                await _saveContext.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return BadRequest("Combo Product Tag Adding Failed");
            }
        }


        [HttpPut("UpdateComboImage")]
        public async Task<ActionResult> UpdateComboImage([FromForm] ImageComboProductDTO imageComboProductDTO, [FromForm] string ComboCode)
        {
            try
            {
                var comboProduct = _context.ComboProducts.FirstOrDefault(s => s.ComboCode == ComboCode);
                if (comboProduct != null)
                {
                    comboProduct.ImageName = await SaveImage(imageComboProductDTO.ImageFile);
                    comboProduct.ImageUrl = Path.Combine("Images", comboProduct.ImageName);
                    if (await _saveContext.SaveChangesAsync())
                    {
                        return Ok(comboProduct);
                    }
                    else
                    {
                        _repository.DeleteAddedProducts(ComboCode);
                        return BadRequest();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return BadRequest("Update Failed");
            }
        }

        [NonAction]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            try
            {
                string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
                imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
                var imagePath = Path.Combine("Images", imageName);
                using (var fileStream = new FileStream(imagePath, FileMode.Create))
                {
                    await imageFile.CopyToAsync(fileStream);
                }
                return imageName;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
