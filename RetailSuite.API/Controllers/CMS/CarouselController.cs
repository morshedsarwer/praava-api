﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CMS.DTOS.CarouselDTO;
using RetailSuite.Application.CMS.DTOS.SideBannerDTO;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Carousel;
using RetailSuite.Domain.CMS.Models.SideBanner;
using RetailSuite.Infrastructure.Contexts;

namespace RetailSuite.API.Controllers.CMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarouselController : ControllerBase
    {
        private readonly RetailDbContext _context;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public CarouselController(RetailDbContext context,IMapper mapper,ISaveContexts saveContexts)
        {
            _context = context;
            _mapper = mapper;
            _saveContext = saveContexts;

        }

        [HttpGet]
        public async Task<ActionResult> GetAllCarousel()
        {
            try
            {
                var carousels = await _context.Carousels.Where(s => s.IsPublished == true).ToListAsync();
                return Ok(_mapper.Map<IEnumerable<ReadCarouselDTO>>(carousels));

            }
            catch
            {
                return NotFound("Carousel Not Found");
            }

        }


        [HttpPost]
        public async Task<ActionResult> CreateCarousel([FromForm] CreateCarouselDTO createCarouselDTO)
        {
            try 
            {
                var carousel = new Carousel();
                carousel.Name = createCarouselDTO.Name;
                carousel.Url = createCarouselDTO.Url;
                carousel.ImageName = await SaveImage(createCarouselDTO.ImageFile);
                carousel.ImageUrl = Path.Combine("Images", carousel.ImageName);
                carousel.Description = createCarouselDTO.Description;
                carousel.IsPublished = createCarouselDTO.IsPublished;
                _context.Carousels.Add(carousel);
                await _saveContext.SaveChangesAsync();
                return Ok(carousel);
            }
            catch
            {
                return BadRequest("Post Failed");
            }       


        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteCarousel(int Id)
        {
            try
            {
                var carousal = await _context.Carousels.FirstOrDefaultAsync(s => s.Id == Id);
                _context.Carousels.Remove(carousal);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
        }

        [HttpGet("getAllSideBanners")]
        public async Task<ActionResult> GetAllSideBanners()
        {
            try
            {
                var sideBanners = await _context.SideBanners.ToListAsync();
                return Ok(_mapper.Map<IEnumerable<SideBanner>>(sideBanners));
            }
            catch
            {
                return BadRequest("Side Banner Not Found");
            }
            
        }      
        [HttpPost("sideBanner")]
        public async Task<ActionResult> CreateSideBanner([FromForm]CreateSideBannerDTO createSideBannerDTO)
        {
            try
            {
                var sideBanner = new SideBanner();
                sideBanner.Link = createSideBannerDTO.Link;
                sideBanner.Position = createSideBannerDTO.Position;
                sideBanner.ImageName = await SaveImage(createSideBannerDTO.ImageFile);
                sideBanner.ImageUrl = Path.Combine("Images", sideBanner.ImageName);
                _context.SideBanners.Add(sideBanner);
                await _saveContext.SaveChangesAsync();
                return Ok(sideBanner);
            }
            catch
            {
                return BadRequest("Side Banner Post Failed");
            }
            
        }

        [HttpDelete("sideBanner/{Id}")]
        public async Task<ActionResult>DeleteSideBanner(int Id)
        {
            try
            {
                var sideBanner = await _context.SideBanners.FirstOrDefaultAsync(s => s.Id == Id);
                _context.SideBanners.Remove(sideBanner);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
        }

        [NonAction]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine("Images", imageName);
            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }
            return imageName;

        }
    }
}
