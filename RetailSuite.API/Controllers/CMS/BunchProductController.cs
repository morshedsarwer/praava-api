﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CMS.ViewModels.Products.Bunch;
using RetailSuite.Application.CommonServices;
using RetailSuite.Application.DTOS.BunchProductDTO;
using RetailSuite.Application.Services.BunchProductServices;
using RetailSuite.Domain.CMS.Models.Products;

namespace RetailSuite.API.Controllers.CMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class BunchProductsController : ControllerBase
    {
        private readonly IBunchProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public BunchProductsController(IBunchProductRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadBunchProductDTO>>> GetAllBunchProduct()
        {
            try
            {
                var bunchproducts = await _repository.GetBunchProductsAsync();
                return Ok(_mapper.Map<IEnumerable<ReadBunchProductDTO>>(bunchproducts));
          
            }
            catch {
                return NotFound("Bunch Product Not Found");
            
            }

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BunchContextViewModels>> GetbunchProductById(int id)
        {
            try
            {
               var bunchProduct = await _repository.GetBunchProductById(id);
               return Ok(_mapper.Map<ReadBunchProductDTO>(bunchProduct));
               
            }
            catch {
                return NotFound("Bunch Product Not Found");
            
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateBunchProduct(BunchViewModels bunchViewModels)
        {
            try
            {
                var bunch = _mapper.Map<BunchProduct>(bunchViewModels.BunchProductDTO);
                var bunchProduct = _mapper.Map<IEnumerable<BunchProductTag>>(bunchViewModels.BunchProductTag);
                var price = _mapper.Map<BunchPrice>(bunchViewModels.BunchPriceDTO);
                var contextModel = new BunchContextViewModels {BunchPrice=price, BunchProduct=bunch,BunchProductTags=bunchProduct  };
                //var bunchProduct = _mapper.Map<BunchProduct>(createBunchProductDTO);
                _repository.AddBunchProduct(contextModel);
                await _saveContext.SaveChangesAsync();
                return Ok("Bunch Product Created");
            }
            catch {
                return BadRequest("Bunch Product Creation Failed");
            }
        
        }

        [HttpPut("{Id}")]
        public async Task<ActionResult> UpdateBunchProduct(int Id, UpdateBunchProductDTO updateBunchProductDTO) {

            try
            {
                var bunchProduct = await _repository.GetBunchProductByIdAsync(Id);
                _mapper.Map(updateBunchProductDTO, bunchProduct);
                await _saveContext.SaveChangesAsync();
                return Ok("Bunch Product Updated");
            }
            catch {

                return BadRequest("Bunch Product Update Failed");
            }
            
            
                   
        }


        [HttpDelete]
        public async Task<ActionResult> DeleteBunchProduct(int id)
        {
            try
            {
                var item = await _repository.GetBunchProductById(id);
                await _repository.DeleteBunchProductAsync(id);
                await _saveContext.SaveChangesAsync();
                return Ok("Bunch Product Deleted");
            }
            catch {

                return BadRequest("Bunch Product Delete Failed");
            }
        }
        

    }
}
