﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.DTOS;
using RetailSuite.Application.DTOS.BrandDTO;
using RetailSuite.Application.Services.BrandServices;
using RetailSuite.Domain.CMS.Models.Brands;
using System.Security.Authentication;
using System.Security.Claims;

namespace RetailSuite.API.Controllers.CMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandsController : ControllerBase
    {
        private readonly IBrandRepository _repository;
        private readonly IMapper _mapper;


        public BrandsController(IBrandRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;

        }
        //[Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadBrandDTO>>> GetAllBrands()
        {

            try
            {

                var brands = await _repository.GetBrandsAsync();
                return Ok(_mapper.Map<IEnumerable<ReadBrandDTO>>(brands));

                //return Ok(brands);
            }
            catch
            {
                return NotFound("Brands Not Found");
            }
        }

        //[Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadBrandByIdDTO>> GetBrandById(int id)
        {
            try
            {
                var brand = await _repository.GetBrandByIdAsync(id);
                return Ok(_mapper.Map<ReadBrandByIdDTO>(brand));
                //return Ok(brand);

            }
            catch
            {
                return NotFound("Brand Not Found");
            }
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<ReadBrandDTO>> CreateBrand(CreateBrandDTO createBrandDTO)
        {
            try
            {
                // get the username

                var username = HttpContext.User.Identity.Name;                       
                var brand = _mapper.Map<Brand>(createBrandDTO);
                var output = await _repository.AddBrand(brand,username);
                return Ok(_mapper.Map<ReadBrandByIdDTO>(output)); ;

            }
            catch
            {
                return BadRequest("Brand Creation Failed");
            }
        }

        // HTTP PUT WIlL BE WRITTEN WITH AUTO MAPPER
        //[Authorize]
        [HttpPut("{id}")]
        
        public async Task<ActionResult> UpdateBrand(int id, UpdateBrandDTO updateBrandDTO)
        {

            try
            {
                var brandModelFromRepo = await _repository.GetBrandByIdAsync(id);
                _mapper.Map(updateBrandDTO, brandModelFromRepo);
                await _repository.SaveChangesAsync();
                return Ok(brandModelFromRepo);
            }
            catch
            {
                return BadRequest("Brand Update Failed");
            }
        }
        //[Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteBrand(int id)
        {
            try
            {
                var item = await _repository.GetBrandByIdAsync(id);
                await _repository.DeleteBrandAsync(id);
                await _repository.SaveChangesAsync();
                return Ok(item);

            }
            catch
            {
                return BadRequest("Brand Delete Failed");

            }
        }
    }
}
