﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CMS.DTOS.ProductShadeDTO;
using RetailSuite.Application.CMS.Services.ProductShadeService;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Products;

namespace RetailSuite.API.Controllers.CMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductShadesController : ControllerBase
    {
        private readonly IProductShadeRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public ProductShadesController(IProductShadeRepository repository,IMapper mapper,ISaveContexts saveContexts)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadProductShadeDTO>>> GetAllProductShadesAsync() 
        {
            try
            {
                var productShades = await _repository.GetProductShadeAsync();
                return Ok(_mapper.Map<IEnumerable<ReadProductShadeDTO>>(productShades));
            }
            catch (Exception ex) 
            {
                return NotFound("Product Shades Not Found");
            }
        
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadProductShadeDTO>> GetProuductShadeByIdAsync(int Id)
        {
            try
            {
                var productShade = await _repository.GetProductShadeByIdAsync(Id);
                return Ok(_mapper.Map<ReadProductShadeDTO>(productShade));
            }
            catch (Exception ex) 
            {
                return NotFound("Product Shade Not Found");
            }
            
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateProductShade(CreateProductShadeDTO createProductShadeDTO) {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var productShade = _mapper.Map<ProductShade>(createProductShadeDTO);
                var pshade = await _repository.AddProductShade(productShade,username);
                await _saveContext.SaveChangesAsync();
                return Ok(_mapper.Map<ReadProductShadeDTO>(pshade));
            }
            catch (Exception ex)
            {
                return BadRequest("Product Shade Creation Failed");
            }
            
        
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteProductShade(int Id) 
        {
            try
            {
                var item = await _repository.GetProductShadeByIdAsync(Id);
                await _repository.DeleteProductShadeAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }
            catch (Exception ex) 
            {
                return BadRequest("Delete Failed");
            }
        }
        
        [HttpPut("{Id}")]
        public async Task<ActionResult> UpdateProductShade(int Id,UpdateProductShadeDTDO updateProductShadeDTDO)
        {
            try
            {
                var oldProductShade = await _repository.GetProductShadeByIdAsync(Id);
                _mapper.Map(updateProductShadeDTDO, oldProductShade);
                await _saveContext.SaveChangesAsync();
                return Ok(oldProductShade);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            
        }

        

    }
}
