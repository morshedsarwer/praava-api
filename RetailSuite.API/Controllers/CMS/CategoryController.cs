﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RetailSuite.Application.CMS.DTOS.CategoryDTO;
using RetailSuite.Application.CMS.POCO.Categories;
using RetailSuite.Application.CMS.Services.CategoryServices;
using RetailSuite.Application.CMS.ViewModels.Categories;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Categories;
using RetailSuite.Infrastructure.Contexts;
using System.IO;
using GrandChildCategory = RetailSuite.Application.CMS.POCO.Categories.GrandChildCategory;

namespace RetailSuite.API.Controllers.CMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        private readonly RetailDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;

        public CategoryController(ICategoryRepository repository, IMapper mapper, ISaveContexts saveContexts, RetailDbContext context,IWebHostEnvironment hostEnvironment)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
            _context = context;
            _hostEnvironment = hostEnvironment;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ParentCategory>>> GetParentCategories()
        {
            try
            {
                var categories = await _repository.GetParentCategoriesAsync();
                return Ok(categories);
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }
        [HttpGet("GrandChildCat/{childId}")]
        public async Task<ActionResult<IEnumerable<GrandChildCategory>>> GetGrandChildCategories(int childId)
        {
            try
            {
                var categories = await _repository.GetGChildAndGGchildsAsync(childId);
                return Ok(categories);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<ReadCategory>> GetCategorybyId(int Id)
        {

            try
            {
                return Ok(await _repository.GetCategoryByIdAsync(Id));
            }
            catch
            {

                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }

        [HttpPost("parent")]
        public async Task<ActionResult<ParentCategoryDTO>> CreateCategory(ParentCategoryDTO createCategoryDTO)
        {

            try
            {
                var result = await _repository.AddParentCategory(_mapper.Map<Category>(createCategoryDTO));
                var catId = _context.Cats.FirstOrDefault(s => s.ParentCat == createCategoryDTO.ParentCat);
                if (catId != null)
                {
                    return Ok(catId.Id);
                }
                return BadRequest("Error Creating Parent Category");
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                "Error creating new parent category record");
            }

        }
        [HttpPut("parent/{Id}")]
        public async Task<ActionResult> UpdateParentCategory(int Id, ParentCategoryDTO updateParentCategoryDTO)
        {
            try
            {
                var Category = await _context.Cats.FirstOrDefaultAsync(s => s.Id == Id);
                Category.ParentCat = updateParentCategoryDTO.ParentCat;
                await _saveContext.SaveChangesAsync();
                return Ok(Category.ParentCat);

            }
            catch
            {
                return BadRequest("Update Failed");
            }
        }
        [HttpPut("child/{Id}")]
        public async Task<ActionResult> UpdateChildCategory(int Id, ParentCategoryDTO updateParentCategoryDTO)
        {
            try
            {
                var Category = await _context.Cats.FirstOrDefaultAsync(s => s.Id == Id);
                Category.ChildCat = updateParentCategoryDTO.ParentCat;
                await _saveContext.SaveChangesAsync();
                return Ok(Category.ChildCat);

            }
            catch
            {
                return BadRequest("Update Failed");
            }
        }


        [HttpPost("child")]
        public async Task<ActionResult<ParentCategory>> CreateChildCategory(ChildCategoryDTO createCategoryDTO)
        {

            try
            {
                return Ok(await _repository.AddChildCategory(_mapper.Map<Category>(createCategoryDTO)));
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                "Error creating new child category record");
            }

        }
        [HttpPost("grandChild")]
        public async Task<ActionResult<ParentCategory>> GrandChildCategory(GChildCategoryDTO createCategoryDTO)
        {

            try
            {
                return Ok(await _repository.AddGChildCategory(_mapper.Map<Category>(createCategoryDTO)));
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                "Error creating new grand child category record");
            }

        }
        [HttpPost("greatGrandChild")]
        public async Task<ActionResult<ParentCategory>> GreatGrandChildCategory(GGChildCategoryDTO createCategoryDTO)
        {

            try
            {
                return Ok(await _repository.AddGGChildCategory(_mapper.Map<Category>(createCategoryDTO)));
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                "Error creating new grand child category record");
            }

        }
        [HttpGet("childcategories")]
        public async Task<ActionResult<ReadCategoryViewModels>> ChildCategories()
        {

            try
            {
                var result = await _repository.GetChildCategories();
                return Ok(result);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                "Error retrieving child category record");
            }

        }
        [HttpGet("grandchildcategories")]
        public async Task<ActionResult<ReadCategoryViewModels>> GrandChildCategories()
        {

            try
            {
                var result = await _repository.GetGrandChildCategories();
                return Ok(result);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                "Error retrieving child category record");
            }

        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteCategory(int Id)
        {
            try
            {
                var item = await _repository.GetCategoryByIdAsync(Id);
                await _repository.DeleteCategoryAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }


        }
        [HttpPost("CategoryImage")]
        public async Task<ActionResult> UploadParentCategoryImage([FromForm] ImageCategoryDTO imageCategoryDTO, [FromForm] int ParentCatId)
        {
            try
            {
                var category = _context.Cats.FirstOrDefault(s => s.Id == ParentCatId);
                if (category != null)
                {
                    category.ImageName = await SaveImage(imageCategoryDTO.ImageFile);
                    category.ImageUrl = Path.Combine("Images", category.ImageName);
                    await _saveContext.SaveChangesAsync();
                    return Ok(category);
                }
                else
                {
                    return BadRequest("No Category found");
                }
            }
            catch
            {
                return BadRequest("Image Upload Failed");
            }
        }
        
        [HttpPut("UpdateCategoryImage")]
        public async Task<ActionResult> EditCategoryImage([FromForm] ImageCategoryDTO imageCategoryDTO,[FromForm]int parentCategoryId)
        {
            try
            {
                // find the category
                var category = _context.Cats.FirstOrDefault(s => s.Id == parentCategoryId);
                if(category != null)
                {
                    // if category exists
                    // find its image path
                    var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images", category.ImageName);
                    // check if its available
                    var isavailable = System.IO.File.Exists(imagePath);
                    if (isavailable)
                    {
                        System.IO.File.Delete(imagePath);

                    }
                    category.ImageName = await SaveImage(imageCategoryDTO.ImageFile);
                    category.ImageUrl = Path.Combine("Images", category.ImageName);
                    await _saveContext.SaveChangesAsync();
                    return Ok(category);
                }
                else
                {
                    return BadRequest("Category Not Found");
                }
            }
            catch
            {
                return BadRequest("ImageUpdateFailed");
            }
        }



        [NonAction]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine("Images", imageName);
            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }
            return imageName;

        }


    }
}
