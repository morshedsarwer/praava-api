﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CMS.DTOS.CampaignDTO;
using RetailSuite.Application.CMS.Services.CampaignServices;
using RetailSuite.Application.CMS.ViewModels.Campaigns;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Campaigns;
using RetailSuite.Infrastructure.Contexts;

namespace RetailSuite.API.Controllers.CMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class CampaignController : ControllerBase
    {
        private readonly ICampaignRepository _repositry;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        private readonly RetailDbContext _context;

        public CampaignController(ICampaignRepository reposiitory, IMapper mapper, ISaveContexts saveContexts, RetailDbContext context)
        {
            _repositry = reposiitory;
            _mapper = mapper;
            _saveContext = saveContexts;
            _context = context;
        }

        [HttpGet("getcampaigns")]
        public async Task<ActionResult> GetAllCampaigns()
        {
            try
            {
                var campaigns = await _repositry.GetAllCampaignAsync();
                return Ok(campaigns);
            }
            catch
            {
                return BadRequest("Campaign Not Found");
            }
        }
        [HttpGet("getcampaignproducts/{campaignCode}")]
        public async Task<ActionResult> GetCampaignsProduct(string campaignCode)
        {
            try
            {
                return Ok(_repositry.GetCampaignProducts(campaignCode));
            }
            catch (Exception)
            {

                return BadRequest("Campaign Product Not Found");
            }
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult> GetCampaignById(int Id)
        {
            try
            {
                var campaign = await _repositry.GetCampaignByIdAsync(Id);
                return Ok(campaign);
            }
            catch
            {
                return BadRequest("Campaign Not Found");
            }
            
        }

        [HttpPost]
        public async Task<ActionResult> CreateCampaign(CampaignViewModel campaignViewModel)
        {
            try
            {
                var campaign = _mapper.Map<Campaign>(campaignViewModel.CreateCampaignDTO);
                var campaignProducts = _mapper.Map<IEnumerable<CampaignProducts>>(campaignViewModel.CampaignProductsDTOs);
                var campaignContextViewModel = new CampaignContextViewModel { Campaign = campaign, CampaignProducts = campaignProducts };
                var result = _repositry.AddCampaign(campaignContextViewModel);
                await _saveContext.SaveChangesAsync();
                return Ok(result);
            }
            catch
            {
                return BadRequest("Campaign Creattion Failed");
            }

        }

        [HttpPut("{Id}")]
        public async Task<ActionResult> UpdateCampaign(int Id, UpdateCampaignDTO updateCampaignDTO)
        {
            try
            {
                var oldCampaign = _context.Campaigns.FirstOrDefault(c => c.Id == Id);
                if(oldCampaign != null)
                {
                    _mapper.Map(updateCampaignDTO, oldCampaign);
                    await _saveContext.SaveChangesAsync();
                    return Ok("updated successfully");
                }
                return NotFound("Campaign not found");
            }
            catch
            {
                return BadRequest("Update Campaign Failed");
            }

        }

        [HttpGet("getCampaignProduct/{Id}")]
        public async Task<ActionResult> GetCampaignProductById(int Id)
        {
            try
            {
                var campaignProduct = await _repositry.GetCampaignProductById(Id);
                return Ok(campaignProduct);
            }
            catch
            {
                return BadRequest("Campaign Product Not Found");
            }

        }

        [HttpDelete("deleteCampaignProduct/{Id}")]
        public async Task<ActionResult> DeleteCamPaignProduct(int Id)
        {
            try
            {
                await _repositry.DeleteCampaignProduct(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }

        }

        [HttpPut("updateCampaignProduct/{Id}")]
        public async Task<ActionResult> UpdateCampaignProduct(int Id, CampaignProductsDTO campaignProductsDTO)
        {
            try
            {
                var oldCampaignProduct = await _repositry.GetCampaignProductById(Id);
                if (oldCampaignProduct != null)
                {
                    _mapper.Map(campaignProductsDTO, oldCampaignProduct);
                    await _saveContext.SaveChangesAsync();
                    return Ok("updated successfully");
                }
                return NotFound("Product Not found");
            }
            catch
            {
                return BadRequest("Campaign Update Failed");
            }
        }

        [HttpPost("AddCampaignProduct")]
        public async Task<ActionResult> AddCampaignProduct(CreateCampaignProductDTO createCampaignProductDTO)
        {
            try
            {
                var campaignProduct = _mapper.Map<CampaignProducts>(createCampaignProductDTO);
                await _repositry.AddCampaignProduct(campaignProduct);
                await _saveContext.SaveChangesAsync();
                return Ok("Campaign Product Added");
            }
            catch
            {
                return BadRequest("Campaign Add Failed");
            }

        }



        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteCampaign(int Id)
        {
            try
            {
                await _repositry.DeleteCampaign(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Campaign Deleted");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
        }
    }
}
