﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Office.Interop.Excel;
using OfficeOpenXml;
using RetailSuite.Application.CMS.DTOS.FreeProductDTO;
using RetailSuite.Application.CMS.DTOS.InitialProductDTO;
using RetailSuite.Application.CMS.DTOS.ProductAttributeDTO;
using RetailSuite.Application.CMS.DTOS.ProductBarcodeDTO;
using RetailSuite.Application.CMS.DTOS.ProductCategoriesDTO;
using RetailSuite.Application.CMS.DTOS.ProductDTO;
using RetailSuite.Application.CMS.DTOS.ProductImageDTO;
using RetailSuite.Application.CMS.DTOS.ProductMetaTagDTO;
using RetailSuite.Application.CMS.DTOS.ProductPriceDTO;
using RetailSuite.Application.CMS.POCO.Product;
using RetailSuite.Application.CMS.Services.ProductServices;
using RetailSuite.Application.CMS.ViewModels.BarCode;
using RetailSuite.Application.CMS.ViewModels.Products;
using RetailSuite.Application.CMS.ViewModels.ProductVM;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.CMS.Models.Products;
using RetailSuite.Infrastructure.Contexts;

namespace RetailSuite.API.Controllers.CMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly RetailDbContext _context;

        public ProductsController(IProductRepository repository, IMapper mapper, ISaveContexts saveContexts, IWebHostEnvironment hostEnvironment, RetailDbContext context)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContexts;
            _hostEnvironment = hostEnvironment;
            _context = context;

        }
        [HttpGet("testcat")]
        public async Task<ActionResult> cats()
        {
            var parents = from p in _context.Cats
                          where !string.IsNullOrWhiteSpace(p.ParentCat)
                          select p;
            return Ok(parents);
        }
        [HttpGet("GetAllProducts")]
        public async Task<ActionResult<IEnumerable<ReadProductDTO>>> GetAllProducts()
        {
            try
            {
                var products = await _repository.GetAllProducts();
                return Ok(_mapper.Map<IEnumerable<ReadProductDTO>>(products));
            }
            catch
            {
                return NotFound("Products Not Found");
            }
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<CategoriesVM>> GetProductById(int Id)
        {
            try
            {
                var product = await _repository.ProductCategories(Id);
                if (product != null)
                {
                    return Ok(product);
                }
                return NotFound("Product Not Found");
            }
            catch (Exception e)
            {
                return NotFound("Product Not Found");
            }

        }
        [HttpPost("productfilters")]
        public async Task<ActionResult<ProductFiltersData>> ProductFilter(ProdcutFilterParameter prodcutFilter)
        {
            try
            {
                var result = _repository.GetProductsFilterData(prodcutFilter);
                return Ok(result);
            }
            catch (Exception e)
            {
                return NotFound("Failed");
            }

        }
        [HttpGet("productfilters")]
        public async Task<ActionResult<ProductFiltersData>> ProductFilterInitial()
        {
            try
            {
                var id = 0;
                var cats = from c in _context.ProductCategories orderby c.ParentCatId descending select c;
                var catsList = new List<ProductCategory>();
                foreach (var c in cats)
                {
                    if (c.ParentCatId == id)
                    {
                        continue;
                    }
                    id = c.ParentCatId;
                    catsList.Add(new ProductCategory
                    {
                        Id = c.ParentCatId
                    });
                }
                var filter = new ProdcutFilterParameter()
                {
                    From = 0,
                    isPublish = 3,
                    ProductCatsId = catsList,
                    To = 0
                };
                var result = _repository.GetProductsFilterData(filter);
                return Ok(result);
            }
            catch (Exception e)
            {
                return NotFound();
            }

        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateProduct(ProductViewModelDTO createProductDTO)
        {
            try
            {
                var product = _mapper.Map<Product>(createProductDTO.ProductDTO);
                var isskualready = _context.Products.FirstOrDefault(s => s.productsku == product.productsku);
                if (isskualready != null)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "SKU already Exists");
                }
                else
                {
                    //var image = _mapper.Map<IEnumerable<ProductImage>>(createProductDTO.ImageDTO);
                    //var freeProduct = _mapper.Map<IEnumerable<FreeProduct>>(createProductDTO.FreeProducts);
                    var productCat = _mapper.Map<IEnumerable<ProductCategories>>(createProductDTO.ProdCategories);
                    var tags = _mapper.Map<IEnumerable<ProductMetaTag>>(createProductDTO.ProdMetaTags);
                    var attributes = _mapper.Map<IEnumerable<ProductAttribute>>(createProductDTO.Attributes);
                    var products = new ProductContextViewModel
                    {
                        //ProductImages = image,
                        Product = product,
                        //FreeProducts = freeProduct,
                        ProductCategories = productCat,
                        ProductMetaTags = tags,
                        Attributes = attributes
                    };
                    var result = await _repository.AddProduct(products);
                    if (result != "failed")
                        return Ok(result);
                    return StatusCode(StatusCodes.Status500InternalServerError,
                   "Error creating new product record");
                }



            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError,
                "Error creating new product record");
            }

        }
        [HttpPost("setproductprice")]
        public async Task<ActionResult> SetProductPrice(ProductPriceDTO productPriceDTO)
        {
            try
            {
                var result = await _repository.AddProducPricetAsync(productPriceDTO);
                return Ok(result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                "Error creating new product record");
            }
        }
        [HttpPost("addmetatag/{ProductCode}")]
        public async Task<ActionResult> AddMetaTag(CreateProdMetaTag createProdMetaTag, string ProductCode)
        {
            try
            {
                _repository.AddMetaTag(ProductCode, createProdMetaTag);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                "Error creating new product record");
            }
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult> ActionResult(int Id)
        {
            try
            {
                var item = await _repository.GetProductByIdAsync(Id);
                await _repository.DeleteProductAsync(Id);
                await _saveContext.SaveChangesAsync();
                return Ok(item);
            }
            catch (Exception e)
            {
                return BadRequest("Delete Failed");
            }

        }
        [HttpGet("get-products")]
        public async Task<ActionResult> Products()
        {
            try
            {
                return Ok(await _repository.GetProducts());
            }
            catch (Exception)
            {
                return BadRequest("Products Not Found");
            }
        }
        [HttpGet("product-images/{productCode}")]
        public async Task<ActionResult> ProductImages(string productCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(productCode))
                {
                    return Ok(await _repository.GetProductImages(productCode));
                }
                return BadRequest("null product code");

            }
            catch (Exception)
            {
                return BadRequest("Product Image Not Found");
            }
        }
        [HttpPost("barcode")]
        public async Task<ActionResult> AddProductbarCode(BarCodeViewModels barCode)
        {
            try
            {
                var result = await _repository.AddBarCode(barCode);
                if (result)
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError,
                "Error creating new product barcode record");
            }
        }

        [HttpPut("{Id}")]
        public async Task<ActionResult> UpdateProduct(int Id, UpdateProductDTO updateProductDTO)
        {
            try
            {
                var isskualready = _context.Products.FirstOrDefault(s => s.productsku == updateProductDTO.productsku);

                if (isskualready != null)
                {
                    var oldProduct = await _repository.GetProductByIdAsync(Id);
                    _mapper.Map(updateProductDTO, oldProduct);
                    await _saveContext.SaveChangesAsync();
                    return Ok("Product updated successfully");
                }
                return NotFound("Product not found");
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            

        }

        [HttpGet("ImagesByProductCode/{ProductCode}")]
        public async Task<ActionResult> GetProductImagesByProductCode(string ProductCode)
        {
            try
            {
                var productImages = await _repository.GetProductImagesByProductCode(ProductCode);
                return Ok(productImages);
            }
            catch
            {
                return BadRequest("Product Image Not Found");
            }
            
        }

        [HttpPut("editProductImages/{Id}")]
        public async Task<ActionResult> UpdateProductImages(int Id, UpdateProductImageDTO updateProductImageDTO)
        {
            try
            {
                var oldProductImage = await _repository.GetProductImageByIdAsync(Id);
                _mapper.Map(updateProductImageDTO, oldProductImage);
                await _saveContext.SaveChangesAsync();
                return Ok(oldProductImage);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            
        }

        [HttpPost("postProductImage")]
        public async Task<ActionResult> PostProductImages([FromForm] CreateProductImageDTO createProductImageDTO)
        {
            var productImage = _mapper.Map<ProductImage>(createProductImageDTO);
            productImage.ImageName = await SaveImage(productImage.ImageFile);
            productImage.Url = Path.Combine(_hostEnvironment.ContentRootPath, "Images", productImage.ImageName);
            _context.ProductImages.Add(productImage);
            await _context.SaveChangesAsync();
            return StatusCode(201);
        }


        [HttpPost("freeProductimageupload")]
        public async Task<ActionResult> PostFreeProduct([FromForm] CreateFreeProductDTO createFreeProduct)
        {
            var freeproduct = _mapper.Map<FreeProduct>(createFreeProduct);
            freeproduct.ImageName = await SaveImage(freeproduct.ImageFile);
            freeproduct.ImgUrl = Path.Combine(_hostEnvironment.ContentRootPath, "Images", freeproduct.ImageName);
            _context.FreeProducts.Add(freeproduct);
            await _context.SaveChangesAsync();
            return StatusCode(201);

        }

        [HttpPut("updateProductImage")]
        public async Task<ActionResult> UpdateProductImage([FromForm] List<IFormFile> files, [FromForm] string ProductCode)
        {
            // get all the image first
            var images = _context.ProductImages.Where(s => s.ProductCode == ProductCode).ToList();
            if (images != null)
            {
                _context.ProductImages.RemoveRange(images);
                _context.SaveChanges();
                //await _saveContext.SaveChangesAsync();
                var result = new List<ProductImage>();
                foreach (var item in files)
                {
                    var tmp = new ProductImage();
                    tmp.ImageName = await SaveImage(item);
                    tmp.Url = Path.Combine("Images", tmp.ImageName);
                    tmp.ProductCode = ProductCode;
                    result.Add(tmp);
                }
                _context.ProductImages.AddRange(result);
                _context.SaveChanges();
                return Ok(result);
            }
            else
            {
                return BadRequest();
            }


            return Ok(images);
        }



        [HttpPost("ProductImageUpload")]
        public async Task<ActionResult> uploadbulk([FromForm] List<IFormFile> files, [FromForm] string ProductCode)
        {
            try
            {
                var product = _context.Products.FirstOrDefault(s => s.ProductCode == ProductCode);
                if (product != null)
                {
                    var result = new List<ProductImage>();
                    foreach (var item in files)
                    {
                        var tmp = new ProductImage();
                        tmp.ImageName = await SaveImage(item);
                        tmp.Url = Path.Combine("Images", tmp.ImageName);
                        tmp.ProductCode = ProductCode;
                        result.Add(tmp);
                    }
                    _context.ProductImages.AddRange(result);
                    if (await _saveContext.SaveChangesAsync())
                    {
                        return Ok();

                    }
                    else
                    {
                        await _repository.DeleteAddedProducts(ProductCode);
                        return BadRequest();
                    }
                }
                else
                {
                    return BadRequest();
                }

            }
            catch
            {

                return BadRequest();
            }

        }

        [HttpDelete("DeleteProductImage/{ProductImageId}")]
        public async Task<ActionResult> DeleteProductImage(int ProductImageId)
        {
            // get the image object
            var image = _context.ProductImages.FirstOrDefault(s => s.ProductImageId == ProductImageId);
            var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images", image.ImageName);
            var isavailable = System.IO.File.Exists(imagePath);
            if (isavailable)
            {
                try
                {
                    System.IO.File.Delete(imagePath);
                    _context.ProductImages.Remove(image);
                    _context.SaveChanges();
                    return Ok("Image Removed");
                }
                catch
                {
                    return BadRequest("Problem Removing Image");
                }

            }
            else
            {
                return NotFound("Image Dont exists");
            }

        }


        [NonAction]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            try
            {
                string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
                imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
                var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images", imageName);
                using (var fileStream = new FileStream(imagePath, FileMode.Create))
                {
                    await imageFile.CopyToAsync(fileStream);
                }
                return imageName;
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet("FreeProductByProductCode/{ProductCode}")]
        public async Task<ActionResult> GetFreeProductsByProductCode(string ProductCode)
        {
            try
            {
                var freeProducts = await _repository.GetFreeProductsByProductCode(ProductCode);
                return Ok(freeProducts);
            }
            catch (Exception)
            {

                return BadRequest("Not Found");
            }
        }


        [HttpPut("editFreeProduct/{FreeProductId}")]
        public async Task<ActionResult> UpdateFreeProduct(int FreeProductId, UpdateFreeProductDTO updateFreeProductDTO)
        {
            try
            {
                var oldProduct = await _repository.GetFreeProductByIdAsync(FreeProductId);
                _mapper.Map(updateFreeProductDTO, oldProduct);
                await _saveContext.SaveChangesAsync();
                return Ok("Edit Successful");
            }
            catch
            {
                return BadRequest("Edit Failed");
            }
            
        }


        [HttpDelete("deleteFreeProduct/{FreeProductId}")]
        public async Task<ActionResult> DeleteFreeProduct(int FreeProductId)
        {
            try
            {
                await _repository.DeleteFreeProduct(FreeProductId);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
            
        }


        [HttpGet("GetProductAttributeByProductCode/{ProductCode}")]
        public async Task<ActionResult> GetProductAttributeByProductCode(string ProductCode)
        {
            try
            {
                var attributes = await _repository.GetProductAttributeByProductCode(ProductCode);
                return Ok(attributes);
            }
            catch
            {
                return BadRequest("Not Found");
            }
            
        }

        [HttpPut("editProductAttribute/{Id}")]
        public async Task<ActionResult> UpdateProductAttribute(int Id, UpdateProductAttributeDTO updateProductAttributeDTO)
        {
            try
            {
                var oldAttribute = await _repository.GetProductAttributeById(Id);
                _mapper.Map(updateProductAttributeDTO, oldAttribute);
                await _saveContext.SaveChangesAsync();
                return Ok(oldAttribute);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            
        }

        [HttpDelete("DeleteProductAttribute/{Id}")]
        public async Task<ActionResult> DeleteProductAttribute(int Id)
        {
            try
            {
                await _repository.DeleteProductAttribute(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
            
        }

        [HttpGet("GetProductCategoriesByProductCode/{ProductCode}")]
        public async Task<ActionResult> GetProductCategoriesByProductCode(string ProductCode)
        {
            try
            {
                var categories = await _repository.GetProductCategoriesByProductCode(ProductCode);
                return Ok(categories);
            }
            catch
            {
                return BadRequest("Not Found");
            }
            
        }

        [HttpPut("editProductCategories/{Id}")]
        public async Task<ActionResult> EditProductCategories(int Id, UpdateProductCategoriesDTO updateProductCategoriesDTO)
        {
            try
            {
                var oldcategories = await _repository.GetProductCategoriesById(Id);
                _mapper.Map(updateProductCategoriesDTO, oldcategories);
                await _saveContext.SaveChangesAsync();
                return Ok(oldcategories);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            
        }

        [HttpDelete("DeleteProductCategories/{Id}")]
        public async Task<ActionResult> DeleteProductCategories(int Id)
        {
            try
            {
                await _repository.DeleteProductCategories(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
            
        }

        [HttpGet("GetProductMetaTagsByProductCode/{ProductCode}")]
        public async Task<ActionResult> GetProductMetaTagsByProductCode(string ProductCode)
        {
            try
            {
                var tags = await _repository.GetProductMetatagByProductCode(ProductCode);
                return Ok(tags);
            }
            catch
            {
                return BadRequest("Not Found");
            }
            
        }

        [HttpPut("editProductMetaTags/{Id}")]
        public async Task<ActionResult> EditProductMetatag(int Id, UpdateProductMetaTagDTO updateProductMetaTagDTO)
        {
            try
            {
                var oldTag = await _repository.GetProductMetaTagById(Id);
                _mapper.Map(updateProductMetaTagDTO, oldTag);
                await _saveContext.SaveChangesAsync();
                return Ok(oldTag);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            
        }
        [HttpDelete("DeleteProductmetaTag/{Id}")]
        public async Task<ActionResult> DeleteProductMetatag(int Id)
        {
            await _repository.DeleteProductMetaTag(Id);
            await _saveContext.SaveChangesAsync();
            return Ok();
        }

        // productprice
        [HttpPut("editproductPrice/{Id}")]
        public async Task<ActionResult> UpdateProductPrice(int Id, UpdateProductPriceDTO updateProductPriceDTO)
        {
            try
            {
                var oldprice = await _repository.GetProductPriceById(Id);
                updateProductPriceDTO.IsApproved = false;
                _mapper.Map(updateProductPriceDTO, oldprice);
                await _saveContext.SaveChangesAsync();
                return Ok(oldprice);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            
        }

        [HttpDelete("deleteProductPrice/{Id}")]
        public async Task<ActionResult> DeleteProductPrice(int Id)
        {
            try
            {
                await _repository.DeleteProductPrice(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
            
        }

        [HttpPut("editproductbarcode/{Id}")]
        public async Task<ActionResult> UpdateProductBarcode(int Id, UpdateProductBarcodeDTO updateProductBarcodeDTO)
        {
            try
            {
                var oldcode = await _repository.GetProductBarcodeById(Id);
                _mapper.Map(updateProductBarcodeDTO, oldcode);
                await _saveContext.SaveChangesAsync();
                return Ok(oldcode);
            }
            catch
            {
                return BadRequest("Update Failed");
            }
            
        }
        [HttpDelete("deleteProductBarcode/{Id}")]
        public async Task<ActionResult> DeleteProductBarcode(int Id)
        {
            try
            {
                await _repository.DeleteProductBarcode(Id);
                await _saveContext.SaveChangesAsync();
                return Ok("Delete Successful");
            }
            catch
            {
                return BadRequest("Delete Failed");
            }
            
        }

        [HttpPost("InitialProduct")]
        public async Task<ActionResult> InitialProduct(CreateInitialProductDTO createInitialProductDTO)
        {
            var product = await _context.Products.FirstOrDefaultAsync(s => s.ProductCode == createInitialProductDTO.ProductCode);
            if (product != null)
            {

                using (var transaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        product.IsPublish = createInitialProductDTO.IsPublish;
                        _context.SaveChanges();
                        var productprice = new ProductPrice
                        {
                            Price = createInitialProductDTO.ProductPrice,
                            IsApproved = true,
                            ProductId = product.ProductId,

                        };
                        _context.ProductPrices.Add(productprice);

                        //var productprice = await _context.ProductPrices.FirstOrDefaultAsync(s => s.ProductId == product.ProductId);
                        //productprice.IsApproved = false;
                        _context.SaveChanges();
                        transaction.Commit();
                        return Ok(productprice);
                    }
                    catch
                    {
                        transaction.Rollback();
                        return BadRequest("Initial Product Post failed");
                    }
                }
            }
            else
            {
                return BadRequest("Product is Null");
            }

        }
        [HttpGet("getproductsbycategories")]
        public async Task<ActionResult> GetProductsByCategories()
        {
            try
            {
                var results = await _repository.GetProductsBYCategories();
                return Ok(results);
            }
            catch (Exception)
            {
                return BadRequest("Not Found");
            }
        }
        [HttpGet("getproductsbysearch/{search}")]
        public async Task<ActionResult> GetProductsByCategories(string search)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(search))
                {
                    var parameter = search.ToLower();
                    var results = await _repository.GetProductsBYCategories(parameter);
                    return Ok(results);
                }
                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest("Not Found");
            }
        }
        [HttpGet("getproductsbycatid/{catId}")]
        public async Task<ActionResult> GetProductsByCatId(int catId)
        {
            try
            {
                var cat = _context.Cats.FirstOrDefault(c=>c.Id==catId);
                if(cat != null)
                {
                    if (!string.IsNullOrEmpty(cat.ParentCat))
                    {
                        var results = await _repository.GetProductsByParentCategories(catId);
                        return Ok(results);
                    }
                    else if(!string.IsNullOrEmpty(cat.ChildCat))
                    {
                        var results = await _repository.GetProductsByChildCategories(catId);
                        return Ok(results);
                    }
                    return NotFound();
                }
                
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest("Not Found");
            }
        }


    }
}
