﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RetailSuite.Application.CMS.DTOS.CustomersDTO.WishListDTO;
using RetailSuite.Application.CMS.POCO.Customer;
using RetailSuite.Application.CMS.Services.Customer;
using RetailSuite.Application.CommonServices;
using RetailSuite.Domain.Account.Models;
using RetailSuite.Domain.CMS.Models.Customer;

namespace RetailSuite.API.Controllers.CMS
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerRepository _repository;
        private readonly IMapper _mapper;
        private readonly ISaveContexts _saveContext;

        public CustomerController(ICustomerRepository repository,IMapper mapper, ISaveContexts saveContext)
        {
            _repository = repository;
            _mapper = mapper;
            _saveContext = saveContext;
        }
        [Authorize]
        [HttpPost("wishlist")]
        public async Task<ActionResult> WishList(WishListCartProductsDTO productsDTO)
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var wish = new WishListProducts
                {
                    ProductId = productsDTO.ProductId,
                    CustomerId = username,
                    CreatedAt=DateTime.Now,
                    CreatedBy=username
                };
                var result = await _repository.AddWishList(wish);
                if (result)
                    return Ok("success");
                return BadRequest("failed");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,"error inserting data");
            }
        }
        [Authorize]
        [HttpDelete("deletewishlist/{wishid}")]
        public async Task<ActionResult> DeleteWishList(int wishid)
        {
            try
            {
                var result = await _repository.DeleteishList(wishid);
                if (result == true)
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "error inserting data");
            }
        }
        [Authorize]
        [HttpPost("cart")]
        public async Task<IActionResult> Cart(WishListCartProductsDTO cart)
        {
            try
            {
                 var userName = HttpContext.User.Identity.Name;
                    var newCart = new Cart()
                    {
                        ProductId=cart.ProductId,
                        CustomerId= userName,
                        CreatedAt=DateTime.Now,
                        CreatedBy=userName,
                        Quantity=cart.Quantity,
                        Price=cart.Price,
                    };
                var result = await _repository.AddCart(newCart);
                if(result)
                {
                    return Ok("success");
                }
                return BadRequest("failed");
            }
            catch (System.Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,"error inserting data");
            }
        }
        [Authorize]
        [HttpPut("cart/{Id}")]
        public async Task<IActionResult> CartUpdate(int Id,WishListCartProductsDTO cart)
        {
            try
            {   
                var result = await _repository.UpdateCart(cart,Id);
                if (result)
                {
                    return Ok("success");
                }
                return BadRequest("failed");
            }
            catch (System.Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "error inserting data");
            }
        }
        [Authorize]
        [HttpDelete("deletecart/{id}")]
        public async Task<IActionResult> DeleteCart(int id){
            try
            {
                var result = await _repository.DeleteCart(id);
                if(result){
                    return Ok("deleted");
                }
                return BadRequest("failed");
            }
            catch (System.Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "error inserting data");
            }
        }
        [Authorize(Roles =UserRoles.Customer)]
        [HttpGet("getwishlist")]
        public async Task<ActionResult<IEnumerable<WishListPoco>>> GetWishLists()
        {
            try
            {
                var userName = HttpContext.User.Identity.Name;
                //var userName = "FR20220220145601";                
                if (!string.IsNullOrEmpty(userName))
                {
                    var user = _saveContext.GetUserFromUserName(userName);
                    return Ok( await _repository.GetWishList(user.UserName));
                }
                return BadRequest("data not found");
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "error");
            }
        }
        [Authorize(Roles= UserRoles.Customer)]
        [HttpGet("getcarts")]
        public async Task<ActionResult<IEnumerable<CartPoco>>> GetCarts()
        {
            try
            {
                var userName = HttpContext.User.Identity.Name;
                //var userName = "amitum78";
                if (!string.IsNullOrEmpty(userName))
                {
                    return Ok(await _repository.GetCarts(userName));
                }
                return BadRequest("data not found");
            }
            catch (Exception)
            {
                //throw;
                return StatusCode(StatusCodes.Status500InternalServerError, "error");
            }
        }
    }
}
